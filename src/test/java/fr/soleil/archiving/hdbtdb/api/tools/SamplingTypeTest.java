package fr.soleil.archiving.hdbtdb.api.tools;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SamplingTypeTest {

	@Test
	public void getSamplingTypeTest() {
		SamplingType[] st = SamplingType.getChoices();
		SamplingType all = st[0];
		assertEquals("YYYY-MM-DD HH24:MI:SS.FF", all.getOracleFormat());
		assertEquals("%Y-%m-%d %H:%i:%s.%f", all.getMySqlFormat());
		assertEquals("None", all.getLabel());
		assertEquals(SamplingType.ALL, all.getType());
		SamplingType second = st[1];
		assertEquals("YYYY-MM-DD HH24:MI:SS", second.getOracleFormat());
		assertEquals("%Y-%m-%d %H:%i:%s", second.getMySqlFormat());
		assertEquals("Seconds", second.getLabel());
		assertEquals(SamplingType.SECOND, second.getType());
		SamplingType minute = st[2];
		assertEquals("YYYY-MM-DD HH24:MI", minute.getOracleFormat());
		assertEquals("%Y-%m-%d %H:%i", minute.getMySqlFormat());
		assertEquals("Minutes", minute.getLabel());
		assertEquals(SamplingType.MINUTE, minute.getType());
		SamplingType hour = st[3];
		assertEquals("YYYY-MM-DD HH24", hour.getOracleFormat());
		assertEquals("%Y-%m-%d %H", hour.getMySqlFormat());
		assertEquals("Hours", hour.getLabel());
		assertEquals(SamplingType.HOUR, hour.getType());
		SamplingType day = st[4];
		assertEquals("YYYY-MM-DD", day.getOracleFormat());
		assertEquals("%Y-%m-%d", day.getMySqlFormat());
		assertEquals("Days", day.getLabel());
		assertEquals(SamplingType.DAY, day.getType());
		SamplingType Month = SamplingType.getSamplingType(SamplingType.MONTH);
		assertEquals("YYYY-MM", Month.getOracleFormat());
		assertEquals("%Y-%m", Month.getMySqlFormat());
		assertEquals("Months", Month.getLabel());
		assertEquals(SamplingType.MONTH, Month.getType());
	}

}
