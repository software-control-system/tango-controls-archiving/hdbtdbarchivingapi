package fr.soleil.archiving.hdbtdb.api.utils.database;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;

import org.junit.Test;

import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;
import fr.soleil.lib.project.math.MathConst;

public class DbUtilsTest extends OracleConnectorTest {

    @Test
    public void getScalarValueTest() {
        DbUtils dbUtils = new OracleDbUtils(hdbConnector);
        NullableData<boolean[]>[] result = dbUtils.getScalarValue("3", "5", TangoConst.Tango_DEV_DOUBLE,
                AttrWriteType._READ_WRITE);
        assertTrue(Arrays.equals(new double[] { 3 }, (double[]) result[READ_INDEX].getValue()));
        assertTrue(Arrays.equals(new double[] { 5 }, (double[]) result[WRITE_INDEX].getValue()));
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getScalarValue("3", "NaN", TangoConst.Tango_DEV_DOUBLE, AttrWriteType._READ_WRITE);
        assertArrayEquals(new double[] { 3 }, (double[]) result[READ_INDEX].getValue(), 0);
        assertArrayEquals(new double[] { MathConst.NAN_FOR_NULL }, (double[]) result[WRITE_INDEX].getValue(), 0);
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getScalarValue("3", "true", TangoConst.Tango_DEV_BOOLEAN, AttrWriteType._READ_WRITE);
        assertArrayEquals(new boolean[] { true }, (boolean[]) result[READ_INDEX].getValue());
        assertArrayEquals(new boolean[] { true }, (boolean[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getScalarValue("0", null, TangoConst.Tango_DEV_BOOLEAN, AttrWriteType._READ_WRITE);
        assertArrayEquals(new boolean[] { false }, (boolean[]) result[READ_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, (boolean[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getScalarValue("3", "5", TangoConst.Tango_DEV_LONG, AttrWriteType._READ_WRITE);
        assertArrayEquals(new int[] { 3 }, (int[]) result[READ_INDEX].getValue());
        assertArrayEquals(new int[] { 5 }, (int[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getScalarValue("0.1", "NaN", TangoConst.Tango_DEV_LONG, AttrWriteType._READ_WRITE);
        assertArrayEquals(new int[] { 0 }, (int[]) result[READ_INDEX].getValue());
        assertArrayEquals(new int[] { 0 }, (int[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getScalarValue("3", "5", TangoConst.Tango_DEV_LONG64, AttrWriteType._READ_WRITE);
        assertArrayEquals(new long[] { 3 }, (long[]) result[READ_INDEX].getValue());
        assertArrayEquals(new long[] { 5 }, (long[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getScalarValue("0.1", "NaN", TangoConst.Tango_DEV_LONG64, AttrWriteType._READ_WRITE);
        assertArrayEquals(new long[] { 0 }, (long[]) result[READ_INDEX].getValue());
        assertArrayEquals(new long[] { 0, }, (long[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getScalarValue("3", "5", TangoConst.Tango_DEV_SHORT, AttrWriteType._READ_WRITE);
        assertArrayEquals(new short[] { 3 }, (short[]) result[READ_INDEX].getValue());
        assertArrayEquals(new short[] { 5 }, (short[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getScalarValue("0.1", "NaN", TangoConst.Tango_DEV_SHORT, AttrWriteType._READ_WRITE);
        assertArrayEquals(new short[] { 0 }, (short[]) result[READ_INDEX].getValue());
        assertArrayEquals(new short[] { 0 }, (short[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getScalarValue("3", "5", TangoConst.Tango_DEV_FLOAT, AttrWriteType._READ_WRITE);
        assertTrue(Arrays.equals(new float[] { 3 }, (float[]) result[READ_INDEX].getValue()));
        assertTrue(Arrays.equals(new float[] { 5 }, (float[]) result[WRITE_INDEX].getValue()));
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getScalarValue("0.1", "NaN", TangoConst.Tango_DEV_FLOAT, AttrWriteType._READ_WRITE);
        assertTrue(Arrays.equals(new float[] { 0.1f }, (float[]) result[READ_INDEX].getValue()));
        assertTrue(Arrays.equals(new float[] { Float.NaN }, (float[]) result[WRITE_INDEX].getValue()));
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getScalarValue("SomeString", null, TangoConst.Tango_DEV_STRING, AttrWriteType._READ_WRITE);
        assertArrayEquals(new String[] { "SomeString" }, (String[]) result[READ_INDEX].getValue());
        assertArrayEquals(new String[] { null }, (String[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getScalarValue("#91#", "null", TangoConst.Tango_DEV_STRING, AttrWriteType._READ_WRITE);
        assertArrayEquals(new String[] { "[" }, (String[]) result[READ_INDEX].getValue());
        assertArrayEquals(new String[] { null }, (String[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true }, result[WRITE_INDEX].getNullElements());
    }

    @Test
    public void getSpectrumValueTest() {
        DbUtils dbUtils = new OracleDbUtils(null);
        NullableData<boolean[]>[] result = dbUtils.getSpectrumValue("3,4", "5,6", TangoConst.Tango_DEV_DOUBLE);
        assertArrayEquals(new double[] { 3, 4 }, (double[]) result[READ_INDEX].getValue(), 0);
        assertArrayEquals(new double[] { 5, 6 }, (double[]) result[WRITE_INDEX].getValue(), 0);
        assertArrayEquals(new boolean[] { false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false, false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getSpectrumValue("3,4", "NaN,6", TangoConst.Tango_DEV_DOUBLE);
        assertArrayEquals(new double[] { 3, 4 }, (double[]) result[READ_INDEX].getValue(), 0);
        assertArrayEquals(new double[] { MathConst.NAN_FOR_NULL, 6 }, (double[]) result[WRITE_INDEX].getValue(), 0);
        assertArrayEquals(new boolean[] { false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true, false }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getSpectrumValue("3,0,true", "true,null", TangoConst.Tango_DEV_BOOLEAN);
        assertArrayEquals(new boolean[] { true, false, true }, (boolean[]) result[READ_INDEX].getValue());
        assertArrayEquals(new boolean[] { true, false }, (boolean[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false, false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false, true }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getSpectrumValue("0,", null, TangoConst.Tango_DEV_BOOLEAN);
        assertArrayEquals(new boolean[] { false }, (boolean[]) result[READ_INDEX].getValue());
        assertNull(result[WRITE_INDEX]);
        assertArrayEquals(new boolean[] { false }, result[READ_INDEX].getNullElements());

        result = dbUtils.getSpectrumValue("3,4", "5,6", TangoConst.Tango_DEV_LONG);
        assertArrayEquals(new int[] { 3, 4 }, (int[]) result[READ_INDEX].getValue());
        assertArrayEquals(new int[] { 5, 6 }, (int[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false, false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getSpectrumValue("3,4.1,NaN", "NaN,6", TangoConst.Tango_DEV_LONG);
        assertArrayEquals(new int[] { 3, 4, 0 }, (int[]) result[READ_INDEX].getValue());
        assertArrayEquals(new int[] { 0, 6 }, (int[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false, false, true }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true, false }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getSpectrumValue("3,4", "5,6", TangoConst.Tango_DEV_LONG64);
        assertArrayEquals(new long[] { 3, 4 }, (long[]) result[READ_INDEX].getValue());
        assertArrayEquals(new long[] { 5, 6 }, (long[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false, false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getSpectrumValue("3,4.1,NaN", "NaN,6", TangoConst.Tango_DEV_LONG64);
        assertArrayEquals(new long[] { 3, 4, 0 }, (long[]) result[READ_INDEX].getValue());
        assertArrayEquals(new long[] { 0, 6 }, (long[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false, false, true }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true, false }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getSpectrumValue("3,4", "5,6", TangoConst.Tango_DEV_SHORT);
        assertArrayEquals(new short[] { 3, 4 }, (short[]) result[READ_INDEX].getValue());
        assertArrayEquals(new short[] { 5, 6 }, (short[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false, false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getSpectrumValue("3,4.1,NaN", "NaN,6", TangoConst.Tango_DEV_SHORT);
        assertArrayEquals(new short[] { 3, 4, 0, }, (short[]) result[READ_INDEX].getValue());
        assertArrayEquals(new short[] { 0, 6 }, (short[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false, false, true }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true, false }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getSpectrumValue("3,4", "5,6", TangoConst.Tango_DEV_FLOAT);
        assertArrayEquals(new float[] { 3, 4 }, (float[]) result[READ_INDEX].getValue(), 0);
        assertArrayEquals(new float[] { 5, 6 }, (float[]) result[WRITE_INDEX].getValue(), 0);
        assertArrayEquals(new boolean[] { false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false, false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getSpectrumValue("3,4.1", "NaN,6", TangoConst.Tango_DEV_FLOAT);
        assertArrayEquals(new float[] { 3, 4.1f }, (float[]) result[READ_INDEX].getValue(), 0);
        assertArrayEquals(new float[] { Float.NaN, 6 }, (float[]) result[WRITE_INDEX].getValue(), 0);
        assertArrayEquals(new boolean[] { false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true, false }, result[WRITE_INDEX].getNullElements());

        result = dbUtils.getSpectrumValue("SomeString,SomeString", "SomeString,", TangoConst.Tango_DEV_STRING);
        assertArrayEquals(new String[] { "SomeString", "SomeString" }, (String[]) result[READ_INDEX].getValue());
        assertArrayEquals(new String[] { "SomeString" }, (String[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { false }, result[WRITE_INDEX].getNullElements());
        result = dbUtils.getSpectrumValue("#91#,SomeString", ",SomeString,null", TangoConst.Tango_DEV_STRING);
        assertArrayEquals(new String[] { "[", "SomeString" }, (String[]) result[READ_INDEX].getValue());
        assertArrayEquals(new String[] { null, "SomeString", null }, (String[]) result[WRITE_INDEX].getValue());
        assertArrayEquals(new boolean[] { false, false }, result[READ_INDEX].getNullElements());
        assertArrayEquals(new boolean[] { true, false, true }, result[WRITE_INDEX].getNullElements());
    }

    @Test
    public void getTimeOfLastInsertTest() throws ArchivingException {
        DbUtils dbUtils = new OracleDbUtils(hdbConnector);
        Timestamp time = dbUtils.getTimeOfLastInsert(DEFAULT + "double_scalar_rw", false);
        assertEquals(1355236686000l, time.getTime());
        time = dbUtils.getTimeOfLastInsert(DEFAULT + "double_scalar_rw", true);
        assertEquals(1355236687000l, time.getTime());
        try {
            time = dbUtils.getTimeOfLastInsert(DEFAULT + "double_scalar_4", true);
            assertFalse("Should not reach", true);
        } catch (ArchivingException e) {
            assertEquals("Id not found", e.getMessage());
        }

    }

    @Test
    public void deleteOldRecordsTest() throws ArchivingException, SQLException {
        DbUtils dbUtils = new OracleDbUtils(hdbConnector);
        Timestamp time = dbUtils.getTimeOfLastInsert(DEFAULT + "double_scalar_rw", false);
        assertEquals(1355236686000l, time.getTime());
        dbUtils.deleteOldRecords(0, new String[] { DEFAULT + "double_scalar_rw" });
        time = dbUtils.getTimeOfLastInsert(DEFAULT + "double_scalar_rw", false);
        assertNull(time);
    }

    @Test
    public void nowTest() throws ArchivingException, SQLException {
        DbUtils dbUtils = new OracleDbUtils(hdbConnector);
        Timestamp time = dbUtils.now();
        assertNotNull(time);
    }
}
