package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.esrf.TangoDs.TimedAttrData;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;

public class InfSupGettersBetweenDatesTest extends OracleConnectorTest {

    @Test
    public void getAttDataInfThanBetweenDatesTest() throws ArchivingException {
        InfSupGettersBetweenDates datagetter = new InfSupGettersBetweenDates(hdbConnector);
        DbData[] result = datagetter.getAttDataInfThanBetweenDates(DEFAULT + "double_scalar_rw", "0", "11-12-2012",
                "12-12-2012");
        TimedAttrData[] timedData = result[0].getDataAsTimedAttrData();
        assertEquals(1, timedData.length);
        assertEquals(-255.84408754008876, timedData[0].db_ptr[0], 0);
    }

    @Test
    public void getAttDataInfThanBetweenDatesCountTest() throws ArchivingException {
        InfSupGettersBetweenDates datagetter = new InfSupGettersBetweenDates(hdbConnector);
        int result = datagetter.getAttDataInfThanBetweenDatesCount(DEFAULT + "double_scalar_rw", "0", "11-12-2012",
                "12-12-2012");

        assertEquals(1, result);
    }

    @Test
    public void getAttDataSupThanBetweenDatesTest() throws ArchivingException {
        InfSupGettersBetweenDates datagetter = new InfSupGettersBetweenDates(hdbConnector);
        DbData[] result = datagetter.getAttDataSupThanBetweenDates(DEFAULT + "double_scalar_rw", "0", "11-12-2012",
                "12-12-2012");
        TimedAttrData[] timedData = result[0].getDataAsTimedAttrData();
        assertEquals(2, timedData.length);
        assertEquals(235.908766, timedData[0].db_ptr[0], 0);
    }

    @Test
    public void getAttDataSupThanBetweenDatesCount() throws ArchivingException {
        InfSupGettersBetweenDates datagetter = new InfSupGettersBetweenDates(hdbConnector);
        int result = datagetter.getAttDataSupThanBetweenDatesCount(DEFAULT + "double_scalar_rw", "0", "11-12-2012",
                "12-12-2012");

        assertEquals(2, result);
    }

    @Test
    public void getAttDataInfOrSupThanBetweenDatesTest() throws ArchivingException {
        InfSupGettersBetweenDates datagetter = new InfSupGettersBetweenDates(hdbConnector);
        DbData[] result = datagetter.getAttDataInfOrSupThanBetweenDates(DEFAULT + "double_scalar_rw", "0", "202",
                "11-12-2012", "12-12-2012");
        TimedAttrData[] timedData = result[0].getDataAsTimedAttrData();
        assertEquals(2, timedData.length);
        assertEquals(-255.84408754008876, timedData[0].db_ptr[0], 0);
    }

    @Test
    public void getAttDataInfOrSupThanBetweenDatesCount() throws ArchivingException {
        InfSupGettersBetweenDates datagetter = new InfSupGettersBetweenDates(hdbConnector);
        int result = datagetter.getAttDataInfOrSupThanBetweenDatesCount(DEFAULT + "double_scalar_rw", "0", "202",
                "11-12-2012", "12-12-2012");

        assertEquals(2, result);
    }

    @Test
    public void getAttDataSupAndInfThanBetweenDatesTest() throws ArchivingException {
        InfSupGettersBetweenDates datagetter = new InfSupGettersBetweenDates(hdbConnector);
        DbData[] result = datagetter.getAttDataSupAndInfThanBetweenDates(DEFAULT + "double_scalar_rw", "0", "202",
                "11-12-2012", "12-12-2012");
        TimedAttrData[] timedData = result[0].getDataAsTimedAttrData();
        assertEquals(1, timedData.length);
        assertEquals(201.73063210110848, timedData[0].db_ptr[0], 0);
    }

    @Test
    public void getAttDataSupAndInfThanBetweenDatesCount() throws ArchivingException {
        InfSupGettersBetweenDates datagetter = new InfSupGettersBetweenDates(hdbConnector);
        int result = datagetter.getAttDataSupAndInfThanBetweenDatesCount(DEFAULT + "double_scalar_rw", "0", "202",
                "11-12-2012", "12-12-2012");

        assertEquals(1, result);
    }

}
