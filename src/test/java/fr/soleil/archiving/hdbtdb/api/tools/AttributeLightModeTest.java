package fr.soleil.archiving.hdbtdb.api.tools;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;

public class AttributeLightModeTest {
	String attName = "fakName";
	String deviceInCharge = "deviceInCharge";
	String[] lightvalues = new String[] { attName, String.valueOf(0),
			String.valueOf(TangoConst.Tango_DEV_DOUBLE),
			String.valueOf(AttrDataFormat._SPECTRUM),
			String.valueOf(AttrWriteType._WRITE) };

	String[] modeConf = new String[] { "MODE_P", "10000", "MODE_D", "20000" };
	Mode mode = new Mode(modeConf);

	String[] fullInformation = ArrayUtils.addAll(new String[] {
			attName,
      String.valueOf(TangoConst.Tango_DEV_DOUBLE),
      String.valueOf(AttrDataFormat._SPECTRUM),
      String.valueOf(AttrWriteType._WRITE),deviceInCharge,
			"NULL", String.valueOf(modeConf.length) }, 
	modeConf);

	String[] withoutFullInformation = ArrayUtils.addAll(new String[] { attName }, modeConf);

	@Test
	public void constructorTest() {

		AttributeLightMode attLight = new AttributeLightMode();
		assertEquals("", attLight.getAttributeCompleteName());
		assertNull(attLight.getMode());
		assertEquals("", attLight.getDevice_in_charge());
		assertEquals(0, attLight.getDataFormat());
		assertEquals(0, attLight.getDataType());
		assertEquals(0, attLight.getWritable());

		attLight = new AttributeLightMode(attName);
		assertEquals(attName, attLight.getAttributeCompleteName());
		assertNull(attLight.getMode());
		assertEquals("", attLight.getDevice_in_charge());
		assertEquals(0, attLight.getDataFormat());
		assertEquals(0, attLight.getDataType());
		assertEquals(0, attLight.getWritable());

		attLight = new AttributeLightMode(attName, mode);
		assertEquals(attName, attLight.getAttributeCompleteName());
		assertEquals(mode, attLight.getMode());
		assertEquals("", attLight.getDevice_in_charge());
		assertEquals(0, attLight.getDataFormat());
		assertEquals(0, attLight.getDataType());
		assertEquals(0, attLight.getWritable());

		attLight = new AttributeLightMode(attName, deviceInCharge, mode);
		assertEquals(attName, attLight.getAttributeCompleteName());
		assertEquals(mode, attLight.getMode());
		assertEquals(deviceInCharge, attLight.getDevice_in_charge());
		assertEquals(0, attLight.getDataFormat());
		assertEquals(0, attLight.getDataType());
		assertEquals(0, attLight.getWritable());

		attLight = new AttributeLightMode(new AttributeLight(lightvalues), deviceInCharge, mode);
		assertEquals(attName, attLight.getAttributeCompleteName());
		assertEquals(mode, attLight.getMode());
		assertEquals(deviceInCharge, attLight.getDevice_in_charge());
		assertEquals(TangoConst.Tango_DEV_DOUBLE, attLight.getDataType());
		assertEquals(AttrDataFormat._SPECTRUM, attLight.getDataFormat());
		assertEquals(AttrWriteType._WRITE, attLight.getWritable());
		
		attLight = AttributeLightMode.creationWithFullInformation(fullInformation);
		assertEquals(attName, attLight.getAttributeCompleteName());
		assertEquals(mode, attLight.getMode());
		assertEquals(deviceInCharge, attLight.getDevice_in_charge());
		assertEquals(TangoConst.Tango_DEV_DOUBLE, attLight.getDataType());
		assertEquals(AttrDataFormat._SPECTRUM, attLight.getDataFormat());
		assertEquals(AttrWriteType._WRITE, attLight.getWritable());

		attLight = AttributeLightMode.creationWithoutFullInformation(withoutFullInformation);
		assertEquals(attName, attLight.getAttributeCompleteName());
		assertEquals(mode, attLight.getMode());
		assertEquals(null, attLight.getDevice_in_charge());
		assertEquals(0, attLight.getDataType());
		assertEquals(0, attLight.getDataFormat());
		assertEquals(0, attLight.getWritable());

	}


	@Test
	public void toArrayTest() {
		AttributeLightMode attLight = AttributeLightMode.creationWithFullInformation(fullInformation);
		assertArrayEquals(fullInformation, attLight.toArray());

	}

	@Test
	public void equalsTest() {
		AttributeLightMode attLight = AttributeLightMode.creationWithFullInformation(fullInformation);
		AttributeLightMode attLightCopy = AttributeLightMode.creationWithFullInformation(fullInformation);
		assertTrue(attLight.equals(attLight));
		assertTrue(attLight.equals(attLightCopy));

		Object object = new Object();
		assertFalse(attLight.equals(object));

		attLightCopy = AttributeLightMode.creationWithFullInformation(fullInformation);
		attLightCopy.setDataFormat(-1);
		assertFalse(attLight.equals(attLightCopy));

		attLightCopy = AttributeLightMode.creationWithFullInformation(fullInformation);
		attLightCopy.setDataType(-1);
		assertFalse(attLight.equals(attLightCopy));

		attLightCopy = AttributeLightMode.creationWithFullInformation(fullInformation);
		attLightCopy.setWritable(-1);
		assertFalse(attLight.equals(attLightCopy));

		attLightCopy = AttributeLightMode.creationWithFullInformation(fullInformation);
		attLightCopy.setAttributeCompleteName("Pink Elephant");
		assertFalse(attLight.equals(attLightCopy));

	}
}
