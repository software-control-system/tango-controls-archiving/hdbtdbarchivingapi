package fr.soleil.archiving.hdbtdb.api.tools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;

public class AttributeSupportTest {

	@Test
	public void checkAttributeSupportTest() throws ArchivingException {
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_SHORT, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_USHORT, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_DOUBLE, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_FLOAT, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_BOOLEAN, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STRING, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STATE, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_UCHAR, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG64, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG64, AttrDataFormat._SCALAR,
				AttrWriteType._READ));
		try {
			AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_CHAR, AttrDataFormat._SCALAR,
					AttrWriteType._READ);
			fail("Should throw exception ");
		} catch (ArchivingException aExp) {
			assertEquals("ARCHIVING ERROR :  : Attribute data type not supported...", aExp.getMessage());
		}

		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_SHORT, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_USHORT, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_DOUBLE, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_FLOAT, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_BOOLEAN, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STRING, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_UCHAR, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG64, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG64, AttrDataFormat._SCALAR,
				AttrWriteType._WRITE));

		try {
			AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STATE, AttrDataFormat._SCALAR,
					AttrWriteType._WRITE);
			fail("Should throw exception ");
		} catch (ArchivingException aExp) {
			assertEquals("ARCHIVING ERROR :  : Attribute data type not supported...", aExp.getMessage());
		}
	}

	@Test
	public void checkAttributeSupportScepctrumTest() throws ArchivingException {
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_SHORT, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_USHORT, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_DOUBLE, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_FLOAT, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_BOOLEAN, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STRING, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STATE, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_UCHAR, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG64, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG64, AttrDataFormat._SPECTRUM,
				AttrWriteType._READ));
		try {
			AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_CHAR, AttrDataFormat._SPECTRUM,
					AttrWriteType._READ);
			fail("Should throw exception ");
		} catch (ArchivingException aExp) {
			assertEquals("ARCHIVING ERROR :  : Attribute data type not supported...", aExp.getMessage());
		}

		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_SHORT, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_USHORT, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_DOUBLE, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_FLOAT, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_BOOLEAN, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STRING, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_UCHAR, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG64, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));
		assertTrue(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG64, AttrDataFormat._SPECTRUM,
				AttrWriteType._WRITE));

		try {
			AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STATE, AttrDataFormat._SPECTRUM,
					AttrWriteType._WRITE);
			fail("Should throw exception ");
		} catch (ArchivingException aExp) {
			assertEquals("ARCHIVING ERROR :  : Attribute data type not supported...", aExp.getMessage());
		}
	}

	@Test
	public void checkAttributeSupportImagemTest() throws ArchivingException {
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_SHORT, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_USHORT, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_DOUBLE, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_FLOAT, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_BOOLEAN, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STRING, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_UCHAR, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_ULONG64, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		assertFalse(AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_LONG64, AttrDataFormat._IMAGE,
				AttrWriteType._READ));
		try {
			AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_CHAR, AttrDataFormat._IMAGE,
					AttrWriteType._READ);
			fail("Should throw exception ");
		} catch (ArchivingException aExp) {
			assertEquals("ARCHIVING ERROR :  : Attribute data type not supported...", aExp.getMessage());
		}

		try {
			AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_STATE, AttrDataFormat._IMAGE,
					AttrWriteType._READ);
			fail("Should throw exception ");
		} catch (ArchivingException aExp) {
			assertEquals("ARCHIVING ERROR :  : Attribute data type not supported...", aExp.getMessage());
		}

		try {
			AttributeSupport.checkAttributeSupport("", TangoConst.Tango_DEV_SHORT, AttrDataFormat._IMAGE,
					AttrWriteType._WRITE);
			fail("Should throw exception ");
		} catch (ArchivingException aExp) {
			assertEquals("ARCHIVING ERROR :  : Attribute writable access not supported...", aExp.getMessage());
		}

	}
}
