package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;

public class AttributeDomainsTest extends OracleConnectorTest {

	@Test
	public void getDomainsTest() throws ArchivingException {
		AttributeDomains domain = new AttributeDomains(hdbConnector);

		String[] attributes = domain.getDomains();
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}
    }

	@Test
	public void getDomainsByCriterionTest() throws ArchivingException {
		String domain = null;
		AttributeDomains domains = new AttributeDomains(hdbConnector);

		String[] attributes = domains.getDomainsByCriterion(domain);
		assertTrue(attributes.length == 0);

		domain = "*";

		attributes = domains.getDomainsByCriterion(domain);
		assertTrue(attributes.length > 0);

		domain = "liquibase";

		attributes = domains.getDomainsByCriterion(domain);
		assertTrue(attributes.length > 0);

		domain = "Liquibase";

		attributes = domains.getDomainsByCriterion(domain);
		assertTrue(attributes.length > 0);

		domain = "Liqu*";

		attributes = domains.getDomainsByCriterion(domain);
		assertTrue(attributes.length > 0);
    }

	@Test
	public void getDomainsCountTest() throws ArchivingException {
		AttributeDomains domain = new AttributeDomains(hdbConnector);

		int attributes = domain.getDomainsCount();
		assertTrue(attributes > 0);

    }

}
