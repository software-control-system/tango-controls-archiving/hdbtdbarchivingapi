package fr.soleil.archiving.hdbtdb.api.tools;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Array;
import java.util.Arrays;

import org.junit.Test;

import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.lib.project.math.MathConst;

public class SpectrumEvent_RWTest {

	@Test
	public void consturctorTest() {
		String[] params = new String[] { "attCompleteName", "1355236687000", "4", "0", "null", "3.5", "-4.56789",
				"12" };
		SpectrumEvent_RW specRw = new SpectrumEvent_RW();

		specRw = new SpectrumEvent_RW(params);
		assertEquals("attCompleteName", specRw.getAttributeCompleteName());
		assertEquals(1355236687000l, specRw.getTimeStamp());
		assertEquals(4, specRw.getDimX());
		assertTrue(Arrays.equals(new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 },
				(double[]) specRw.getValue()));
		assertArrayEquals(new boolean[] { true, false, false, false }, specRw.getNullElements());

		params[4] = "NaN";
		params[7] = "12.0";
		assertArrayEquals(params, specRw.toArray());

		assertEquals("Source : \tattCompleteName\r\n" + "TimeSt : \t1355236687000\r\n" + "Value :  \t...\r\n",
				specRw.toString());
	}

	@Test
	public void getValueAsStringTest() {
		SpectrumEvent_RW specRw = new SpectrumEvent_RW();
		assertEquals(GlobalConst.ARCHIVER_NULL_VALUE, specRw.getValueAsString());

		String exprected = "NAN,3.50000000000000000E+00,-4.56789000000000000E+00,1.20000000000000000E+01";
		specRw.setValue(new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "NAN,3.50000000000000000E+00,-4.56789016723632800E+00,1.20000000000000000E+01";
		specRw.setValue(new float[] { (float) MathConst.NAN_FOR_NULL, 3.5f, -4.56789f, 12 }, null);
		assertEquals(exprected, specRw.getValueAsString());

		specRw.setValue(new byte[] { 0, 3, -4, 12 }, null);
		exprected = "0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00,1.20000000000000000E+01";
		assertEquals(exprected, specRw.getValueAsString());

		specRw.setValue(new short[] { 0, 3, -4, 12 }, null);
		assertEquals(exprected, specRw.getValueAsString());

		specRw.setValue(new int[] { 0, 3, -4, 12 }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00,1.35523668700000000E+12";
		specRw.setValue(new long[] { 0, 3, -4, 1355236687000l }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "true,false,false,true";
		specRw.setValue(new boolean[] { true, false, false, true }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "true,false,false,true";
		specRw.setValue(new String[] { "true", "false", "false", "true" }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "true,false,false,true";
		specRw.setValue("true,false,false,true", null);
		assertEquals(exprected, specRw.getValueAsString());

	}

	@Test
	public void getSpectrumValueRWReadTest() {
		SpectrumEvent_RW specRw = new SpectrumEvent_RW();
		assertNull(specRw.getSpectrumValueRWRead());

		specRw.setDimX(3);

		Object value = new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 };
		Object exprected = new double[specRw.getDimX()];
		System.arraycopy(value, 0, exprected, 0, specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWRead());
		assertTrue(Arrays.equals((double[]) exprected, (double[]) specRw.getSpectrumValueRWRead()));

		value = new float[] { (float) MathConst.NAN_FOR_NULL, 3.5f, -4.56789f, 12 };
		exprected = new float[specRw.getDimX()];
		System.arraycopy(value, 0, exprected, 0, specRw.getDimX());
		specRw.setValue(exprected, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWRead());
		assertTrue(Arrays.equals((float[]) exprected, (float[]) specRw.getSpectrumValueRWRead()));

		value = new byte[] { 0, 3, -4, 12 };
		exprected = new byte[specRw.getDimX()];
		System.arraycopy(value, 0, exprected, 0, specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWRead());
		assertTrue(Arrays.equals((byte[]) exprected, (byte[]) specRw.getSpectrumValueRWRead()));

		value = new short[] { 0, 3, -4, 12 };
		exprected = new short[specRw.getDimX()];
		System.arraycopy(value, 0, exprected, 0, specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWRead());
		assertTrue(Arrays.equals((short[]) exprected, (short[]) specRw.getSpectrumValueRWRead()));

		value = new int[] { 0, 3, -4, 12 };
		exprected = new int[specRw.getDimX()];
		System.arraycopy(value, 0, exprected, 0, specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWRead());
		assertTrue(Arrays.equals((int[]) exprected, (int[]) specRw.getSpectrumValueRWRead()));

		value = new long[] { 0, 3, -4, 1355236687000l };
		exprected = new long[specRw.getDimX()];
		System.arraycopy(value, 0, exprected, 0, specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWRead());
		assertArrayEquals((long[]) exprected, (long[]) specRw.getSpectrumValueRWRead());

		value = new boolean[] { true, false, false, true };
		exprected = new boolean[specRw.getDimX()];
		System.arraycopy(value, 0, exprected, 0, specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWRead());
		assertTrue(Arrays.equals((boolean[]) exprected, (boolean[]) specRw.getSpectrumValueRWRead()));

		value = new String[] { "true", "false", "false", "true" };
		exprected = new String[specRw.getDimX()];
		System.arraycopy(value, 0, exprected, 0, specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWRead());
		assertTrue(Arrays.equals((String[]) exprected, (String[]) specRw.getSpectrumValueRWRead()));

		specRw.setValue("true,false,false,true", null);
		assertNull(specRw.getSpectrumValueRWRead());

	}

	@Test
	public void getSpectrumValueRWWriteTest() {
		SpectrumEvent_RW specRw = new SpectrumEvent_RW();
		assertNull(specRw.getSpectrumValueRWWrite());

		specRw.setDimX(3);

		Object value = new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 };
		Object exprected = new double[Array.getLength(value) - specRw.getDimX()];
		System.arraycopy(value, specRw.getDimX(), exprected, 0, Array.getLength(value) - specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((double[]) exprected, (double[]) specRw.getSpectrumValueRWWrite()));

		value = new float[] { (float) MathConst.NAN_FOR_NULL, 3.5f, -4.56789f, 12 };
		exprected = new float[Array.getLength(value) - specRw.getDimX()];
		System.arraycopy(value, specRw.getDimX(), exprected, 0, Array.getLength(value) - specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((float[]) exprected, (float[]) specRw.getSpectrumValueRWWrite()));

		value = new byte[] { 0, 3, -4, 12 };
		exprected = new byte[Array.getLength(value) - specRw.getDimX()];
		System.arraycopy(value, specRw.getDimX(), exprected, 0, Array.getLength(value) - specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((byte[]) exprected, (byte[]) specRw.getSpectrumValueRWWrite()));

		value = new short[] { 0, 3, -4, 12 };
		exprected = new short[Array.getLength(value) - specRw.getDimX()];
		System.arraycopy(value, specRw.getDimX(), exprected, 0, Array.getLength(value) - specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((short[]) exprected, (short[]) specRw.getSpectrumValueRWWrite()));

		value = new int[] { 0, 3, -4, 12 };
		exprected = new int[Array.getLength(value) - specRw.getDimX()];
		System.arraycopy(value, specRw.getDimX(), exprected, 0, Array.getLength(value) - specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((int[]) exprected, (int[]) specRw.getSpectrumValueRWWrite()));

		value = new long[] { 0, 3, -4, 1355236687000l };
		exprected = new long[Array.getLength(value) - specRw.getDimX()];
		System.arraycopy(value, specRw.getDimX(), exprected, 0, Array.getLength(value) - specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertArrayEquals((long[]) exprected, (long[]) specRw.getSpectrumValueRWWrite());

		value = new boolean[] { true, false, false, true };
		exprected = new boolean[Array.getLength(value) - specRw.getDimX()];
		System.arraycopy(value, specRw.getDimX(), exprected, 0, Array.getLength(value) - specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((boolean[]) exprected, (boolean[]) specRw.getSpectrumValueRWWrite()));

		value = new String[] { "true", "false", "false", "true" };
		exprected = new String[Array.getLength(value) - specRw.getDimX()];
		System.arraycopy(value, specRw.getDimX(), exprected, 0, Array.getLength(value) - specRw.getDimX());
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((String[]) exprected, (String[]) specRw.getSpectrumValueRWWrite()));

		specRw.setValue("true,false,false,true", null);
		assertNull(specRw.getSpectrumValueRWWrite());

		specRw.setDimX(5);

		value = new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 };
		exprected = new double[0];
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((double[]) exprected, (double[]) specRw.getSpectrumValueRWWrite()));

		value = new float[] { (float) MathConst.NAN_FOR_NULL, 3.5f, -4.56789f, 12 };
		exprected = new float[0];
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((float[]) exprected, (float[]) specRw.getSpectrumValueRWWrite()));

		value = new byte[] { 0, 3, -4, 12 };
		exprected = new byte[0];
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((byte[]) exprected, (byte[]) specRw.getSpectrumValueRWWrite()));

		value = new short[] { 0, 3, -4, 12 };
		exprected = new short[0];
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((short[]) exprected, (short[]) specRw.getSpectrumValueRWWrite()));

		value = new int[] { 0, 3, -4, 12 };
		exprected = new int[0];
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((int[]) exprected, (int[]) specRw.getSpectrumValueRWWrite()));

		value = new long[] { 0, 3, -4, 1355236687000l };
		exprected = new long[0];
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertArrayEquals((long[]) exprected, (long[]) specRw.getSpectrumValueRWWrite());

		value = new boolean[] { true, false, false, true };
		exprected = new boolean[0];
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((boolean[]) exprected, (boolean[]) specRw.getSpectrumValueRWWrite()));

		value = new String[] { "true", "false", "false", "true" };
		exprected = new String[0];
		specRw.setValue(value, null);
		assertNotEquals(exprected, specRw.getSpectrumValueRWWrite());
		assertTrue(Arrays.equals((String[]) exprected, (String[]) specRw.getSpectrumValueRWWrite()));
	}

	@Test
	public void getSpectrumValueRW_AsString_ReadTest() {
		SpectrumEvent_RW specRw = new SpectrumEvent_RW();
		specRw.setDimX(3);

		assertEquals(GlobalConst.ARCHIVER_NULL_VALUE, specRw.getSpectrumValueRW_AsString_Read());

		String exprected = "NAN,3.50000000000000000E+00,-4.56789000000000000E+00";
		specRw.setValue(new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Read());

		exprected = "NAN,3.50000000000000000E+00,-4.56789016723632800E+00";
		specRw.setValue(new float[] { (float) MathConst.NAN_FOR_NULL, 3.5f, -4.56789f, 12 }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Read());

		specRw.setValue(new byte[] { 0, 3, -4, 12 }, null);
		exprected = "0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00";
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Read());

		specRw.setValue(new short[] { 0, 3, -4, 12 }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Read());

		specRw.setValue(new int[] { 0, 3, -4, 12 }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Read());

		exprected = "0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00";
		specRw.setValue(new long[] { 0, 3, -4, 1355236687000l }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Read());

		exprected = "true,false,false";
		specRw.setValue(new boolean[] { true, false, false, true }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Read());

		exprected = "true,false,false";
		specRw.setValue(new String[] { "true", "false", "false", "true" }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Read());

		specRw.setValue("true,false,false,true", null);
		assertEquals(GlobalConst.ARCHIVER_NULL_VALUE, specRw.getSpectrumValueRW_AsString_Read());
	}

	@Test
	public void getSpectrumValueRW_AsString_WriteTest() {
		SpectrumEvent_RW specRw = new SpectrumEvent_RW();
		specRw.setDimX(3);

		assertEquals(GlobalConst.ARCHIVER_NULL_VALUE, specRw.getSpectrumValueRW_AsString_Write());

		String exprected = "1.20000000000000000E+01";
		specRw.setValue(new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Write());


		specRw.setValue(new float[] { (float) MathConst.NAN_FOR_NULL, 3.5f, -4.56789f, 12 }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Write());

		specRw.setValue(new byte[] { 0, 3, -4, 12 }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Write());

		specRw.setValue(new short[] { 0, 3, -4, 12 }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Write());

		specRw.setValue(new int[] { 0, 3, -4, 12 }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Write());

		exprected = "1.35523668700000000E+12";
		specRw.setValue(new long[] { 0, 3, -4, 1355236687000l }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Write());

		exprected = "true";
		specRw.setValue(new boolean[] { true, false, false, true }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Write());

		exprected = "true";
		specRw.setValue(new String[] { "true", "false", "false", "true" }, null);
		assertEquals(exprected, specRw.getSpectrumValueRW_AsString_Write());

		specRw.setValue("true,false,false,true", null);
		assertEquals(GlobalConst.ARCHIVER_NULL_VALUE, specRw.getSpectrumValueRW_AsString_Write());
	}
}
