package fr.soleil.archiving.hdbtdb.api.tools;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import fr.soleil.lib.project.math.MathConst;

public class ImageEvent_ROTest {

	@Test
	public void consturctorTest() {
		String[] params = new String[] { "attCompleteName", "1355236687000", "4", "1", "null", "3.5", "-4.56789",
				"12" };
		ImageEvent_RO specRw = new ImageEvent_RO();

		specRw = new ImageEvent_RO(params);
		assertEquals("attCompleteName", specRw.getAttributeCompleteName());
		assertEquals(1355236687000l, specRw.getTimeStamp());
		assertEquals(4, specRw.getDimX());
		assertEquals(1, specRw.getDimY());
		assertTrue(Arrays.equals(new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 },
				((double[][]) specRw.getValue())[0]));
		assertTrue(Arrays.equals(new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 },
				 specRw.getImageValueRO()[0]));
		assertArrayEquals(new boolean[] { true, false, false, false }, specRw.getNullElements()[0]);

		params[4] = "NaN";
		params[7] = "12.0";
		assertArrayEquals(params, specRw.toArray());

		assertEquals("Source : \tattCompleteName\r\n" + "TimeSt : \t1355236687000\r\n" + "Value :  \t...\r\n",
				specRw.toString());
	}

	@Test
	public void getValueAsStringTest() {
		ImageEvent_RO specRw = new ImageEvent_RO();
		assertEquals("", specRw.getValueAsString());

		String exprected = "NAN,3.50000000000000000E+00,-4.56789000000000000E+00,1.20000000000000000E+01"
				+ "~NAN,3.50000000000000000E+00,-4.56789000000000000E+00,1.20000000000000000E+01";
		specRw.setValue(new double[][] { new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 },
				new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 } }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "NAN,3.50000000000000000E+00,-4.56789016723632800E+00,1.20000000000000000E+01"
				+ "~NAN,3.50000000000000000E+00,-4.56789016723632800E+00,1.20000000000000000E+01";
		specRw.setValue(new float[][] { new float[] { (float) MathConst.NAN_FOR_NULL, 3.5f, -4.56789f, 12 },
				new float[] { (float) MathConst.NAN_FOR_NULL, 3.5f, -4.56789f, 12 } }, null);
		assertEquals(exprected, specRw.getValueAsString());

		specRw.setValue(new byte[][] { new byte[] { 0, 3, -4, 12 }, new byte[] { 0, 3, -4, 12 } }, null);
		exprected = "0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00,1.20000000000000000E+01"
				+ "~0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00,1.20000000000000000E+01";
		assertEquals(exprected, specRw.getValueAsString());

		specRw.setValue(new short[][] { new short[] { 0, 3, -4, 12 }, new short[] { 0, 3, -4, 12 } }, null);
		assertEquals(exprected, specRw.getValueAsString());

		specRw.setValue(new int[][] { new int[] { 0, 3, -4, 12 }, new int[] { 0, 3, -4, 12 } }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00,1.35523668700000000E+12"
				+ "~0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00,1.35523668700000000E+12";
		specRw.setValue(
				new long[][] { new long[] { 0, 3, -4, 1355236687000l }, new long[] { 0, 3, -4, 1355236687000l } },
				null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "true,false,false,true~true,false,false,true";
		specRw.setValue(new boolean[][] { new boolean[] { true, false, false, true },
				new boolean[] { true, false, false, true } }, null);
		assertEquals(exprected, specRw.getValueAsString());


		specRw.setValue(new String[][] { new String[] { "true", "false", "false", "true" },
				new String[] { "true", "false", "false", "true" } }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "true,false,false,true";
		specRw.setValue("true,false,false,true", null);
		assertEquals(exprected, specRw.getValueAsString());

	}



	@Test
	public void getImageValueRO_AsStringTest() {
		double[][] value = new double[][] { new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 } };
		ImageEvent_RO specRw = new ImageEvent_RO();
		specRw.setDimX(4);
		specRw.setDimY(1);

		String exprected = "NAN	3.50000000000000000E+00	-4.56789000000000000E+00	1.20000000000000000E+01\r\n";
		specRw.setValue(value, null);
		assertEquals(exprected, specRw.getImageValueRO_AsString());
	}


}
