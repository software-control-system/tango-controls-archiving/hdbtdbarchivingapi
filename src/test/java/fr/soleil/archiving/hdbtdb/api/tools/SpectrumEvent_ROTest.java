package fr.soleil.archiving.hdbtdb.api.tools;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.lib.project.math.MathConst;

public class SpectrumEvent_ROTest {

	@Test
	public void consturctorTest() {
		String[] params = new String[] { "attCompleteName", "1355236687000", "4", "0", "null", "3.5", "-4.56789",
				"12" };
		SpectrumEvent_RO specRw = new SpectrumEvent_RO();

		specRw = new SpectrumEvent_RO(params);
		assertEquals("attCompleteName", specRw.getAttributeCompleteName());
		assertEquals(1355236687000l, specRw.getTimeStamp());
		assertEquals(4, specRw.getDimX());
		assertTrue(Arrays.equals(new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 },
				(double[]) specRw.getValue()));
		assertArrayEquals(new boolean[] { true, false, false, false }, specRw.getNullElements());

		params[4] = "NaN";
		params[7] = "12.0";
		assertArrayEquals(params, specRw.toArray());
		assertEquals("SpectrumEvent_RO : attCompleteName, timestamp = 1355236687000",
				specRw.toString());
	}

	@Test
	public void getValueAsStringTest() {
		SpectrumEvent_RO specRw = new SpectrumEvent_RO();
		assertEquals(GlobalConst.ARCHIVER_NULL_VALUE, specRw.getValueAsString());

		String exprected = "NAN,3.50000000000000000E+00,-4.56789000000000000E+00,1.20000000000000000E+01";
		specRw.setValue(new double[] { MathConst.NAN_FOR_NULL, 3.5, -4.56789, 12 }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "NAN,3.50000000000000000E+00,-4.56789016723632800E+00,1.20000000000000000E+01";
		specRw.setValue(new float[] { (float) MathConst.NAN_FOR_NULL, 3.5f, -4.56789f, 12 }, null);
		assertEquals(exprected, specRw.getValueAsString());

		specRw.setValue(new byte[] { 0, 3, -4, 12 }, null);
		exprected = "0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00,1.20000000000000000E+01";
		assertEquals(exprected, specRw.getValueAsString());

		specRw.setValue(new short[] { 0, 3, -4, 12 }, null);
		assertEquals(exprected, specRw.getValueAsString());

		specRw.setValue(new int[] { 0, 3, -4, 12 }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "0.00000000000000000E+00,3.00000000000000000E+00,-4.00000000000000000E+00,1.35523668700000000E+12";
		specRw.setValue(new long[] { 0, 3, -4, 1355236687000l }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "true,false,false,true";
		specRw.setValue(new boolean[] { true, false, false, true }, null);
		assertEquals(exprected, specRw.getValueAsString());

		exprected = "true,false,false,true";
		specRw.setValue(new String[] { "true", "false", "false", "true" }, null);
		assertEquals(exprected, specRw.getValueAsString());

	}
}
