package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;

public class AttributeNamesTest extends OracleConnectorTest {
	

	@Test
	public void getAllAttributes() throws ArchivingException, DevFailed {
		AttributeNames names = new AttributeNames(hdbConnector);
		Collection<String> attributes = names.getAttributes();
		for (String att : attributes) {
			System.out.println(att);
		}
	}

	@Test
	public void getAttributesByDomainFamilyMember() throws ArchivingException, DevFailed {
		String domain = null;
		String family = null;
		String member = null;
		AttributeNames names = new AttributeNames(hdbConnector);

		String[] attributes = names.getAttributes(domain, family, member);
		assertTrue(attributes.length == 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "liquibase";
		family = "TANGOTEST";
		member = "member";

		attributes = names.getAttributes(domain, family, member);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liquibase";
		family = "tangoTEST";
		member = "MembEr";

		attributes = names.getAttributes(domain, family, member);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liq*";
		family = "t*";
		member = "me*";

		attributes = names.getAttributes(domain, family, member);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}
	}

	@Test
	public void getAttributesSimpleNamesTest() throws ArchivingException {
		String domain = null;
		String family = null;
		String member = null;
		AttributeNames names = new AttributeNames(hdbConnector);

		Collection<String> attributes = names.getAttributesSimpleNames(domain, family, member);
		assertTrue(attributes.size() == 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "liquibase";
		family = "TANGOTEST";
		member = "member";

		attributes = names.getAttributesSimpleNames(domain, family, member);
		assertTrue(attributes.size() > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liquibase";
		family = "tangoTEST";
		member = "MembEr";

		attributes = names.getAttributesSimpleNames(domain, family, member);
		assertTrue(attributes.size() > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liq*";
		family = "t*";
		member = "me*";

		attributes = names.getAttributesSimpleNames(domain, family, member);
		assertTrue(attributes.size() > 0);
		for (String att : attributes) {
			System.out.println(att);
		}
	}

	@Test
	public void getAttributesCountTest() throws ArchivingException {
		String domain = null;
		String family = null;
		String member = null;
		AttributeNames names = new AttributeNames(hdbConnector);

		int attributes = names.getAttributesCount(domain, family, member);
		assertTrue(attributes == 0);


		domain = "liquibase";
		family = "TANGOTEST";
		member = "member";

		attributes = names.getAttributesCount(domain, family, member);
		assertTrue(attributes > 0);

		domain = "Liquibase";
		family = "tangoTEST";
		member = "MembEr";

		attributes = names.getAttributesCount(domain, family, member);
		assertTrue(attributes > 0);

		domain = "Liq*";
		family = "t*";
		member = "me*";

		attributes = names.getAttributesCount(domain, family, member);
		assertTrue(attributes > 0);
	}

	@Test
	public void getAttributesByCriterionTest() throws ArchivingException {
		String domain = null;
		String family = null;
		String member = null;
		String attRegexp = null;
		AttributeNames names = new AttributeNames(hdbConnector);

		String[] attributes = names.getAttributesByCriterion(domain, family, member, attRegexp);
		assertNull(attributes);

		domain = "liquibase";
		family = "TANGOTEST";
		member = "member";
		attRegexp = "*";

		attributes = names.getAttributesByCriterion(domain, family, member, attRegexp);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liquibase";
		family = "tangoTEST";
		member = "MembEr";
		attRegexp = "*";

		attributes = names.getAttributesByCriterion(domain, family, member, attRegexp);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liq*";
		family = "t*";
		member = "me*";
		attRegexp = "*";

		attributes = names.getAttributesByCriterion(domain, family, member, attRegexp);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}
	}


}
