package fr.soleil.archiving.hdbtdb.api.management.modes;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.lib.project.ObjectUtils;

public class HdbModeTest extends OracleConnectorTest {

    @Test
    public void getCurrentArchivedAttTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);
        assertArrayEquals(new String[] { DEFAULT + "double_scalar_rw" }, mode.getCurrentArchivedAtt());
    }

    @Test
    public void getAttributeTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);
        assertEquals(10, mode.getAttribute(false).length);
    }

    @Test
    public void getCurrentArchivedAttCountTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);
        assertEquals(1, mode.getCurrentArchivedAttCount());
    }

    @Test
    public void getDeviceInChargeTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);
        assertEquals("archiving/hdb-oracle/hdbarchiver.01", mode.getDeviceInCharge(DEFAULT + "double_scalar_rw"));
        assertEquals(ObjectUtils.EMPTY_STRING, mode.getDeviceInCharge(DEFAULT + "double_scalar_ro"));
    }

    @Test
    public void isArchivedTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);
        assertTrue(mode.isArchived(DEFAULT + "double_scalar_rw"));
        assertFalse(mode.isArchived(DEFAULT + "double_scalar_rw", "archiving/hdb-oracle/hdbarchiverfake"));
        assertFalse(mode.isArchived(DEFAULT + "double_scalar_ro"));
    }

    @Test
    public void getArchiverForAttributeTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);
        assertEquals("archiving/hdb-oracle/hdbarchiver.01", mode.getArchiverForAttribute(DEFAULT + "double_scalar_rw"));
        assertEquals(ObjectUtils.EMPTY_STRING, mode.getArchiverForAttribute(DEFAULT + "double_scalar_ro"));
    }

    @Test
    public void getCurrentArchivingModeTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);

        Mode currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_rw");
        assertEquals(60, currentMode.getModeP().getPeriod());
    }

    @Test
    public void getArchiverCurrentTasksTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);

        Collection<AttributeLightMode> list = mode.getArchiverCurrentTasks("archiving/hdb-oracle/hdbarchiver.01");
        assertEquals(1, list.size());
        AttributeLightMode att = list.iterator().next();
        assertEquals(DEFAULT + "double_scalar_rw", att.getAttributeCompleteName());
        assertEquals(60, att.getMode().getModeP().getPeriod());
    }

    @Test
    public void insertModeRecordTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);
        String newArchiverName = "archiving/hdb-oracle/hdbarchiverfake";

        Mode currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_ro");
        assertNull(currentMode.getModeP());
        assertNull(currentMode.getModeA());
        assertNull(currentMode.getModeC());
        assertNull(currentMode.getModeD());
        assertNull(currentMode.getModeR());
        assertNull(currentMode.getModeT());
        Collection<AttributeLightMode> list = mode.getArchiverCurrentTasks(newArchiverName);
        assertTrue(list.isEmpty());
        AttributeLightMode attMode = new AttributeLightMode(DEFAULT + "double_scalar_ro");
        attMode.setDevice_in_charge(newArchiverName);
        Mode newMode = new Mode();
        attMode.setMode(newMode);
        mode.insertModeRecord(attMode);

        currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_ro");
        assertNull(currentMode.getModeP());
        assertNull(currentMode.getModeA());
        assertNull(currentMode.getModeC());
        assertNull(currentMode.getModeD());
        assertNull(currentMode.getModeR());
        assertNull(currentMode.getModeT());

        list = mode.getArchiverCurrentTasks(newArchiverName);
        assertFalse(list.isEmpty());
        assertEquals(1, list.size());

        ModePeriode modeP = new ModePeriode(60);
        newMode.setModeP(modeP);
        mode.insertModeRecord(attMode);
        currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_ro");
        assertEquals(modeP, currentMode.getModeP());
        assertNull(currentMode.getModeA());
        assertNull(currentMode.getModeC());
        assertNull(currentMode.getModeD());
        assertNull(currentMode.getModeR());
        assertNull(currentMode.getModeT());

        list = mode.getArchiverCurrentTasks(newArchiverName);
        assertEquals(2, list.size());

        ModeAbsolu modeA = new ModeAbsolu(10, -3.9999, 6.5678998);
        newMode.setModeA(modeA);
        mode.insertModeRecord(attMode);
        currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_ro");
        assertEquals(modeP, currentMode.getModeP());
        assertEquals(modeA, currentMode.getModeA());
        assertNull(currentMode.getModeC());
        assertNull(currentMode.getModeD());
        assertNull(currentMode.getModeR());
        assertNull(currentMode.getModeT());

        list = mode.getArchiverCurrentTasks(newArchiverName);
        assertEquals(3, list.size());

        ModeRelatif modeR = new ModeRelatif(10, -3.9999, 6.5678998);
        newMode.setModeR(modeR);
        ModeCalcul modeC = new ModeCalcul(12, 30, 45);
        newMode.setModeC(modeC);
        ModeDifference modeD = new ModeDifference(90);
        newMode.setModeD(modeD);
        ModeSeuil modeT = new ModeSeuil(12, 30, 45);
        newMode.setModeT(modeT);
        mode.insertModeRecord(attMode);
        currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_ro");
        assertEquals(modeP, currentMode.getModeP());
        assertEquals(modeA, currentMode.getModeA());
        assertEquals(modeC, currentMode.getModeC());
        assertEquals(modeD, currentMode.getModeD());
        assertEquals(modeR, currentMode.getModeR());
        assertEquals(modeT, currentMode.getModeT());

        list = mode.getArchiverCurrentTasks(newArchiverName);
        assertEquals(4, list.size());
    }

    @Test
    public void updateModeRecordTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);
        assertArrayEquals(new String[] { DEFAULT + "double_scalar_rw" }, mode.getCurrentArchivedAtt());
        mode.updateModeRecord(DEFAULT + "double_scalar_rw");
        assertArrayEquals(new String[] {}, mode.getCurrentArchivedAtt());
    }

    @Test
    public void updateModeRecordWithoutStopTest() throws ArchivingException {
        HdbMode mode = new HdbMode(hdbConnector);
        String newArchiverName = "archiving/hdb-oracle/hdbarchiverfake";
        ModePeriode modeP = new ModePeriode(60);
        ModeDifference modeD = new ModeDifference(10);
        Mode currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_rw");
        assertEquals(modeP, currentMode.getModeP());
        assertNull(currentMode.getModeA());
        assertNull(currentMode.getModeC());
        assertEquals(modeD, currentMode.getModeD());
        assertNull(currentMode.getModeR());
        assertNull(currentMode.getModeT());
        Collection<AttributeLightMode> list = mode.getArchiverCurrentTasks(newArchiverName);
        assertTrue(list.isEmpty());
        AttributeLightMode attMode = new AttributeLightMode(DEFAULT + "double_scalar_rw");
        attMode.setDevice_in_charge(newArchiverName);
        Mode newMode = new Mode();
        attMode.setMode(newMode);
        modeP.setPeriod(10);
        newMode.setModeP(modeP);
        mode.updateModeRecordWithoutStop(attMode);

        currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_rw");
        assertEquals(modeP, currentMode.getModeP());
        assertNull(currentMode.getModeA());
        assertNull(currentMode.getModeC());
        assertNull(currentMode.getModeD());
        assertNull(currentMode.getModeR());
        assertNull(currentMode.getModeT());

        list = mode.getArchiverCurrentTasks(newArchiverName);
        assertFalse(list.isEmpty());
        assertEquals(1, list.size());

        mode.updateModeRecordWithoutStop(attMode);
        currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_rw");
        assertEquals(modeP, currentMode.getModeP());
        assertNull(currentMode.getModeA());
        assertNull(currentMode.getModeC());
        assertNull(currentMode.getModeD());
        assertNull(currentMode.getModeR());
        assertNull(currentMode.getModeT());

        list = mode.getArchiverCurrentTasks(newArchiverName);
        assertEquals(1, list.size());

        ModeAbsolu modeA = new ModeAbsolu(10, -3.9999, 6.5678998);
        newMode.setModeA(modeA);
        mode.updateModeRecordWithoutStop(attMode);
        currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_rw");
        assertEquals(modeP, currentMode.getModeP());
        assertEquals(modeA, currentMode.getModeA());
        assertNull(currentMode.getModeC());
        assertNull(currentMode.getModeD());
        assertNull(currentMode.getModeR());
        assertNull(currentMode.getModeT());

        list = mode.getArchiverCurrentTasks(newArchiverName);
        assertEquals(1, list.size());

        ModeRelatif modeR = new ModeRelatif(10, -3.9999, 6.5678998);
        newMode.setModeR(modeR);
        ModeCalcul modeC = new ModeCalcul(12, 30, 45);
        newMode.setModeC(modeC);
        modeD.setPeriod(90);
        newMode.setModeD(modeD);
        ModeSeuil modeT = new ModeSeuil(12, 30, 45);
        newMode.setModeT(modeT);
        mode.updateModeRecordWithoutStop(attMode);
        currentMode = mode.getCurrentArchivingMode(DEFAULT + "double_scalar_rw");
        assertEquals(modeP, currentMode.getModeP());
        assertEquals(modeA, currentMode.getModeA());
        assertEquals(modeC, currentMode.getModeC());
        assertEquals(modeD, currentMode.getModeD());
        assertEquals(modeR, currentMode.getModeR());
        assertEquals(modeT, currentMode.getModeT());

        list = mode.getArchiverCurrentTasks(newArchiverName);
        assertEquals(1, list.size());
    }
}
