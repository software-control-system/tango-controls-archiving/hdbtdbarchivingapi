package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;

public class AttributeFamiliesTest extends OracleConnectorTest {

    @Test
	public void getAllFamiliesTest() throws ArchivingException {
		AttributeFamilies families = new AttributeFamilies(hdbConnector);

		String[] attributes = families.getFamilies();
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}
    }

    @Test
	public void getAllFamiliesCountTest() throws ArchivingException {
		AttributeFamilies families = new AttributeFamilies(hdbConnector);

		int attributes = families.getFamiliesCount();
		assertTrue(attributes > 0);
    }

	@Test
	public void getFamilies() throws ArchivingException {
		String domain = null;
		AttributeFamilies families = new AttributeFamilies(hdbConnector);

		String[] attributes = families.getFamilies(domain);
		assertTrue(attributes.length == 0);

		domain = "*";

		attributes = families.getFamilies(domain);
		assertTrue(attributes.length > 0);

		domain = "liquibase";

		attributes = families.getFamilies(domain);
		assertTrue(attributes.length > 0);

		domain = "Liquibase";

		attributes = families.getFamilies(domain);
		assertTrue(attributes.length > 0);

		domain = "Liqui*";

		attributes = families.getFamilies(domain);
		assertTrue(attributes.length > 0);
    }

	@Test
	public void getFamiliesByCriterion()
            throws ArchivingException {
		String domain = null;
		String family = null;
		AttributeFamilies families = new AttributeFamilies(hdbConnector);

		String[] attributes = families.getFamiliesByCriterion(domain, family);
		assertTrue(attributes.length == 0);

		domain = "*";
		family = "*";

		attributes = families.getFamiliesByCriterion(domain, family);
		assertTrue(attributes.length > 0);

		domain = "liquibase";
		family = "tangotest";

		attributes = families.getFamiliesByCriterion(domain, family);
		assertTrue(attributes.length > 0);

		domain = "Liquibase";
		family = "Tangotest";

		attributes = families.getFamiliesByCriterion(domain, family);
		assertTrue(attributes.length > 0);

		domain = "liqu*";
		family = "tangot*";

		attributes = families.getFamiliesByCriterion(domain, family);
		assertTrue(attributes.length > 0);
    }

	@Test
	public void getFamiliesCountTest() throws ArchivingException {
		String domain = null;
		AttributeFamilies families = new AttributeFamilies(hdbConnector);

		int attributes = families.getFamiliesCount(domain);
		assertTrue(attributes == 0);

		domain = "*";

		attributes = families.getFamiliesCount(domain);
		assertTrue(attributes > 0);

		domain = "liquibase";

		attributes = families.getFamiliesCount(domain);
		assertTrue(attributes > 0);

		domain = "Liquibase";

		attributes = families.getFamiliesCount(domain);
		assertTrue(attributes > 0);

		domain = "Liq*";

		attributes = families.getFamiliesCount(domain);
		assertTrue(attributes > 0);
    }
}
