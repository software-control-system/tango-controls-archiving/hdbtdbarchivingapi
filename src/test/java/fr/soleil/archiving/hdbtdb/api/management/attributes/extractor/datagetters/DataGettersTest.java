package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;

public class DataGettersTest extends OracleConnectorTest {

    protected static final String[] EXPECTED_STRING_SPECTRUM_READ = { null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null, null, null };
    protected static final String[] EXPECTED_STRING_SPECTRUM_WRITE = { "Not initialised" };
    protected static final boolean[] EXPECTED_STRING_SPECTRUM_NULL_ELEMENTS_READ = { true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true,
            true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true };
    protected static final boolean[] EXPECTED_STRING_SPECTRUM_NULL_ELEMENTS_WRITE = { false };

    protected static final short[] EXPECTED_SHORT_RO_FIRST_SPECTRUM = { 119, 35, 201, 116, 240, 228, 222, 57, 53, 137,
            219, 197, 118, 80, 132, 151, 185, 113, 93, 144, 46, 131, 72, 53, 58, 224, 254, 87, 33, 7, 204, 17, 235, 170,
            74, 32, 52, 37, 230, 117, 106, 65, 144, 35, 6, 226, 149, 99, 114, 195, 231, 187, 249, 33, 155, 247, 95, 243,
            24, 102, 191, 41, 81, 105, 115, 113, 157, 152, 87, 71, 13, 194, 157, 229, 143, 128, 122, 242, 242, 217, 173,
            55, 250, 73, 46, 89, 60, 70, 191, 251, 111, 16, 100, 226, 130, 2, 122, 73, 135, 155, 210, 37, 129, 97, 165,
            251, 84, 151, 57, 45, 69, 112, 40, 142, 158, 129, 202, 228, 65, 197, 83, 41, 53, 211, 43, 175, 173, 117, 55,
            72, 71, 92, 201, 169, 1, 253, 152, 254, 42, 221, 111, 82, 107, 13, 212, 53, 242, 250, 69, 102, 36, 123, 58,
            79, 42, 231, 196, 97, 47, 12, 249, 181, 190, 190, 178, 87, 188, 220, 52, 43, 47, 160, 57, 3, 43, 24, 208,
            112, 126, 244, 235, 184, 67, 22, 159, 8, 119, 207, 20, 53, 200, 201, 243, 134, 123, 74, 87, 127, 110, 134,
            31, 167, 137, 244, 210, 161, 196, 66, 32, 184, 46, 216, 252, 68, 120, 4, 187, 71, 24, 240, 225, 228, 149,
            92, 46, 215, 179, 173, 69, 58, 204, 195, 193, 190, 101, 133, 1, 133, 62, 47, 93, 58, 115, 213, 62, 28, 86,
            31, 43, 55, 3, 192, 147, 49, 152, 70 };
    protected static final short[] EXPECTED_SHORT_RO_LAST_SPECTRUM = { 226, 203, 144, 180, 243, 24, 134, 130, 49, 56,
            184, 226, 255, 56, 116, 227, 122, 204, 183, 29, 34, 194, 5, 189, 14, 55, 159, 187, 3, 48, 111, 246, 72, 121,
            121, 46, 49, 91, 46, 105, 207, 17, 227, 156, 201, 31, 185, 71, 225, 206, 106, 164, 211, 39, 178, 11, 198,
            109, 14, 246, 221, 4, 62, 211, 125, 184, 1, 174, 19, 47, 24, 227, 65, 251, 127, 10, 27, 56, 81, 252, 6, 187,
            160, 218, 226, 82, 229, 169, 192, 243, 159, 157, 247, 222, 112, 117, 150, 35, 169, 161, 59, 140, 226, 55,
            11, 236, 82, 68, 61, 78, 74, 249, 239, 36, 219, 65, 9, 132, 1, 252, 36, 158, 244, 2, 14, 105, 152, 128, 140,
            65, 33, 200, 206, 3, 255, 217, 239, 81, 29, 44, 159, 104, 37, 142, 140, 1, 208, 150, 209, 146, 169, 112,
            134, 171, 126, 239, 67, 254, 124, 133, 31, 83, 34, 67, 44, 17, 148, 74, 62, 51, 178, 99, 194, 62, 100, 146,
            212, 234, 103, 147, 211, 237, 63, 82, 221, 130, 80, 89, 7, 112, 157, 90, 146, 224, 135, 164, 116, 226, 167,
            131, 69, 105, 193, 170, 251, 148, 95, 253, 39, 50, 234, 102, 132, 199, 233, 213, 32, 240, 69, 189, 75, 215,
            157, 210, 123, 163, 93, 185, 38, 163, 34, 231, 77, 125, 225, 125, 122, 8, 175, 101, 111, 52, 44, 88, 9, 77,
            72, 78, 10, 147, 37, 168, 101, 161, 185, 8 };
    protected static final boolean[] EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS = { false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false, false, false, false, false, false };

    @Test
    public void getAttDataTestScalarRWDouble() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttData(DEFAULT + "double_scalar_rw");

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);

        assertEquals(3, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_DOUBLE, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SCALAR, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistDataRead = data[READ_INDEX].getTimedData()[0];
        NullableTimedData fistDataWrite = data[WRITE_INDEX].getTimedData()[0];
        NullableTimedData lastDataRead = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        NullableTimedData lastDataWrite = data[WRITE_INDEX].getTimedData()[data[WRITE_INDEX].getTimedData().length - 1];
        assertEquals(1, fistDataRead.getX());
        assertEquals(1, fistDataWrite.getX());
        assertEquals(1, lastDataRead.getX());
        assertEquals(1, lastDataWrite.getX());
        assertEquals(1355236686000l, fistDataRead.getTime());
        assertEquals(1355236686000l, fistDataWrite.getTime());
        assertEquals(1355236687000l, lastDataRead.getTime());
        assertEquals(1355236687000l, lastDataWrite.getTime());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, fistDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, fistDataWrite.getDataType());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, lastDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, lastDataWrite.getDataType());
        assertArrayEquals(new double[] { -255.84408754008876 }, (double[]) fistDataRead.getValue(), 0);
        assertArrayEquals(new double[] { 0 }, (double[]) fistDataWrite.getValue(), 0);
        assertArrayEquals(new double[] { 201.73063210110848 }, (double[]) lastDataRead.getValue(), 0);
        assertArrayEquals(new double[] { 0 }, (double[]) lastDataWrite.getValue(), 0);
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataWrite.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataWrite.getNullElements());
    }

    @Test
    public void getAttDataTestScalarRWLong() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttData(DEFAULT + "long64_scalar_rw");

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);
        assertEquals(3, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_LONG64, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SCALAR, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistDataRead = data[READ_INDEX].getTimedData()[0];
        NullableTimedData fistDataWrite = data[WRITE_INDEX].getTimedData()[0];
        NullableTimedData lastDataRead = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        NullableTimedData lastDataWrite = data[WRITE_INDEX].getTimedData()[data[WRITE_INDEX].getTimedData().length - 1];
        assertEquals(1, fistDataRead.getX());
        assertEquals(1, fistDataWrite.getX());
        assertEquals(1, lastDataRead.getX());
        assertEquals(1, lastDataWrite.getX());
        assertEquals(1355236686000l, fistDataRead.getTime());
        assertEquals(1355236686000l, fistDataWrite.getTime());
        assertEquals(1355236687000l, lastDataRead.getTime());
        assertEquals(1355236687000l, lastDataWrite.getTime());
        assertEquals(TangoConst.Tango_DEV_LONG64, fistDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_LONG64, fistDataWrite.getDataType());
        assertEquals(TangoConst.Tango_DEV_LONG64, lastDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_LONG64, lastDataWrite.getDataType());
        assertArrayEquals(new long[] { 1353490620371l }, (long[]) fistDataRead.getValue());
        assertArrayEquals(new long[] { 1 }, (long[]) fistDataWrite.getValue());
        assertArrayEquals(new long[] { -1353491160371l }, (long[]) lastDataRead.getValue());
        assertArrayEquals(new long[] { 1 }, (long[]) lastDataWrite.getValue());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataWrite.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataWrite.getNullElements());
    }

    @Test
    public void getAttDataTestScalarRWString() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttData(DEFAULT + "string_scalar_rw");

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);
        assertEquals(3, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_STRING, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SCALAR, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistDataRead = data[READ_INDEX].getTimedData()[0];
        NullableTimedData fistDataWrite = data[WRITE_INDEX].getTimedData()[0];
        NullableTimedData lastDataRead = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        NullableTimedData lastDataWrite = data[WRITE_INDEX].getTimedData()[data[WRITE_INDEX].getTimedData().length - 1];
        assertEquals(1, fistDataRead.getX());
        assertEquals(1, fistDataWrite.getX());
        assertEquals(1, lastDataRead.getX());
        assertEquals(1, lastDataWrite.getX());
        assertEquals(1355236686000l, fistDataRead.getTime());
        assertEquals(1355236686000l, fistDataWrite.getTime());
        assertEquals(1355236687000l, lastDataRead.getTime());
        assertEquals(1355236687000l, lastDataWrite.getTime());
        assertEquals(TangoConst.Tango_DEV_STRING, fistDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, fistDataWrite.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, lastDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, lastDataWrite.getDataType());
        assertArrayEquals(new String[] { "SomeStringRead" }, (String[]) fistDataRead.getValue());
        assertArrayEquals(new String[] { "SomeStringWrite" }, (String[]) fistDataWrite.getValue());
        assertArrayEquals(new String[] { "1" }, (String[]) lastDataRead.getValue());
        assertArrayEquals(new String[] { "1" }, (String[]) lastDataWrite.getValue());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataWrite.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataWrite.getNullElements());
    }

    @Test
    public void getAttDataTestSpectrumRWString() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttData(DEFAULT + "string_spectrum_rw");

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[WRITE_INDEX]);
        assertEquals(3, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_STRING, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SPECTRUM, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistDataRead = data[READ_INDEX].getTimedData()[0];
        NullableTimedData fistDataWrite = data[WRITE_INDEX].getTimedData()[0];
        NullableTimedData lastDataRead = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        NullableTimedData lastDataWrite = data[WRITE_INDEX].getTimedData()[data[WRITE_INDEX].getTimedData().length - 1];
        assertEquals(256, fistDataRead.getX());
        assertEquals(256, lastDataRead.getX());
        assertEquals(1355236686000l, fistDataRead.getTime());
        assertEquals(1355236686000l, fistDataWrite.getTime());
        assertEquals(1355236687000l, lastDataRead.getTime());
        assertEquals(1355236687000l, lastDataWrite.getTime());
        assertEquals(TangoConst.Tango_DEV_STRING, fistDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, fistDataWrite.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, lastDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, lastDataWrite.getDataType());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_READ, (String[]) fistDataRead.getValue());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_WRITE, (String[]) fistDataWrite.getValue());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_READ, (String[]) lastDataRead.getValue());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_WRITE, (String[]) lastDataWrite.getValue());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_NULL_ELEMENTS_READ, (boolean[]) fistDataRead.getNullElements());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_NULL_ELEMENTS_WRITE, (boolean[]) fistDataWrite.getNullElements());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_NULL_ELEMENTS_READ, (boolean[]) lastDataRead.getNullElements());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_NULL_ELEMENTS_WRITE, (boolean[]) lastDataWrite.getNullElements());
    }

    @Test
    public void getAttDataTestSpectrumRWShort() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttData(DEFAULT + "short_spectrum_rw");

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);
        assertEquals(3, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_SHORT, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SPECTRUM, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistData = data[READ_INDEX].getTimedData()[0];
        NullableTimedData lastData = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        assertEquals(256, fistData.getX());
        assertEquals(256, lastData.getX());
        assertEquals(1355236686000l, fistData.getTime());
        assertEquals(1355236687000l, lastData.getTime());
        assertEquals(TangoConst.Tango_DEV_SHORT, fistData.getDataType());
        assertEquals(TangoConst.Tango_DEV_SHORT, lastData.getDataType());
        assertArrayEquals(EXPECTED_SHORT_RO_FIRST_SPECTRUM, (short[]) fistData.getValue());
        assertArrayEquals(EXPECTED_SHORT_RO_LAST_SPECTRUM, (short[]) lastData.getValue());
        assertArrayEquals(EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS, (boolean[]) fistData.getNullElements());
        assertArrayEquals(EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS, (boolean[]) lastData.getNullElements());
    }

    @Test
    public void getAttDataTestSpectrumROShort() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttData(DEFAULT + "short_spectrum_ro");

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNull(data[1]);
        assertEquals(3, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_SHORT, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SPECTRUM, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ, data[READ_INDEX].getWritable());

        NullableTimedData fistData = data[READ_INDEX].getTimedData()[0];
        NullableTimedData lastData = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        assertEquals(256, fistData.getX());
        assertEquals(256, lastData.getX());
        assertEquals(1355236686000l, fistData.getTime());
        assertEquals(1355236687000l, lastData.getTime());
        assertEquals(TangoConst.Tango_DEV_SHORT, fistData.getDataType());
        assertEquals(TangoConst.Tango_DEV_SHORT, lastData.getDataType());
        assertArrayEquals(EXPECTED_SHORT_RO_FIRST_SPECTRUM, (short[]) fistData.getValue());
        assertArrayEquals(EXPECTED_SHORT_RO_LAST_SPECTRUM, (short[]) lastData.getValue());
        assertArrayEquals(EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS, (boolean[]) fistData.getNullElements());
        assertArrayEquals(EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS, (boolean[]) lastData.getNullElements());
    }

    @Test
    public void getAttDataLastNTestScalarRWDouble() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttDataLastN(new String[] { DEFAULT + "double_scalar_rw", "2" });

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);
        assertEquals(2, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_DOUBLE, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SCALAR, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistDataRead = data[READ_INDEX].getTimedData()[0];
        NullableTimedData fistDataWrite = data[WRITE_INDEX].getTimedData()[0];
        NullableTimedData lastDataRead = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        NullableTimedData lastDataWrite = data[WRITE_INDEX].getTimedData()[data[WRITE_INDEX].getTimedData().length - 1];
        assertEquals(1, fistDataRead.getX());
        assertEquals(1, fistDataWrite.getX());
        assertEquals(1, lastDataRead.getX());
        assertEquals(1, lastDataWrite.getX());
        assertEquals(1355236686500l, fistDataRead.getTime());
        assertEquals(1355236686500l, fistDataWrite.getTime());
        assertEquals(1355236687000l, lastDataRead.getTime());
        assertEquals(1355236687000l, lastDataWrite.getTime());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, fistDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, fistDataWrite.getDataType());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, lastDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, lastDataWrite.getDataType());
        assertArrayEquals(new double[] { 235.908766 }, (double[]) fistDataRead.getValue(), 0);
        assertArrayEquals(new double[] { 0 }, (double[]) fistDataWrite.getValue(), 0);
        assertArrayEquals(new double[] { 201.73063210110848 }, (double[]) lastDataRead.getValue(), 0);
        assertArrayEquals(new double[] { 0 }, (double[]) lastDataWrite.getValue(), 0);
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataWrite.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataWrite.getNullElements());
    }

    @Test
    public void getAttDataLastNTestScalarRWLong() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttDataLastN(new String[] { DEFAULT + "long64_scalar_rw", "2" });

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);
        assertEquals(2, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_LONG64, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SCALAR, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistDataRead = data[READ_INDEX].getTimedData()[0];
        NullableTimedData fistDataWrite = data[WRITE_INDEX].getTimedData()[0];
        NullableTimedData lastDataRead = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        NullableTimedData lastDataWrite = data[WRITE_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        assertEquals(1, fistDataRead.getX());
        assertEquals(1, fistDataWrite.getX());
        assertEquals(1, lastDataRead.getX());
        assertEquals(1, lastDataWrite.getX());
        assertEquals(1355236686500l, fistDataRead.getTime());
        assertEquals(1355236686500l, fistDataWrite.getTime());
        assertEquals(1355236687000l, lastDataRead.getTime());
        assertEquals(1355236687000l, lastDataWrite.getTime());
        assertEquals(TangoConst.Tango_DEV_LONG64, fistDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_LONG64, fistDataWrite.getDataType());
        assertEquals(TangoConst.Tango_DEV_LONG64, lastDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_LONG64, lastDataWrite.getDataType());
        assertArrayEquals(new long[] { 1353491160371l }, (long[]) fistDataRead.getValue());
        assertArrayEquals(new long[] { 1 }, (long[]) fistDataWrite.getValue());
        assertArrayEquals(new long[] { -1353491160371l }, (long[]) lastDataRead.getValue());
        assertArrayEquals(new long[] { 1 }, (long[]) lastDataWrite.getValue());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataWrite.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataWrite.getNullElements());
    }

    @Test
    public void getAttDataLastNTestScalarRWString() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttDataLastN(new String[] { DEFAULT + "string_scalar_rw", "2" });

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);
        assertEquals(2, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_STRING, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SCALAR, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistDataRead = data[READ_INDEX].getTimedData()[0];
        NullableTimedData fistDataWrite = data[WRITE_INDEX].getTimedData()[0];
        NullableTimedData lastDataRead = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        NullableTimedData lastDataWrite = data[WRITE_INDEX].getTimedData()[data[WRITE_INDEX].getTimedData().length - 1];
        assertEquals(1, fistDataRead.getX());
        assertEquals(1, fistDataWrite.getX());
        assertEquals(1, lastDataRead.getX());
        assertEquals(1, lastDataWrite.getX());
        assertEquals(1355236686500l, fistDataRead.getTime());
        assertEquals(1355236686500l, fistDataWrite.getTime());
        assertEquals(1355236687000l, lastDataRead.getTime());
        assertEquals(1355236687000l, lastDataWrite.getTime());
        assertEquals(TangoConst.Tango_DEV_STRING, fistDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, fistDataWrite.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, lastDataRead.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, lastDataWrite.getDataType());
        assertArrayEquals(new String[] { "2" }, (String[]) fistDataRead.getValue());
        assertArrayEquals(new String[] { "3" }, (String[]) fistDataWrite.getValue());
        assertArrayEquals(new String[] { "1" }, (String[]) lastDataRead.getValue());
        assertArrayEquals(new String[] { "1" }, (String[]) lastDataWrite.getValue());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) fistDataWrite.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataWrite.getNullElements());
    }

    @Test
    public void getAttDataLastNTestSpectrumRWString() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttDataLastN(new String[] { DEFAULT + "string_spectrum_rw", "2" });

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);
        assertEquals(2, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_STRING, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SPECTRUM, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistData = data[READ_INDEX].getTimedData()[0];
        NullableTimedData lastData = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        assertEquals(256, fistData.getX());
        assertEquals(256, lastData.getX());
        assertEquals(1355236686500l, fistData.getTime());
        assertEquals(1355236687000l, lastData.getTime());
        assertEquals(TangoConst.Tango_DEV_STRING, fistData.getDataType());
        assertEquals(TangoConst.Tango_DEV_STRING, lastData.getDataType());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_READ, (String[]) fistData.getValue());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_READ, (String[]) lastData.getValue());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_NULL_ELEMENTS_READ, (boolean[]) fistData.getNullElements());
        assertArrayEquals(EXPECTED_STRING_SPECTRUM_NULL_ELEMENTS_READ, (boolean[]) lastData.getNullElements());
    }

    @Test
    public void getAttDataLastNTestSpectrumRWShort() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);

        DbData[] data = datagetter.getAttDataLastN(new String[] { DEFAULT + "short_spectrum_rw", "2" });

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);
        assertEquals(2, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_SHORT, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SPECTRUM, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistData = data[READ_INDEX].getTimedData()[0];
        NullableTimedData lastData = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        assertEquals(256, fistData.getX());
        assertEquals(256, lastData.getX());
        assertEquals(1355236686500l, fistData.getTime());
        assertEquals(1355236687000l, lastData.getTime());
        assertEquals(TangoConst.Tango_DEV_SHORT, fistData.getDataType());
        assertEquals(TangoConst.Tango_DEV_SHORT, lastData.getDataType());
        short[] expectedShortROFirstSprectrum2 = EXPECTED_SHORT_RO_FIRST_SPECTRUM.clone();
        expectedShortROFirstSprectrum2[0] = 12;
        expectedShortROFirstSprectrum2[255] = 71;
        assertArrayEquals(expectedShortROFirstSprectrum2, (short[]) fistData.getValue());
        assertArrayEquals(EXPECTED_SHORT_RO_LAST_SPECTRUM, (short[]) lastData.getValue());
        assertArrayEquals(EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS, (boolean[]) fistData.getNullElements());
        assertArrayEquals(EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS, (boolean[]) lastData.getNullElements());
    }

    @Test
    public void getAttDataLastNTestSpectrumROShort() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);
        DbData[] data = datagetter.getAttDataLastN(new String[] { DEFAULT + "short_spectrum_ro", "2" });

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNull(data[1]);
        assertEquals(2, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_SHORT, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SPECTRUM, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ, data[READ_INDEX].getWritable());

        NullableTimedData fistData = data[READ_INDEX].getTimedData()[0];
        NullableTimedData lastData = data[READ_INDEX].getTimedData()[data[READ_INDEX].getTimedData().length - 1];
        assertEquals(256, fistData.getX());
        assertEquals(256, lastData.getX());
        assertEquals(1355236686500l, fistData.getTime());
        assertEquals(1355236687000l, lastData.getTime());
        assertEquals(TangoConst.Tango_DEV_SHORT, fistData.getDataType());
        assertEquals(TangoConst.Tango_DEV_SHORT, lastData.getDataType());
        assertEquals(12, ((short[]) fistData.getValue())[0]);
        assertEquals(226, ((short[]) lastData.getValue())[0]);
        assertArrayEquals(EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS, (boolean[]) fistData.getNullElements());
        assertArrayEquals(EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS, (boolean[]) lastData.getNullElements());
    }

    @Test
    public void getNewestValueTest() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);
        DbData data = datagetter.getNewestValue(DEFAULT + "double_scalar_rw");

        assertNotNull(data);
        assertEquals(1, data.getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_DOUBLE, data.getDataType());
        assertEquals(AttrDataFormat._SCALAR, data.getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data.getWritable());

        NullableTimedData lastData = data.getTimedData()[0];
        assertEquals(1, lastData.getX());
        assertEquals(1355236687000l, lastData.getTime());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, lastData.getDataType());
        assertArrayEquals(new String[] { "201.73063210110848" }, (String[]) lastData.getValue());
        assertNull(lastData.getNullElements());
    }

    @Test
    public void getNearestValueTest() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);
        DbData[] data = datagetter.getNearestValue(DEFAULT + "double_scalar_rw", "2012-12-11 15:38:06.4");

        assertNotNull(data);
        assertEquals(2, data.length);
        assertNotNull(data[READ_INDEX]);
        assertNotNull(data[1]);
        assertEquals(1, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_DOUBLE, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SCALAR, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData lastDataRead = data[READ_INDEX].getTimedData()[0];
        NullableTimedData lastDataWrite = data[WRITE_INDEX].getTimedData()[0];
        assertEquals(1, lastDataRead.getX());
        assertEquals(1355236686500l, lastDataRead.getTime());
        assertEquals(TangoConst.Tango_DEV_DOUBLE, lastDataRead.getDataType());
        assertArrayEquals(new double[] { 235.908766 }, (double[]) lastDataRead.getValue(), 0);
        assertArrayEquals(new double[] { 0 }, (double[]) lastDataWrite.getValue(), 0);
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataRead.getNullElements());
        assertArrayEquals(new boolean[] { false }, (boolean[]) lastDataWrite.getNullElements());

        data = datagetter.getNearestValue(DEFAULT + "short_spectrum_rw", "2012-12-11 15:38:06.4");

        assertNotNull(data);
        assertEquals(1, data[READ_INDEX].getTimedData().length);
        assertEquals(TangoConst.Tango_DEV_SHORT, data[READ_INDEX].getDataType());
        assertEquals(AttrDataFormat._SPECTRUM, data[READ_INDEX].getDataFormat());
        assertEquals(AttrWriteType._READ_WRITE, data[READ_INDEX].getWritable());

        NullableTimedData fistData = data[READ_INDEX].getTimedData()[0];
        assertEquals(256, fistData.getX());
        assertEquals(1355236686500l, fistData.getTime());
        assertEquals(TangoConst.Tango_DEV_SHORT, fistData.getDataType());
        short[] expectedShortROFirstSprectrum2 = EXPECTED_SHORT_RO_FIRST_SPECTRUM.clone();
        expectedShortROFirstSprectrum2[0] = 12;
        expectedShortROFirstSprectrum2[255] = 71;
        assertArrayEquals(expectedShortROFirstSprectrum2, (short[]) fistData.getValue());
        assertArrayEquals(EXPECTED_SHORT_RO_SPECTRUM_NULL_ELEMENTS, (boolean[]) fistData.getNullElements());
    }

    @Test
    public void getAttDataCountTest() throws ArchivingException {
        DataGetters datagetter = new OracleDataGetters(hdbConnector);
        assertEquals(3, datagetter.getAttDataCount(DEFAULT + "double_scalar_rw"));
    }

}
