package fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;
import fr.soleil.archiving.hdbtdb.api.tools.ScalarEvent;

public class HdbAttributeInsertTest extends OracleConnectorTest {
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Test
	public void insert_ScalarDataTest() throws ArchivingException, InterruptedException {
		String att = DEFAULT + "float_scalar_ro";

		IHdbAttributeInsert hdbAttInsert = HdbAttributeInsertFactory.getInstance(hdbConnector, null);
		ScalarEvent scalarEvent = new ScalarEvent();
		scalarEvent.setAttributeCompleteName(att);
		scalarEvent.setDataType(TangoConst.Tango_DEV_FLOAT);
		scalarEvent.setWritable(0);
		scalarEvent.setValue(Double.NaN, null);
		scalarEvent.setTimeStamp(Timestamp.from(Instant.now()).getTime());
		hdbAttInsert.insert_ScalarData(scalarEvent);
		TimeUnit.MILLISECONDS.sleep(10);
		scalarEvent = new ScalarEvent();
		scalarEvent.setAttributeCompleteName(att);
		scalarEvent.setDataType(TangoConst.Tango_DEV_FLOAT);
		scalarEvent.setWritable(0);
		scalarEvent.setValue(null, null);
		scalarEvent.setTimeStamp(Timestamp.from(Instant.now()).getTime());
		hdbAttInsert.insert_ScalarData(scalarEvent);
		TimeUnit.MILLISECONDS.sleep(10);
		scalarEvent = new ScalarEvent();
		scalarEvent.setAttributeCompleteName(att);
		scalarEvent.setDataType(TangoConst.Tango_DEV_FLOAT);
		scalarEvent.setWritable(0);
		scalarEvent.setValue(Float.NaN, null);
		scalarEvent.setTimeStamp(Timestamp.from(Instant.now()).getTime());
		hdbAttInsert.insert_ScalarData(scalarEvent);
		TimeUnit.MILLISECONDS.sleep(10);
		scalarEvent = new ScalarEvent();
		scalarEvent.setAttributeCompleteName(att);
		scalarEvent.setDataType(TangoConst.Tango_DEV_FLOAT);
		scalarEvent.setWritable(0);
		scalarEvent.setValue(Float.intBitsToFloat(0xffffffff), null);
		scalarEvent.setTimeStamp(Timestamp.from(Instant.now()).getTime());
		hdbAttInsert.insert_ScalarData(scalarEvent);
		TimeUnit.MILLISECONDS.sleep(10);

		hdbAttInsert.closeConnection();
	}
}
