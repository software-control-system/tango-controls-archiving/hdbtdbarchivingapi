package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;

public class MinMawAvgGettersBetweenDatesTest extends OracleConnectorTest {

	@Test
	public void getAttDataMinBetweenDatesTest() throws ArchivingException {
		MinMaxAvgGettersBetweenDates datagetter = new MinMaxAvgGettersBetweenDates(hdbConnector);
		double result = datagetter.getAttDataMinBetweenDates(
				new String[] { DEFAULT + "double_scalar_rw", "11-12-2012", "12-12-2012" });
		assertEquals(-255.84408754008876, result, 0);
	}

	@Test
	public void getAttDataMaxBetweenDatesTest() throws ArchivingException {
		MinMaxAvgGettersBetweenDates datagetter = new MinMaxAvgGettersBetweenDates(hdbConnector);
		double result = datagetter
				.getAttDataMaxBetweenDates(new String[] { DEFAULT + "double_scalar_rw", "11-12-2012", "12-12-2012" });
		assertEquals(235.908766, result, 0);
	}

	@Test
	public void getAttDataAvgBetweenDatesTest() throws ArchivingException {
		MinMaxAvgGettersBetweenDates datagetter = new MinMaxAvgGettersBetweenDates(hdbConnector);
		double result = datagetter
				.getAttDataAvgBetweenDates(new String[] { DEFAULT + "double_scalar_rw", "11-12-2012", "12-12-2012" });
		assertEquals(60.59843685367324, result, 0);
	}

}
