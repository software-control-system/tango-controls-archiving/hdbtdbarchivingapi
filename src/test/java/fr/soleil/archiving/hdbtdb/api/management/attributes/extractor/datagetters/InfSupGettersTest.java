package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import fr.esrf.TangoDs.TimedAttrData;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;

public class InfSupGettersTest extends OracleConnectorTest {

    @Test
    public void getAttDataInfThanTest() throws ArchivingException {
        InfSupGetters datagetter = new InfSupGetters(hdbConnector);
        DbData[] result = datagetter.getAttDataInfThan(DEFAULT + "double_scalar_rw", "0");
        assertNotNull(result);
        assertEquals(2, result.length);
        assertNotNull(result[0]);
        TimedAttrData[] timedData = result[0].getDataAsTimedAttrData();
        assertEquals(1, timedData.length);
        assertEquals(-255.84408754008876, timedData[0].db_ptr[0], 0);
    }

    @Test
    public void getAttDataInfThanCountTest() throws ArchivingException {
        InfSupGetters datagetter = new InfSupGetters(hdbConnector);
        int result = datagetter.getAttDataInfThanCount(DEFAULT + "double_scalar_rw", "0");

        assertEquals(1, result);
    }

    @Test
    public void getAttDataSupThanTest() throws ArchivingException {
        InfSupGetters datagetter = new InfSupGetters(hdbConnector);
        DbData[] result = datagetter.getAttDataSupThan(DEFAULT + "double_scalar_rw", "0");
        assertNotNull(result);
        assertEquals(2, result.length);
        assertNotNull(result[0]);
        TimedAttrData[] timedData = result[0].getDataAsTimedAttrData();
        assertEquals(2, timedData.length);
        assertEquals(235.908766, timedData[0].db_ptr[0], 0);
    }

    @Test
    public void getAttDataSupThanCount() throws ArchivingException {
        InfSupGetters datagetter = new InfSupGetters(hdbConnector);
        int result = datagetter.getAttDataSupThanCount(DEFAULT + "double_scalar_rw", "0");

        assertEquals(2, result);
    }

    @Test
    public void getAttDataInfOrSupThanTest() throws ArchivingException {
        InfSupGetters datagetter = new InfSupGetters(hdbConnector);
        DbData[] result = datagetter.getAttDataInfOrSupThan(DEFAULT + "double_scalar_rw", "0", "202");
        assertNotNull(result);
        assertEquals(2, result.length);
        assertNotNull(result[0]);
        TimedAttrData[] timedData = result[0].getDataAsTimedAttrData();
        assertEquals(2, timedData.length);
        assertEquals(-255.84408754008876, timedData[0].db_ptr[0], 0);
    }

    @Test
    public void getAttDataInfOrSupThanCount() throws ArchivingException {
        InfSupGetters datagetter = new InfSupGetters(hdbConnector);
        int result = datagetter.getAttDataInfOrSupThanCount(DEFAULT + "double_scalar_rw", "0", "202");

        assertEquals(2, result);
    }

    @Test
    public void getAttDataSupAndInfThanTest() throws ArchivingException {
        InfSupGetters datagetter = new InfSupGetters(hdbConnector);
        DbData[] result = datagetter.getAttDataSupAndInfThan(DEFAULT + "double_scalar_rw", "0", "202");
        assertNotNull(result);
        assertEquals(2, result.length);
        assertNotNull(result[0]);
        TimedAttrData[] timedData = result[0].getDataAsTimedAttrData();
        assertEquals(1, timedData.length);
        assertEquals(201.73063210110848, timedData[0].db_ptr[0], 0);
    }

    @Test
    public void getAttDataSupAndInfThanCount() throws ArchivingException {
        InfSupGetters datagetter = new InfSupGetters(hdbConnector);
        int result = datagetter.getAttDataSupAndInfThanCount(DEFAULT + "double_scalar_rw", "0", "202");

        assertEquals(1, result);
    }
}
