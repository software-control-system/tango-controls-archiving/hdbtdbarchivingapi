package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.Collection;

import org.junit.Test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.cache.CacheAttribute;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeIds;

public class AdtAptAttributesTest extends OracleConnectorTest {

	static String attributeTestFullName = DEFAULT + "string_spectrum_rw";

	@Test
	public void getDataFormatTest() throws ArchivingException {

		OracleAdtAptAttributes adp = new OracleAdtAptAttributes(hdbConnector, new AttributeIds(hdbConnector));
		adp.getAttrCache().clear();
		CacheAttribute attribute = adp.getAttrCache().getAttribute(attributeTestFullName);
		assertEquals(-1, attribute.getDataFormat());
		assertEquals(-1, attribute.getDataType());
		assertEquals(-1, attribute.getWritable());

		assertEquals(AttrDataFormat._SPECTRUM, adp.getAttDataFormat(attributeTestFullName));
		assertEquals(TangoConst.Tango_DEV_STRING, adp.getAttDataType(attributeTestFullName));
		assertEquals(AttrWriteType._READ_WRITE, adp.getAttDataWritable(attributeTestFullName));

		attribute = adp.getAttrCache().getAttribute(attributeTestFullName);
		assertEquals(adp.getAttDataFormat(attributeTestFullName), attribute.getDataFormat());
		assertEquals(adp.getAttDataType(attributeTestFullName), attribute.getDataType());
		assertEquals(adp.getAttDataWritable(attributeTestFullName), attribute.getWritable());

	}

	@Test
	public void getAttributeLightInfoTest() throws ArchivingException, SQLException {

		OracleAdtAptAttributes adp = new OracleAdtAptAttributes(hdbConnector, new AttributeIds(hdbConnector));
		adp.getAttrCache().clear();
		CacheAttribute cache = adp.getAttrCache().getAttribute(attributeTestFullName);
		assertEquals(-1, cache.getDataFormat());
		assertEquals(-1, cache.getDataType());
		assertEquals(-1, cache.getWritable());

		AttributeLight att  = adp.getAttributeLightInfo(attributeTestFullName);
		assertEquals(AttrDataFormat._SPECTRUM, att.getDataFormat());
		assertEquals(TangoConst.Tango_DEV_STRING, att.getDataType());
		assertEquals(AttrWriteType._READ_WRITE, att.getWritable());

		cache = adp.getAttrCache().getAttribute(attributeTestFullName);
		assertEquals(adp.getAttDataFormat(attributeTestFullName), cache.getDataFormat());
		assertEquals(adp.getAttDataType(attributeTestFullName), cache.getDataType());
		assertEquals(adp.getAttDataWritable(attributeTestFullName), cache.getWritable());

	}

	@Test
	public void getAttDefinitionDataTest() throws ArchivingException, SQLException {
		OracleAdtAptAttributes adp = new OracleAdtAptAttributes(hdbConnector, new AttributeIds(hdbConnector));
		Collection<String> result = adp.getAttDefinitionData(attributeTestFullName);
		assertFalse(result.isEmpty());
		assertArrayEquals(new String[] {
				"ID::9",
				"time::21-NOV-12 10.37.02.000000000 AM",
				"full_name::liquibase/tangotest/member/string_spectrum_rw",
				"device::tango/tangotest/7", 
				"domain::liquibase", 
				"family::tangotest", 
				"member::member", 
				"att_name::string_spectrum_rw",
				"data_type::8",
				"data_format::1",
				"writable::3",
				"max_dim_x::256",
				"max_dim_y::0", 
				"levelg::0",
				"facility::calypso:20001",
				"archivable::0",
				"substitute::0"
		}, result.toArray());
	}

	@Test
	public void isRegisteredADTTest() throws ArchivingException {
		OracleAdtAptAttributes adp = new OracleAdtAptAttributes(hdbConnector, new AttributeIds(hdbConnector));
		assertTrue(adp.isRegisteredADT(attributeTestFullName));
		assertFalse(adp.isRegisteredADT(attributeTestFullName + "fake"));
	}

	@Test
	public void getAttRecordCountTest() throws ArchivingException {
		OracleAdtAptAttributes adp = new OracleAdtAptAttributes(hdbConnector, new AttributeIds(hdbConnector));
		assertEquals(3, adp.getAttRecordCount(attributeTestFullName));
		try {
			assertEquals(0, adp.getAttRecordCount(attributeTestFullName + "fake"));
			assertFalse("Shoudl not reach", true);
		}catch (ArchivingException e) {
			assertEquals("Id not found", e.getMessage());
		}
	}

	@Test
	public void getAttTFWDataByIdTest() throws ArchivingException {
		OracleAdtAptAttributes adp = new OracleAdtAptAttributes(hdbConnector, new AttributeIds(hdbConnector));
		int[] tfw = adp.getAttTFWDataById(1);

		assertEquals(TangoConst.Tango_DEV_DOUBLE, tfw[0]);
		assertEquals(AttrDataFormat._SCALAR, tfw[1]);
		assertEquals(AttrWriteType._READ_WRITE, tfw[2]);

	}

}
