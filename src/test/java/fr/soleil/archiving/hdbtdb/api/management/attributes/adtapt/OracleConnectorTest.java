package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt;

import java.sql.SQLException;

import org.jdbi.v3.core.Handle;
import org.junit.Before;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.DataBaseParameters;
import liquibase.Liquibase;
import liquibase.database.jvm.HsqlConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

abstract public class OracleConnectorTest implements DBExtractionConst {
    protected static AbstractDataBaseConnector hdbConnector;
    protected static final String DEFAULT = "liquibase/tangotest/member/";

    @SuppressWarnings("resource")
    @Before
    public void connectHdb() throws DevFailed, ClassNotFoundException, SQLException, LiquibaseException {
        Class.forName("org.hsqldb.jdbcDriver");
        Class.forName("org.h2.Driver");
        final DataBaseParameters params = new DataBaseParameters();
        params.setHost("");
        params.setUser("hdb");
        params.setPassword("hdb");
        params.setName("hdb");
        params.setSchema("hdb");
        params.setDbType(DataBaseParameters.DataBaseType.H2);
        System.setProperty("log4jdbc.active", "false");
        try {
            hdbConnector = ConnectionFactory.connect(params);
        } catch (ArchivingException e) {
            throw e.toTangoException();
        }

        // Supprime la bdd de test précedente si elle existe
        try (Handle handle = hdbConnector.getJdbi().open()) {
            handle.createUpdate("DROP ALL OBJECTS").execute();
        }
        HsqlConnection hsconn = new HsqlConnection(hdbConnector.getConnection());
        Liquibase liquibase = new Liquibase("liquibase\\master.xml", new ClassLoaderResourceAccessor(), hsconn);
        liquibase.update("");
        hsconn.close();
    }

}
