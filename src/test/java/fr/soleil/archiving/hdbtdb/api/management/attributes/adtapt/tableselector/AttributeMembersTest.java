package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.OracleConnectorTest;

public class AttributeMembersTest extends OracleConnectorTest {

	@Test
	public void getAllMembersTest() throws ArchivingException {
		AttributeMembers members = new AttributeMembers(hdbConnector);
		String[] attributes = members.getMembers();
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}
    }

	@Test
	public void getMembersCountTest() throws ArchivingException {
		AttributeMembers members = new AttributeMembers(hdbConnector);
		int attributes = members.getMembersCount();
		assertTrue(attributes > 0);
    }

	@Test
	public void getMembersFilterTest() throws ArchivingException {
		String domain = "liqu*";
		String family = "tang*";
		AttributeMembers members = new AttributeMembers(hdbConnector);
		String[] attributes = members.getMembers(domain, family);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = null;
		family = null;
		attributes = members.getMembers(domain, family);
		assertTrue(attributes.length == 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "*";
		family = "*";
		attributes = members.getMembers(domain, family);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liq*";
		family = "t*";
		attributes = members.getMembers(domain, family);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liquibase";
		family = "T*";
		attributes = members.getMembers(domain, family);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

    }

	@Test
	public void getMembersByCriterionTest()
            throws ArchivingException {

		String domain = "liqui*";
		String family = "T*";
		String member = "*";
		AttributeMembers members = new AttributeMembers(hdbConnector);
		String[] attributes = members.getMembersByCriterion(domain, family, member);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = null;
		family = null;
		member = null;
		attributes = members.getMembersByCriterion(domain, family, member);
		assertTrue(attributes.length == 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "*";
		family = "*";
		member = "*";
		attributes = members.getMembersByCriterion(domain, family, member);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "liqu*";
		family = "ta*";
		member = "MEM*";
		attributes = members.getMembersByCriterion(domain, family, member);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liqui*";
		family = "ta*";
		member = "Mem*";
		attributes = members.getMembersByCriterion(domain, family, member);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

		domain = "Liquibase";
		family = "tan*";
		attributes = members.getMembersByCriterion(domain, family, member);
		assertTrue(attributes.length > 0);
		for (String att : attributes) {
			System.out.println(att);
		}

    }

	@Test
	public void getMembersFilerCountTest() throws ArchivingException {

		String domain = "liq*";
		String family = "tang*";
		AttributeMembers members = new AttributeMembers(hdbConnector);
		int attributes = members.getMembersCount(domain, family);
		assertTrue(attributes > 0);

		domain = null;
		family = null;
		attributes = members.getMembersCount(domain, family);
		assertTrue(attributes == 0);

		domain = "*";
		family = "*";
		attributes = members.getMembersCount(domain, family);
		assertTrue(attributes > 0);

		domain = "Liqu*";
		family = "t*";
		attributes = members.getMembersCount(domain, family);
		assertTrue(attributes > 0);

		domain = "Liquibase";
		family = "t*";
		attributes = members.getMembersCount(domain, family);
		assertTrue(attributes > 0);

    }

}
