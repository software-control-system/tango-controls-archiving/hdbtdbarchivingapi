//+============================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Mode/ModeRoot.java,v $
//
// Project:      Tango Archiving Service
//
// Description: Of this class inherit all the modes containing the notion of "period".
//
// $Author: chinkumo $
//
// $Revision: 1.3 $
//
// $Log: ModeRoot.java,v $
// Revision 1.3  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.2.16.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.2  2005/01/26 15:35:38  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :   Synchrotron SOLEIL
//			    L'Orme des Merisiers
//			    Saint-Aubin - BP 48
//			    91192 GIF-sur-YVETTE CEDEX
//              FRANCE
//
//+============================================================================

package fr.soleil.archiving.hdbtdb.api.tools.mode;

/**
 * <p/>
 * <B>Description :</B><BR>
 * Of this class inherit all the modes containing the notion of "period".<BR>
 * </p>
 * 
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version $Revision: 1.3 $
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.Mode
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference
 * @see EventMode
 */

public abstract class ModeRoot {

    public static final double SLOW_DRIFT_FIRST_VAL = -99999.99;
    protected int period = 0;

    /**
     * Default constructor
     * 
     * @see #ModeRoot(int)
     */
    public ModeRoot() {
    }

    /**
     * This constructor takes one parameter as inputs.
     * 
     * @param pe
     *            Archiving (polling) period time
     */
    public ModeRoot(final int pe) {
        period = pe;
    }

    /**
     * Gets the archiving (polling) period time
     * 
     * @return the archiving (polling) period time
     */
    public int getPeriod() {
        return period;
    }

    /**
     * Sets the archiving (polling) period time
     */
    public void setPeriod(final int pe) {
        period = pe;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ModeRoot)) {
            return false;
        }

        final ModeRoot modeRoot = (ModeRoot) o;

        if (period != modeRoot.period) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return period;
    }
}