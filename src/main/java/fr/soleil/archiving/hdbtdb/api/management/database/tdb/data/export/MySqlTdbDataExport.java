/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.database.tdb.data.export;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 *
 */
public class MySqlTdbDataExport extends TdbDataExport {

    /**
     * @param f
     */
    public MySqlTdbDataExport(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    public void exportToDB_Data(final String remoteDir, final String fileName, final String tableName,
            final int writable) throws ArchivingException {
        if (remoteDir.endsWith("/") || remoteDir.endsWith("\\")) {
            exportToDB(tableName, remoteDir + fileName);
        } else {
            exportToDB(tableName, remoteDir + File.separator + fileName);
        }

    }

    private void exportToDB(final String tableName, String fullFileName) throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            Statement stmt = null;
            fullFileName = fullFileName.replaceAll(
                    new StringBuilder().append(File.separator).append(File.separator).toString(),
                    new StringBuilder().append(File.separator).append(File.separator).append(File.separator)
                    .append(File.separator).toString());
            final StringBuilder export_query = new StringBuilder().append("LOAD DATA LOCAL INFILE  '")
                    .append(fullFileName).append("' INTO TABLE ").append(tableName)
                    .append(" FIELDS TERMINATED BY ',\\n' LINES TERMINATED BY '\\n%%%\\n'");
            // LOAD DATA LOCAL INFILE att_00025-20080314-062059.dat INTO TABLE
            // att_00016 FIELDS TERMINATED BY ',\n' LINES TERMINATED BY '\n%%%\n'
            try {

                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    stmt.executeUpdate(export_query.toString());
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, export_query.toString());
            } finally {
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
    }

    @Override
    public void forceDatabaseToImportFile(final String tableName) throws ArchivingException {
        // TODO Auto-generated method stub

    }

}
