package fr.soleil.archiving.hdbtdb.api.tools;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.GlobalConst;

public class LoadBalancedList {
    protected Map<String, LoadBalancedArchiver> loadBalancingList = new ConcurrentHashMap<String, LoadBalancedArchiver>();;
    protected boolean historic;
    private final Logger logger = LoggerFactory.getLogger(LoadBalancedList.class);

    /**
     * Initialize an empty LoadBalancedList. This object contains
     * LoadBalancedArchiver object. This object is used to manage the archiving
     * load balancing
     */
    public LoadBalancedList(final boolean historic) {
        this.historic = historic;
    }

    /**
     * Add an archiver (device) to the LoadBalancedList
     *
     * @param archiverName
     *            the archiver name
     * @param scLoad
     *            the given archiver scalar charge
     * @param spLoad
     *            the given archiver spectrum charge
     * @param imLoad
     *            the given archiver image charge
     */
    public void addArchiver(final String archiverName, final int scLoad, final int spLoad, final int imLoad) {
        /*
         * if ( !_lblist.containsKey(archiverName) ) CLA 16/11/06 {
         */
        logger.debug("adding archiver {} with scalar load = {}", archiverName, scLoad);
        final LoadBalancedArchiver loadBalancedArchiver = new LoadBalancedArchiver(scLoad, spLoad, imLoad);
        loadBalancingList.put(archiverName, loadBalancedArchiver);
        // }
    }

    /**
     * Return the light loaded archiver for the given data format (Scalar,
     * Spectrum or Image)
     *
     * @param dataFormat
     *            the given data format
     * @param deviceDefinedAttributeToDedicatedArchiver
     * @return the light loaded archiver for the given data format (Scalar,
     *         Spectrum or Image)
     */
    private String findLightArchiver(final int dataFormat) {
        String lightArch = "";

        switch (dataFormat) {
            case AttrDataFormat._SCALAR:
                lightArch = findLightScalarArchiver();
                break;
            case AttrDataFormat._SPECTRUM:
                lightArch = findLightSpectrumArchiver();
                break;
            case AttrDataFormat._IMAGE:
                lightArch = findLightImageArchiver();
                break;
        }
        return lightArch;
    }

    /**
     * Return the light loaded archiver for scalar data format
     *
     * @param attributeToDedicatedArchiver
     *
     * @return the light loaded archiver for scalar data format
     */
    private String findLightScalarArchiver() {
        String lightArch = "";
        int minLoad = -1;
        logger.debug("looking for an archiver for scalar ");
        for (final Entry<String, LoadBalancedArchiver> entry : loadBalancingList.entrySet()) {
            final String archiverName = entry.getKey();
            Archiver nextVal;
            try {
                nextVal = Archiver.findArchiver(archiverName, historic);
            } catch (final ArchivingException e) {
                logger.error("Error while tring to find archiver " + archiverName, e);
                continue;
            }
            if (nextVal == null || !nextVal.isDedicated()) {
                final int scalarLoad = entry.getValue().get_scalarLoad(); // getScalarLoad(arch);
                logger.debug("{} archiver scalar load is {}", archiverName, scalarLoad);
                if (lightArch.isEmpty() || scalarLoad < minLoad) {
                    lightArch = archiverName;
                    minLoad = scalarLoad;
                }
            }
        }
        logger.debug("got archiver {} with a scalar load of {}", lightArch, minLoad);
        return lightArch;
    }

    /**
     * Return the light loaded archiver for spectrum data format
     *
     * @param deviceDefinedAttributeToDedicatedArchiver
     *
     * @return the light loaded archiver for spectrum data format
     */
    private String findLightSpectrumArchiver() {
        String lightArch = "";
        int minLoad = -1;
        for (final Entry<String, LoadBalancedArchiver> entry : loadBalancingList.entrySet()) {
            final String archiverName = entry.getKey();
            Archiver nextVal;
            try {
                nextVal = Archiver.findArchiver(archiverName, historic);
            } catch (final ArchivingException e) {
                logger.error("Error while tring to find archiver " + archiverName, e);
                continue;
            }

            if (nextVal == null || !nextVal.isDedicated()) {
                final int spectrumLoad = entry.getValue().get_spectrumLoad();
                if (lightArch.isEmpty() || minLoad > spectrumLoad) {
                    lightArch = archiverName;
                    minLoad = spectrumLoad;
                }
            }

        }
        logger.debug("got archiver {} with a spectrum load of {}", lightArch, minLoad);
        return lightArch;
    }

    /**
     * Return the light loaded archiver for image data format
     *
     * @param deviceDefinedAttributeToDedicatedArchiver
     *
     * @return the light loaded archiver for image data format
     */
    private String findLightImageArchiver() {
        String lightArch = "";
        int minLoad = -1;
        for (final Entry<String, LoadBalancedArchiver> entry : loadBalancingList.entrySet()) {
            final String archiverName = entry.getKey();
            Archiver nextVal;
            try {
                nextVal = Archiver.findArchiver(archiverName, historic);
            } catch (final ArchivingException e) {
                e.printStackTrace();
                logger.debug("Error while tring to find archiver " + archiverName, e);
                continue;
            }

            if (nextVal == null || !nextVal.isDedicated()) {
                final int imageLoad = entry.getValue().get_imageLoad();
                if (lightArch.isEmpty() || minLoad > imageLoad) {
                    lightArch = archiverName;
                    minLoad = imageLoad;
                }
            }
        }
        logger.debug("got archiver {} with a image load of {}", lightArch, minLoad);
        return lightArch;
    }

    /**
     * Add the given attribute to the LoadBalancedList
     *
     * @param att
     *            the attribute name
     * @param dataFormat
     *            the data format of the given attribute
     * @param deviceDefinedAttributeToDedicatedArchiver
     * @throws ArchivingException
     */
    public void addAtt2LBL(final String att, final int dataFormat) throws ArchivingException {
        final String archiverName = findLightArchiver(dataFormat);

        if (archiverName == null || archiverName.equals("")) {
            final String message = GlobalConst.ARCHIVING_ERROR_PREFIX;
            final String reason = GlobalConst.NO_ARC_EXCEPTION;
            String desc = "Failed while executing ArchivingManagerApi.addAtt2LBL() method...";
            desc += "can't find a non-dedicated archiver for attribute|" + att + "|";

            throw new ArchivingException(message, reason, ErrSeverity.WARN, desc, "");
        }
        addAtt2Archiver(att, archiverName, dataFormat);
    }

    /**
     * forces to assign an attribute to an archiver
     *
     * @param att
     *            the attribute name
     * @param arch
     *            the archiver name
     * @param dataFormat
     *            the data format of the given attribute
     */
    public void forceAddAtt2Archiver(final String att, final String arch, final int dataFormat) {
        logger.debug("Load balancing - forcing attribute {} to archiver {}", att, arch);
        addAtt2Archiver(att, arch, dataFormat);
    }

    private void addAtt2Archiver(final String att, final String arch, final int dataFormat) {
        if (loadBalancingList.get(arch) == null) {
            addArchiver(arch, 0, 0, 0);
        }

        switch (dataFormat) {
            case AttrDataFormat._SCALAR:
                loadBalancingList.get(arch).addScAttribute(att);
                break;
            case AttrDataFormat._SPECTRUM:
                loadBalancingList.get(arch).addSpAttribute(att);
                break;
            case AttrDataFormat._IMAGE:
                loadBalancingList.get(arch).addImAttribute(att);
                break;
        }
    }

    /**
     * Returns the attribute list managed by the given archiver
     *
     * @param archiver
     *            the archiver name
     * @return the attribute list managed by the given archiver
     */
    public String[] getArchiverAssignedAtt(final String archiver) {
        return loadBalancingList.get(archiver).getAssignedAttributes();
    }

    /**
     * Returns the list of archivers used in this LoadBanlancedList object
     *
     * @return the list of archivers used in this LoadBanlancedList object
     */
    public Collection<String> getArchivers() {
        return loadBalancingList.keySet();
    }

}
