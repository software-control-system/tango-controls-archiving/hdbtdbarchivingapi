package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor;

import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.common.api.tools.StringFormater;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtils;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.lib.project.math.MathConst;

import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class GenericExtractorMethods implements DBExtractionConst {

    protected final AbstractDataBaseConnector connector;
    private volatile Statement lastStatement;
    private volatile boolean canceled;

    public GenericExtractorMethods(final AbstractDataBaseConnector connector) {
        lastStatement = null;
        canceled = false;
        this.connector = connector;
    }

    public void makeDataException(final int format, final String type1, final String type2) throws ArchivingException {
        String message = "", reason = "", desc = "";
        message = "Failed retrieving data ! ";
        reason = "The attribute should be " + type1;
        desc = "The attribute format is  not " + type1 + " : " + format + " (" + type2 + ") !!";
        throw new ArchivingException(message, reason, null, desc, this.getClass().getName());

    }

    public int getDataCountFromQuery(final String query) throws ArchivingException {
        int valuesCount = 0;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rset = null;
        // Create and execute the SQL query string
        // Build the query string
        // MySQL and Oracle queries are the same in this case : no test

        if (connector != null) {
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(query);
                    while (rset.next()) {
                        if (isCanceled()) {
                            break;
                        }
                        valuesCount = rset.getInt(1);
                    }
                }
            } catch (final SQLException e) {
                String message = "";
                if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                        || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.ADB_CONNECTION_FAILURE;
                } else {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.STATEMENT_FAILURE;
                }

                final String reason = GlobalConst.QUERY_FAILURE;
                final String desc = "Failed while executing GenericExtractorMethods.getAttDataCount() method...";
                throw new ArchivingException(message, reason, query, ErrSeverity.WARN, desc, this.getClass().getName(),
                        e);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        // Returns the number of corresponding records
        return valuesCount;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(final boolean canceled) throws ArchivingException {
        this.canceled = canceled;
        if (canceled) {
            if (canceled) {
                ConnectionCommands.cancelStatement(lastStatement);
            }
        }
    }

    public DbData[] getAttScalarDataForQuery(final String query, final DbData... dbData) throws ArchivingException {
        DbData[] result;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rset = null;
        if (connector == null || dbData == null) {
            result = null;
        } else {
            result = dbData;
            // Create and execute the SQL query string

            // My statement
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(query);
                    treatStatementResultForGetScalarData(rset, result);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;
    }

    public void treatStatementResultForGetScalarData(ResultSet rset, DbData... result)
            throws SQLException, ArchivingException {
        if ((rset != null) && (result != null) && (result.length == 2)
                && ((result[0] != null) || (result[1] != null))) {
            int writable, dataType;
            if (result[0] == null) {
                writable = result[1].getWritable();
                dataType = result[1].getDataType();
            } else {
                writable = result[0].getWritable();
                dataType = result[0].getDataType();
            }
            final boolean roFields = writable == AttrWriteType._READ || writable == AttrWriteType._WRITE;
            List<NullableTimedData> readList = new ArrayList<>(), writeList = new ArrayList<>();
            while (rset.next()) {
                if (isCanceled()) {
                    break;
                }
                long date = DateUtil.stringToMilli(rset.getString(1));
                NullableTimedData readData = DbData.initNullableTimedData(dataType, date, 1);
                NullableTimedData writeData = roFields ? null : DbData.initNullableTimedData(dataType, date, 1);
                final boolean[] nullElementsRead = new boolean[1];
                final Object readValue = DbData.initPrimitiveArray(dataType, 1);
                final Object writeValue = roFields ? null : DbData.initPrimitiveArray(dataType, 1);
                final boolean[] nullElementsWrite = roFields ? null : new boolean[1];

                Object read = extractData(rset, 2, dataType, nullElementsRead);
                Array.set(readValue, 0, read);
                readData.setValue(readValue, nullElementsRead);
                if (writable == AttrWriteType._WRITE) {
                    writeList.add(readData);
                } else {
                    readList.add(readData);
                }

                if ((writeValue != null) && (result[WRITE_INDEX] != null)) {
                    Object write = extractData(rset, 3, dataType, nullElementsWrite);
                    Array.set(writeValue, 0, write);
                    writeData.setValue(writeValue, nullElementsWrite);
                    writeList.add(writeData);
                }

            } // end while (rset.next())
            DbData.afterExtraction(readList, writeList, result);
        }
    }

    private Object extractData(ResultSet rset, int index, int dataType, boolean... nullElements) throws SQLException {
        Object value;
        if (dataType == TangoConst.Tango_DEV_STRING) {
            value = rset.getString(index);
            if (rset.wasNull()) {
                nullElements[0] = true;
            } else {
                value = StringFormater.formatStringToRead((String) value);
            }
        } else if (dataType == TangoConst.Tango_DEV_BOOLEAN) {
            value = rset.getBoolean(index);
            if (rset.wasNull()) {
                nullElements[0] = true;
            }
        } else {
            value = rset.getDouble(index);
            if (rset.wasNull()) {
                nullElements[0] = true;
                value = DbUtils.cast(dataType, MathConst.NAN_FOR_NULL);
            } else {
                value = DbUtils.cast(dataType, (double) value);
            }
        }
        return value;
    }

    public abstract DbData[] treatStatementResultForGetSpectData(ResultSet rset, final String attributeName,
                                                                 DbData... dbData) throws ArchivingException;

    public abstract DbData[] treatStatementResultForGetImageData(ResultSet rset, final String attributeName,
                                                                 DbData... dbData) throws ArchivingException;

    /**
     * @param attributeName
     * @param writable
     * @param cmd
     * @return
     * @throws ArchivingException
     */
    public double getAttScalarDataMinMaxAvg(final String attributeName, final int writable, final String cmd)
            throws ArchivingException {
        double min;
        if (connector == null) {
            min = MathConst.NAN_FOR_NULL;
        } else {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            String selectField;
            if (writable == AttrWriteType._READ || writable == AttrWriteType._WRITE) {
                selectField = ConfigConst.VALUE;
            } else { // if (writable == AttrWriteType._READ_WITH_WRITE || writable
                // ==
                // AttrWriteType._READ_WRITE)
                selectField = ConfigConst.READ_VALUE;
            }
            final String getAttributeDataQuery = new StringBuilder("SELECT ").append(cmd).append("(")
                    .append(selectField).append(") FROM ").append(connector.getSchema()).append(".")
                    .append(DbUtilsFactory.getInstance(connector).getTableName(attributeName.trim())).toString();

            try {
                conn = connector.getConnection();
                if (conn == null) {
                    min = MathConst.NAN_FOR_NULL;
                } else {
                    min = 0;
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(getAttributeDataQuery);
                    while (rset.next()) {
                        if (isCanceled()) {
                            break;
                        }
                        min = rset.getDouble(1);
                        if (rset.wasNull()) {
                            min = MathConst.NAN_FOR_NULL;
                        }
                    }
                }
            } catch (final SQLException e) {
                String message = "";
                if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                        || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.ADB_CONNECTION_FAILURE;
                } else {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.STATEMENT_FAILURE;
                }

                final String reason = GlobalConst.QUERY_FAILURE;
                final String desc = "Failed while executing GenericExtractorMethods.getAttDataMin() method...";
                throw new ArchivingException(message, reason, getAttributeDataQuery, ErrSeverity.WARN, desc,
                        this.getClass().getName(), e);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return min;
    }

    /*
     *
     */
    public double getAttScalarDataMinMaxAvgBetweenDates(final String attributeName, final String time0,
                                                        final String time1, final int writable, final String cmd) throws ArchivingException {
        double max;
        if (connector == null) {
            max = MathConst.NAN_FOR_NULL;
        } else {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            String selectField0;
            String selectField1;
            final IDbUtils utils = DbUtilsFactory.getInstance(connector);
            if (writable == AttrWriteType._READ || writable == AttrWriteType._WRITE) {
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.VALUE;
            } else {
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.READ_VALUE;
            }
            final String getAttributeDataQuery = new StringBuilder("SELECT ").append(cmd).append("(")
                    .append(selectField1).append(") FROM ").append(connector.getSchema()).append(".")
                    .append(utils.getTableName(attributeName)).append(" WHERE (").append(selectField0)
                    .append(" BETWEEN ").append(utils.toDbTimeString(time0.trim())).append(" AND ")
                    .append(utils.toDbTimeString(time1.trim())).append(")").toString();
            try {
                conn = connector.getConnection();
                if (conn == null) {
                    max = MathConst.NAN_FOR_NULL;
                } else {
                    max = 0;
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(getAttributeDataQuery);
                    while (rset.next()) {
                        if (isCanceled()) {
                            break;
                        }
                        max = rset.getDouble(1);
                        if (rset.wasNull()) {
                            max = MathConst.NAN_FOR_NULL;
                        }
                    }
                }
            } catch (final SQLException e) {
                String message = "";
                if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                        || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.ADB_CONNECTION_FAILURE;
                } else {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.STATEMENT_FAILURE;
                }

                final String reason = GlobalConst.QUERY_FAILURE;
                final String desc = "Failed while executing GenericExtractorMethods.getAttDataMaxBetweenDates() method...";
                throw new ArchivingException(message, reason, getAttributeDataQuery, ErrSeverity.WARN, desc,
                        this.getClass().getName(), e);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        // Returns the names list
        return max;
    }

    /*
     *
     */
    public void buildAttributeScalarTab(final String attributeID, final int dataType, final String[] tab)
            throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            Statement stmt = null;
            // Create and execute the SQL query string
            // Build the query string
            String type = "double";
            if (dataType == TangoConst.Tango_DEV_STRING || dataType == TangoConst.Tango_DEV_BOOLEAN) {
                type = "varchar(255)";
            }

            final StringBuilder query = new StringBuilder("CREATE TABLE `").append(attributeID).append("` (`")
                    .append(tab[0]).append("` ").append("datetime NOT NULL default '0000-00-00 00:00:00', ").append("`")
                    .append(tab[1]).append("` ").append(type).append(" default NULL ");
            if (tab.length == 3) {
                query.append(",`").append(tab[2]).append("` ").append(type).append(" default NULL)");
            } else {
                query.append(")");
            }
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    // dbConn.setLastStatement(stmt);
                    stmt.executeUpdate(query.toString().trim());
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query.toString());
            } finally {
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
    }

    /*
     *
     */
    public void buildAttributeSpectrumTab(final String attributeID, final String[] tab, final int dataType)
            throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            Statement stmt = null;
            // Create and execute the SQL query string
            // Build the query string
            final StringBuilder queryBuilder = new StringBuilder("CREATE TABLE `").append(attributeID).append("` (")
                    .append("`").append(tab[0]).append("` ").append("DATETIME NOT NULL default '0000-00-00 00:00:00', ")
                    .append("`").append(tab[1]).append("` ").append("SMALLINT NOT NULL, ").append("`").append(tab[2])
                    .append("` ").append("BLOB default NULL");
            if (tab.length == 4) {
                queryBuilder.append(",`").append(tab[3]).append("` ").append("BLOB default NULL");
            }
            queryBuilder.append(")");
            final String query = queryBuilder.toString().trim();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    stmt.executeUpdate(query);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
    }

    /**
     * <b>Description : </b> Creates the table of an image read/write type
     * attribute <I>(mySQL only)</I>.
     *
     * @param attributeID The ID of the associated attribute
     */
    public void buildAttributeImageTab(final String attributeID, final String[] tab, final int dataType)
            throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            Statement stmt = null;
            // Create and execute the SQL query string
            // Build the query string
            final StringBuilder queryBuilder = new StringBuilder("CREATE TABLE `").append(attributeID).append("` (")
                    .append("`").append(tab[0]).append("` ")
                    .append("DATETIME NOT NULL default '0000-00-00 00:00:00', `").append(tab[1])
                    .append("` SMALLINT NOT NULL, `").append(tab[2]).append("` SMALLINT NOT NULL, ").append("`")
                    .append(tab[3]).append("` ").append("LONGBLOB default NULL, ");
            if (tab.length == 5) {
                queryBuilder.append("`").append(tab[4]).append("` LONGBLOB default NULL)");
            }
            final String query = queryBuilder.toString().trim();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    stmt.executeUpdate(query);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
    }

}
