/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.ImageData;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.DataGetters;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.InfSupGetters;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.MinMaxAvgGetters;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates.DataGettersBetweenDates;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates.InfSupGettersBetweenDates;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates.MinMaxAvgGettersBetweenDates;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 */
public class AttributeExtractor extends DataExtractor {

    protected DataGetters dataGetters;
    protected DataGettersBetweenDates dataGettersBetweenDates;
    protected InfSupGetters infSupGetters;
    protected InfSupGettersBetweenDates infSupGettersBetweenDates;
    protected MinMaxAvgGetters minMaxAvgGetters;
    protected MinMaxAvgGettersBetweenDates minMaxAvgGettersBetweenDates;

    /**
     * @param con
     * @param ut
     * @param at
     */
    public AttributeExtractor(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description : </b> Retrieves timestamps associated with data records
     * data between two dates, for a given scalar attribute.
     *
     * @param argin
     *            The attribute's name, the beginning date (DD-MM-YYYY
     *            HH24:MI:SS.FF) and the ending date (DD-MM-YYYY HH24:MI:SS.FF).
     * @param samplingType
     * @param historic Whether database is historic
     * @return The list of timestamps for the specified attribute <br>
     * @throws ArchivingException
     */
    public Collection<ImageData> getAttPartialImageDataBetweenDates(final String[] argin,
            final SamplingType samplingType, final boolean historic) throws ArchivingException {
        final Collection<ImageData> ret;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if (connector == null || att == null || dbUtils == null) {
            ret = null;
        } else {
            final String attributeName = argin[0].trim();

            final String time0 = dbUtils.getTime(argin[1]);
            final String time1 = dbUtils.getTime(argin[2]);

            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            String query = "";
            final String tableName = connector.getSchema() + DB_SEPARATOR + dbUtils.getTableName(attributeName);

            if (samplingType.hasSampling()) {

                final String format = dbUtils.getFormat(samplingType);// isMySQL ?
                query = new StringBuilder("SELECT ").append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format))
                        .append(VALUE_SEPARATOR).append(SamplingType.AVERAGING_NORMALISATION).append(" ( ")
                        .append(ConfigConst.DIM_X).append(" ) , ").append(SamplingType.AVERAGING_NORMALISATION)
                        .append(" ( ").append(ConfigConst.DIM_Y).append(" ) FROM ").append(tableName).append(" WHERE ")
                        .append(ConfigConst.TIME).append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim()))
                        .append(" AND ").append(dbUtils.toDbTimeString(time1.trim())).append(" GROUP BY ")
                        .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format)).append(" ORDER BY ")
                        .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format)).toString();
            } else {
                query = new StringBuilder("SELECT ").append(dbUtils.toDbTimeFieldString(ConfigConst.TIME))
                        .append(VALUE_SEPARATOR).append(ConfigConst.DIM_X).append(VALUE_SEPARATOR)
                        .append(ConfigConst.DIM_Y).append(" FROM ").append(tableName).append(" WHERE (")
                        .append(ConfigConst.TIME).append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim()))
                        .append(" AND ").append(dbUtils.toDbTimeString(time1.trim())).append(")").toString();
            }

            try {
                ret = new ArrayList<ImageData>();
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;

                    rset = stmt.executeQuery(query);

                    while (rset.next()) {
                        if (isCanceled()) {
                            break;
                        }
                        final String dateString = rset.getString(1);
                        final Timestamp date = new Timestamp(DateUtil.stringToMilli(dateString));
                        final int dimX = rset.getInt(2);
                        final int dimY = rset.getInt(3);

                        final ImageData data = new ImageData(this);
                        data.setDate(date);
                        data.setDimX(dimX);
                        data.setDimY(dimY);
                        data.setName(attributeName);
                        data.setHistoric(historic);

                        ret.add(data);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                rset = null;
                ConnectionCommands.close(stmt);
                stmt = null;
                connector.closeConnection(conn);
            }
        }
        return ret;
    }

    public double[][] getAttImageDataForDate(final String attributeName, final String time) throws ArchivingException {
        double[][] dvalueArr = null;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if (connector != null && dbUtils != null) {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string

            final String query = new StringBuilder("SELECT ").append(ConfigConst.VALUE).append(" FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE (").append(ConfigConst.TIME).append(" = '").append(time.trim())
                    .append("') ORDER BY time").toString();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    rset = stmt.executeQuery(query);
                    if (rset.next()) {
                        // Value
                        String valueReadSt = null;
                        java.util.StringTokenizer stringTokenizerReadRows = null;
                        if (rset.getObject(1) != null) {
                            valueReadSt = rset.getString(1);
                            if (rset.wasNull()) {
                                valueReadSt = NULL;
                            }
                            stringTokenizerReadRows = new StringTokenizer(valueReadSt,
                                    GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                        }

                        int rowIndex = 0;
                        int numberOfRows = 0;
                        int numberOfCols = 0;

                        if (stringTokenizerReadRows != null) {
                            numberOfRows = stringTokenizerReadRows.countTokens();
                            dvalueArr = new double[numberOfRows][];

                            while (stringTokenizerReadRows.hasMoreTokens()) {
                                final String currentRowRead = stringTokenizerReadRows.nextToken();
                                if (currentRowRead == null || currentRowRead.trim().isEmpty()) {
                                    break;
                                }

                                final StringTokenizer stringTokenizerReadCols = new StringTokenizer(currentRowRead,
                                        GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                                numberOfCols = stringTokenizerReadCols.countTokens();
                                final double[] currentRow = new double[numberOfCols];
                                int colIndex = 0;

                                while (stringTokenizerReadCols.hasMoreTokens()) {
                                    String currentValRead = stringTokenizerReadCols.nextToken();
                                    if (currentValRead == null) {
                                        break;
                                    }
                                    currentValRead = currentValRead.trim();
                                    if (currentValRead.isEmpty()) {
                                        break;
                                    }

                                    currentRow[colIndex] = Double.parseDouble(currentValRead);
                                    colIndex++;
                                }

                                dvalueArr[rowIndex] = currentRow;
                                rowIndex++;
                            }
                        }
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                rset = null;
                ConnectionCommands.close(stmt);
                stmt = null;
                connector.closeConnection(conn);
            }
        }
        return dvalueArr;
    }

    public DataGetters getDataGetters() {
        return dataGetters;
    }

    public DataGettersBetweenDates getDataGettersBetweenDates() {
        return dataGettersBetweenDates;
    }

    public InfSupGetters getInfSupGetters() {
        return infSupGetters;
    }

    public InfSupGettersBetweenDates getInfSupGettersBetweenDates() {
        return infSupGettersBetweenDates;
    }

    public MinMaxAvgGetters getMinMaxAvgGetters() {
        return minMaxAvgGetters;
    }

    public MinMaxAvgGettersBetweenDates getMinMaxAvgGettersBetweenDates() {
        return minMaxAvgGettersBetweenDates;
    }
}
