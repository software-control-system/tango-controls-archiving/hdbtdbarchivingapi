/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api;

import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class DataBaseManagerFactory {

    private DataBaseManagerFactory() {
    }

    public static DataBaseManager getDataBaseManager(final boolean historic, final AbstractDataBaseConnector connector) {
        return historic ? new HDBDataBaseManager(connector) : new TDBDataBaseManager(connector);
    }

}
