package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.cache;

public class CacheAttribute {
	private int dataFormat = -1;
	private int dataType = -1;
	private int writable = -1;

	public int getDataFormat() {
		return dataFormat;
	}

	public void setDataFormat(int dataFormat) {
		this.dataFormat = dataFormat;
	}

	public int getDataType() {
		return dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	public int getWritable() {
		return writable;
	}

	public void setWritable(int writable) {
		this.writable = writable;
	}
}
