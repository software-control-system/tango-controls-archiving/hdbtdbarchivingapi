/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.manager;

import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public final class ArchivingManagerApiRefFactory {

    public static synchronized IArchivingManagerApiRef getInstance(final boolean historic,
            final AbstractDataBaseConnector connector) {
        IArchivingManagerApiRef instance;
        if (historic) {
            instance = new HdbArchivingManagerApiRef(connector);
        } else {
            instance = new TdbArchivingManagerApiRef(connector);
        }
        return instance;
    }

}
