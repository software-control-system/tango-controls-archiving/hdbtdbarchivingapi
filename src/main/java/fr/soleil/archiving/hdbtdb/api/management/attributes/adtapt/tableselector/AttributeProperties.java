/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 *
 */
public class AttributeProperties extends AttributesData implements DBExtractionConst {

    /**
     * @param m
     */
    public AttributeProperties(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description : </b> Returns an array containing the different
     * properties informations for the given attribute
     *
     * @param attributeName The attribute's name
     * @param caseSensitiveDb Whether database is case sensitive
     * @return An array containing the different properties informations for the given attribute
     * @throws ArchivingException
     */
    public Collection<String> getAttPropertiesData(final String attributeName, final boolean caseSensitiveDb)
            throws ArchivingException {
        final List<String> properties = new ArrayList<String>();
        boolean isArchived = false;
        if (connector != null) {
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            final StringBuilder builder = new StringBuilder("SELECT ").append(ConfigConst.APT).append(DB_SEPARATOR)
                    .append("* FROM ").append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.ADT)
                    .append(VALUE_SEPARATOR).append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.APT)
                    .append(" WHERE ((").append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.ID)
                    .append(" = ").append(ConfigConst.APT).append(DB_SEPARATOR).append(ConfigConst.ID).append(")")
                    .append(" AND (");
            if (caseSensitiveDb) {
                builder.append("LOWER(").append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.FULL_NAME)
                        .append(") = LOWER(?))) ");
            } else {
                builder.append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.FULL_NAME).append(" = ?)) ");
            }
            final String sqlStr = builder.toString();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(sqlStr);
                    // dbConn.setLastStatement(preparedStatement);
                    preparedStatement.setString(1, attributeName.trim());
                    rset = preparedStatement.executeQuery();
                    // Gets the result of the query
                    while (rset.next()) {
                        isArchived = true;
                        for (int i = 0; i < ConfigConst.TAB_PROP.length; i++) {
                            final String info = ConfigConst.TAB_PROP[i] + "::" + rset.getString(i + 1);
                            properties.add(info);
                        }
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, sqlStr);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
        if (isArchived) {
            // Returns the names list
            return properties;
        } else {
            throw new ArchivingException("Invalid attribute: " + attributeName, "Invalid attribute: " + attributeName,
                    ErrSeverity.WARN,
                    "No database connection or \"" + attributeName + "\" attribute not found in database",
                    this.getClass().getName());
        }
    }

    /**
     * This methods returns the display format for the attribute of the given name
     *
     * @param attributeName the attribute name
     * @param caseSensitiveDb Whether database is case sensitive
     * @return the display format for the attribute of the given name
     * @throws ArchivingException
     */
    public String getDisplayFormat(final String attributeName, final boolean caseSensitiveDb)
            throws ArchivingException {
        String displayFormat;
        if (connector == null) {
            displayFormat = null;
        } else {
            displayFormat = "";
            ResultSet rset = null;
            Connection conn = null;
            PreparedStatement getAttID = null;
            // My statement
            final StringBuilder builder = new StringBuilder("SELECT ").append(ConfigConst.FORMAT).append(" FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.ADT).append(VALUE_SEPARATOR)
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.APT).append(" WHERE ");
            if (caseSensitiveDb) {
                builder.append("LOWER(").append(ConfigConst.FULL_NAME).append(") = LOWER(?)");
            } else {
                builder.append(ConfigConst.FULL_NAME).append(" = ?");
            }
            builder.append(" AND ").append(ConfigConst.APT).append(DB_SEPARATOR).append(ConfigConst.ID).append("=")
                    .append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.ID);
            final String selectString = builder.toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    getAttID = conn.prepareStatement(selectString);
                    // dbConn.setLastStatement(ps_get_att_id);
                    final String field1 = attributeName.trim();
                    getAttID.setString(1, field1);
                    rset = getAttID.executeQuery();
                    // Gets the result of the query
                    if (rset.next()) {
                        displayFormat = rset.getString(1);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, selectString);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(getAttID);
                connector.closeConnection(conn);
            }
            // Returns the total number of signals defined in HDB
        }
        return displayFormat;
    }
}
