//+======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/LoadBalancedArchiver.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  LoadBalancedArchiver.
//						(Chinkumo Jean) - Jun 6, 2004
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: LoadBalancedArchiver.java,v $
// Revision 1.4  2006/04/11 09:10:45  ounsy
// double archiving protection
//
// Revision 1.3  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.2.12.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.2  2005/06/14 10:12:11  chinkumo
// Branch (tangORBarchiving_1_0_1-branch_0)  and HEAD merged.
//
// Revision 1.1.4.1  2005/06/13 15:06:51  chinkumo
// Methods were commented.
//
// Revision 1.1  2005/03/07 18:19:39  chinkumo
// This class was added to manage the load balancing when distributing attributes on archivers
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.archiving.hdbtdb.api.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LoadBalancedArchiver {
    private int scalarLoad;
    private int spectrumLoad;
    private int imageLoad;
	private List<String> assignedAttributes;

    /**
     * Initialize an empty LoadBalancedArchiver object
     */
    public LoadBalancedArchiver() {
        scalarLoad = 0;
        spectrumLoad = 0;
        imageLoad = 0;
		assignedAttributes = Collections.synchronizedList(new ArrayList<String>());
    }

    /**
     * Initialize a LoadBalancedArchiver object with the given load informations
     * 
     * @param scalarLoad
     *            scalar attribute load
     * @param spectrumLoad
     *            spectrum attribute load
     * @param imageLoad
     *            image attribute load
     */
    public LoadBalancedArchiver(int scalarLoad, int spectrumLoad, int imageLoad) {
        this.scalarLoad = scalarLoad;
        this.spectrumLoad = spectrumLoad;
        this.imageLoad = imageLoad;
		assignedAttributes = Collections.synchronizedList(new ArrayList<String>());
    }

    /**
     * Return the image load
     * 
     * @return the image load
     */
    public int get_imageLoad() {
        return imageLoad;
    }

    /**
     * Set the image load
     * 
     * @param imageLoad
     *            the image load
     */
    public void set_imageLoad(int imageLoad) {
        this.imageLoad = imageLoad;
    }

    /**
     * Return the scalar load
     * 
     * @return
     */
    public int get_scalarLoad() {
        return scalarLoad;
    }

    /**
     * Set the scalar load
     * 
     * @param scalarLoad
     *            the scalar load
     */
    public void set_scalarLoad(int scalarLoad) {
        this.scalarLoad = scalarLoad;
    }

    /**
     * Return the spectrum load
     * 
     * @return
     */
    public int get_spectrumLoad() {
        return spectrumLoad;
    }

    /**
     * Set the spectrum load
     * 
     * @param spectrumLoad
     *            the spectrum load
     */
    public void set_spectrumLoad(int spectrumLoad) {
        this.spectrumLoad = spectrumLoad;
    }

    /**
     * Assign the given scalar attribute to the LoadBalancedArchiver
     * 
     * @param att
     *            the given scalar attribute
     */
    public void addScAttribute(String att) {
        String toAdd = new String(att);
        if (!assignedAttributes.contains(toAdd)) {
            assignedAttributes.add(toAdd);
            scalarLoad++;
        }
    }

    /**
     * Assign the given spectrum attribute to the LoadBalancedArchiver
     * 
     * @param att
     *            the given spectrum attribute
     */
    public void addSpAttribute(String att) {
        String toAdd = new String(att);
        if (!assignedAttributes.contains(toAdd)) {
            assignedAttributes.add(toAdd);
            spectrumLoad++;
        }
    }

    /**
     * Assign the given image attribute to the LoadBalancedArchiver
     * 
     * @param att
     *            the given image attribute
     */
    public void addImAttribute(String att) {
        String toAdd = new String(att);
        if (!assignedAttributes.contains(toAdd)) {
            assignedAttributes.add(toAdd);
            imageLoad++;
        }
    }

    /**
     * Return the assigned attribute list
     * 
     * @return the assigned attribute list
     */
    public String[] getAssignedAttributes() {
        return assignedAttributes.toArray(new String[assignedAttributes.size()]);
    }

    @Override
    public String toString() {
        StringBuilder archiverLoadStr = new StringBuilder();
        archiverLoadStr.append("Scalar Load   = ").append(scalarLoad).append("\r\n");
        archiverLoadStr.append("Spectrum Load = ").append(spectrumLoad + "\r\n");
        archiverLoadStr.append("Image Load    = ").append(imageLoad + "\r\n");
        return archiverLoadStr.toString();
    }
}
