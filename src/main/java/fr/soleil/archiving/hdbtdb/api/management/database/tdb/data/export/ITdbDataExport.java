/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.database.tdb.data.export;

import fr.soleil.archiving.common.api.exception.ArchivingException;

/**
 * @author AYADI
 * 
 */
public interface ITdbDataExport {
    void exportToDB_Data(String remoteDir, String fileName, String tableName, int writable) throws ArchivingException;

    void ExportData2Tdb(String attributeName, String endDate) throws ArchivingException;

    void forceDatabaseToImportFile(String tableName) throws ArchivingException;
}
