package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class AttributeNames extends AttributesData {

    private static final String SELECT = "SELECT ";
    private static final String SELECT_DISTINCT = "SELECT DISTINCT ";
    private static final String SELECT_COUNT_FROM = "SELECT count(*) FROM ";
    private static final String SELECT_COUNT_DISTINCT = "SELECT COUNT(DISTINCT ";
    private static final String FROM = " FROM ";
    private static final String COND_FROM = ") FROM ";
    private static final String WHERE = " WHERE ";
    private static final String WHERE_COND = " WHERE (";
    private static final String POINT = ".";
    private static final String EQUALS = " = ";
    private static final String EQUALS_ANY = " = ?";
    private static final String AND = " AND ";
    private static final String LIKE_ANY = " LIKE ?)";

    public AttributeNames(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description : </b> Gets all the attributes's complete names for a given domain,
     * family, member<br>
     *
     * @param domain The given domain used to retrieve the names
     * @param family The given family used to retrieve the names
     * @param member The given member used to retrieve the names
     * @return array of name strings
     * @throws ArchivingException
     */
    public String[] getAttributes(final String domain, final String family, final String member)
            throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = new String[0];
        } else {
            final String sqlStr = new StringBuilder(SELECT).append(ConfigConst.FULL_NAME)
            		.append(FROM)
					.append(connector.getSchema()).append(POINT).append(ConfigConst.ADT)
					.append(WHERE).append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domain))
					.append(AND).append(compareLikeCaseInsensitive(ConfigConst.FAMILY, family))
					.append(AND).append(compareLikeCaseInsensitive(ConfigConst.MEMBER, member))
					.toString();
            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    /**
     * <b>Description : </b> Gets all the attributes's names for a given domain,
     * family, member<br>
     *
     * @param domain The given domain used to retrieve the names
     * @param family The given family used to retrieve the names
     * @param member The given member used to retrieve the names
     * @return array of name strings
     * @throws ArchivingException
     */
    public Collection<String> getAttributesSimpleNames(String domain, String family, String member)
            throws ArchivingException {
        Collection<String> result;
        if (connector == null) {
            result = null;
        } else {
            final String sqlStr = new StringBuilder(SELECT).append(ConfigConst.ATT_NAME).append(FROM)
					.append(connector.getSchema()).append(POINT).append(ConfigConst.ADT)
					.append(WHERE).append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domain)).append(AND)
					.append(compareLikeCaseInsensitive(ConfigConst.FAMILY, family)).append(AND)
					.append(compareLikeCaseInsensitive(ConfigConst.MEMBER, member))
					.toString();
            result = getStringVectorAttributeData(sqlStr);
        }
        return result;
    }

    /**
     * <b>Description : </b> Gets the list of attributes registered in
     * <I>HDB</I>
     *
     * @return an array of String containing the attributes names <br>
     * @throws ArchivingException
     */
    public Collection<String> getAttributes() throws ArchivingException {
        Collection<String> result;
        if (connector == null) {
            result = null;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder(SELECT).append(ConfigConst.FULL_NAME).append(FROM)
					.append(connector.getSchema()).append(POINT).append(ConfigConst.ADT).toString();
            // Returns the names list
            result = getStringVectorAttributeData(sqlStr);
        }
        return result;
    }

    public String[] getAttributesCompleteNames() throws ArchivingException {
        String[] result;
        Collection<String> completeNames = getAttributes();
        if (completeNames == null) {
            result = new String[0];
        } else {
            result = DbUtils.toStringArray(completeNames);
        }
        return result;
    }

    /**
     * <b>Description : </b> Gets the list of attributes registered in the
     * historical (resp. temporary) database, that belong to the specified
     * facility.
     *
     * @return an array of String containing the attributes names <br>
     * @throws ArchivingException
     */
    public Collection<String> getAttributes(final String facility) throws ArchivingException {
        final List<String> nameList;
        if (connector == null) {
            nameList = null;
        } else {
            nameList = new ArrayList<String>();
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            ResultSet rset = null;

            // Create and execute the SQL query string
			final String getAttributeDataQuery = new StringBuilder(SELECT_DISTINCT).append(ConfigConst.ADT)
                    .append(POINT).append(ConfigConst.FULL_NAME).append(FROM).append(connector.getSchema())
					.append(POINT).append(ConfigConst.ADT).append(WHERE_COND).append(ConfigConst.ADT)
					.append(POINT).append(ConfigConst.FACILITY).append(LIKE_ANY).toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(getAttributeDataQuery);
                    // dbConn.setLastStatement(preparedStatement);
                    preparedStatement.setString(1, facility.trim());
                    rset = preparedStatement.executeQuery();
                    // Gets the result of the query
                    while (rset.next()) {
                        nameList.add(rset.getString(1));
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getAttributeDataQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
        return nameList;
    }

    /**
     * <b>Description : </b> Returns the number of attributes defined in
     * <I>HDB</I>
     *
     * @return The total number of attributes defined in <I>HDB</I>
     * @throws ArchivingException
     */
    public int getAttributesCount() throws ArchivingException {
        int count;
        if (connector == null) {
            count = 0;
        } else {
            // Build the query string
            final String getAttributeCountQuery = new StringBuilder(SELECT_COUNT_FROM).append(connector.getSchema())
					.append(POINT).append(ConfigConst.ADT).toString();
            // Returns the total number of signals defined in HDB
            count = getIntAttributeData(getAttributeCountQuery);
        }
        return count;
    }

    public String[] getAttributesByCriterion(final String domain, final String family, final String member,
            final String attRegexp) throws ArchivingException {
        String[] result;
		if (connector == null || attRegexp == null) {
            result = null;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder(SELECT).append(ConfigConst.FULL_NAME).append(FROM)
					.append(connector.getSchema()).append(POINT).append(ConfigConst.ADT)
					.append(WHERE).append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domain)).append(AND)
					.append(compareLikeCaseInsensitive(ConfigConst.FAMILY, family)).append(AND)
					.append(compareLikeCaseInsensitive(ConfigConst.MEMBER, member))
					.append(AND).append(compareLikeCaseInsensitive(ConfigConst.ATT_NAME, attRegexp))
					.toString();
            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    /**
     * <b>Description : </b> Returns the number of registered the attributes for
     * a given domain, family, member<br>
     *
     * @param domain The given domain used to retrieve the names
     * @param family The given family used to retrieve the names
     * @param member The given member used to retrieve the names
     * @return array of name strings
     * @throws ArchivingException
     */
    public int getAttributesCount(final String domain, final String family, final String member)
            throws ArchivingException {
        int count;
        if (connector == null) {
            count = 0;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder(SELECT_COUNT_DISTINCT).append(ConfigConst.FULL_NAME)
					.append(COND_FROM).append(connector.getSchema()).append(POINT).append(ConfigConst.ADT)
					.append(WHERE).append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domain)).append(AND)
					.append(compareLikeCaseInsensitive(ConfigConst.FAMILY, family)).append(AND)
					.append(compareLikeCaseInsensitive(ConfigConst.MEMBER, member))
                    .toString();
            count = getIntAttributeData(sqlStr);
        }
        return count;
    }

    /**
     * <b>Description : </b> Gets the total number of attributes defined in
     * <I>HDB</I> with the given type (0: DevShort - 1: DevLong - 2: DevDouble -
     * 3: DevString) <br>
     *
     * @param dataType
     * @return The total number of attributes for the specified type
     * @throws ArchivingException
     */
    public int getAttributesCountT(final int dataType) throws ArchivingException {
        int count;
        if (connector == null) {
            count = 0;
        } else {
            final String getAttributeCountQuery = new StringBuilder(SELECT_COUNT_FROM).append(connector.getSchema())
					.append(POINT).append(ConfigConst.ADT).append(WHERE).append(ConfigConst.DATA_TYPE).append("=")
                    .append(dataType).toString();
            // Execute the SQL query string
            // Returns the total number of signals defined in HDB
            count = getIntAttributeData(getAttributeCountQuery);
        }
        return count;
    }

    /**
     * <b>Description : </b> Gets the total number of attributes defined in
     * <I>HDB</I> with the given format (0 -> scalar | 1 -> spectrum | 2 ->
     * image) <br>
     *
     * @param dataFormat
     * @return The total number of attributes for the specified type
     * @throws ArchivingException
     */
    public int getAttributesCountF(final int dataFormat) throws ArchivingException {
        int count;
        if (connector == null) {
            count = 0;
        } else {
            // Build the query string
            final String getAttributeCountQuery = new StringBuilder(SELECT_COUNT_FROM).append(connector.getSchema())
					.append(POINT).append(ConfigConst.ADT).append(WHERE).append(ConfigConst.DATA_FORMAT).append("=")
                    .append(dataFormat).toString();
            // Returns the total number of signals defined in HDB
            count = getIntAttributeData(getAttributeCountQuery);
        }
        return count;
    }

    /**
     * <b>Description : </b> Gets the list of <I>HDB</I> registered attributes
     * for the given format (0 -> scalar | 1 -> spectrum | 2 -> image)
     *
     * @param connector The {@link AbstractDataBaseConnector} that knows how to connect to the database
     * @return An array containing the attributes names <br>
     * @throws ArchivingException
     */
    public Collection<String> getAttributesNamesF(final int dataFormat) throws ArchivingException {
        Collection<String> result;
        if (connector == null) {
            result = null;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder(SELECT).append(ConfigConst.FULL_NAME).append(FROM)
					.append(connector.getSchema()).append(POINT).append(ConfigConst.ADT).append(WHERE)
                    .append(ConfigConst.DATA_FORMAT).append(EQUALS).append(dataFormat).toString();
            // Returns the names list
            result = getStringVectorAttributeData(sqlStr);
        }
        return result;
    }

    /**
     * <b>Description : </b> Gets the list of registered <I>HDB</I> attributes
     * for the given type (2 -> Tango::DevShort | 3 -> Tango::DevLong | 5 ->
     * Tango::DevDouble and 8 -> Tango::DevString)
     *
     * @return An array containing the attributes names <br>
     * @throws ArchivingException
     */
    public Collection<String> getAttributesNamesT(final int dataType) throws ArchivingException {
        Collection<String> result;
        if (connector == null) {
            result = null;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder(SELECT_DISTINCT).append(ConfigConst.FULL_NAME).append(FROM)
					.append(connector.getSchema()).append(POINT).append(ConfigConst.ADT).append(WHERE)
                    .append(ConfigConst.DATA_TYPE).append(EQUALS).append(dataType).toString();
            // Returns the name list
            result = getStringVectorAttributeData(sqlStr);
        }
        return result;
    }

    /**
     * <b>Description : </b> Gets for a specified attribute its full as defined
     * in HDB
     *
     * @param attributeID The attribute's ID
     * @return The <I>HDB</I>'s full name that characterizes the given attribute id
     * @throws ArchivingException
     */
    public String getAttFullName(final int attributeID) throws ArchivingException {
        String attributesFullName = "";
        if (connector != null) {
            ResultSet rset = null;
            Connection conn = null;
            PreparedStatement getAttFullName = null;
            // My statement
            final String selectString = new StringBuilder(SELECT).append(ConfigConst.FULL_NAME).append(FROM)
					.append(connector.getSchema()).append(POINT).append(ConfigConst.ADT).append(WHERE)
                    .append(ConfigConst.ID).append(EQUALS_ANY).toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    getAttFullName = conn.prepareStatement(selectString);
                    // dbConn.setLastStatement(ps_get_att_full_name);
                    final int field1 = attributeID;
                    getAttFullName.setInt(1, field1);
                    rset = getAttFullName.executeQuery();
                    // Gets the result of the query
                    if (rset.next()) {
                        attributesFullName = rset.getString(1);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, selectString);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(getAttFullName);
                connector.closeConnection(conn);
            }
            // Returns the total number of signals defined in HDB
        }
        return attributesFullName;
    }
}
