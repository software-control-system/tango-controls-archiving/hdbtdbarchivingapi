package fr.soleil.archiving.hdbtdb.api;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert.HdbAttributeInsertFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert.IHdbAttributeInsert;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class HDBDataBaseManager extends DataBaseManager {
    private static IHdbAttributeInsert attributeInsert;
    private static final Object LOCK = new Object();

    public HDBDataBaseManager(final AbstractDataBaseConnector connector) {
        super(connector, true);
    }

    public IHdbAttributeInsert getHdbAttributeInsert() throws ArchivingException {
        if (attributeInsert == null) {
            synchronized (LOCK) {
                if (attributeInsert == null) {
                    attributeInsert = HdbAttributeInsertFactory.getInstance(connector, getAttribute());
                }
            }
        }
        return attributeInsert;
    }

}
