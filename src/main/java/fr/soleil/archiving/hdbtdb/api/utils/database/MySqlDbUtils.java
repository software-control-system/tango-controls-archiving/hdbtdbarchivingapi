/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.utils.database;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class MySqlDbUtils extends DbUtils {

    public MySqlDbUtils(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    public String getFormat(final SamplingType samplingType) {
        return samplingType.getMySqlFormat();
    }

    @Override
    protected String getRequest() {
        return "SELECT SYSDATE()";
    }

    @Override
    public String toDbTimeFieldString(final String timeField) {
        return " `" + timeField + "`";
    }

    @Override
    public String toDbTimeFieldString(final String timeField, final String format) {
        return "DATE_FORMAT(" + timeField + " ,'" + format + "')";
    }

    @Override
    public String toDbTimeString(final String timeField) {
        return " \"" + timeField + "\"";
    }

    @Override
    public String getTableName(final int index) {
        String tableName = ConfigConst.TAB_PREF;
        if (index < 10) {
            tableName = tableName + "0000" + index;
        } else if (index < 100) {
            tableName = tableName + "000" + index;
        } else if (index < 1000) {
            tableName = tableName + "00" + index;
        } else { // if (index < 10000) {
            tableName = tableName + "0" + index;
        }

        return tableName;
    }

    @Override
    protected String getFormattedTimeField(final String maxOrMin) {
        return maxOrMin + "(" + ConfigConst.TIME + ")";
    }

    @Override
    public String getTime(final String string) {
        return string;
    }

    @Override
    public String[] getListOfPartitions() throws ArchivingException {
        return new String[0];
    }

}
