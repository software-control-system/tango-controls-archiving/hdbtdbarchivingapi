//+======================================================================
// $Source:  $
//
// Project:      Tango Archiving Service
//
// Description:  (Claisse Laurent) - 29 ao�t 2006
//
// $Author:  $
//
// $Revision: $
//
// $Log: $
//
//
// copyleft :	Synchrotron SOLEIL
//				L'Orme des Merisiers
//				Saint-Aubin - BP 48
//				91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.archiving.hdbtdb.api.tools;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import fr.esrf.TangoDs.TangoConst;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.lib.project.ObjectUtils;

/**
 * 
 * @author PIERREJOSEPH
 * 
 */
public final class SamplingType implements Serializable, DBExtractionConst {

    private static final long serialVersionUID = 5751834279000910923L;

    private short type;
    private String oracleFormat;
    private String mySqlFormat;
    private String label;
    private boolean hasAdditionalFiltering;
    private short additionalFilteringFactor;

    public static final short MONTH = 5;
    public static final short DAY = 4;
    public static final short HOUR = 3;
    public static final short MINUTE = 2;
    public static final short SECOND = 1;
    public static final short ALL = -1;

    public static final String EVERY_DAY_LABEL = "DAY";
    public static final String EVERY_HOUR_LABEL = "HOUR";
    public static final String EVERY_MINUTE_LABEL = "MINUTE";
    public static final String EVERY_SECOND_LABEL = "SECOND";
    public static final String NO_SAMPLING_LABEL = "ALL";

    public static final String AVERAGING_NORMALISATION = "AVG";
    public static final String MINIMUM_NORMALISATION = "MIN";

    private SamplingType(final short type, final String oracleFormat, final String mySqlFormat, final String label) {
        this.type = type;
        this.oracleFormat = oracleFormat;
        this.mySqlFormat = mySqlFormat;
        this.label = label;
    }

    /**
     * @return Returns the format.
     */
    public String getOracleFormat() {
        return this.oracleFormat;
    }

    /**
     * @param format
     *            The format to set.
     */
    public void setOracleFormat(final String format) {
        this.oracleFormat = format;
    }

    /**
     * @return Returns the label.
     */
    public String getLabel() {
        switch (type) {
            case SamplingType.SECOND:
                label = "Seconds";
                break;
            case SamplingType.MINUTE:
                label = "Minutes";
                break;
            case SamplingType.HOUR:
                label = "Hours";
                break;
            case SamplingType.DAY:
                label = "Days";
                break;
            case SamplingType.MONTH:
                label = "Months";
                break;
            default:
                label = "None";
                break;
        }
        return this.label;
    }

    /**
     * @return Returns the type.
     */
    public short getType() {
        return this.type;
    }

    /**
     * @param type
     *            The type to set.
     */
    public void setType(final short type) {
        this.type = type;
    }

    public static SamplingType getSamplingType(final short samplingType) {
        String oracleFormat;
        String mySqlFormat;
        String label;

        switch (samplingType) {
            case SamplingType.SECOND:
                oracleFormat = "YYYY-MM-DD HH24:MI:SS";
                mySqlFormat = "%Y-%m-%d %H:%i:%s";
                label = "Seconds";
                break;

            /*
             * case SamplingType.EVERY_TEN_SECONDS: format = "YYYY-MM-DD HH24:MI:S";
             * label = "Every ten seconds"; break;
             */

            case SamplingType.MINUTE:
                oracleFormat = "YYYY-MM-DD HH24:MI";
                mySqlFormat = "%Y-%m-%d %H:%i";
                label = "Minutes";
                break;

            /*
             * case SamplingType.EVERY_TEN_MINUTES: format = "YYYY-MM-DD HH24:M";
             * break;
             */

            case SamplingType.HOUR:
                oracleFormat = "YYYY-MM-DD HH24";
                mySqlFormat = "%Y-%m-%d %H";
                label = "Hours";
                break;

            case SamplingType.DAY:
                oracleFormat = "YYYY-MM-DD";
                mySqlFormat = "%Y-%m-%d";
                label = "Days";
                break;

            case SamplingType.MONTH:
                oracleFormat = "YYYY-MM";
                mySqlFormat = "%Y-%m";
                label = "Months";
                break;

            default:
                oracleFormat = "YYYY-MM-DD HH24:MI:SS.FF";
                mySqlFormat = "%Y-%m-%d %H:%i:%s.%f";
                label = "None";
                break;
        }

        return new SamplingType(samplingType, oracleFormat, mySqlFormat, label);
    }

    public static SamplingType[] getChoices() {
        // Basic types
        final SamplingType defaultSampling = SamplingType.getSamplingType(SamplingType.ALL);
        final SamplingType everySecondSampling = SamplingType.getSamplingType(SamplingType.SECOND);
        final SamplingType everyMinuteSampling = SamplingType.getSamplingType(SamplingType.MINUTE);
        final SamplingType everyHourSampling = SamplingType.getSamplingType(SamplingType.HOUR);
        final SamplingType everyDaySampling = SamplingType.getSamplingType(SamplingType.DAY);

        // Additionnal types
        /*
         * SamplingType everyFiveMinutesSampling = SamplingType.getSamplingType
         * ( SamplingType.MINUTE );
         * everyFiveMinutesSampling.setHasAdditionalFiltering ( true );
         * everyFiveMinutesSampling.setAdditionalFilteringFactor ( (short) 5 );
         * everyFiveMinutesSampling.setLabel ( "5 Minutes" );
         */

        final SamplingType[] ret = new SamplingType[5];
        ret[0] = defaultSampling;
        ret[1] = everySecondSampling;
        ret[2] = everyMinuteSampling;
        ret[3] = everyHourSampling;
        ret[4] = everyDaySampling;

        return ret;
    }

    @Override
    public String toString() {
        return String.valueOf(this.getType());
    }

    public String getDescription() {
        final String CRLF = System.getProperty("line.separator");

        String ret = "getType: " + this.getType();
        ret += CRLF;
        ret += "hasSampling: " + this.hasSampling();
        ret += CRLF;
        ret += "getLabel: " + this.getLabel();
        ret += CRLF;
        ret += "hasAdditionalFiltering: " + this.hasAdditionalFiltering();
        ret += CRLF;
        ret += "getAdditionalFilteringFactor: " + this.getAdditionalFilteringFactor();

        return ret;
    }

    public static SamplingType fromString(final String samplingType_s) {
        short samplingType;
        try {
            samplingType = Short.parseShort(samplingType_s);
        } catch (final Exception e) {
            samplingType = SamplingType.ALL;
        }

        return SamplingType.getSamplingType(samplingType);
    }

    public boolean hasSampling() {
        return this.getType() != SamplingType.ALL;
    }

    /**
     * @return Returns the mySqlFormat.
     */
    public String getMySqlFormat() {
        return this.mySqlFormat;
    }

    /**
     * @param mySqlFormat
     *            The mySqlFormat to set.
     */
    public void setMySqlFormat(final String mySqlFormat) {
        this.mySqlFormat = mySqlFormat;
    }

    public static String getGroupingNormalisationType(final int dataType) {
        String result;
        switch (dataType) {
            case TangoConst.Tango_DEV_STRING:
            case TangoConst.Tango_DEV_BOOLEAN:
                result = MINIMUM_NORMALISATION;
                break;
            default:
                result = AVERAGING_NORMALISATION;
                break;
        }
        return result;
    }

    public static SamplingType getSamplingTypeByLabel(final String typeName) throws IllegalArgumentException {
        boolean invalidType = false;
        short typeId = ALL;

        if (typeName == null) {
            invalidType = true;
        } else if (typeName.equals(EVERY_DAY_LABEL)) {
            typeId = DAY;
        } else if (typeName.equals(EVERY_HOUR_LABEL)) {
            typeId = HOUR;
        } else if (typeName.equals(EVERY_MINUTE_LABEL)) {
            typeId = MINUTE;
        } else if (typeName.equals(EVERY_SECOND_LABEL)) {
            typeId = SECOND;
        } else if (typeName.equals(NO_SAMPLING_LABEL)) {
            typeId = ALL;
        } else {
            invalidType = true;
        }

        if (invalidType) {
            throw new IllegalArgumentException(
                    "SamplingType.getSamplingTypeByLabel ( " + typeName + " ): Invalid typeName.");
        }

        return SamplingType.getSamplingType(typeId);
    }

    /**
     * @return Returns a clone of this SamplingType.
     */
    public SamplingType cloneSamplingType() {
        final SamplingType newSamplingType = SamplingType.getSamplingType(this.getType());

        newSamplingType.setHasAdditionalFiltering(this.hasAdditionalFiltering);
        if (this.hasAdditionalFiltering) {
            newSamplingType.setAdditionalFilteringFactor(this.additionalFilteringFactor);
        }

        return newSamplingType;
    }

    /**
     * @return Returns the additionalFilteringFactor.
     */
    public short getAdditionalFilteringFactor() {
        return this.additionalFilteringFactor;
    }

    /**
     * @param additionalFilteringFactor
     *            The additionalFilteringFactor to set.
     */
    public void setAdditionalFilteringFactor(final short additionalFilteringFactor) {
        this.additionalFilteringFactor = additionalFilteringFactor;
        switch (type) {
            case SamplingType.SECOND:
                while (this.additionalFilteringFactor >= 60) {
                    type = SamplingType.MINUTE;
                    this.additionalFilteringFactor = (short) TimeUnit.SECONDS.toMinutes(additionalFilteringFactor);
                    if (this.additionalFilteringFactor >= 60) {
                        setAdditionalFilteringFactor(this.additionalFilteringFactor);
                    }
                }
                break;
            case SamplingType.MINUTE:
                while (this.additionalFilteringFactor >= 60) {
                    type = SamplingType.HOUR;
                    this.additionalFilteringFactor = (short) TimeUnit.MINUTES.toHours(additionalFilteringFactor);
                    if (this.additionalFilteringFactor >= 60) {
                        setAdditionalFilteringFactor(this.additionalFilteringFactor);
                    }
                }
                break;
            case SamplingType.HOUR:
                while (this.additionalFilteringFactor >= 24) {
                    type = SamplingType.DAY;
                    this.additionalFilteringFactor = (short) TimeUnit.HOURS.toDays(additionalFilteringFactor);
                    if (this.additionalFilteringFactor >= 24) {
                        setAdditionalFilteringFactor(this.additionalFilteringFactor);
                    }
                }
                break;
            case SamplingType.DAY:
                type = SamplingType.DAY;
                this.additionalFilteringFactor = additionalFilteringFactor;
                break;
            default:
                type = SamplingType.ALL;
                this.additionalFilteringFactor = additionalFilteringFactor;
                break;
        }
    }

    /**
     * @return Returns the hasAdditionalFiltering.
     */
    public boolean hasAdditionalFiltering() {
        return this.hasAdditionalFiltering;
    }

    /**
     * @param hasAdditionalFiltering
     *            The hasAdditionalFiltering to set.
     */
    public void setHasAdditionalFiltering(final boolean hasAdditionalFiltering) {
        this.hasAdditionalFiltering = hasAdditionalFiltering;
    }

    public boolean isSameTypeAs(final SamplingType another) {
        final boolean ret = this.getType() == another.getType();
        /*
         * ret = ret && ( this.hasAdditionalFiltering () ==
         * another.hasAdditionalFiltering () ); ret = ret && (
         * this.getAdditionalFilteringFactor () ==
         * another.getAdditionalFilteringFactor () );
         */
        return ret;
    }

    public String getOneLevelHigherFormat(final boolean b) {
        final short oneLevelHigherType = (short) (this.getType() + 1);
        final SamplingType oneLevelHigherSamplingType = SamplingType.getSamplingType(oneLevelHigherType);
        return oneLevelHigherSamplingType.getFormat(b);
    }

    public String getAdditionalFilteringClause(final boolean isMySQL, final String field) {
        if (!this.hasAdditionalFiltering) {
            return ObjectUtils.EMPTY_STRING;
        }
        if (!this.hasSampling()) {
            return ObjectUtils.EMPTY_STRING;
        }

        if (isMySQL) {
            String typeOfAdditionalFiltering;
            switch (this.type) {
                case SamplingType.SECOND:
                    typeOfAdditionalFiltering = "'%s'";
                    break;

                case SamplingType.MINUTE:
                    typeOfAdditionalFiltering = "'%i'";
                    break;

                case SamplingType.HOUR:
                    typeOfAdditionalFiltering = "'%H'";
                    break;

                case SamplingType.DAY:
                    typeOfAdditionalFiltering = "'%d'";
                    break;

                default:
                    return ObjectUtils.EMPTY_STRING;
            }
            final String to_char = "DATE_FORMAT(" + field + VALUE_SEPARATOR + typeOfAdditionalFiltering + ")";
            final String to_number = to_char;
            return ", FLOOR(" + to_number + "/" + this.additionalFilteringFactor + ")";
        } else {
            String typeOfAdditionalFiltering;
            switch (this.type) {
                case SamplingType.SECOND:
                    typeOfAdditionalFiltering = "'SS'";
                    break;

                case SamplingType.MINUTE:
                    typeOfAdditionalFiltering = "'MI'";
                    break;

                case SamplingType.HOUR:
                    typeOfAdditionalFiltering = "'HH24'";
                    break;

                case SamplingType.DAY:
                    typeOfAdditionalFiltering = "'DD'";
                    break;

                default:
                    return ObjectUtils.EMPTY_STRING;
            }

            final String toChar = "to_char(" + field + VALUE_SEPARATOR + typeOfAdditionalFiltering + ")";
            final String toNumber = "to_number(" + toChar + ")";
            return ", FLOOR(" + toNumber + "/" + this.additionalFilteringFactor + ")";
        }
    }

    public String getFormat(final boolean isMySQL) {
        if (isMySQL) {
            return this.mySqlFormat;
        } else {
            return this.oracleFormat;
        }
    }

}
