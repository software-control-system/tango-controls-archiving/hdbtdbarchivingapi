// +============================================================================
// $Source:
// /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Mode/Mode.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: A Mode is an object that describes the way an attribute is
// archived.
// Let us note that a 'Mode' is a combination 'modes'.
// A 'mode' can be periodic, absolute, relative, on calculation, on difference
// or external.
//
// $Author: pierrejoseph $
//
// $Revision: 1.5 $
//
// $Log: Mode.java,v $
// Revision 1.5 2007/02/16 08:26:29 pierrejoseph
// The HDB minimal period is 10 seconds (instead of 1s).
//
// Revision 1.4 2006/10/30 14:36:07 ounsy
// added a toStringWatcher method used by the ArchivingWatcher's
// getAllArchivingAttributes command
//
// Revision 1.3 2005/11/29 17:11:17 chinkumo
// no message
//
// Revision 1.2.16.2 2005/11/15 13:34:38 chinkumo
// no message
//
// Revision 1.2.16.1 2005/09/26 08:39:14 chinkumo
// checkMode(..) method was added. This method checks if the current mode is
// valid (function of archiving type - historical/temporary).
//
// Revision 1.2 2005/01/26 15:35:38 chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1 2004/12/06 17:39:56 chinkumo
// First commit (new API architecture).
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
// FRANCE
//
// +============================================================================

package fr.soleil.archiving.hdbtdb.api.tools.mode;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;

/**
 * <p/>
 * <B>Description :</B><BR>
 * A Mode is an object that describes the way an attribute is archived. Let us note that a 'Mode' is a combination
 * 'modes'. A 'mode' can be periodic, absolute, relative, on calculation, on difference or external.
 * </p>
 *
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version $Revision: 1.5 $
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference
 * @see EventMode
 */

public final class Mode {

    private static final int HDB_PERIOD_MIN_VALUE = 10000; // ms
    private static final int TDB_PERIOD_MIN_VALUE = 100; // ms
    private final Logger logger = org.slf4j.LoggerFactory.getLogger(Mode.class);
    // private static int modeLengh = 18;
    private ModePeriode periodicMode;
    private ModeAbsolu absoluteMode;
    private ModeRelatif relativeMode;
    private ModeSeuil thresholdMode;
    private ModeCalcul calculationMode;
    private ModeDifference differenceMode;
    private EventMode eventMode;
    private TdbSpec tdbSpec;

    /**
     * Default constructor
     */
    public Mode() {
    }

    /**
     * Constructor
     *
     * @param mp : periodic mode
     * @param ma : absolute mode
     * @param mr : relative mode
     * @param mc : on calculation mode
     * @param md : on difference mode
     * @param me : external mode
     * @see #Mode().
     */
    public Mode(final ModePeriode mp, final ModeAbsolu ma, final ModeRelatif mr, final ModeSeuil ms,
                final ModeCalcul mc, final ModeDifference md, final EventMode me) {
        periodicMode = mp;
        absoluteMode = ma;
        relativeMode = mr;
        thresholdMode = ms;
        calculationMode = mc;
        differenceMode = md;
        eventMode = me;
    }

    /**
     * Constructor
     *
     * @param mp : periodic mode
     * @param ma : absolute mode
     * @param mr : relative mode
     * @param mc : on calculation mode
     * @param md : on difference mode
     * @param me : external mode
     * @see #Mode().
     */
    public Mode(final ModePeriode mp, final ModeAbsolu ma, final ModeRelatif mr, final ModeSeuil ms,
                final ModeCalcul mc, final ModeDifference md, final EventMode me, final TdbSpec ts) {
        periodicMode = mp;
        absoluteMode = ma;
        relativeMode = mr;
        thresholdMode = ms;
        calculationMode = mc;
        differenceMode = md;
        eventMode = me;
        tdbSpec = ts;
    }

    /**
     * Constructor
     *
     * @param modeArray : an array containing all information defining the mode. This
     *                  constructor will be primarily called when building a Mode from
     *                  data that are coming from the database.
     * @see #Mode().
     * @see #Mode(ModePeriode, ModeAbsolu, ModeRelatif, ModeSeuil, ModeCalcul, ModeDifference, EventMode)
     */
    public Mode(final String[] modeArray) {
        int index = 0;
        final int my_array_size = modeArray.length;
        for (int j = 0; j < modeArray.length; j++) {
            logger.debug("modeArray[{}]:{}", String.valueOf(j), modeArray[j]);
        }
        while (index < my_array_size && modeArray[index] != null) {
            if (modeArray[index].equals(Tag.MODE_P)) {
                // Mode P�riodique
                periodicMode = new ModePeriode(Integer.parseInt(modeArray[index + 1]));
                index = index + 2;
            } else if (modeArray[index].equals(Tag.MODE_A)) {
                // Mode Absolu
                if (modeArray.length > index + 4) {
                    absoluteMode = new ModeAbsolu(Integer.parseInt(modeArray[index + 1]),
                            Double.parseDouble(modeArray[index + 2]), Double.parseDouble(modeArray[index + 3]),
                            Boolean.parseBoolean(modeArray[index + 4]));
                    index = index + 5;
                } else {
                    absoluteMode = new ModeAbsolu(Integer.parseInt(modeArray[index + 1]),
                            Double.parseDouble(modeArray[index + 2]), Double.parseDouble(modeArray[index + 3]));
                    index = index + 4;
                }
            } else if (modeArray[index].equals(Tag.MODE_R)) {
                // Mode Relatif
                if (modeArray.length > index + 4) {
                    relativeMode = new ModeRelatif(Integer.parseInt(modeArray[index + 1]),
                            Double.parseDouble(modeArray[index + 2]), Double.parseDouble(modeArray[index + 3]),
                            Boolean.parseBoolean(modeArray[index + 4]));
                    index = index + 5;
                } else {
                    relativeMode = new ModeRelatif(Integer.parseInt(modeArray[index + 1]),
                            Double.parseDouble(modeArray[index + 2]), Double.parseDouble(modeArray[index + 3]));
                    index = index + 4;
                }
            } else if (modeArray[index].equals(Tag.MODE_T)) {
                // Mode Threshold
                thresholdMode = new ModeSeuil(Integer.parseInt(modeArray[index + 1]),
                        Double.parseDouble(modeArray[index + 2]), Double.parseDouble(modeArray[index + 3]));
                index = index + 4;
            } else if (modeArray[index].equals(Tag.MODE_C)) {
                // Mode Calcul
                calculationMode = new ModeCalcul(Integer.parseInt(modeArray[index + 1]),
                        Integer.parseInt(modeArray[index + 2]), Integer.parseInt(modeArray[index + 3]));
                // Warning : there is one more field for this mode type
                index = index + 5;
            } else if (modeArray[index].equals(Tag.MODE_D)) {
                // Mode Diff�rent
                differenceMode = new ModeDifference(Integer.parseInt(modeArray[index + 1]));
                index = index + 2;
            } else if (modeArray[index].equals(Tag.MODE_EVT)) {
                // Mode event
                eventMode = new EventMode();
                index = index + 1;
            } else if (modeArray[index].equals(Tag.TDB_SPEC)) {
                // Specific temporary database
                tdbSpec = new TdbSpec(Long.parseLong(modeArray[index + 1]), Long.parseLong(modeArray[index + 2]));
                index = index + 3;
            } else {
                index++;
            }
        }
    }

    public EventMode getEventMode() {
        return eventMode;
    }

    public void setEventMode(EventMode eventMode) {
        this.eventMode = eventMode;
    }

    public void reinitialize() {
        periodicMode = null;
        absoluteMode = null;
        relativeMode = null;
        thresholdMode = null;
        calculationMode = null;
        differenceMode = null;
        eventMode = null;
        tdbSpec = null;
    }

    /*
     * public static int getModeLengh() { return modeLengh; }
     */

    /**
     * getModeP returns the object which corresponds to the 'periodic' field.
     *
     * @return ModePeriode
     * @see #setModeP(fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode)
     * @see #getModeA()
     * @see #getModeR()
     * @see #getModeC()
     * @see #getModeD()
     * @see #getEventMode()
     */
    public ModePeriode getModeP() {
        return periodicMode;
    }

    /**
     * setModeP sets the object which corresponds to the 'periodic' field.
     *
     * @see #getModeP()
     * @see #setModeA(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu)
     * @see #setModeR(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif)
     * @see #setModeC(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul)
     * @see #setModeD(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference)
     * @see #setEventMode(EventMode)
     */
    public void setModeP(final ModePeriode mp) {
        periodicMode = mp;
    }

    /**
     * getModeA returns the object which corresponds to the 'absolute' field.
     *
     * @return ModeAbsolu
     * @see #setModeA(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu)
     * @see #getModeP()
     * @see #getModeR()
     * @see #getModeC()
     * @see #getModeD()
     * @see #getEventMode()
     */
    public ModeAbsolu getModeA() {
        return absoluteMode;
    }

    /**
     * setModeA sets the object which corresponds to the 'absolute' field.
     *
     * @see #getModeA()
     * @see #setModeP(fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode)
     * @see #setModeR(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif)
     * @see #setModeC(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul)
     * @see #setModeD(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference)
     * @see #setEventMode(EventMode)
     */
    public void setModeA(final ModeAbsolu ma) {
        absoluteMode = ma;
    }

    /**
     * getModeR returns the object which corresponds to the 'relative' field.
     *
     * @return ModeRelatif
     * @see #setModeR(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif)
     * @see #getModeP()
     * @see #getModeA()
     * @see #getModeC()
     * @see #getModeD()
     * @see #getEventMode()
     */
    public ModeRelatif getModeR() {
        return relativeMode;
    }

    /**
     * setModeR sets the object which corresponds to the 'relative' field.
     *
     * @see #getModeR()
     * @see #setModeP(fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode)
     * @see #setModeA(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu)
     * @see #setModeC(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul)
     * @see #setModeD(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference)
     * @see #setEventMode(EventMode)
     */
    public void setModeR(final ModeRelatif mr) {
        relativeMode = mr;
    }

    /**
     * getModeT returns the object which corresponds to the 'threshold' field.
     *
     * @return ModeCalcul
     * @see #setModeC(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul)
     * @see #getModeP()
     * @see #getModeA()
     * @see #getModeR()
     * @see #getModeC()
     * @see #getModeD()
     * @see #getEventMode()
     */
    public ModeSeuil getModeT() {
        return thresholdMode;
    }

    /**
     * setModeR sets the object which corresponds to the 'relative' field.
     *
     * @see #getModeR()
     * @see #setModeP(fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode)
     * @see #setModeA(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu)
     * @see #setModeR(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif)
     * @see #setModeC(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul)
     * @see #setModeD(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference)
     * @see #setEventMode(EventMode)
     */
    public void setModeT(final ModeSeuil thresholdMode) {
        this.thresholdMode = thresholdMode;
    }

    /**
     * getModeC returns the object which corresponds to the 'on calculation'
     * field.
     *
     * @return ModeCalcul
     * @see #setModeC(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul)
     * @see #getModeP()
     * @see #getModeA()
     * @see #getModeR()
     * @see #getModeD()
     * @see #getEventMode()
     */
    public ModeCalcul getModeC() {
        return calculationMode;
    }

    /**
     * setModeC sets the object which corresponds to the 'on calculation' field.
     *
     * @see #getModeC()
     * @see #setModeP(fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode)
     * @see #setModeA(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu)
     * @see #setModeR(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif)
     * @see #setModeD(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference)
     * @see #setEventMode(EventMode)
     */
    public void setModeC(final ModeCalcul mc) {
        calculationMode = mc;
    }

    /**
     * getModeD returns the object which corresponds to the 'on difference'
     * field.
     *
     * @return ModeDifference
     * @see #setModeD(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference)
     * @see #getModeP()
     * @see #getModeA()
     * @see #getModeR()
     * @see #getModeC()
     * @see #getEventMode()
     */
    public ModeDifference getModeD() {
        return differenceMode;
    }

    /**
     * setModeD sets the object which corresponds to the 'on difference' field.
     *
     * @see #getModeD()
     * @see #setModeP(fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode)
     * @see #setModeA(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu)
     * @see #setModeR(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif)
     * @see #setModeC(fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul)
     * @see #setEventMode(EventMode)
     */
    public void setModeD(final ModeDifference md) {
        differenceMode = md;
    }

    public TdbSpec getTdbSpec() {
        return tdbSpec;
    }

    public void setTdbSpec(final TdbSpec tdbSpec) {
        this.tdbSpec = tdbSpec;
    }

    /**
     * Returns a string representation of the object <I>Mode</I>.
     *
     * @return a string representation of the object <I>Mode</I>.
     */
    @Override
    public String toString() {
        // final StringBuilder buf = new StringBuilder("");

        final ToStringBuilder buf = new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        buf.append("periodic", periodicMode);
        buf.append("absolute", absoluteMode);
        buf.append("relative", relativeMode);
        buf.append("threshold", thresholdMode);
        buf.append("calculation", calculationMode);
        buf.append("difference", differenceMode);
        buf.append("event", eventMode);
        buf.append("tdbSpec", tdbSpec);
        // if (mode_p != null) {
        // buf.append(mode_p.toString() + "\r\n");
        // }
        // if (mode_a != null) {
        // buf.append(mode_a.toString() + "\r\n");
        // }
        // if (mode_r != null) {
        // buf.append(mode_r.toString() + "\r\n");
        // }
        // if (mode_s != null) {
        // buf.append(mode_s.toString() + "\r\n");
        // }
        // if (mode_c != null) {
        // buf.append(mode_c.toString() + "\r\n");
        // }
        // if (mode_d != null) {
        // buf.append(mode_d.toString() + "\r\n");
        // }
        // if (mode_e != null) {
        // buf.append(mode_e.toString() + "\r\n");
        // }
        // if (tdbSpec != null) {
        // buf.append(tdbSpec.toString() + "\r\n");
        // }
        return buf.toString();
    }

    public String toStringSimple() {
        final StringBuilder buf = new StringBuilder("[ ");
        if (periodicMode != null) {
            buf.append(" " + Tag.MODE_P_TAG + " ");
        }
        if (absoluteMode != null) {
            buf.append(" " + Tag.MODE_A_TAG + " ");
        }
        if (relativeMode != null) {
            buf.append(" " + Tag.MODE_R_TAG + " ");
        }
        if (thresholdMode != null) {
            buf.append(" " + Tag.MODE_T_TAG + " ");
        }
        if (calculationMode != null) {
            buf.append(" " + Tag.MODE_C_TAG + " ");
        }
        if (differenceMode != null) {
            buf.append(" " + Tag.MODE_D_TAG + " ");
        }
        if (eventMode != null) {
            buf.append(" " + Tag.MODE_E_TAG + " ");
        }
        if (tdbSpec != null) {
            buf.append(" " + Tag.MODE_SPEC_TAG + " ");
        }
        buf.append(" " + " ]");
        return buf.toString();
    }

    /**
     * Returns an array representation of the object <I>Mode</I>. This array
     * only contains not-null canonicals components of this Object.
     *
     * @return an array representation of the object <I>Mode</I>.
     */
    public String[] toArray() {
        String[] modeArray;
        modeArray = new String[getArraySizeSmall()];
        int index = 0;
        if (getModeP() != null) {
            modeArray[index] = Tag.MODE_P;
            index++;
            modeArray[index] = Integer.toString(getModeP().getPeriod());
            index++;
        }
        if (getModeA() != null) {
            modeArray[index] = Tag.MODE_A;
            index++;
            modeArray[index] = Integer.toString(getModeA().getPeriod());
            index++;
            modeArray[index] = Double.toString(getModeA().getValInf());
            index++;
            modeArray[index] = Double.toString(getModeA().getValSup());
            index++;
            modeArray[index] = Boolean.toString(getModeA().isSlow_drift());
            index++;

        }
        if (getModeR() != null) {
            modeArray[index] = Tag.MODE_R;
            index++;
            modeArray[index] = Integer.toString(getModeR().getPeriod());
            index++;
            modeArray[index] = Double.toString(getModeR().getPercentInf());
            index++;
            modeArray[index] = Double.toString(getModeR().getPercentSup());
            index++;
            modeArray[index] = Boolean.toString(getModeR().isSlow_drift());
            index++;
        }
        if (getModeT() != null) {
            modeArray[index] = Tag.MODE_T;
            index++;
            modeArray[index] = Integer.toString(getModeT().getPeriod());
            index++;
            modeArray[index] = Double.toString(getModeT().getThresholdInf());
            index++;
            modeArray[index] = Double.toString(getModeT().getThresholdSup());
            index++;
        }
        if (getModeC() != null) {
            modeArray[index] = Tag.MODE_C;
            index++;
            modeArray[index] = Integer.toString(getModeC().getPeriod());
            index++;
            modeArray[index] = Double.toString(getModeC().getRange());
            index++;
            modeArray[index] = Double.toString(getModeC().getTypeCalcul());
            index++;
            modeArray[index] = "NULL";
            index++;// Algo
        }
        if (getModeD() != null) {
            modeArray[index] = Tag.MODE_D;
            index++;
            modeArray[index] = Integer.toString(getModeD().getPeriod());
            index++;

        }
        if (getEventMode() != null) {
            modeArray[index] = Tag.MODE_EVT;
            index++;
        }
        if (getTdbSpec() != null) {
            modeArray[index] = Tag.TDB_SPEC;
            index++;
            modeArray[index] = Long.toString(getTdbSpec().getExportPeriod());
            index++;
            modeArray[index] = Long.toString(getTdbSpec().getKeepingPeriod());
            index++;
        }
        return modeArray;
    }

    /**
     * Returns the smallest size of this object's array representation.
     *
     * @return the smallest size of this object's array representation.
     */
    public int getArraySizeSmall() {
        int size = 0;
        if (getModeP() != null) {
            size = size + 2;
        }
        if (getModeA() != null) {
            size = size + 5;
        }
        if (getModeR() != null) {
            size = size + 5;
        }
        if (getModeT() != null) {
            size = size + 4;
        }
        if (getModeC() != null) {
            size = size + 5;
        }
        if (getModeD() != null) {
            size = size + 2;
        }
        if (getEventMode() != null) {
            size = size + 1;
        }
        if (getTdbSpec() != null) {
            size = size + 3;
        }
        return size;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Mode)) {
            return false;
        }

        final Mode mode = (Mode) o;

        if (absoluteMode != null ? !absoluteMode.equals(mode.absoluteMode) : mode.absoluteMode != null) {
            return false;
        }
        if (calculationMode != null ? !calculationMode.equals(mode.calculationMode) : mode.calculationMode != null) {
            return false;
        }
        if (differenceMode != null ? !differenceMode.equals(mode.differenceMode) : mode.differenceMode != null) {
            return false;
        }
        if (eventMode != null ? !eventMode.equals(mode.eventMode) : mode.eventMode != null) {
            return false;
        }
        if (periodicMode != null ? !periodicMode.equals(mode.periodicMode) : mode.periodicMode != null) {
            return false;
        }
        if (relativeMode != null ? !relativeMode.equals(mode.relativeMode) : mode.relativeMode != null) {
            return false;
        }
        if (thresholdMode != null ? !thresholdMode.equals(mode.thresholdMode) : mode.thresholdMode != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = periodicMode != null ? periodicMode.hashCode() : 0;
        result = 29 * result + (absoluteMode != null ? absoluteMode.hashCode() : 0);
        result = 29 * result + (relativeMode != null ? relativeMode.hashCode() : 0);
        result = 29 * result + (thresholdMode != null ? thresholdMode.hashCode() : 0);
        result = 29 * result + (calculationMode != null ? calculationMode.hashCode() : 0);
        result = 29 * result + (differenceMode != null ? differenceMode.hashCode() : 0);
        result = 29 * result + (eventMode != null ? eventMode.hashCode() : 0);
        return result;
    }

    public void checkMode(final boolean historic, final String attributeName) throws ArchivingException {
        if (eventMode == null && periodicMode == null) {
            // for event mode, periodic mode is not mandatory
            throw new ArchivingException("Periodic mode missing !!");
        }

        doCheck(periodicMode, attributeName, historic);
        doCheck(absoluteMode, attributeName, historic);
        doCheck(relativeMode, attributeName, historic);
        doCheck(thresholdMode, attributeName, historic);
        doCheck(calculationMode, attributeName, historic);
        doCheck(differenceMode, attributeName, historic);
        doCheck(eventMode, attributeName, historic);
    }

    private void doCheck(final ModeRoot mode, final String attributeName, final boolean isHistoric)
            throws ArchivingException {
        if (mode != null) {
            int minVal = HDB_PERIOD_MIN_VALUE;
            if (mode instanceof EventMode) {
                // there no minimum period for event mode
                minVal = 0;
            } else if (!isHistoric) {
                minVal = TDB_PERIOD_MIN_VALUE;
            }
            if (mode.getPeriod() < minVal) {
                // allowed short period in HDB only for previously declared
                // attributes
                if (ShortPeriodAttributesManager.isShortPeriodAttribute(attributeName) && isHistoric) {
                    final int attributePeriod = ShortPeriodAttributesManager
                            .getPeriodFromShortAttributeName(attributeName);
                    if (mode.getPeriod() / 1000 < attributePeriod) {
                        throw new ArchivingException("Archiving period too low !!" + "(" + attributeName
                                + "'s period must be greater or equal to " + attributePeriod + " ms" + ")");
                    }
                } else {
                    throw new ArchivingException("archiving period to low !!" + "(" + mode.getPeriod() + "<" + minVal
                            + " ms" + ")");
                }
            }
        }
    }

    public String toStringWatcher() {
        final StringBuilder ret = new StringBuilder();

        if (getModeP() != null) {
            ret.append(getModeP().toStringWatcher());
        }
        if (getModeA() != null) {
            ret.append(" ");
            ret.append(getModeA().toStringWatcher());
        }
        if (getModeR() != null) {
            ret.append(" ");
            ret.append(getModeR().toStringWatcher());
        }
        if (getModeT() != null) {
            ret.append(" ");
            ret.append(getModeT().toStringWatcher());
        }
        if (getModeD() != null) {
            ret.append(" ");
            ret.append(getModeD().toStringWatcher());
        }

        return ret.toString();
    }
}
