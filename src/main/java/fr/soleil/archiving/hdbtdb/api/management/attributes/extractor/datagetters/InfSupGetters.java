/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters;

import fr.esrf.Tango.AttrDataFormat;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.DataExtractor;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class InfSupGetters extends DataExtractor {

    private static final String IMAGE = "Image";
    private static final String SPECTRUM = "Spectrum";
    private static final String SCALAR = "Scalar";

    /**
     * @param con
     * @param ut
     * @param at
     */
    public InfSupGetters(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description : </b> Returns the data lower than the given value.
     * 
     * @param argin The attribute's name and the upper limit.
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataInfThan(final String... argin) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final DbData[] dbData;
        final String attributeName = argin[0];
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final int upperValue = Integer.parseInt(argin[1]);
            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);
            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataInfThan(attributeName, upperValue, dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(tfw[1], SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(tfw[1], SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the names list
        return dbData;
    }

    /**
     * <b>Description : </b> Returns the number of data lower than the given
     * value.
     * 
     * @param argin
     *            The attribute's name and the upper limit.
     * @return The number of scalar data lower than the given value and for the
     *         specified attribute.<br>
     * @throws ArchivingException
     */
    public int getAttDataInfThanCount(final String... argin) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        int valuesCount = 0;
        if (att != null) {
            final String attributeName = argin[0];
            final int upperValue = Integer.parseInt(argin[1]);
            // Retrieve informations on format and writable
            final int[] tfw = att.getAttTFWData(attributeName);
            // int data_type = tfw[0];
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    valuesCount = getAttScalarDataInfThanCount(attributeName, upperValue, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the number of records
        return valuesCount;
    }

    /**
     * <b>Description : </b> Returns the data higher than the given value.
     * 
     * @param argin The attribute's name and the lower limit
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataSupThan(final String... argin) throws ArchivingException {
        final String attributeName = argin[0];
        final DbData[] dbData;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final int upperValue = Integer.parseInt(argin[1]);
            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);
            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataSupThan(attributeName, upperValue, dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(tfw[1], SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(tfw[1], SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the names list
        return dbData;
    }

    /**
     * <b>Description : </b> Returns the number of data higher than the given
     * value.
     * 
     * @param argin
     *            The attribute's name and the lower limit
     * @return The number of data higher than the given value.<br>
     * @throws ArchivingException
     */
    public int getAttDataSupThanCount(final String... argin) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        int valuesCount = 0;
        if (att != null) {
            final String attributeName = argin[0];
            final int lowerValue = Integer.parseInt(argin[1]);
            // Retrieve informations on format and writable
            final int[] tfw = att.getAttTFWData(attributeName);
            // int data_type = tfw[0];
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    valuesCount = getAttScalarDataSupThanCount(attributeName, lowerValue, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the number of records
        return valuesCount;
    }

    /**
     * <b>Description : </b> Returns data that are lower than the given value x OR higher than the given value y.
     * 
     * @param argin The attribute's name, the lower limit and the upper limit
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataInfOrSupThan(final String... argin) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final String attributeName = argin[0];
        final DbData[] dbData;
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final int lowerValue = Integer.parseInt(argin[1]);
            final int upperValue = Integer.parseInt(argin[2]);
            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);
            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataInfOrSupThan(attributeName, lowerValue, upperValue, dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(tfw[1], SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(tfw[1], SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the names list
        return dbData;
    }

    /**
     * <b>Description : </b> Returns the number of data lower than the given value x OR higher than the given value y.
     * 
     * @param argin The attribute's name, the lower limit and the upper limit
     * @return The number of scalar data lower than the given value x OR higher than the given value y, associated with
     *         their corresponding timestamp <br>
     * @throws ArchivingException
     */
    public int getAttDataInfOrSupThanCount(final String... argin) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        int valuesCount = 0;
        if (att != null) {
            final String attributeName = argin[0];
            final int lowerValue = Integer.parseInt(argin[1]);
            final int upperValue = Integer.parseInt(argin[2]);

            // Retrieve informations on format and writable
            final int[] tfw = att.getAttTFWData(attributeName);
            final int data_format = tfw[1];
            final int writable = tfw[2];
            switch (data_format) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    valuesCount = getAttScalarDataInfOrSupThanCount(attributeName, lowerValue, upperValue, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(data_format, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(data_format, SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the number of records
        return valuesCount;
    }

    /**
     * <b>Description : </b> Returns data that are higher than the given value x OR lower than the given value y.
     * 
     * @param argin The attribute's name, the lower limit and the upper limit
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataSupAndInfThan(final String... argin) throws ArchivingException {
        final String attributeName = argin[0];
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final DbData[] dbData;
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final int lowerValue = Integer.parseInt(argin[1]);
            final int upperValue = Integer.parseInt(argin[2]);
            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);
            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataSupAndInfThan(attributeName, lowerValue, upperValue, dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(tfw[1], SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(tfw[1], SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the names list
        return dbData;
    }

    /**
     * <b>Description : </b> Returns data that are higher than the given value
     * x AND lower than the given value y.
     * 
     * @param argin
     *            The attribute's name, the lower limit and the upper limit
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public int getAttDataSupAndInfThanCount(final String... argin) throws ArchivingException {
        int valuesCount = 0;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (att != null) {
            final String attributeName = argin[0];
            final int lowerValue = Integer.parseInt(argin[1]);
            final int upperValue = Integer.parseInt(argin[2]);

            // Retrieve informations on format and writable
            final int[] tfw = att.getAttTFWData(attributeName);
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    valuesCount = getAttScalarDataSupAndInfThanCount(attributeName, lowerValue, upperValue, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the number of records
        return valuesCount;
    }

    private DbData[] getAttScalarDataInfThan(final String attributeName, final int upperValue, final DbData... dbData)
            throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);
            final String fields = roFields
                    ? dbUtils.toDbTimeFieldString(ConfigConst.TIME) + VALUE_SEPARATOR + ConfigConst.VALUE
                    : dbUtils.toDbTimeFieldString(ConfigConst.TIME) + VALUE_SEPARATOR + ConfigConst.READ_VALUE
                            + VALUE_SEPARATOR + ConfigConst.WRITE_VALUE;

            final String query = new StringBuilder("SELECT ").append(fields).append(" FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE ").append(roFields ? ConfigConst.VALUE : ConfigConst.READ_VALUE).append(" < ")
                    .append(upperValue).append(" ORDER BY time").toString();

            result = methods.getAttScalarDataForQuery(query, result);
        }
        return result;
    }

    private DbData[] getAttScalarDataSupThan(final String attributeName, final int lowerValue, final DbData... dbData)
            throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);
            final StringBuilder queryBuilder = new StringBuilder("SELECT ");
            if (roFields) {
                queryBuilder.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.VALUE);
            } else {
                queryBuilder.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.READ_VALUE).append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
            }
            queryBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(" WHERE (");
            if (roFields) {
                queryBuilder.append(ConfigConst.VALUE);
            } else {
                queryBuilder.append(ConfigConst.READ_VALUE);
            }
            queryBuilder.append(" > ").append(lowerValue).append(") ORDER BY time");
            String query = queryBuilder.toString();
            result = methods.getAttScalarDataForQuery(query, result);
        }
        return result;
    }

    public DbData[] getAttScalarDataInfOrSupThan(final String attributeName, final int lowerValue, final int upperValue,
            final DbData... dbData) throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);
            final String fields = roFields
                    ? dbUtils.toDbTimeFieldString(ConfigConst.TIME) + VALUE_SEPARATOR + ConfigConst.VALUE
                    : dbUtils.toDbTimeFieldString(ConfigConst.TIME) + VALUE_SEPARATOR + ConfigConst.READ_VALUE
                            + VALUE_SEPARATOR + ConfigConst.WRITE_VALUE; // if
            // (writable
            // ==
            // AttrWriteType._READ_WITH_WRITE
            // ||
            // writable
            // ==
            // AttrWriteType._READ_WRITE)
            final String selectField = roFields ? ConfigConst.VALUE : ConfigConst.READ_VALUE;
            final StringBuilder query = new StringBuilder("SELECT ").append(fields).append(" FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE (").append(selectField).append(" < ").append(lowerValue).append(" OR ")
                    .append(selectField).append(" > ").append(upperValue).append(") ORDER BY time");
            result = methods.getAttScalarDataForQuery(query.toString(), result);
        }
        return result;
    }

    public DbData[] getAttScalarDataSupAndInfThan(final String attributeName, final int lowerValue,
            final int upperValue, final DbData... dbData) throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);
            final String selectField = roFields ? ConfigConst.VALUE : ConfigConst.READ_VALUE;
            final StringBuilder query = new StringBuilder("SELECT ");
            if (roFields) {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.VALUE);
            } else {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.READ_VALUE).append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
            }
            query.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(" WHERE (").append(selectField).append(" > ")
                    .append(lowerValue).append(" AND ").append(selectField).append(" < ").append(upperValue)
                    .append(") ORDER BY time");
            result = methods.getAttScalarDataForQuery(query.toString(), result);
        }
        return result;
    }

    /*
     * 
     */
    private int getAttScalarDataInfThanCount(final String attributeName, final int upperValue, final int writable)
            throws ArchivingException {
        int result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null)) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            // Build the query string
            final String selectField1;
            if (isROFields(writable)) {
                selectField1 = ConfigConst.VALUE;
            } else {
                selectField1 = ConfigConst.READ_VALUE;
            }
            final String getAttributeDataQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE (").append(selectField1).append(" < ").append(upperValue).append(") ORDER BY time")
                    .toString();
            result = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return result;
    }

    public int getAttScalarDataSupThanCount(final String attributeName, final int lowerValue, final int writable)
            throws ArchivingException {
        int result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null)) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            // Build the query string
            final String selectField1;
            if (isROFields(writable)) {
                selectField1 = ConfigConst.VALUE;
            } else {
                selectField1 = ConfigConst.READ_VALUE;
            }

            final String selectFields = "COUNT(*)";

            final String getAttributeDataQuery = new StringBuilder("SELECT ").append(selectFields).append(" FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE (").append(selectField1).append(" > ").append(lowerValue).append(")").toString();
            result = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return result;
    }

    public int getAttScalarDataInfOrSupThanCount(final String attributeName, final int lowerValue, final int upperValue,
            final int writable) throws ArchivingException {
        int result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null)) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            // Build the query string
            final String selectField;
            if (isROFields(writable)) {
                selectField = ConfigConst.VALUE;
            } else {
                selectField = ConfigConst.READ_VALUE;
            }

            final String getAttributeDataQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE (").append(selectField).append(" < ").append(lowerValue).append(" OR ")
                    .append(selectField).append(" > ").append(upperValue).append(")").toString();
            result = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return result;
    }

    /*
     * 
     */
    public int getAttScalarDataSupAndInfThanCount(final String attributeName, final int lowerValue,
            final int upperValue, final int writable) throws ArchivingException {
        int result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null)) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            // Build the query string
            final String selectField_1;
            if (isROFields(writable)) {
                selectField_1 = ConfigConst.VALUE;
            } else {
                selectField_1 = ConfigConst.READ_VALUE;
            }

            final String getAttributeDataQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE (").append(selectField_1).append(" > ").append(lowerValue).append(" AND ")
                    .append(selectField_1).append(" < ").append(upperValue).append(") ORDER BY time").toString();
            result = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return result;
    }

}
