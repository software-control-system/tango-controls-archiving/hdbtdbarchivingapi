/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.GetArchivingConf;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import org.slf4j.LoggerFactory;

/**
 * @author AYADI
 *
 */
public class HDBOracleAttributeInsert extends HdbAttributeInsert {

    private static final long DEFAULT_COMMIT_COUNTER = 10;
    private long insertionCounter = 0;
    private final long maxInsertionCounter;
    private final long maxInsertionPeriod;
    private static final String COMMIT_COUNTER = "CommitCounter";
    private static final String COMMIT_PERIOD = "CommitPeriod";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private Connection connectionForInsertScalar;
    private final Chronometer chrono = new Chronometer();

    private final Logger logger = LoggerFactory.getLogger(HDBOracleAttributeInsert.class);

    /**
     * @param con
     * @param ut
     * @param at
     */
    public HDBOracleAttributeInsert(final AbstractDataBaseConnector connector, final IAdtAptAttributes at) {
        super(connector, at);
        long counter = DEFAULT_COMMIT_COUNTER, period = DEFAULT_COMMIT_COUNTER;
        try {
            counter = GetArchivingConf.readLongInDB(ConfigConst.HDB_CLASS_DEVICE, COMMIT_COUNTER,
                    DEFAULT_COMMIT_COUNTER);
            period = GetArchivingConf.readLongInDB(ConfigConst.HDB_CLASS_DEVICE, COMMIT_PERIOD, DEFAULT_COMMIT_COUNTER);
            logger.debug("commit counter = {}, commit period = {}", counter, period);
        } catch (final ArchivingException e) {
            // use default values
            logger.error("ERROR:", e);
        }
        insertionCounter = 0;
        maxInsertionCounter = counter;
        maxInsertionPeriod = period;
    }

    @Override
    protected synchronized void insertScalarDataInDB(final String tableName, final StringBuilder query,
            final String attributeName, final int writable, final Timestamp timeSt, final Object value,
            final int dataType) throws ArchivingException {
        if (insertionCounter >= maxInsertionCounter || chrono.isOver()) {
            logger.debug("chrono over = {}, insertionCounter = {}", chrono.isOver(), insertionCounter);
            closeConnection();
        }
        PreparedStatement preparedStatement = null;
        try {
            openConnection();
            preparedStatement = executeInsert(query, writable, timeSt, value, dataType, preparedStatement);
        } catch (final SQLRecoverableException e) {
            // resilience: retry an insertion in case of error
            logger.error("RETRY error inserting {} in table {} [value = {}, timestamp = {}]", new Object[] {
                    attributeName, tableName, value, DATE_FORMAT.format(new Date(timeSt.getTime())) });
            closeConnection();
            try {
                openConnection();
                preparedStatement = executeInsert(query, writable, timeSt, value, dataType, preparedStatement);
            } catch (final SQLException e1) {
                throw new ArchivingException(e, query.toString());
            }
        } catch (final SQLException e) {
            logger.error("error inserting {} in table {} [value = {}, timestamp = {}]", new Object[] { attributeName,
                    tableName, value, DATE_FORMAT.format(new Date(timeSt.getTime())) });
            logger.error("ERROR:", e);
            throw new ArchivingException(e, query.toString());
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (final SQLException e) {
                    logger.error("closing statement failed", e);
                }
            }
        }
    }

    private PreparedStatement executeInsert(final StringBuilder query, final int writable, final Timestamp timeSt,
            final Object value, final int dataType, PreparedStatement preparedStatement) throws SQLException,
            ArchivingException {
        if (connectionForInsertScalar != null) {
            insertionCounter++;
            preparedStatement = connectionForInsertScalar.prepareStatement(query.toString());
            ConnectionCommands.prepareSmtScalarOracle(preparedStatement, dataType, writable, value, 2);
            preparedStatement.setTimestamp(1, timeSt);
            preparedStatement.executeUpdate();
        }
        return preparedStatement;
    }

    @Override
    protected void insert_ImageData_RO_DataBase(StringBuilder query, final StringBuilder tableName,
            final StringBuilder tableFields, final int dimX, final int dimY, final Timestamp timeSt,
            final double[][] dvalue, final StringBuilder valueStr, final String attributeName)
                    throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            CallableStatement cstmt = null;
            query = new StringBuilder().append("{call ").append(connector.getSchema())
                    .append(".ins_im_1val (?, ?, ?, ?, ?)}");

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    cstmt = conn.prepareCall(query.toString());
                    cstmt.setString(1, attributeName);
                    cstmt.setTimestamp(2, timeSt);
                    cstmt.setInt(3, dimX);
                    cstmt.setInt(4, dimY);
                    if (dvalue == null) {
                        cstmt.setNull(4, java.sql.Types.CLOB);
                    } else {
                        cstmt.setString(5, valueStr.toString());
                    }

                    cstmt.executeUpdate();
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query.toString());
            } finally {
                ConnectionCommands.close(cstmt);
                connector.closeConnection(conn);
            }
        }
    }

    @Override
    protected void insert_SpectrumData_RO_DataBase(final String tableName, final StringBuilder tableFields,
            final int dim_x, final Timestamp timeSt, final StringBuilder valueStr, final String attributeName)
                    throws ArchivingException {
        if (connector != null) {
            CallableStatement cstmt = null;

            final StringBuilder query = new StringBuilder().append("{call ").append(connector.getSchema())
                    .append(".ins_sp_1val (?, ?, ?, ?)}");
            Connection conn = null;
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    cstmt = conn.prepareCall(query.toString());
                    cstmt.setString(1, attributeName);
                    cstmt.setTimestamp(2, timeSt);
                    cstmt.setInt(3, dim_x);
                    if (valueStr == null) {
                        cstmt.setNull(4, java.sql.Types.CLOB);
                    } else {
                        cstmt.setString(4, valueStr.toString());
                    }

                    cstmt.executeUpdate();
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query.toString());
            } finally {
                ConnectionCommands.close(cstmt);
                connector.closeConnection(conn);
            }
        }
    }

    @Override
    protected void insert_SpectrumData_RW_DataBase(final String tableName, final StringBuilder tableFields,
            final int dimX, final Timestamp timeSt, final StringBuilder valueWriteStr,
            final StringBuilder valueReadStr, final String attributeName) throws ArchivingException {
        if (connector != null) {
            CallableStatement cstmt = null;
            Connection conn = null;
            final StringBuilder query = new StringBuilder().append("{call ").append(connector.getSchema())
                    .append(".ins_sp_2val (?, ?, ?, ?, ?)}");
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    cstmt = conn.prepareCall(query.toString());
                    cstmt.setString(1, attributeName);
                    cstmt.setTimestamp(2, timeSt);
                    cstmt.setInt(3, dimX);
                    if (valueReadStr == null) {
                        cstmt.setNull(4, java.sql.Types.CLOB);
                    } else {
                        cstmt.setString(4, valueReadStr.toString());
                    }
                    if (valueWriteStr == null) {
                        cstmt.setNull(5, java.sql.Types.CLOB);
                    } else {
                        cstmt.setString(5, valueWriteStr.toString());
                    }

                    cstmt.executeUpdate();
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query.toString());
            } finally {
                ConnectionCommands.close(cstmt);
                connector.closeConnection(conn);
            }
        }
    }

    @Override
    public synchronized void closeConnection() {
        insertionCounter = 0;
        try {
            connector.commitAndClose(connectionForInsertScalar);
            connectionForInsertScalar = null;
            logger.debug("commit OK");
        } catch (final SQLException e) {
            logger.error("commit failed", e);
        }
    }

    private void openConnection() throws ArchivingException, SQLException {
		if (connectionForInsertScalar == null || connectionForInsertScalar.isClosed()) {
            connectionForInsertScalar = connector.getConnection();
            logger.debug("new connection for insert scalar");
            chrono.start(maxInsertionPeriod);
            connectionForInsertScalar.setAutoCommit(false);
        }
    }

}