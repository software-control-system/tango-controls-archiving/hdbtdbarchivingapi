/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.AttributeLight;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.cache.CacheAttribute;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeDomains;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeFamilies;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeIds;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeMembers;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeNames;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeProperties;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 *
 */
public abstract class AdtAptAttributes implements IAdtAptAttributes, DBExtractionConst {

    private final Logger logger = LoggerFactory.getLogger(AdtAptAttributes.class);

    private final AttributeFamilies families;
    private final AttributeMembers members;
    private final AttributeNames names;
    private final AttributeDomains domains;
    protected final AttributeIds ids;
    private final AttributeProperties properties;
    protected final AbstractDataBaseConnector connector;
    private static final Map<String, AttrCache> ATTR_CACHES = new HashMap<String, AttrCache>();
    private final AttrCache attrCache;

    public AdtAptAttributes(final AbstractDataBaseConnector connector, final AttributeIds ids) {
        this.connector = connector;
        families = new AttributeFamilies(connector);
        members = new AttributeMembers(connector);
        names = new AttributeNames(connector);
        domains = new AttributeDomains(connector);
        this.ids = ids;
        properties = new AttributeProperties(connector);
        synchronized (ATTR_CACHES) {
            final String key = connector.getParams().getUniqueKey();
            AttrCache tmpCache = ATTR_CACHES.get(key);
            if (tmpCache == null) {
                logger.debug("new attribute cache for {}", key);
                tmpCache = new AttrCache(key);
                ATTR_CACHES.put(key, tmpCache);
            }
            this.attrCache = tmpCache;
        }
    }

    abstract protected String getRequest(String attributeName) throws ArchivingException;

    /**
     * This methods retreives some informations associated to the given attribute,
     * builds an AttributeLight and returns it.
     *
     * @param attributeName attribute name
     * @return an AttributeLight object (built with the retrieved informations).
     * @throws ArchivingException
     */
    public AttributeLight getAttributeLightInfo(final String attributeName) throws SQLException, ArchivingException {
        final AttributeLight attributeLight;
        if (connector == null) {
            attributeLight = null;
        } else {
            attributeLight = new AttributeLight(attributeName);

            attributeLight.setDataType(getAttDataType(attributeName));
            attributeLight.setDataFormat(getAttDataFormat(attributeName));
            attributeLight.setWritable(getAttDataWritable(attributeName));

        }
        return attributeLight;
    }

    /**
     * <b>Description : </b> Returns an array containing the different definition
     * informations for the given attribute
     *
     * @param attributeName The attribute name
     * @return An array containing the different definition informations for the
     *         given attribute
     * @throws ArchivingException
     */

    @Override
    public Collection<String> getAttDefinitionData(final String attributeName) throws ArchivingException {
        final List<String> definitionsList = new ArrayList<String>();

        boolean isArchived = false;

        if (connector != null) {

            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // First connect with the database
            // if ( dbConn.isAutoConnect() )
            // dbConn.connect();
            // Create and execute the SQL query string
            final String sqlStr = getRequest(attributeName);

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(sqlStr);
                    // Gets the result of the query
                    while (rset.next()) {
                        isArchived = true;
                        for (int i = 0; i < ConfigConst.TAB_DEF.length; i++) {
                            final String info = new StringBuilder(ConfigConst.TAB_DEF[i]).append("::")
                                    .append(rset.getString(i + 1)).toString();
                            definitionsList.add(info);
                        }
                    }
                }
            } catch (final SQLException e) {
                String message = "";
                if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                        || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                    message = new StringBuilder(GlobalConst.ARCHIVING_ERROR_PREFIX)
                            .append(GlobalConst.ADB_CONNECTION_FAILURE).toString();
                } else {
                    message = new StringBuilder(GlobalConst.ARCHIVING_ERROR_PREFIX)
                            .append(GlobalConst.STATEMENT_FAILURE).toString();
                }

                final String reason = GlobalConst.QUERY_FAILURE;
                final String desc = "Failed while executing AdtAptAttributes.getAttDefinitionData() method...";
                throw new ArchivingException(message, reason, sqlStr, ErrSeverity.WARN, desc, this.getClass().getName(),
                        e);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }

        // Close the connection with the database
        // if ( dbConn.isAutoConnect() )
        // dbConn.close();
        // Returns the names list

        if (isArchived) {
            return definitionsList;
        } else {
            throw new ArchivingException("Invalid attribute: " + attributeName, "Invalid attribute: " + attributeName,
                    ErrSeverity.WARN,
                    "No database connection or \"" + attributeName + "\" attribute not found in database",
                    this.getClass().getName());
        }

    }

    /**
     * <b>Description : </b> Checks if the attribute of the given name, is already
     * registered in <I>HDB</I> (and more particularly in the table of the
     * definitions).
     *
     * @param attributeName The name of the attribute to check
     * @return boolean
     * @throws ArchivingException
     */
    @Override
    public boolean isRegisteredADT(final String attributeName) throws ArchivingException {
        try {
            ids.getAttID(attributeName.trim(), isCaseSentitiveDatabase());
            return true;
        } catch (ArchivingException e) {
            if (e.getMessage().equals("Id not found")) {
                return false;
            } else {
                throw e;
            }
        }

    }

    /**
     * Returns the tango format of a given attribute
     *
     * @param attributeName The attribute name
     * @return An int that describe the format of the given attribute.
     * @throws ArchivingException
     */
    @Override
    public int getAttDataFormat(final String attributeName) throws ArchivingException {

        CacheAttribute cache = this.attrCache.getAttribute(attributeName);
        int dataFormat = cache.getDataFormat() == -1 ? 0 : cache.getDataFormat();
        if (connector != null && cache.getDataFormat() == -1) {
            dataFormat = getAttTFWData(attributeName)[1];
        }
        return dataFormat;
    }

    /**
     * Returns the tango type of a given attribute
     *
     * @param attributeName The attribute name
     * @return An int containing that describe the type of the given attribute.
     * @throws ArchivingException
     */
    @Override
    public int getAttDataType(final String attributeName) throws ArchivingException {
        CacheAttribute cache = this.attrCache.getAttribute(attributeName);
        int dataType = cache.getDataType() == -1 ? 0 : cache.getDataType();
        if (connector != null && cache.getDataType() == -1) {
            dataType = getAttTFWData(attributeName)[0];
        }
        return dataType;
    }

    /**
     * Returns the tango writable parameter for a given attribute
     *
     * @param attributeName The attribute name
     * @return An int that is the writable value of the given attribute.
     * @throws ArchivingException
     */
    @Override
    public int getAttDataWritable(final String attributeName) throws ArchivingException {
        CacheAttribute cache = this.attrCache.getAttribute(attributeName);
        int dataWritable = cache.getWritable() == -1 ? 0 : cache.getWritable();
        if (connector != null && cache.getWritable() == -1) {
            dataWritable = getAttTFWData(attributeName)[2];
        }
        // Returns the names list
        return dataWritable;
    }

    /**
     * Returns the type, the format and the writable property of a given attribute
     *
     * @param attributeName The attribute name
     * @return An array (int) containing the type, the format and the writable
     *         property of the given attribute.
     * @throws ArchivingException
     */
    @Override
    public int[] getAttTFWData(final String attributeName) throws ArchivingException {
        int[] tfwData;
        if (connector == null) {
            tfwData = null;
        } else {
            tfwData = new int[3];

            CacheAttribute cache = this.attrCache.getAttribute(attributeName);
            if (cache.getDataFormat() == -1) {
                Connection conn = null;
                PreparedStatement preparedStatement = null;
                ResultSet rset = null;
                // Create and execute the SQL query string
                // Build the query string
                final StringBuilder builder = new StringBuilder("SELECT ").append(ConfigConst.ADT).append(DB_SEPARATOR)
                        .append(ConfigConst.DATA_TYPE).append(VALUE_SEPARATOR).append(ConfigConst.ADT)
                        .append(DB_SEPARATOR).append(ConfigConst.DATA_FORMAT).append(VALUE_SEPARATOR)
                        .append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.WRITABLE).append(" FROM ")
                        .append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.ADT);
                if (isCaseSentitiveDatabase()) {
                    builder.append(" WHERE (LOWER(").append(ConfigConst.ADT).append(DB_SEPARATOR)
                            .append(ConfigConst.FULL_NAME).append(") = LOWER(?)) ");
                } else {
                    builder.append(" WHERE (").append(ConfigConst.ADT).append(DB_SEPARATOR)
                            .append(ConfigConst.FULL_NAME).append(" = ?) ");
                }
                final String query = builder.toString();
                try {
                    conn = connector.getConnection();
                    if (conn == null) {
                        tfwData = null;
                    } else {
                        preparedStatement = conn.prepareStatement(query);
                        // dbConn.setLastStatement(preparedStatement);
                        preparedStatement.setString(1, attributeName.trim());
                        rset = preparedStatement.executeQuery();
                        while (rset.next()) {
                            cache.setDataType(rset.getInt(ConfigConst.DATA_TYPE));
                            cache.setDataFormat(rset.getInt(ConfigConst.DATA_FORMAT));
                            cache.setWritable(rset.getInt(ConfigConst.WRITABLE));
                        }
                        this.attrCache.putAttribute(attributeName, cache);
                    }
                } catch (final SQLException e) {
                    String message = "";
                    if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                            || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                        message = GlobalConst.ARCHIVING_ERROR_PREFIX + GlobalConst.ADB_CONNECTION_FAILURE;
                    } else {
                        message = GlobalConst.ARCHIVING_ERROR_PREFIX + GlobalConst.STATEMENT_FAILURE;
                    }

                    final String reason = GlobalConst.QUERY_FAILURE;
                    final String desc = "Failed while executing AdtAptAttributes.getAtt_TFW_Data() method...";
                    final String queryDebug = query + " -> " + attributeName.trim();
                    throw new ArchivingException(message, reason, queryDebug, ErrSeverity.WARN, desc,
                            this.getClass().getName(), e);
                } finally {
                    ConnectionCommands.close(rset);
                    ConnectionCommands.close(preparedStatement);
                    connector.closeConnection(conn);
                }
            }
            tfwData[0] = cache.getDataType();
            tfwData[1] = cache.getDataFormat();
            tfwData[2] = cache.getWritable();
        }
        // Returns the names list
        return tfwData;
    }

    /**
     * <b>Description : </b> Retrieves the number of records for a given attribute.
     *
     * @param attributeName The attribute's name.
     * @return The record's number (int)
     * @throws ArchivingException
     */
    @Override
    public int getAttRecordCount(final String attributeName) throws ArchivingException {
        int count = 0;
        if (connector != null) {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            final String getAttributeDataQuery = new StringBuilder("SELECT COUNT(*)").append(" FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(DbUtilsFactory.getInstance(connector).getTableName(attributeName.trim())).toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(getAttributeDataQuery);
                    while (rset.next()) {
                        count = rset.getInt(1);
                    }
                }
            } catch (final SQLException e) {
                String message = "";
                if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                        || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                    message = new StringBuilder(GlobalConst.ARCHIVING_ERROR_PREFIX).append(" : ")
                            .append(GlobalConst.ADB_CONNECTION_FAILURE).toString();
                } else {
                    message = new StringBuilder(GlobalConst.ARCHIVING_ERROR_PREFIX).append(" : ")
                            .append(GlobalConst.STATEMENT_FAILURE).toString();
                }

                final String reason = GlobalConst.QUERY_FAILURE;
                final String desc = "Failed while executing AdtAptAttributes.getAttRecordCount() method...";
                throw new ArchivingException(message, reason, getAttributeDataQuery, ErrSeverity.WARN, desc,
                        this.getClass().getName(), e);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return count;
    }

    @Override
    public AttributeFamilies getFamilies() {
        return families;
    }

    @Override
    public AttributeMembers getMembers() {
        return members;
    }

    @Override
    public AttributeNames getNames() {
        return names;
    }

    @Override
    public AttributeDomains getDomains() {
        return domains;
    }

    @Override
    public AttributeIds getIds() {
        return ids;
    }

    @Override
    public AttributeProperties getProperties() {
        return properties;
    }

    /**
     * Returns the type, the format and the writable property of a given attribute
     *
     * @param att_name
     * @return An array (int) containing the type, the format and the writable
     *         property of the given attribute.
     * @throws ArchivingException
     */
    public int[] getAttTFWDataById(final int id) throws ArchivingException {
        final int[] tfwData = new int[3];
        if (connector != null) {
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string

            final String query = new StringBuilder("SELECT ").append(ConfigConst.ADT).append(DB_SEPARATOR)
                    .append(ConfigConst.DATA_TYPE).append(VALUE_SEPARATOR).append(ConfigConst.ADT).append(DB_SEPARATOR)
                    .append(ConfigConst.DATA_FORMAT).append(VALUE_SEPARATOR).append(ConfigConst.ADT)
                    .append(DB_SEPARATOR).append(ConfigConst.WRITABLE).append(" FROM ").append(connector.getSchema())
                    .append(DB_SEPARATOR).append(ConfigConst.ADT).append(" WHERE (").append(ConfigConst.ADT)
                    .append(DB_SEPARATOR).append(ConfigConst.ID).append(" = ?) ").toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(query);
                    preparedStatement.setInt(1, id);
                    rset = preparedStatement.executeQuery();
                    while (rset.next()) {
                        tfwData[0] = rset.getInt(ConfigConst.DATA_TYPE);
                        tfwData[1] = rset.getInt(ConfigConst.DATA_FORMAT);
                        tfwData[2] = rset.getInt(ConfigConst.WRITABLE);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                connector.closeConnection(conn);
            }
        }
        // Returns the names list
        return tfwData;
    }

    public AttrCache getAttrCache() {
        return attrCache;
    }

    /**
     * Maintains a cache of attribute id
     *
     * @author thiamm
     */
    static class AttrCache {
        private final ConcurrentMap<String, CacheAttribute> attributeIdCache = new ConcurrentHashMap<String, CacheAttribute>();
        private final String key;
        private final Logger logger = LoggerFactory.getLogger(AttrCache.class);;

        public AttrCache(final String key) {
            this.key = key;
        }

        public void putAttribute(final String attributeName, CacheAttribute attribute) {
            if (attributeName != null) {
                logger.debug("put attribute in cache {}={}", attributeName);
                attributeIdCache.put(attributeName.toLowerCase().trim(), attribute);
            }
            logger.debug("cache size is {} ", attributeIdCache.size());
        }

        public CacheAttribute getAttribute(final String attributeName) {
            CacheAttribute temp = null;
            if (attributeName != null) {
                temp = attributeIdCache.get(attributeName.toLowerCase().trim());
                if (temp == null) {
                    temp = new CacheAttribute();
                }
            }
            logger.debug("get attribute from cache {}", attributeName);
            return temp;
        }

        public void clear() {
            attributeIdCache.clear();
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }

        public String getKey() {
            return key;
        }
    }
}
