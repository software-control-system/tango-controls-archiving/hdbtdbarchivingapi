package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt;

import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeIds;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

public final class AdtAptAttributesFactory {

    private AdtAptAttributesFactory() {

    }

    /**
     *
     * @param dbType
     * @param manager
     * @return
     */
    public static IAdtAptAttributes getInstance(final AbstractDataBaseConnector connector) {
        IAdtAptAttributes attributes;
        if (connector == null) {
            attributes = null;
        } else {
            final AttributeIds ids = new AttributeIds(connector);
            if (connector instanceof OracleDataBaseConnector) {
                attributes = new OracleAdtAptAttributes(connector, ids);
            } else {
                attributes = new MySqlAdtAptAttributes(connector, ids);
            }
        }
        return attributes;
    }
}
