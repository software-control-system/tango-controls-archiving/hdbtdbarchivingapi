//+============================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Mode/ModeSeuil.java,v $
//
// Project:      Tango Archiving Service
//
// Description: This object is one of the Mode class fields.
//				This class describes the absolute mode (the archiving occurs each time the received value is upper/lower a specified value).
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: ModeSeuil.java,v $
// Revision 1.4  2006/10/30 14:36:06  ounsy
// added a toStringWatcher method used by the ArchivingWatcher's getAllArchivingAttributes command
//
// Revision 1.3  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.2.16.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.2  2005/01/26 15:35:38  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :   Synchrotron SOLEIL
//			    L'Orme des Merisiers
//			    Saint-Aubin - BP 48
//			    91192 GIF-sur-YVETTE CEDEX
//              FRANCE
//
//+============================================================================

package fr.soleil.archiving.hdbtdb.api.tools.mode;

/**
 * <p/>
 * <B>Description :</B><BR>
 * This object is one of the <I>Mode class</I> fields. This class describes the
 * absolute mode (the archiving occurs each time the received value is
 * upper/lower a specified value).<BR>
 * </p>
 * 
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version $Revision: 1.4 $
 * @see Mode
 * @see ModePeriode
 * @see ModeRelatif
 * @see ModeCalcul
 * @see ModeDifference
 * @see EventMode
 */
public final class ModeSeuil extends ModeRoot {
    /**
     * the <I>upper limit</I> field of the object
     */
    private double thresholdInf = 0;
    /**
     * the <I>lower limit</I> field of the object
     */
    private double thresholdSup = 0;

    /**
     * Default constructor
     * 
     * @see #ModeSeuil(double, double)
     * @see #ModeSeuil(int, double, double)
     */
    public ModeSeuil() {
    }

    /**
     * This constructor takes two parameters as inputs.
     * 
     * @param i
     *            Any received event upper value below this parameter is
     *            archived
     * @param s
     *            Any received event with value upper this parameter is archived
     * @see #ModeSeuil()
     * @see #ModeSeuil(int, double, double)
     */
    public ModeSeuil(final double i, final double s) {
        thresholdInf = i;
        thresholdSup = s;
    }

    /**
     * This constructor takes three parameters as inputs :
     * 
     * @param p
     *            Archiving (polling) period time
     * @param i
     *            Any received event upper value below this parameter is
     *            archived
     * @param s
     *            Any received event with value upper this parameter is archived
     * @see #ModeSeuil()
     * @see #ModeSeuil(int, double, double)
     */
    public ModeSeuil(final int p, final double i, final double s) {
        super.period = p;
        thresholdInf = i;
        thresholdSup = s;
    }

    /**
     * Returns the <I>lower limit</I> field of the object.
     * 
     * @return the <I>lower limit</I> field of the object.
     * @see #setThresholdInf
     * @see #getThresholdSup
     * @see #setThresholdSup
     */
    public double getThresholdInf() {
        return thresholdInf;
    }

    /**
     * Sets the <I>lower limit</I> field of the object.
     * 
     * @param i
     *            : the <I>lower limit</I> field to set.
     * @see #getThresholdInf
     * @see #getThresholdSup
     * @see #setThresholdSup
     */
    public void setThresholdInf(final double i) {
        thresholdInf = i;
    }

    /**
     * Returns the <I>upper limit</I> field of the object.
     * 
     * @return the <I>upper limit</I> field of the object.
     * @see #getThresholdInf
     * @see #setThresholdInf
     * @see #setThresholdSup
     */
    public double getThresholdSup() {
        return thresholdSup;
    }

    /**
     * Sets the <I>upper limit</I> field of the object.
     * 
     * @param s
     *            : the <I>upper limit</I> field to set..
     * @see #getThresholdInf
     * @see #setThresholdInf
     * @see #getThresholdSup
     */
    public void setThresholdSup(final double s) {
        thresholdSup = s;
    }

    /**
     * Returns a string representation of the object <I>ModeSeuil</I>.
     * 
     * @return a string representation of the object <I>ModeSeuil</I>.
     */
    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder("");
        buf.append("[" + Tag.MODE_T_TAG + "\r\n" + "\t(" + Tag.MODE_P0_TAG + " = \"" + this.getPeriod() + "\" "
                + Tag.TIME_UNIT + "\r\n" + "\t" + Tag.MODE_T1_TAG + " = \"" + this.getThresholdInf() + "\" \r\n" + "\t"
                + Tag.MODE_T2_TAG + " = \"" + this.getThresholdSup() + "\"" + ")]");
        return buf.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ModeSeuil)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        final ModeSeuil ModeSeuil = (ModeSeuil) o;

        if (thresholdInf != ModeSeuil.thresholdInf) {
            return false;
        }
        if (thresholdSup != ModeSeuil.thresholdSup) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(thresholdInf);
        result = 29 * result + (int) (temp ^ temp >>> 32);
        temp = Double.doubleToLongBits(thresholdSup);
        result = 29 * result + (int) (temp ^ temp >>> 32);
        return result;
    }

    public String toStringWatcher() {
        return "MODE_T + " + super.getPeriod() + " + " + this.getThresholdInf() + " + " + this.getThresholdSup();
    }
}