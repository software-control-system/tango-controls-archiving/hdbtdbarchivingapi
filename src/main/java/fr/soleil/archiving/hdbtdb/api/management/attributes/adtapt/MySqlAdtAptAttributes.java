/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.AttributeHeavy;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeIds;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.AttributeExtractorFactroy;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 *
 */
public class MySqlAdtAptAttributes extends AdtAptAttributes {

    public MySqlAdtAptAttributes(final AbstractDataBaseConnector connector, final AttributeIds ids) {
        super(connector, ids);
    }

    @Override
    public boolean isCaseSentitiveDatabase() {
        return false;
    }

    /**
     * This method registers a given attribute into the hdb database This method
     * does not take care of id parameter of the given attribute as this
     * parameter is managed in the database side (autoincrement).
     *
     * @param attributeHeavy
     *            the attribute to register
     * @throws ArchivingException
     */

    private void registerInADT(final AttributeHeavy attributeHeavy) throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            PreparedStatement preparedStatement = null;

            // Create and execute the SQL query string
            // Build the query string
            final StringBuilder queryBuilder = new StringBuilder("INSERT INTO ").append(connector.getSchema())
                    .append(DB_SEPARATOR).append(ConfigConst.ADT).append(" (");
            for (int i = 1; i < ConfigConst.TAB_DEF.length - 1; i++) {
                queryBuilder.append(ConfigConst.TAB_DEF[i]).append(VALUE_SEPARATOR);
            }
            queryBuilder.append(ConfigConst.TAB_DEF[ConfigConst.TAB_DEF.length - 1])
                    .append(") VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
            final String query = queryBuilder.toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(query);
                    // dbConn.setLastStatement(preparedStatement);
                    // preparedStatement.setInt(1, attributeHeavy.getAttribute_id());
                    preparedStatement.setTimestamp(1, attributeHeavy.getRegistration_time());
                    preparedStatement.setString(2, attributeHeavy.getAttributeCompleteName());
                    preparedStatement.setString(3, attributeHeavy.getAttribute_device_name());
                    preparedStatement.setString(4, attributeHeavy.getDomain());
                    preparedStatement.setString(5, attributeHeavy.getFamily());
                    preparedStatement.setString(6, attributeHeavy.getMember());
                    preparedStatement.setString(7, attributeHeavy.getAttribute_name());
                    preparedStatement.setInt(8, attributeHeavy.getDataType());
                    preparedStatement.setInt(9, attributeHeavy.getDataFormat());
                    preparedStatement.setInt(10, attributeHeavy.getWritable());
                    preparedStatement.setInt(11, attributeHeavy.getMax_dim_x());
                    preparedStatement.setInt(12, attributeHeavy.getMax_dim_y());
                    preparedStatement.setInt(13, attributeHeavy.getLevel());

                    preparedStatement.setString(14, attributeHeavy.getCtrl_sys());
                    preparedStatement.setInt(15, attributeHeavy.getArchivable());
                    preparedStatement.setInt(16, attributeHeavy.getSubstitute());

                    preparedStatement.executeUpdate();
                }
            } catch (final SQLException e) {
                String message = "";
                if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                        || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                    message = new StringBuilder(GlobalConst.ARCHIVING_ERROR_PREFIX).append(" : ")
                            .append(GlobalConst.ADB_CONNECTION_FAILURE).toString();
                } else {
                    message = new StringBuilder(GlobalConst.ARCHIVING_ERROR_PREFIX).append(" : ")
                            .append(GlobalConst.STATEMENT_FAILURE).toString();
                }

                final String reason = GlobalConst.QUERY_FAILURE;
                final String desc = "Failed while executing MySqlAdtAptAttributes.registerInADT() method...";
                throw new ArchivingException(message, reason, query, ErrSeverity.WARN, desc, this.getClass().getName(),
                        e);
            } finally {
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
    }

    /**
     * This method registers a given attribute into the hdb database It inserts
     * a record in the "Attribute Properties Table" <I>(mySQL only)</I> This
     * method does not take care of id parameter of the given attribute as this
     * parameter is managed in the database side (autoincrement).
     *
     * @param attributeHeavy
     *            the attribute to register
     * @throws ArchivingException
     */
    private void registerInAPT(final int id, final AttributeHeavy attributeHeavy) throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            PreparedStatement preparedStatement = null;

            // Create and execute the SQL query string
            // Build the query string
            StringBuilder insertQuery;
            insertQuery = new StringBuilder("INSERT INTO ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.APT).append(" (");
            for (int i = 0; i < ConfigConst.TAB_PROP.length - 1; i++) {
                insertQuery.append(ConfigConst.TAB_PROP[i]).append(VALUE_SEPARATOR);
            }
            insertQuery.append(ConfigConst.TAB_PROP[ConfigConst.TAB_PROP.length - 1]).append(")");
            insertQuery.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?) ");
            String[] listValue = null;
            StringBuilder valueConcat = new StringBuilder();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(insertQuery.toString());
                    // dbConn.setLastStatement(preparedStatement);
                    preparedStatement.setInt(1, id);
                    preparedStatement.setTimestamp(2, attributeHeavy.getRegistration_time());

                    String description = attributeHeavy.getDescription();
                    final int maxDescriptionLength = 255;
                    if (description != null && description.length() > maxDescriptionLength) {
                        description = description.substring(0, maxDescriptionLength);
                    }

                    listValue = new String[] { description, attributeHeavy.getLabel(), attributeHeavy.getUnit(),
                            attributeHeavy.getStandard_unit(), attributeHeavy.getDisplay_unit(),
                            attributeHeavy.getFormat(), attributeHeavy.getMin_value(), attributeHeavy.getMax_value(),
                            attributeHeavy.getMin_alarm(), attributeHeavy.getMax_alarm() };

                    valueConcat.append("(");
                    for (int i = 3; i < listValue.length + 3; ++i) {
                        final String value = listValue[i - 3];
                        preparedStatement.setString(i, value);
                        valueConcat.append(value);
                        valueConcat.append(",");
                    }
                    valueConcat.append(")");

                    preparedStatement.executeUpdate();
                    listValue = null;
                    valueConcat = null;
                }
            } catch (final SQLException e) {
                String message = "";
                if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                        || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.ADB_CONNECTION_FAILURE;
                } else {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.STATEMENT_FAILURE;
                }

                final String reason = GlobalConst.QUERY_FAILURE;
                final String desc = "Failed while executing MySqlAdtAptAttributes.registerInAPT() method...";
                final String query = insertQuery + " = " + valueConcat;
                throw new ArchivingException(message, reason, query, ErrSeverity.WARN, desc, this.getClass().getName(),
                        e);
            } finally {
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
    }

    @Override
    protected String getRequest(final String attributeName) throws ArchivingException {
        final String str;
        if (connector == null) {
            str = null;
        } else {
            str = new StringBuilder("SELECT * FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.ADT).append(" WHERE ").append(ConfigConst.FULL_NAME).append("='")
                    .append(attributeName.trim()).append("'").toString();
        }
        return str;
    }

    /**
     * This method registers a given attribute into the hdb database It inserts
     * a record in the "Attribute Definition Table" <I>(mySQL only)</I> This
     * methos does not take care of id parameter of the given attribute as this
     * parameter is managed in the database side (autoincrement).
     *
     * @param attributeHeavy
     *            the attribute to register
     * @throws ArchivingException
     */
    @Override
    public void registerAttribute(final AttributeHeavy attributeHeavy) throws ArchivingException {
        // Register the attribute in the 'Attribute Definition Table'
        registerInADT(attributeHeavy);
        // int id = getAttID(attributeHeavy.getAttribute_complete_name());
        final int id = ids.getAttID(attributeHeavy.getAttributeCompleteName(), false);

        // Register the attribute in the 'Attribute Property Table'
        registerInAPT(id, attributeHeavy);

    }

    /**
     * test if attribute's table exist, otherwise, it creates a new table
     *
     * @param attributeName
     * @throws ArchivingException
     */
    @Override
    public void createAttributeTableIfNotExist(final String attributeName, final AttributeHeavy attr)
            throws ArchivingException {
        final int id = ids.getAttID(attributeName, false);
        final String tableName = DbUtilsFactory.getInstance(connector).getTableName(id);
        // Build the associated table
        if (!isTableExist(tableName)) {
            AttributeExtractorFactroy.getAttributeExtractor(connector).getDataGetters().buildAttributeTab(tableName,
                    attr.getDataType(), attr.getDataFormat(), attr.getWritable());
        }
    }

    private boolean isTableExist(final String tableName) throws ArchivingException {
        // show tables like "att_00021"
        boolean exists = false;
        if (connector != null) {
            final String query = new StringBuilder("SHOW TABLES LIKE ").append("\"").append(tableName).append("\"")
                    .toString();
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(query);
                    while (rset.next()) {
                        exists = rset.getString(1).equalsIgnoreCase(tableName);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(stmt);
                ConnectionCommands.close(rset);
                connector.closeConnection(conn);
            }
        }
        return exists;
    }

}
