package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor;

import java.sql.ResultSet;
import java.sql.Statement;

import fr.esrf.Tango.AttrWriteType;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

public class DataExtractor implements DBExtractionConst {

    protected static final String IMAGE = "Image";
    protected static final String SPECTRUM = "Spectrum";
    protected static final String SCALAR = "Scalar";

    protected GenericExtractorMethods methods;
    protected volatile Statement lastStatement;
    protected final AbstractDataBaseConnector connector;

    public DataExtractor(final AbstractDataBaseConnector connector) {
        if (connector == null) {
            methods = new GenericExtractorMethods(connector) {
                @Override
                public DbData[] treatStatementResultForGetSpectData(ResultSet rset, String attributeName,
                        DbData... dbData) throws ArchivingException {
                    return null;
                }

                @Override
                public DbData[] treatStatementResultForGetImageData(ResultSet rset, String attributeName,
                        DbData... dbData) throws ArchivingException {
                    return null;
                }
            };
        } else if (connector instanceof OracleDataBaseConnector) {
            methods = new OracleExtractorMethods(connector);
        } else {
            methods = new MySQlExtractorMethods(connector);
        }
        this.connector = connector;
        lastStatement = null;
    }

    public void setMethods(GenericExtractorMethods methods) {
        this.methods = methods;
    }

    public boolean isCanceled() {
        return methods.isCanceled();
    }

    public void setCanceled(boolean canceled) throws ArchivingException {
        methods.setCanceled(canceled);
        if (canceled) {
            ConnectionCommands.cancelStatement(lastStatement);
        }
    }

    protected final boolean isROFields(final int writable) {
        return (writable == AttrWriteType._READ || writable == AttrWriteType._WRITE);
    }

}
