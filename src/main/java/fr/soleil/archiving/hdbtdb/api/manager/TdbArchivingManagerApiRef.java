package fr.soleil.archiving.hdbtdb.api.manager;

import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class TdbArchivingManagerApiRef extends ArchivingManagerApiRef {
    // Intermediate Data Base
    private static final String TDB_ARCHIVER_FINDER = System.getProperty("tdb.archiver.finder", FINDER_CLASS);
    public static final String CLASS_DEVICE = System.getProperty("tdb.archiver.class", ConfigConst.TDB_CLASS_DEVICE);
    private static final String TDB_DEVICE_PATTERN = System.getProperty("tdb.archiver.devicepattern",
            "archiving/tdbarchiver/*");
    private boolean facility = false;


    public TdbArchivingManagerApiRef(AbstractDataBaseConnector connector) {
        super(false, connector);

    }

    @Override
    protected String getHost() {
        return ConfigConst.TDB_HOST;
    }

    @Override
    protected String getPassword() {
        return ConfigConst.TDB_BROWSER_PASSWORD;
    }

    @Override
    protected String getSchema() {
        return ConfigConst.TDB_SCHEMA_NAME;
    }

    @Override
    protected String getUser() {
        return ConfigConst.TDB_BROWSER_USER;
    }

    /*
     * (non-Javadoc)
     * 
     * @seefr.soleil.hdbtdbArchivingApi.ArchivingManagerApi.TangoDataBaseApi.
     * TangoDataBaseApi#geDevicePattern()
     */
    @Override
    protected String geDevicePattern() {
        return TDB_DEVICE_PATTERN;
    }

    /*
     * (non-Javadoc)
     * 
     * @seefr.soleil.hdbtdbArchivingApi.ArchivingManagerApi.TangoDataBaseApi.
     * TangoDataBaseApi#getClassDevice()
     */
    @Override
    public String getClassDevice() {
        return CLASS_DEVICE;
    }

    /*
     * (non-Javadoc)
     * 
     * @seefr.soleil.hdbtdbArchivingApi.ArchivingManagerApi.TangoDataBaseApi.
     * TangoDataBaseApi#getDB()
     */
    @Override
    protected String getDB() {
        return "Tdb";
    }





    @Override
    public boolean isMFacility() {
        return facility;
    }



    @Override
    public void setMFacility(final boolean facility) {
        this.facility = facility;
    }



    @Override
    public boolean isFinderClass() {
        return TDB_ARCHIVER_FINDER.equalsIgnoreCase(FINDER_CLASS);
    }
}
