package fr.soleil.archiving.hdbtdb.api;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.tdb.delete.TdbAttributeDelete;
import fr.soleil.archiving.hdbtdb.api.management.database.tdb.data.export.ITdbDataExport;
import fr.soleil.archiving.hdbtdb.api.management.database.tdb.data.export.TdbDataExportfactory;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class TDBDataBaseManager extends DataBaseManager {

    private ITdbDataExport tdbExport;
    private TdbAttributeDelete tdbAttrDelete;

    public TDBDataBaseManager(final AbstractDataBaseConnector connector) {
        super(connector, false);
    }

    public ITdbDataExport getTdbExport() throws ArchivingException {
        if (tdbExport == null) {
            tdbExport = TdbDataExportfactory.getInstance(connector);
        }
        return tdbExport;
    }

    public TdbAttributeDelete getTdbDeleteAttribute() throws ArchivingException {
        if (tdbAttrDelete == null) {
            tdbAttrDelete = new TdbAttributeDelete(connector);
        }
        return tdbAttrDelete;
    }

}
