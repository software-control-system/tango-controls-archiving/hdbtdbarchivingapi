//+======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/ArchivingMessConfig.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ArchivingMessConfig.
//						(Chinkumo Jean) -
//
// $Author: ounsy $
//
// $Revision: 1.8 $
//
// $Log: ArchivingMessConfig.java,v $
// Revision 1.8  2007/03/27 09:19:22  ounsy
// added diary logging in stop_archive_att when an attribute is filtered out of a config
//
// Revision 1.7  2007/01/05 12:47:17  pierrejoseph
// device_in_charge has been supressed.
// Modification of the ArchivingMessConfig object creation.
//
// Revision 1.6  2006/12/21 15:10:36  pierrejoseph
// One unused constructor has been supressed
//
// Revision 1.5  2006/10/11 08:35:18  ounsy
// added a ArchivingMessConfig(String[] archivingMessConfigArray, boolean deviceInChargeUsage) constructor
// commented the ArchivingMessConfig(String[] archivingMessConfigArray) constructor
//
// Revision 1.4  2006/10/05 15:41:04  ounsy
// added the filter() method
//
// Revision 1.3  2006/06/15 15:24:59  ounsy
// -Added a reference to the attributeToDedicatedArchiverHasthable Hasthable, which maps user-defined relations between attributes and dedicated archivers
// -Added a setDeviceInChargeForAttribute, that allows to define this relation on a per-attribute basis
//
// Revision 1.2  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.1.2.4  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.1.2.3  2005/09/26 08:34:26  chinkumo
// no message
//
// Revision 1.1.2.2  2005/09/16 08:02:12  chinkumo
// The grouped info was included in this object.
//
// Revision 1.1.2.1  2005/09/09 08:21:24  chinkumo
// First commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.archiving.hdbtdb.api.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ArchivingMessConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivingMessConfig.class);

    // private String device_in_charge = null;
    private boolean grouped = false;
    private Map<String, AttributeLightMode> attributeList = new HashMap<>();
    private Map<String, String> attributeToDedicatedArchiverHasthable = new HashMap<>();

    /**
     * Create an object with the simple contructor
     * 
     * @return
     */
    public static ArchivingMessConfig basicObjectCreation() {
        return new ArchivingMessConfig();
    }

    /**
     * Default constructor Creates a new instance of ArchivingMessConfig
     */
    private ArchivingMessConfig() {
    }

    /**
     * Create an object from an array who contains all the attributes
     * information (type,format ...)
     * 
     * @return
     */
    static public ArchivingMessConfig creationWithFullInformation(final String[] archivingMessConfigArray) {
        return new ArchivingMessConfig(archivingMessConfigArray, true);
    }

    /**
     * Create an object from an array who does not contain all the attributes
     * information (type,format ...)
     * 
     * @return
     */
    static public ArchivingMessConfig creationWithoutFullInformation(final String[] archivingMessConfigArray) {
        return new ArchivingMessConfig(archivingMessConfigArray, false);
    }

    /**
     * This constructor takes several parameters as inputs.
     * 
     * Example With Not full Information :
     * true/false,1,"tango/tangotest/spjz_1/double_spectrum_ro"
     * ,MODE_P,10000,MODE_D,20000 or
     * true/false,1,"tango/tangotest/spjz_1/double_spectrum_ro",MODE_P,10000 or
     * true/false,2,"tango/tangotest/spjz_1/double_spectrum_ro",
     * "tango/tangotest/spjz_1/double_scalar_ro",MODE_P,10000
     * 
     * true/false,1,"tango/tangotest/spjz_2/double_spectrum_ro",MODE_P,2000,
     * TDB_SPEC,300000,21600000,MODE_D,2000,TDB_SPEC,300000,21600000 or true/
     * false,1,"tango/tangotest/spjz_2/double_spectrum_ro",MODE_P,2000, TDB_SPEC
     * ,300000,21600000
     * 
     * Example With full Information :
     * true/false,11,"tango/tangotest/spjz_1/double_spectrum_ro"
     * ,5,1,0,"archiving/hdbarchiver/01",NULL,4,MODE_P,10000,MODE_D,20000 or
     * true/false,9,"tango/tangotest/spjz_1/double_spectrum_ro",5,1,0,
     * "archiving/hdbarchiver/01",NULL,2,MODE_P,10000
     * 
     * true/false,17,"tango/tangotest/spjz_2/double_spectrum_ro",5,1,0,
     * "archiving/tdbarchiver/01" ,NULL,10,MODE_P,2000,TDB_SPEC,300000,21600000
     * ,MODE_D,2000,TDB_SPEC,300000,21600000 or
     * true/false,12,"tango/tangotest/spjz_2/double_spectrum_ro"
     * ,5,1,0,"archiving/tdbarchiver/01"
     * ,NULL,5,MODE_P,2000,TDB_SPEC,300000,21600000
     * 
     * @param attributeLightModeList
     *            the list of attributes
     * @see #ArchivingMessConfig()
     */
    private ArchivingMessConfig(final String[] archivingMessConfigArray, final boolean fullInformation) {
        // Build the list of attributes that belongs to the object
        attributeList = new HashMap<String, AttributeLightMode>();
        attributeToDedicatedArchiverHasthable = new HashMap<String, String>();

        // Grouped
        grouped = Boolean.getBoolean(archivingMessConfigArray[0]);
        if (fullInformation) {
            for (int index = 1; index < archivingMessConfigArray.length;) {
                final int attributeLightModeSize = Integer.parseInt(archivingMessConfigArray[index]);
                final String[] attributeLightModeArray = new String[attributeLightModeSize];
                index++;
                System.arraycopy(archivingMessConfigArray, index, attributeLightModeArray, 0, attributeLightModeSize);
                add(AttributeLightMode.creationWithFullInformation(attributeLightModeArray));
                index = index + attributeLightModeArray.length;
            }

        } else {
            final int numberOfAttributes = Integer.parseInt(archivingMessConfigArray[1]);
            final int modeSize = archivingMessConfigArray.length - 2 - numberOfAttributes;
            final String[] modeArray = new String[modeSize];
            // separate attributes names and modes in 2 arrays
            System.arraycopy(archivingMessConfigArray, numberOfAttributes + 2, modeArray, 0, modeSize);
            final String[] attributesNames = new String[numberOfAttributes];
            System.arraycopy(archivingMessConfigArray, 2, attributesNames, 0, numberOfAttributes);
            // create a AttributeLightMode per attribute
            for (final String attributeName : attributesNames) {
                final String[] attributeLightModeArray = new String[modeArray.length + 1];
                attributeLightModeArray[0] = attributeName;
                System.arraycopy(modeArray, 0, attributeLightModeArray, 1, modeArray.length);
                attributeList.put(attributeName,
                        AttributeLightMode.creationWithoutFullInformation(attributeLightModeArray));
            }
        }
        LOGGER.debug("adding attributes {}", attributeList);
    }

    public boolean isGrouped() {
        return grouped;
    }

    public void setGrouped(final boolean grouped) {
        this.grouped = grouped;
    }

    /**
     * Returns the <I>list of attributes</I> that are included in this archiving
     * configuration.
     * 
     * @return the <I>list of attributes</I> that are included in the context.
     */
    public Set<String> getAttributeListKeys() {
        return attributeList.keySet();
    }

    public String[] getAttributeList() {
        final Set<String> myEnumeration = attributeList.keySet();
        return myEnumeration.toArray(new String[myEnumeration.size()]);
    }

    /**
     * Returns the <I>list of attributes</I> that are included in this archiving
     * configuration.
     * 
     * @return the <I>list of attributes</I> that are included in the context.
     */
    public Collection<AttributeLightMode> getAttributeListValues() {
        final Collection<AttributeLightMode> collection = attributeList.values();
        return collection;
    }

    /**
     * Sets the ArchivingMessConfig's <I>list of attributes</I> included in the
     * context.
     * 
     * @param attributeName
     *            the <I>list of attributes</I> that are included in the
     *            context.
     */
    public AttributeLightMode getAttribute(final String attributeName) {
        AttributeLightMode attributeLightMode = null;
        for (final Entry<String, AttributeLightMode> attr : attributeList.entrySet()) {
            if (attr.getKey().equalsIgnoreCase(attributeName)) {
                attributeLightMode = attr.getValue();
                break;
            }
        }
        return attributeLightMode;
    }

    public void add(final AttributeLightMode attributeLightMode) {
        attributeList.put(attributeLightMode.getAttributeCompleteName(), attributeLightMode);
    }

    public void remove(final String attributeLightModeName) {
        attributeList.remove(attributeLightModeName);
    }

    public void removeAll() {
        attributeList.clear();
    }

    public int size() {
        return attributeList.size();
    }

    public String[] toArray() {
        String[] archivingMessConfigArray = null;

        final Map<String, String[]> hashtableArr = new HashMap<String, String[]>();
        int archivingMessConfigSize = 0;

        Set<String> attributeKeys = attributeList.keySet();
        for (final String attributeName : attributeKeys) {
            final AttributeLightMode attributeLightMode = attributeList.get(attributeName);
            final String[] attributeLightModeArray = attributeLightMode.toArray();

            // index initial + index sp�cifiant du tableau la taille
            // attributeLightModeArray + place du tableau
            archivingMessConfigSize = archivingMessConfigSize + 1 + attributeLightModeArray.length;
            hashtableArr.put(attributeName, attributeLightModeArray);
        }

        archivingMessConfigArray = new String[1 + archivingMessConfigSize];
        archivingMessConfigArray[0] = Boolean.toString(grouped);
        int i = 1;
        attributeKeys = attributeList.keySet();
        for (final String attributeName : attributeKeys) {
            final String[] attributeLightModeArray = hashtableArr.get(attributeName);
            archivingMessConfigArray[i] = Integer.toString(attributeLightModeArray.length);
            i++;
            System.arraycopy(attributeLightModeArray, 0, archivingMessConfigArray, i, attributeLightModeArray.length);
            i = i + attributeLightModeArray.length;
        }
        return archivingMessConfigArray;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("");
        sb.append("Configuration : ...").append("\r\n");
        // Write the fields values
        sb.append("\tgrouped = ").append(grouped).append("\r\n");
        // Write the attribute list names
        sb.append("\tattributesNames= ").append(getAttributeListKeys()).append("\r\n");
        return sb.toString();
    }

    public Map<String, String> getAttributeToDedicatedArchiverHasthable() {
        return attributeToDedicatedArchiverHasthable;
    }

    public void setDeviceInChargeForAttribute(final AttributeLightMode attributeLightMode,
            final String nextDedicatedArchiver) {
        final boolean hasDedicatedArchiver = nextDedicatedArchiver != null && !nextDedicatedArchiver.equals("");
        if (hasDedicatedArchiver) {
            attributeToDedicatedArchiverHasthable.put(attributeLightMode.getAttributeCompleteName(),
                    nextDedicatedArchiver);
        }
    }

    public void filter(final boolean isDedicated, final String[] reservedAttributes) {
        if (isDedicated) {
            if ((reservedAttributes != null) && (reservedAttributes.length > 0)) {
                final int numberOfReserved = reservedAttributes.length;
                final List<String> reservedAttributesList = new ArrayList<String>(numberOfReserved);
                for (int i = 0; i < numberOfReserved; i++) {
                    reservedAttributesList.add(reservedAttributes[i].toLowerCase());
                }

                final Set<String> myEnumeration = attributeList.keySet();
                for (final String attribute : myEnumeration) {
                    if (!reservedAttributesList.contains(attribute.toLowerCase())) {
                        remove(attribute);
                    }
                }
            }
        }
    }
}
