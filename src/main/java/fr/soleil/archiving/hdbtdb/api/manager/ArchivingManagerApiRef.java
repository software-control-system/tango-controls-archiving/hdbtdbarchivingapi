/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.Group.Group;
import fr.esrf.TangoApi.Group.GroupAttrReply;
import fr.esrf.TangoApi.Group.GroupAttrReplyList;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.AttributeHeavy;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.archiving.hdbtdb.api.DataBaseManagerFactory;
import fr.soleil.archiving.hdbtdb.api.GetArchivingConf;
import fr.soleil.archiving.hdbtdb.api.management.modes.IGenericMode;
import fr.soleil.archiving.hdbtdb.api.tools.Archiver;
import fr.soleil.archiving.hdbtdb.api.tools.ArchivingMessConfig;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeSupport;
import fr.soleil.archiving.hdbtdb.api.tools.AttributesArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.FaultingAttribute;
import fr.soleil.archiving.hdbtdb.api.tools.LoadBalancedList;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.tango.clientapi.TangoAttribute;

/**
 * @author AYADI
 */
public abstract class ArchivingManagerApiRef implements IArchivingManagerApiRef {

    protected static final String FINDER_CLASS = "class";
    private static final int TANGO_TIMEOUT = 60000;
    private final static Logger LOGGER = LoggerFactory.getLogger(ArchivingManagerApiRef.class);
    // Command name
    private static final String ARCHIVINGSTART = "TriggerArchiveConf";
    private static final String ARCHIVINGSTOP = "StopArchiveAtt";
    private static final String m_ARCHIVINGSTOPGROUP = "StopArchiveConf";
    // Attribute Name
    private static final String SCALARCHARGE = "/scalarCharge";
    private static final String SPECTRUMCHARGE = "/spectrumCharge";
    private static final String IMAGECHARGE = "/imageCharge";
    private static Map<String, AttributeInfo> attributeToDeviceInfo = new ConcurrentHashMap<String, AttributeInfo>();
    private final boolean historic;
    private final AbstractDataBaseConnector connector;
    protected DataBaseManager dataBase;
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
    Future<?> archiverListRefresher = null;
    private boolean isConnected;
    private String[] exportedArchiverList;
    private String[] notExportedArchiverList;

    public ArchivingManagerApiRef(final boolean historic, final AbstractDataBaseConnector connector) {
        this.historic = historic;
        this.connector = connector;
        dataBase = null;
        isConnected = false;
    }

    private static void throwArchivingStopException(final ArchivingException archivingStopException,
            final boolean needsThrow, final int badAttributeListSize) throws ArchivingException {
        if (needsThrow) {
            final String message = GlobalConst.ARCHIVING_ERROR_PREFIX;
            final String reason = "Failed while executing ArchivingManagerApi.ArchivingStop() method...";

            if (badAttributeListSize != -1) {
                final String desc = "Impossible to stop the archiving of " + badAttributeListSize
                        + " attributes ! : \r\n";
                final ArchivingException aef = new ArchivingException(message, reason, ErrSeverity.PANIC, desc,
                        "ArchivingManagerApi");
                archivingStopException.addStack(message, aef);
            }

            throw archivingStopException;
        }
    }

    /**
     * generate the ArchivingStart array command used in the Archiver driver
     *
     * @param archivingMessConfig
     * @return the array that correspond to the archivingMessConfig object.
     */
    private static void setAttributeInfoInArchivingMess(final ArchivingMessConfig archivingMessConfig)
            throws ArchivingException {
        final Set<String> attributes = archivingMessConfig.getAttributeListKeys();
        for (final String attribute : attributes) {
            try {
                final AttributeInfo attributeInfo = getAttributeInfo(attribute, false);
                archivingMessConfig.getAttribute(attribute).setDataType(attributeInfo.data_type);
                archivingMessConfig.getAttribute(attribute).setDataFormat(attributeInfo.data_format.value());
                archivingMessConfig.getAttribute(attribute).setWritable(attributeInfo.writable.value());
            } catch (final DevFailed devFailed) {
                throw new ArchivingException(devFailed);
            }
        }
    }

    /**
     * get the number of attribute taken in charge for the given Archiver device
     *
     * @param archiverName
     * @return the number of attribute in charge
     * @throws ArchivingException
     */
    public static int get_charge(final String archiverName) throws ArchivingException {
        try {
            int attributeDeviceCharge = 0;
            final DeviceProxy archiverProxy = new DeviceProxy(archiverName);
            final String[] attributeList = archiverProxy.get_attribute_list();
            for (final String element : attributeList) {
                final DeviceAttribute devattr = archiverProxy.read_attribute(element);
                final short[] shortarray = devattr.extractShortArray();
                final short value = shortarray[0];
                attributeDeviceCharge = attributeDeviceCharge + value;
            }
            return attributeDeviceCharge;
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    /**
     * get the number of scalar taken in charge for the given Archiver device
     *
     * @param archiverName
     * @return the number of scalar in charge
     * @throws ArchivingException
     * @throws DevFailed
     */
    public static int get_scalar_charge(final String archiverName) throws DevFailed {

        return new TangoAttribute(archiverName + SCALARCHARGE).read(int.class);
    }

    /**
     * get the number of spectrum taken in charge for the given Archiver device
     *
     * @param archiverName
     * @return the number of spectrum in charge
     * @throws DevFailed
     */
    public static int get_spectrum_charge(final String archiverName) throws DevFailed {
        return new TangoAttribute(archiverName + SPECTRUMCHARGE).read(int.class);
    }

    /**
     * get the number of image taken in charge for the given Archiver device
     *
     * @param archiverName
     * @return the image of spectrum in charge
     * @throws DevFailed
     */
    public static int get_image_charge(final String archiverName) throws DevFailed {

        return new TangoAttribute(archiverName + IMAGECHARGE).read(int.class);

    }

    /**
     * this method return AttributeInfo tango class
     *
     * @param attributeName
     * @return AttributeInfo
     */
    public static AttributeInfo getAttributeInfo(final String attributeName, final boolean doRefresh) throws DevFailed {
        AttributeInfo att_info = null;
        if (!doRefresh) {
            att_info = getBufferedAttributeInfo(attributeName);
        }

        if (att_info == null) {
            att_info = getAttributeInfoFromTheDevice(attributeName);
        }

        if (doRefresh && att_info != null) {
            bufferAttributeInfo(attributeName, att_info);
        }

        return att_info;
    }

    private static void bufferAttributeInfo(final String attributeName, final AttributeInfo att_info) {
        attributeToDeviceInfo.put(attributeName, att_info);
    }

    private static AttributeInfo getBufferedAttributeInfo(final String attributeName) {
        return attributeToDeviceInfo.get(attributeName);
    }

    private static AttributeInfo getAttributeInfoFromTheDevice(final String attributeName) throws DevFailed {
        final int index = attributeName.lastIndexOf('/');
        final String deviceName = attributeName.substring(0, index);
        return new DeviceProxy(deviceName).get_attribute_info(attributeName.substring(index + 1));
    }

    /**
     * this method return data_format value of the attribute
     *
     * @param attributeName
     * @return AttrDataFormat
     * @throws DevFailed
     */
    private static int getAttributeDataFormat(final String attributeName) throws DevFailed {
        final AttributeInfo attribute = getAttributeInfo(attributeName, false);
        return attribute.data_format.value();

    }

    private static String[] splitAttributeName(String attributeName) throws ArchivingException {
        String host = "";
        String domain = "";
        String family = "";
        String member = "";
        String attribut = "";
        final String[] argout = new String[5];// = {"HOST", "DOMAIN", "FAMILY",
        // "MEMBER", "ATTRIBUT"};
        String[] decoupe; // d�coupage en 5 partie : host, domain, family,
        // member, attribut

        // Host name management
        if (attributeName.startsWith("//")) {
            attributeName = attributeName.substring(2, attributeName.length());
        } else {
            attributeName = TangoAccess.getTangoHost() + "/" + attributeName;
        }

        // Spliting
        decoupe = attributeName.split("/"); // spliting the name in 3 fields
        host = decoupe[0];
        domain = decoupe[1];
        family = decoupe[2];
        member = decoupe[3];
        attribut = decoupe[4];

        argout[0] = host;
        argout[1] = domain;
        argout[2] = family;
        argout[3] = member;
        argout[4] = attribut;
        return argout;
    }

    /**
     * @param vector
     */
    public static String[] toStringArray(final List<String> vector) {
        return vector.toArray(new String[vector.size()]);
    }

    /**
     * @param vector
     */
    public static String[] toStoppingArrayAttLightMode(final List<AttributeLightMode> vector) {
        final List<String> myResultVect = new ArrayList<String>();

        for (final AttributeLightMode attributeLightMode : vector) {
            final String[] attributeLightModeArray = attributeLightMode.toArray();
            myResultVect.add(Integer.toString(attributeLightModeArray.length));
            for (final String element : attributeLightModeArray) {
                myResultVect.add(element);
            }
        }
        return toStringArray(myResultVect);
    }

    /**
     * @param lightAttributes
     */
    public static List<AttributeLightMode> stoppingVector(final String[] lightAttributes) {
        final List<AttributeLightMode> myResultVect = new ArrayList<AttributeLightMode>();
        int index = 0;
        while (index < lightAttributes.length) {
            final int myAttLenght = Integer.parseInt(lightAttributes[index]);
            index++;
            final String[] myLightAttribut = new String[myAttLenght];
            System.arraycopy(lightAttributes, index, myLightAttribut, 0, myAttLenght);
            index = index + myAttLenght;
            final AttributeLightMode attributeLightMode = AttributeLightMode
                    .creationWithFullInformation(myLightAttribut);
            myResultVect.add(attributeLightMode);
        }
        return myResultVect;
    }

    public static boolean checkAttributeSupport(final String attributeName) throws ArchivingException {
        try {
            final AttributeInfo attributeInfo = getAttributeInfo(attributeName, true);
            final int dataType = attributeInfo.data_type;
            final int dataFormat = attributeInfo.data_format.value();
            final int writable = attributeInfo.writable.value();
            return AttributeSupport.checkAttributeSupport(attributeName, dataType, dataFormat, writable);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    /*
     *
     */
    private static Map<String, Archiver> dispatchAttributeByArchiver(final Archiver[] archiversList) {
        if (archiversList == null || archiversList.length == 0) {
            return null;
        }

        final Map<String, Archiver> ret = new HashMap<String, Archiver>();
        for (final Archiver currentArchiver : archiversList) {
            final boolean isDedicated = currentArchiver.isDedicated();
            if (!isDedicated) {
                continue;
            }

            final String[] reservedAttributes = currentArchiver.getReservedAttributes();
            if (reservedAttributes == null || reservedAttributes.length == 0) {
                continue;
            }

            for (final String reservedAttribute : reservedAttributes) {
                ret.put(reservedAttribute, currentArchiver);
            }
        }

        return ret;
    }

    /*
     *
     */
    private static void initArchivers(final LoadBalancedList loadBalancedList, final boolean grouped,
            final String[] referenceArchiverList) throws ArchivingException {
        if (referenceArchiverList.length == 0) {
            LOGGER.error("No running archiver found");
            final String message = GlobalConst.ARCHIVING_ERROR_PREFIX;
            final String reason = GlobalConst.NO_ARC_EXCEPTION;
            final String desc = "Failed while executing ArchivingManagerApi.selectArchiver() method...";
            throw new ArchivingException(message, reason, ErrSeverity.WARN, desc, "");
        }
        // LoadBalancedList archivers initializing
        if (grouped) {
            try {
                loadBalancedList.addArchiver(referenceArchiverList[0], get_scalar_charge(referenceArchiverList[0]),
                        get_spectrum_charge(referenceArchiverList[0]), get_image_charge(referenceArchiverList[0]));
            } catch (final DevFailed e) {
                LOGGER.error("adding archiver {} to load balancing failed", referenceArchiverList[0]);
            }
        } else {
            for (final String element : referenceArchiverList) {
                try {
                    loadBalancedList.addArchiver(element, get_scalar_charge(element), get_spectrum_charge(element),
                            get_image_charge(element));
                } catch (final DevFailed e) {
                    LOGGER.error("adding archiver {} to load balancing failed", element);
                }
            }
        }
    }

    /**
     * This method allows to open the connection on either the historic or the
     * temporary database
     *
     * @param muser
     * @param mpassword
     */
    private void connectArchivingDatabase() {
        dataBase = DataBaseManagerFactory.getDataBaseManager(historic, connector);
        isConnected = true;
    }

    /**
     * Configure the Archiving
     *
     * @throws ArchivingException
     */
    @Override
    public void archivingConfigure() throws ArchivingException {
        setMFacility(getFacility());

        archivingConfigureWithoutArchiverListInit();

        // The created lists are useful during the attribute archiving starting
        // (LoadBalancing)
        initDbArchiverList();
    }

    @Override
    public void archivingConfigureWithoutArchiverListInit() {
        // Database connection
        connectArchivingDatabase();
    }

    @Override
    public DataBaseManager getDataBase() {
        return dataBase;
    }

    /*
     *
     */
    public Map<String, Archiver> getAttributesToDedicatedArchiver(final boolean refreshArchivers)
            throws ArchivingException {
        if (refreshArchivers) {
            initDbArchiverList();
        }

        final String[] exportedArchiversNameList = getMExportedArchiverList();
        final String[] notExportedArchiversNameList = getMNotExportedArchiverList();
        if (exportedArchiversNameList.length == 0 && notExportedArchiversNameList.length == 0) {
            return null;
        }

        final Archiver[] achiversList = new Archiver[exportedArchiversNameList.length
                + notExportedArchiversNameList.length];
        for (int i = 0; i < exportedArchiversNameList.length; i++) {
            final String currentExportedArchiverName = exportedArchiversNameList[i];
            final Archiver currentExportedArchiver = Archiver.findArchiver(currentExportedArchiverName, historic);
            currentExportedArchiver.setExported(true);
            achiversList[i] = currentExportedArchiver;
        }
        for (int i = 0; i < notExportedArchiversNameList.length; i++) {
            final String currentNotExportedArchiverName = notExportedArchiversNameList[i];
            final Archiver currentNotExportedArchiver = Archiver.findArchiver(currentNotExportedArchiverName, historic);
            currentNotExportedArchiver.setExported(false);
            achiversList[i + exportedArchiversNameList.length] = currentNotExportedArchiver;
        }

        final Map<String, Archiver> ret = dispatchAttributeByArchiver(achiversList);
        Archiver.setAttributesToDedicatedArchiver(historic, ret);
        return ret;
    }

    /**
     * @return @return the number of archivers for the given archiving type
     */
    @Override
    public int getArchiverListSize() {
        int count;
        if (getMExportedArchiverList() == null) {
            count = 0;
        } else {
            count = getMExportedArchiverList().length;
        }
        return count;
    }

    protected abstract String getHost();

    protected abstract String getUser();

    protected abstract String getPassword();

    protected abstract String getSchema();

    protected abstract String geDevicePattern();

    public abstract String getClassDevice();

    public abstract boolean isFinderClass();

    protected abstract String getDB();

    @Override
    public boolean getFacility() {
        boolean facility = false;
        try {
            facility = GetArchivingConf.getFacility(getClassDevice());
        } catch (final ArchivingException e) {
            LOGGER.error(e.toString());
        }
        return facility;

    }

    /**
     * Get the status of the archiver in charge of the given attribute
     *
     * @param attributeName the attribute name
     * @return the status of the Archiver driver
     * @throws ArchivingException
     */
    @Override
    public String getStatus(final String attributeName) throws ArchivingException {
        try {
            String archiverName = "";
            if (dataBase != null) {
                archiverName = dataBase.getMode().getDeviceInCharge(attributeName);
            }
            final DeviceProxy archiverProxy = new DeviceProxy(archiverName);
            return archiverProxy.status();
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);

        }
    }

    @Override
    public String[] getMExportedArchiverList() {
        try {
            if (archiverListRefresher == null) {
                initDbArchiverList();
            }
            archiverListRefresher.get();
        } catch (InterruptedException | ExecutionException | ArchivingException e) {
            LOGGER.error("failed to get archivers list", e);
        }
        return exportedArchiverList;
    }

    @Override
    public void setMExportedArchiverList(final String... exportedArchiverList) {
        this.exportedArchiverList = exportedArchiverList;
    }

    private void initDbArchiverList() throws ArchivingException {
        if (archiverListRefresher != null && !archiverListRefresher.isCancelled()) {
            archiverListRefresher.cancel(true);
        }
        archiverListRefresher = executor.submit(() -> {
            try {
                final boolean facility = isMFacility();

                final List<String> runningDevicesListVector = new ArrayList<String>();
                final List<String> notRunningDevicesListVector = new ArrayList<String>();

                final Database dbase = ApiUtil.get_db_obj();

                LOGGER.debug("retrieving archiver device list for class {}", getClassDevice());
                String[] devicesList;
                if (!isFinderClass()) {
                    final String dbDevicePattern = geDevicePattern();
                    devicesList = dbase.getDevices(dbDevicePattern);
                } else {
                    final String dbClassDevice = getClassDevice();
                    devicesList = dbase.get_device_exported_for_class(dbClassDevice);
                }

                Group group = new Group("");
                group.add(devicesList);
                GroupAttrReplyList result = group.read_attribute("state", false);
                for (GroupAttrReply reply : result) {
                    try {
                        reply.get_data().extractDevState();
                        runningDevicesListVector
                                .add((facility ? "//" + dbase.get_tango_host() + "/" : "") + reply.dev_name());
                    } catch (DevFailed e) {
                        notRunningDevicesListVector
                                .add((facility ? "//" + dbase.get_tango_host() + "/" : "") + reply.dev_name());
                    }
                }
                exportedArchiverList = runningDevicesListVector.toArray(new String[runningDevicesListVector.size()]);
                notExportedArchiverList = notRunningDevicesListVector
                        .toArray(new String[notRunningDevicesListVector.size()]);
                LOGGER.debug("will manage {} archivers, {} are down", runningDevicesListVector,
                        notExportedArchiverList);
            } catch (final DevFailed devFailed) {
                LOGGER.error("failed to get archiver list", DevFailedUtils.toString(devFailed));
            }
        });

    }

    /**
     * @return true if the connection to hdb exists
     */
    @Override
    public boolean isDbConnected() {
        return isConnected;
    }

    // !!!!!!!!!!!!!! RG: Why are there 2 methods: ArchivingStop and ArchivingStopConf ? It is very confusing !!!!!!

    /**
     * Start the archiving for the given attribute list and for the given archiving type
     *
     * @param archivingMessConfig An ArchivingMessConfig object (a list of AttributeLightMode object)
     * @throws ArchivingException
     */
    @Override
    public void archivingStart(final ArchivingMessConfig archivingMessConfig) throws ArchivingException {

        // long beforeInitArchiverList = System.currentTimeMillis ();
        final AttributesArchivingException archivingStartException = new AttributesArchivingException();
        boolean needsThrow = false;

        if (dataBase == null) {
            throw new ArchivingException("connection failed");
        }
        if (archiverListRefresher == null) {
            initDbArchiverList();
        }
        try {
            archiverListRefresher.get();
        } catch (InterruptedException e1) {
            archiverListRefresher.cancel(true);
            return;
        } catch (ExecutionException e1) {
            throw new ArchivingException(e1);
        }
        if (getMExportedArchiverList().length == 0) {
            throw new ArchivingException("No archiver running");
        }
        final List<String> goodAttributeList = new ArrayList<String>();
        // The badAttributesList will be used to add a FaultingAttribute for
        // each attribute that causes an ArchivingException
        final List<String> badAttributeList = new ArrayList<String>();
        // Check the support type, format and writable...
        final Set<String> attributeNameList = archivingMessConfig.getAttributeListKeys();
        LOGGER.info("starting archiving for attributes {}", attributeNameList);
        for (final String attribute : attributeNameList) {
            try {
                if (checkAttributeSupport(attribute)) {
                    goodAttributeList.add(attribute);
                } else {
                    LOGGER.error(
                            "starting archiving for attribute {} impossible,format, type or writable not supported ",
                            attribute);
                    archivingStartException.addStack("START_ERROR",
                            attribute + " start failed - format, type or writable not supported",
                            getClass().getCanonicalName() + ".ArchivingStart");
                    badAttributeList.add(attribute);
                    // We add this attribute to the exception that is being
                    // forged
                    archivingStartException.getFaultingAttributes().add(
                            new FaultingAttribute(attribute, "Attribute format, type or writable not supported !"));
                }
            } catch (final ArchivingException e) {
                LOGGER.error("starting archiving failed", e);
                badAttributeList.add(attribute);
                archivingStartException.addStack(e);
                needsThrow = true;
            }

        }

        if (goodAttributeList.size() == 0) {
            throw archivingStartException;
        }

        // Build the new list in case of bad attribute(s)
        String[] finalAttributeList = new String[goodAttributeList.size()];
        for (int i = 0; i < goodAttributeList.size(); i++) {
            finalAttributeList[i] = goodAttributeList.get(i);
        }

        // if the creation table doest not work the archiving is not launched
        finalAttributeList = checkRegistration(finalAttributeList);
        final LoadBalancedList loadBalancedList = selectArchiversTask(archivingMessConfig.isGrouped(),
                archivingMessConfig.getAttributeToDedicatedArchiverHasthable(), finalAttributeList);
        // Sending order to archivers
        final Collection<String> archivers = loadBalancedList.getArchivers();

        for (final String archiver : archivers) {
            final String[] assignedAttributes = loadBalancedList.getArchiverAssignedAtt(archiver);

            if (assignedAttributes.length > 0) {
                final ArchivingMessConfig archiverArchivingMessConfig = ArchivingMessConfig.basicObjectCreation();

                for (final String assignedAttribute : assignedAttributes) {
                    archiverArchivingMessConfig.add(archivingMessConfig.getAttribute(assignedAttribute));
                }
                try {
                    // Set the data_type, format and writable characteristics of
                    // each attributs
                    setAttributeInfoInArchivingMess(archiverArchivingMessConfig);
                    // String[] archivingMessStartArray =
                    // getArchivingMessStartArray(archiver_archivingMessConfig);
                    final DeviceData deviceData = new DeviceData();
                    final String[] messConfigArray = archiverArchivingMessConfig.toArray();
                    deviceData.insert(messConfigArray);
                    final DeviceProxy archiverProxy = new DeviceProxy(archiver);
                    archiverProxy.set_timeout_millis(TANGO_TIMEOUT);
                    LOGGER.info("starting {} on {}", archiverArchivingMessConfig.getAttributeList(), archiver);
                    archiverProxy.command_inout(ARCHIVINGSTART, deviceData);
                } catch (final DevFailed devFailed) {
                    LOGGER.error("start archiving failed {}", devFailed);
                    archivingStartException.addStack(new ArchivingException(devFailed));
                    needsThrow = true;
                }
            }
        }
        if (needsThrow) {
            throw archivingStartException;
        }
        LOGGER.info("start archiving OK");
    }

    /**
     * This method checks the registration and create the table of the attributes in the database
     *
     * @param attributeNameList The attribute complete names
     * @return true if the registration works
     */
    private String[] checkRegistration(final String[] attributeNameList) throws ArchivingException {
        final String tangoHost = TangoAccess.getTangoHost();
        final List<String> myNewListV = new ArrayList<String>();
        for (final String element : attributeNameList) {
            final String attributeName = element;
            try {
                final AttributeInfo attributeInfo = getAttributeInfo(attributeName, false);
                final int index = attributeName.lastIndexOf('/');
                final String deviceName = attributeName.substring(0, index);
                final String[] attributeSplitName = splitAttributeName(attributeName);
                final java.sql.Timestamp time = new java.sql.Timestamp(new java.util.Date().getTime());
                final AttributeHeavy attributeHeavy = new AttributeHeavy(attributeName);
                attributeHeavy.setAttributeCompleteName(attributeName); // ****************
                // The
                // whole
                // attribute
                // name
                // (device_name
                // +
                // attribute_name)
                attributeHeavy.setAttribute_device_name(deviceName);
                attributeHeavy.setRegistration_time(time); // ****************
                // Attribute
                // registration
                // timestamp
                attributeHeavy.setDomain(attributeSplitName[1]); // ****************
                // domain to
                // which the
                // attribute is
                // associated
                attributeHeavy.setFamily(attributeSplitName[2]); // ****************
                // family to
                // which the
                // attribute is
                // associated
                attributeHeavy.setMember(attributeSplitName[3]); // ****************
                // member to
                // which the
                // attribute is
                // associated
                attributeHeavy.setAttribute_name(attributeSplitName[4]); // ****************
                // attribute
                // name
                attributeHeavy.setDataType(attributeInfo.data_type); // ****************
                // Attribute
                // data
                // type
                attributeHeavy.setDataFormat(attributeInfo.data_format.value()); // ****************
                // Attribute
                // data
                // format
                attributeHeavy.setWritable(attributeInfo.writable.value()); // ****************
                // Attribute
                // read/write
                // type
                attributeHeavy.setMax_dim_x(attributeInfo.max_dim_x); // ****************
                // Attribute
                // Maximum
                // X
                // dimension
                attributeHeavy.setMax_dim_y(attributeInfo.max_dim_y); // ****************
                // Attribute
                // Maximum
                // Y
                // dimension
                attributeHeavy.setLevel(attributeInfo.level.value()); // ****************
                // Attribute
                // display
                // level
                attributeHeavy.setCtrl_sys(tangoHost); // ****************
                // Control system to
                // which the attribute
                // belongs
                attributeHeavy.setArchivable(0); // **************** archivable
                // (Property that precises
                // whether the attribute is
                // "on-run-only" archivable, or
                // if it is "always" archivable
                attributeHeavy.setSubstitute(0); // **************** substitute
                attributeHeavy.setDescription(attributeInfo.description);
                attributeHeavy.setLabel(attributeInfo.label);
                attributeHeavy.setUnit(attributeInfo.unit);
                attributeHeavy.setStandard_unit(attributeInfo.standard_unit);
                attributeHeavy.setDisplay_unit(attributeInfo.display_unit);
                attributeHeavy.setFormat(attributeInfo.format);
                attributeHeavy.setMin_value(attributeInfo.min_value);
                attributeHeavy.setMax_value(attributeInfo.max_value);
                attributeHeavy.setMin_alarm(attributeInfo.min_alarm);
                attributeHeavy.setMax_alarm(attributeInfo.max_alarm);

                if (!isRegistred(attributeName)) {

                    dataBase.getAttribute().registerAttribute(attributeHeavy);
                }
                // m_DataBase.getAttribute().CreateAttributeTableIfNotExist(attributeName,
                // m_DataBase.getExtractor().getDataGetters(),
                // m_DataBase.getDbUtil());
                myNewListV.add(attributeName);
                dataBase.getAttribute().createAttributeTableIfNotExist(attributeName, attributeHeavy);
            } catch (final DevFailed devFailed) {
                final String message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.DEV_UNREACH_EXCEPTION
                        + " (" + attributeName + ")";
                final String reason = GlobalConst.TANGO_COMM_EXCEPTION;
                final String desc = "Failed while executing ArchivingManagerApi.checkRegistration() method...";
                throw new ArchivingException(message, reason, ErrSeverity.WARN, desc, "", devFailed);
            } catch (final ArchivingException e) {
                throw e;
            }
        }

        return toStringArray(myNewListV);
    }

    /**
     * Stop the archiving for the given attribute list and for the given
     * archiving type
     *
     * @param attributeNameList the list of attributes (The archiving of each element of this
     *            list is about to be stopped)
     * @throws ArchivingException
     */
    @Override
    public void archivingStop(final String... attributeNameList) throws ArchivingException {
        boolean needsThrow = false;
        int badAttributesCount = 0;
        final List<String> notArchivedAttributes = getNotArchivedAttributes(attributeNameList);
        if (!notArchivedAttributes.isEmpty()) {
            throw new ArchivingException(notArchivedAttributes + " are not archived");
        }
        if (dataBase == null) {
            throw new ArchivingException(GlobalConst.UNCONNECTECTED_ADB, GlobalConst.UNCONNECTECTED_ADB,
                    ErrSeverity.WARN,
                    "Can't stopped archiving for the given attribute list and for given archiving type because the archiving database is not connected",
                    this.getClass().getName());
        }

        String attributeName = "";
        String archiverName = "";

        final AttributesArchivingException archivingStopException = new AttributesArchivingException();
        boolean initCauseAlreadyDoneInException = false;
        final IGenericMode genericMode = dataBase.getMode();

        for (final String element : attributeNameList) {
            attributeName = element;

            try {
                // get the Archiver that take in charge the archiving
                archiverName = genericMode.getDeviceInCharge(attributeName);
                if (archiverName.isEmpty() || !TangoAccess.deviceLivingTest(archiverName)) {
                    throw new ArchivingException(GlobalConst.ARC_UNREACH_EXCEPTION, GlobalConst.ARC_UNREACH_EXCEPTION,
                            ErrSeverity.WARN, "Archiver name is empty or the given device is not alive",
                            this.getClass().getName());
                }

                if (isArchived(attributeName)) {
                    final Mode mode = getArchivingMode(attributeName);
                    final AttributeLightMode attributeLightMode = new AttributeLightMode(attributeName);
                    attributeLightMode.setDevice_in_charge(archiverName);

                    /*
                     * AttributeInfo attributeInfo =
                     * getAttributeInfoFromDatabase(attributeName);
                     * attributeLightMode.setData_type(attributeInfo.data_type);
                     * attributeLightMode
                     * .setData_format(attributeInfo.data_format.value());
                     * attributeLightMode
                     * .setWritable(attributeInfo.writable.value());
                     */
                    final int[] tfw = dataBase.getAttribute().getAttTFWData(attributeName);
                    attributeLightMode.setDataType(tfw[0]);
                    attributeLightMode.setDataFormat(tfw[1]);
                    attributeLightMode.setWritable(tfw[2]);
                    attributeLightMode.setMode(mode);

                    final DeviceProxy archiverProxy = new DeviceProxy(archiverName);
                    final String[] attributeLightModeArray = attributeLightMode.toArray();

                    final DeviceData deviceData = new DeviceData();
                    deviceData.insert(attributeLightModeArray);
                    archiverProxy.command_inout(ARCHIVINGSTOP, deviceData);
                }
            } catch (final DevFailed devFailed) {

                final String message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : ";
                final String reason = "Failed while executing ArchivingManagerApi.ArchivingStop() method...";
                StringBuilder descBuffer = new StringBuilder(GlobalConst.ARCHIVING_ERROR_PREFIX);
                descBuffer.append(" : ");
                descBuffer.append(GlobalConst.ARC_UNREACH_EXCEPTION);
                descBuffer.append(" (Archiver: ");
                descBuffer.append(archiverName);
                descBuffer.append(")");
                final String desc = descBuffer.toString();
                descBuffer = null;
                final ArchivingException archivingException = new ArchivingException(message, reason, ErrSeverity.PANIC,
                        desc, "ArchivingManagerApi");

                if (!initCauseAlreadyDoneInException) {
                    archivingStopException.initCause(devFailed);
                    initCauseAlreadyDoneInException = true;
                }

                archivingStopException.addStack(message, archivingException);
                archivingStopException.getFaultingAttributes().add(new FaultingAttribute(attributeName, desc));
                needsThrow = true;
                badAttributesCount++;
            } catch (final ArchivingException ae) {
                StringBuilder descBuffer = new StringBuilder(GlobalConst.ARCHIVING_ERROR_PREFIX);
                descBuffer.append(" : ");
                descBuffer.append(GlobalConst.ARC_UNREACH_EXCEPTION);
                descBuffer.append(" (Archiver: ");
                descBuffer.append(archiverName);
                descBuffer.append(") : ");
                descBuffer.append(ae.getLastExceptionMessage());
                final String desc = descBuffer.toString();
                descBuffer = null;
                archivingStopException.addStack(ae.getMessage(), ae);
                archivingStopException.getFaultingAttributes().add(new FaultingAttribute(attributeName, desc));
                needsThrow = true;
                badAttributesCount++;
            }
        }

        throwArchivingStopException(archivingStopException, needsThrow, badAttributesCount);
    }

    /**
     * Sstop the archiving in the intermediate database
     *
     * @param attributeNameList The list of attributes (The archiving of each element of this list is about to be
     *            stopped)
     * @throws ArchivingException
     */
    @Override
    public void archivingStopConf(final String... attributeNameList) throws ArchivingException {
        final Map<String, List<AttributeLightMode>> myStoppingConf = new HashMap<String, List<AttributeLightMode>>();
        String attributeName = "";
        String archiverName = "";

        final List<String> notArchivedAttributes = getNotArchivedAttributes(attributeNameList);
        if (!notArchivedAttributes.isEmpty()) {
            throw new ArchivingException(notArchivedAttributes + " are not archived");
        }

        try {
            final IGenericMode genericMode = dataBase.getMode();
            for (final String element : attributeNameList) {
                attributeName = element;
                // get the Archiver that take in charge the archiving
                archiverName = genericMode.getDeviceInCharge(attributeName);

                if (!archiverName.equals("")) {

                    // return ArchivingManagerResult.INEXISTANT_ARCHIVER;
                    if (isArchived(attributeName)) {
                        final Mode mode = getArchivingMode(attributeName);
                        final AttributeLightMode attributeLightMode = new AttributeLightMode(attributeName);
                        attributeLightMode.setDevice_in_charge(archiverName);

                        /*
                         * AttributeInfo attributeInfo =
                         * getAttributeInfoFromDatabase(attributeName);
                         * attributeLightMode
                         * .setData_type(attributeInfo.data_type);
                         * attributeLightMode
                         * .setData_format(attributeInfo.data_format.value());
                         * attributeLightMode
                         * .setWritable(attributeInfo.writable.value());
                         */
                        final int[] tfw = dataBase.getAttribute().getAttTFWData(attributeName);
                        attributeLightMode.setDataType(tfw[0]);
                        attributeLightMode.setDataFormat(tfw[1]);
                        attributeLightMode.setWritable(tfw[2]);

                        attributeLightMode.setMode(mode);
                        if (!myStoppingConf.containsKey(archiverName)) {
                            myStoppingConf.put(archiverName, new ArrayList<AttributeLightMode>());
                        }

                        myStoppingConf.get(archiverName).add(attributeLightMode);
                    }
                }
            }

            for (final Entry<String, List<AttributeLightMode>> entry : myStoppingConf.entrySet()) {
                archiverName = entry.getKey();
                final DeviceProxy archiverProxy = new DeviceProxy(archiverName);
                final DeviceData device_data = new DeviceData();
                final String[] archiverStoppingConf = toStoppingArrayAttLightMode(entry.getValue());
                device_data.insert(archiverStoppingConf);
                archiverProxy.command_inout(m_ARCHIVINGSTOPGROUP, device_data);
            }

        } catch (final DevFailed devFailed) {
            final String message = GlobalConst.ERROR_ARCHIVINGSTOP + " : " + "Problems talking to the archiver "
                    + archiverName + " for the attribute " + attributeName;
            final String reason = GlobalConst.TANGO_COMM_EXCEPTION;
            final String desc = "Failed while executing ArchivingManagerApi.ArchivingStopConf() method...";

            final ArchivingException toBeThrown = new ArchivingException(message, reason, ErrSeverity.WARN, desc, "",
                    devFailed);
            toBeThrown.initCause(devFailed);
            throw toBeThrown;
        }
    }

    /**
     * get the archiving mode of an attribute.
     *
     * @param attributeName the complete name of the attribute
     * @return the mode
     */
    @Override
    public Mode getArchivingMode(final String attributeName) throws ArchivingException {
        // if (isRegistred(attributeName)) {
        // return m_DataBase.getMode().getCurrentArchivingMode(attributeName);
        // }
        // Avoid 2 requests
        final Mode mode = dataBase.getMode().getCurrentArchivingMode(attributeName);

        if (mode == null || mode.toArray().length == 0) {
            final StringBuilder buffer = new StringBuilder("No achiving mode for '");
            buffer.append(attributeName).append("'");
            throw new ArchivingException(buffer.toString());
        }
        return mode;
    }

    /**
     * return the attribute registred status
     *
     * @param attributeName The complete name of the attribute
     * @return true if the attribute has been archived
     */
    protected boolean isRegistred(final String attributeName) throws ArchivingException {
        if (dataBase != null) {
            return dataBase.getAttribute().isRegisteredADT(attributeName);
        }
        return false;
    }

    /**
     * return the attribute archiving status
     *
     * @param attributeName The complete name of the attribute
     * @return true if the attribute is being archived
     * @throws ArchivingException
     */
    @Override
    public boolean isArchived(final String attributeName) throws ArchivingException {
        if (dataBase == null || dataBase.getMode() == null) {
            return false;
        } else {
            return dataBase.getMode().isArchived(attributeName);
        }
    }

    /**
     * Use for ArchivingStop method, to know if all the list is already in
     * archiving
     *
     * @param attributeNameList
     * @return true if at least one attribute of the list is not in progress
     *         filing
     * @throws ArchivingException
     */
    private List<String> getNotArchivedAttributes(final String[] attributeNameList) throws ArchivingException {
        final List<String> notArchivedAttributes = new ArrayList<String>();
        for (final String attributeName : attributeNameList) {
            if (!isArchived(attributeName)) {
                notArchivedAttributes.add(attributeName);
            }
        }
        return notArchivedAttributes;
    }

    /**
     * Distribute attributes on archivers the archiver that will take in charge
     * the archiving
     *
     * @param grouped true if all attributes are associated to the same archiver
     * @param attributeNameList the attributes to distribute on archivers
     * @return A map with archivers name as keys, and attributes list as values
     * @throws ArchivingException
     */
    private LoadBalancedList selectArchiversTask(final boolean grouped, final Map<String, String> map,
            final String[] attributeNameList) throws ArchivingException {
        final Map<String, Archiver> attributeToDedicatedArchiver = getAttributesToDedicatedArchiver(false);// CLA
        // 10/11/06
        LOGGER.debug("Load balancing for attributes isGrouped = {} ", grouped);
        final LoadBalancedList loadBalancedList = new LoadBalancedList(historic);
        final String[] exportedArchiversList = getMExportedArchiverList();
        initArchivers(loadBalancedList, grouped, exportedArchiversList);

        // Attribute distribution
        for (final String currentAttributeName : attributeNameList) {
            final String chosenArchiver = map.get(currentAttributeName);
            final boolean theCurrentAttributeHasAChosenArchiver = chosenArchiver != null && !chosenArchiver.isEmpty();
            String dedicatedArchiver = null;
            boolean theCurrentAttributeHasADedicatedArchiver = false;

            if (attributeToDedicatedArchiver != null) {
                if (attributeToDedicatedArchiver.size() > 0) {
                    final Archiver archiver = attributeToDedicatedArchiver.get(currentAttributeName);
                    dedicatedArchiver = archiver == null ? null : archiver.getName();
                    if (dedicatedArchiver != null && !dedicatedArchiver.isEmpty()) {
                        theCurrentAttributeHasADedicatedArchiver = true;
                    }

                }
            }

            String archiver = "";

            // first let's find out whether the currentAttribute has already
            // been archived
            // if it has, we will use the same archiver
            archiver = dataBase.getMode().getArchiverForAttribute(currentAttributeName);
            LOGGER.debug("Load balancing - get archiver from archiving DB = {}", archiver);

            // if it hasn't, let's see if the currentAttribute has a
            // dedicatedArchiver
            if (archiver == null || "".equals(archiver)) {
                // si l'attribut n'est pas deja en cours d'archivage on regarde
                // s'il a un archiver reserve
                if (theCurrentAttributeHasADedicatedArchiver) {
                    LOGGER.debug("Load balancing - dedicatedArchiver is {}", dedicatedArchiver);
                    archiver = dedicatedArchiver;
                }

                if (archiver == null || "".equals(archiver)) {
                    // si l'attribut n'a pas un archiver reserve , on regarde si
                    // l'utilisateur a specifie un choisi
                    if (theCurrentAttributeHasAChosenArchiver) {
                        LOGGER.debug("Load balancing - chosenArchiver is {}", chosenArchiver);
                        archiver = chosenArchiver;
                    }
                    // sinon le load balancing se chargera de lui en trouver un.
                    else {
                        archiver = "";
                    }
                }
            }

            boolean canForce = false;
            if (!archiver.isEmpty()) {
                // On a determine sur quel archiver l'attribut devait aller:
                // on est dans un cas ou l'attribut a un archiver soit parce
                // qu'il est deja archive, ou parce qu'il a un archiver choisi
                // ou dedie
                // mais il faut encore verifier qu'on ne met pas un attribut
                // non-reserve sur un archiver dedie.
                // Par exemple si l'attribut etait archive par un archiver
                // non-dedie, mais que cet archiver est maintenant dedie:
                // l'attribut n'a pas le droit de rester dessus
                // Ou autre exemple l'archiver choisi est un archiver dedie
                // (meme si Mambo ne le permet plus, l'API doit faire le
                // controle independamment de l'interface graphique)

                // On regarde si l'archiver sur lequel on veut mettre l'attribut
                // est dedie.
                boolean isArchiverDedicated = false;
                if (attributeToDedicatedArchiver != null) {
                    // On parcourt la liste des archivers dedies, et pour chacun
                    // d'entre eux on regarde son nom:
                    // si ce nom = le nom recherche, alors l'archiver sur lequel
                    // on veut mettre l'attribut est un des archivers dedies.
                    for (final Archiver next : attributeToDedicatedArchiver.values()) {
                        if (next.getName() != null && next.getName().equals(archiver)) {
                            isArchiverDedicated = true;
                            break;
                        }
                    }
                }

                // Si oui (l'archiver sur lequel on veut mettre l'attribut est
                // dedie) il faut verifier que :
                // - l'attribut est reserve
                // - son archiver dedie est l'archiver choisi
                if (isArchiverDedicated) {
                    // Pour ce faire, on regarde si l'attribut a un archiver
                    // dedie, et si oui si c'est l'archiver determine plus tot
                    final boolean theCurrentAttributeIsRegisteredToThisDedicatedArchiver = dedicatedArchiver != null
                            && dedicatedArchiver.equals(archiver);
                    // Si oui, pas de probleme
                    if (theCurrentAttributeIsRegisteredToThisDedicatedArchiver) {
                        canForce = true;
                    }
                    // Sinon, on a indument tente d'affecter un attribut a un
                    // archiver dedie
                    // Il faut donc trouver un autre archiver a cet attribut
                    else {
                        canForce = false;
                    }
                } else {
                    canForce = true;
                }
            }

            if (canForce) {
                LOGGER.debug("forcing on archiver {}", archiver);
                try {
                    loadBalancedList.forceAddAtt2Archiver(currentAttributeName, archiver,
                            getAttributeDataFormat(currentAttributeName));
                } catch (final DevFailed e) {
                    throw new ArchivingException(e);
                }
            } else {
                LOGGER.debug("load balancing on an archiver");
                try {
                    loadBalancedList.addAtt2LBL(currentAttributeName, getAttributeDataFormat(currentAttributeName));
                } catch (final DevFailed e) {
                    throw new ArchivingException(e);
                }
            }
        }
        return loadBalancedList;
    }

    @Override
    public String[] getMNotExportedArchiverList() {
        try {
            archiverListRefresher.get();
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.error("failed to get archivers list", e);
        }
        return notExportedArchiverList;
    }

    @Override
    public void setMNotExportedArchiverList(final String... notExportedArchiverList) {
        this.notExportedArchiverList = notExportedArchiverList;
    }
}
