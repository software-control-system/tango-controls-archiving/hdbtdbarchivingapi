package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates;

import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

public final class DataGettersBetweenDatesFactory {

    private DataGettersBetweenDatesFactory() {
    }

    public static DataGettersBetweenDates getDataGatters(final AbstractDataBaseConnector connector) {
        DataGettersBetweenDates result;
        if (connector == null) {
            result = null;
        } else if (connector instanceof OracleDataBaseConnector) {
            result = new OracleDataGettersBetweenDates(connector);
        } else {
            result = new MySqlDataGettersBetweenDates(connector);
        }
        return result;
    }

}
