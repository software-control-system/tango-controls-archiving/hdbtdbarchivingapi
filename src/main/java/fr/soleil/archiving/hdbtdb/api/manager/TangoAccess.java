/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;

/**
 * @author AYADI
 * 
 */
public final class TangoAccess {

    /**
	 * 
	 */
    private TangoAccess() {
    }

    /*
     * /** Get the status of the archiver
     * 
     * @param archiverName
     * 
     * @return the status of the Archiver driver
     * 
     * @throws ArchivingException
     */
    public static String getDeviceStatus(final String archiverName) throws ArchivingException {
        try {
            final DeviceProxy archiverProxy = new DeviceProxy(archiverName);
            return archiverProxy.status();
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static String getDeviceState(final String archiverName) throws ArchivingException {
        try {
            final DeviceProxy archiverProxy = new DeviceProxy(archiverName);
            return TangoConst.Tango_DevStateName[archiverProxy.state().value()];
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    /**
     * Tests if the given device is alive
     * 
     * @param deviceName
     * @return true if the device is running
     */
    public static boolean deviceLivingTest(final String deviceName) {
        try {
            final DeviceProxy deviceProxy = new DeviceProxy(deviceName);
            deviceProxy.ping();
            return true;

        } catch (final DevFailed e) {
            return false;
        }
    }

    /**
     * PART Tango side queries
     */

    public static List<String> getDomains() throws ArchivingException {
        try {
            final Database database = ApiUtil.get_db_obj();
            final List<String> domains = new ArrayList<String>();
            final String[] res_domains = database.get_device_domain("*");
            for (int i = 0; i < res_domains.length; i++) {
                final String res_domain = res_domains[i];
                if (!res_domain.equals("dserver")) {
                    domains.add(res_domain);
                }
            }
            return domains;
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static List<String> getDomains(final String domainsRegExp) throws ArchivingException {
        try {
            final Database database = ApiUtil.get_db_obj();
            final List<String> domains = new ArrayList<String>();
            final String[] res_domains = database.get_device_domain(domainsRegExp /*
                                                                                   * +
                                                                                   * "*"
                                                                                   */);
            for (int i = 0; i < res_domains.length; i++) {
                final String res_domain = res_domains[i];
                if (!res_domain.equals("dserver")) {
                    domains.add(res_domain);
                }
            }
            return domains;
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static String[] getFamilies(final String domain) throws ArchivingException {
        try {
            final Database database = ApiUtil.get_db_obj();
            return database.get_device_family(domain + "/*");
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static String[] getMembers(final String domain, final String family) throws ArchivingException {
        try {
            final Database database = ApiUtil.get_db_obj();
            return database.get_device_member(domain + "/" + family + "/*");
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    /*
	 * 
	 */

    public static String getTangoHost() throws ArchivingException {
        String tangoHost = "Host:port";
        try {
            final Database db = new Database();
            tangoHost = db.get_tango_host();
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
        return tangoHost;
    }

    public static Map<String, List<String>> getClassAndDevices(final String domain) throws ArchivingException {
        try {

            final Database database = ApiUtil.get_db_obj();
            final List<String> devicesCapitalised = new ArrayList<String>();
            final Map<String, List<String>> deviceClassToDevices = new HashMap<String, List<String>>();

            final String[] families = getFamilies(domain);
            for (int i = 0; i < families.length; i++) {
                final String[] members = getMembers(domain, families[i]);

                for (int j = 0; j < members.length; j++) {
                    final String newDevice = domain + "/" + families[i] + "/" + members[j];
                    devicesCapitalised.add(newDevice.toUpperCase());

                    final String dServer = database.get_device_info(newDevice).server;
                    final String[] res = database.get_device_class_list(dServer);

                    for (int k = 0; k < res.length / 2; k++) {
                        final String device = res[2 * k];
                        final String deviceClass = res[2 * k + 1];
                        if (devicesCapitalised.indexOf(device.toUpperCase()) == -1) {
                            continue;
                        }

                        List<String> devicesForTheCurrentDeviceClass = deviceClassToDevices.get(deviceClass);
                        if (devicesForTheCurrentDeviceClass == null) {
                            devicesForTheCurrentDeviceClass = new ArrayList<String>();
                            deviceClassToDevices.put(deviceClass, devicesForTheCurrentDeviceClass);
                        }

                        devicesForTheCurrentDeviceClass.add(device);
                    }
                }
            }

            return deviceClassToDevices;
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static String[] getAttributesForClass(final String classe, final List<String> devOfDomain)
            throws ArchivingException {
        String[] exportedDevices;
        String[] attributeList = new String[5];
        try {
            final Database database = ApiUtil.get_db_obj();
            DeviceProxy deviceProxy;
            exportedDevices = database.get_device_exported_for_class(classe);
            for (int i = 0; i < exportedDevices.length; i++) {
                final String current_dev = exportedDevices[i];
                if (devOfDomain.contains(current_dev)) {
                    try {
                        deviceProxy = new DeviceProxy(current_dev);
                        attributeList = deviceProxy.get_attribute_list();
                        break;
                    } catch (final DevFailed devFailed) {// ignore
                    }
                }
            }
            return attributeList;
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

}
