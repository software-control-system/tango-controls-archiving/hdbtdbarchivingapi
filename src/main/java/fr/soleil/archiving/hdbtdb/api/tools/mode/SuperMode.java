// +======================================================================
// $Source: /cvsroot/tango-cs/tango/jserver/archiving/TdbArchiver/Collector/Tools/SuperMode.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SuperMode.
//						(Chinkumo Jean) - Apr 27, 2004
//
// $Author: chinkumo $
//
// $Revision: 1.3 $
//
// $Log: SuperMode.java,v $
// Revision 1.3  2005/11/29 17:34:14  chinkumo
// no message
//
// Revision 1.2.14.3  2005/11/29 16:15:11  chinkumo
// Code reformated (pogo compatible)
//
// Revision 1.2.14.2  2005/11/15 13:45:38  chinkumo
// ...
//
// Revision 1.2.14.1  2005/09/26 08:01:54  chinkumo
// Minor changes !
//
// Revision 1.2  2005/02/04 17:10:38  chinkumo
// The trouble with the grouped stopping strategy was fixed.
//
// Revision 1.1  2004/12/06 16:43:25  chinkumo
// First commit (new architecture).
//
// Revision 1.3  2004/09/01 15:52:05  chinkumo
// Heading was updated.
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.archiving.hdbtdb.api.tools.mode;

import fr.soleil.lib.project.ObjectUtils;

public class SuperMode {
    private final int format;
    private final int type;
    private final int writable;
    private Mode mode = null;

    public SuperMode(int format, int type, int writable, Mode mode) {
        this.format = format;
        this.type = type;
        this.writable = writable;
        this.mode = mode;
    }

    @Override
    public boolean equals(Object o) {
        boolean equals;
        if (o == null) {
            equals = false;
        } else if (o == this) {
            equals = true;
        } else if (o.getClass().equals(getClass())) {
            final SuperMode superMode = (SuperMode) o;
            equals = (format == superMode.format) && (type == superMode.type) && (writable == superMode.writable)
                    && ObjectUtils.sameObject(mode, superMode.mode);
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int code = 0x509E12;
        int mult = 0x30DE;
        code = code * mult + getClass().hashCode();
        code = code * mult + format;
        code = code * mult + type;
        code = code * mult + writable;
        code = code * mult + ObjectUtils.getHashCode(mode);
        return code;
    }
}
