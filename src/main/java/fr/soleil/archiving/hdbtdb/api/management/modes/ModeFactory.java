/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.modes;

import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class ModeFactory {

    private ModeFactory() {
    }

    public static IGenericMode getInstance(final AbstractDataBaseConnector connector, boolean hdb) {
        return hdb ? new HdbMode(connector) : new TdbMode(connector);
    }

}
