/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.manager;

import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 *
 */
public class HdbArchivingManagerApiRef extends ArchivingManagerApiRef {
    public static final String CLASS_DEVICE = System.getProperty("hdb.archiver.class", ConfigConst.HDB_CLASS_DEVICE);
    private static final String HDB_ARCHIVER_FINDER = System.getProperty("hdb.archiver.finder", FINDER_CLASS);
    private static final String HDB_DEVICE_PATTERN = System.getProperty("hdb.archiver.devicepattern",
            "archiving/hdbarchiver/*");
    private boolean facility = false;

    /**
     *
     */
    public HdbArchivingManagerApiRef(AbstractDataBaseConnector connector) {
        super(true, connector);

    }

    @Override
    protected String getHost() {
        return ConfigConst.HDB_HOST;
    }

    @Override
    protected String getPassword() {
        return ConfigConst.HDB_BROWSER_PASSWORD;
    }

    @Override
    protected String getSchema() {
        return ConfigConst.HDB_SCHEMA_NAME;
    }

    @Override
    protected String getUser() {
        return ConfigConst.HDB_BROWSER_USER;
    }

    /*
     * (non-Javadoc)
     *
     * @seefr.soleil.hdbtdbArchivingApi.ArchivingManagerApi.TangoDataBaseApi.
     * TangoDataBaseApi#geDevicePattern()
     */
    @Override
    protected String geDevicePattern() {
        return HDB_DEVICE_PATTERN;
    }

    @Override
    public String getClassDevice() {
        return CLASS_DEVICE;
    }

    @Override
    protected String getDB() {
        return "Hdb";
    }


    @Override
    public boolean isMFacility() {
        return facility;
    }


    @Override
    public void setMFacility(final boolean facility) {
        this.facility = facility;
    }


    @Override
    public boolean isFinderClass() {
        return HDB_ARCHIVER_FINDER.equalsIgnoreCase(FINDER_CLASS);
    }

}
