/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.modes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class HdbMode extends GenericMode {

    /**
     * @param c
     */
    public HdbMode(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    public StringBuilder updateSelectField(final StringBuilder selectFieldBuilder) {
        return selectFieldBuilder;
    }

    @Override
    public void setSpec(final Mode mode, final ResultSet rset) {

    }

    @Override
    public void appendQuery(final StringBuilder query, final StringBuilder tableName, final StringBuilder selectField) {
        query.append("INSERT INTO ").append(tableName).append(" (").append(selectField).append(")")
                .append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    }

    @Override
    protected void setSpecificStatementForInsertMode(final PreparedStatement preparedStatement,
            final AttributeLightMode attributeLightMode) {
    }

}
