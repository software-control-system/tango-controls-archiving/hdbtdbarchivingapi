/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.database.tdb.data.export;

import java.sql.Timestamp;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.GetArchivingConf;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.modes.IGenericMode;
import fr.soleil.archiving.hdbtdb.api.management.modes.ModeFactory;
import fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.manager.TangoAccess;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.lib.project.ObjectUtils;
import org.tango.utils.DevFailedUtils;

/**
 * @author AYADI
 *
 */
public abstract class TdbDataExport implements ITdbDataExport {
    public static final String EXPORT_PERIOD_PROPERTY = "ExportPeriod";
    public static final int DEFAULT_EXPORT_PERIOD_VALUE = 1800000;
    private static final String ARCHIVINGEXPORT = "ExportData2Db";
    private static final long DEFAULT_EXPORT_PERIOD_PRECISION_VALUE = 900;
    protected AbstractDataBaseConnector connector;
    protected Logger logger = LoggerFactory.getLogger(TdbDataExport.class);

    public TdbDataExport(final AbstractDataBaseConnector connector) {
        this.connector = connector;
    }

    @Override
    public abstract void exportToDB_Data(String remoteDir, String fileName, String tableName, int writable)
            throws ArchivingException;

    @Override
    public abstract void forceDatabaseToImportFile(String tableName) throws ArchivingException;

    /**
     * Export for a given attribute, its file stored data to the database if the selected end date is after the actual
     * date minus export period and the precision value, usually 45 min(30min as the export period and 15min as a
     * precision value).
     *
     * @param attributeName The name of the given attribute
     * @param endDate The end Date of extracting data.
     */
    @Override
    public void ExportData2Tdb(final String attributeName, final String endDate) throws ArchivingException {
        logger.debug("in ExportData2Tdb for {} at {}", attributeName, endDate);
        final IAdtAptAttributes attr = AdtAptAttributesFactory.getInstance(connector);
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if (connector != null && attr != null && dbUtils != null) {
            final IGenericMode m = ModeFactory.getInstance(connector, false);
            Mode mode = null;
            if (attr.isRegisteredADT(attributeName)) {
                mode = m.getCurrentArchivingMode(attributeName);
            }
            if (mode != null && mode.getTdbSpec() != null
                    && Timestamp.valueOf(endDate)
                    .after(new Timestamp(
                            System.currentTimeMillis() - mode.getTdbSpec().getExportPeriod()
                                    - GetArchivingConf.readLongInDB(ConfigConst.TDB_CLASS_DEVICE,
                                    "ExportPeriodPrecision", DEFAULT_EXPORT_PERIOD_PRECISION_VALUE)
                                    * 1000))) {
                String archiverName = ObjectUtils.EMPTY_STRING;
                try {
                    if (m.isArchived(attributeName)) {
                        // get the Archiver that take in charge the archiving
                        archiverName = m.getDeviceInCharge(attributeName);
                        if (archiverName == ObjectUtils.EMPTY_STRING || !TangoAccess.deviceLivingTest(archiverName)) {
                            throw new ArchivingException(GlobalConst.ARC_UNREACH_EXCEPTION,
                                    GlobalConst.ARC_UNREACH_EXCEPTION, ErrSeverity.ERR,
                                    GlobalConst.ARC_UNREACH_EXCEPTION, GlobalConst.ARC_UNREACH_EXCEPTION);
                        }

                        final String tableName = dbUtils.getTableName(attributeName);
                        final int[] tfw = AdtAptAttributesFactory.getInstance(connector).getAttTFWData(attributeName);
                        final AttributeLightMode attributeLightMode = new AttributeLightMode(attributeName);
                        attributeLightMode.setDevice_in_charge(archiverName);
                        attributeLightMode.setDataType(tfw[0]);
                        attributeLightMode.setDataFormat(tfw[1]);
                        attributeLightMode.setWritable(tfw[2]);
                        attributeLightMode.setMode(mode);
                        final DeviceProxy archiverProxy = new DeviceProxy(archiverName);
                        final String[] attributeLightModeArray = attributeLightMode.toArray();
                        logger.debug("Export data on archiver {} for {}", archiverName, Arrays.toString(attributeLightModeArray));
                        final DeviceData device_data = new DeviceData();
                        device_data.insert(attributeLightModeArray);

                        // launch ExportData2DbCmd for the MySQL DataBase
                        archiverProxy.command_inout(ARCHIVINGEXPORT, device_data);
                        forceDatabaseToImportFile(tableName);
                    }
                } catch (final DevFailed devFailed) {
                    logger.error("error export file ", devFailed);
                    logger.error("error export file {}", DevFailedUtils.toString(devFailed));
                    throw new ArchivingException(devFailed);
                }
            }
        }
    }
}
