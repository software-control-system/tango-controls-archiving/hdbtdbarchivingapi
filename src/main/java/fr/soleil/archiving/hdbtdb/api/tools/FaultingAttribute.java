package fr.soleil.archiving.hdbtdb.api.tools;

/**
 * Bean representing an attribute that caused an ArchivingException to be thrown
 * 
 * @author awo
 * 
 */
public final class FaultingAttribute {

    private String name;
    private String cause;

    // Constructor
    public FaultingAttribute(final String name, final String cause) {
        this.name = name;
        this.cause = cause;
    }

    public String getCause() {
        return cause;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setCause(final String cause) {
        this.cause = cause;
    }

    @Override
    public String toString() {
        final StringBuilder buffer = new StringBuilder();
        buffer.append("Attribute name: ");
        buffer.append(this.name);
        buffer.append("\nError reason: ");
        buffer.append(this.cause);
        return buffer.toString();
    }
}
