// +======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/ScalarEvent_WO.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ScalarEvent_WO.
//						(Chinkumo Jean) - Mar 10, 2004
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: ScalarEvent_WO.java,v $
// Revision 1.4  2006/03/10 11:31:00  ounsy
// state and string support
//
// Revision 1.3  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.2.16.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.archiving.hdbtdb.api.tools;

import fr.soleil.archiving.common.api.tools.ArchivingEvent;
import fr.soleil.lib.project.math.MathConst;

public class ScalarEvent_WO extends ArchivingEvent<Boolean> {

    /**
     * Creates a new instance of ScalarEvent_WO
     */
    public ScalarEvent_WO() {
        super();
    }

    /**
     * Creates a new instance of ScalarEvent_WO
     */
    public ScalarEvent_WO(String[] hdbScalarEventRO) {
        this.setAttributeCompleteName(hdbScalarEventRO[0]);
        this.setTimeStamp(Long.parseLong(hdbScalarEventRO[1]));
        this.setScalarValue(
                hdbScalarEventRO[2] == null ? MathConst.NAN_FOR_NULL : Double.parseDouble(hdbScalarEventRO[2]),
                Boolean.valueOf(hdbScalarEventRO[2] == null));
    }

    public void setScalarValue(double value, Boolean nullElements) {
        setValue(Double.valueOf(value), nullElements);
    }

    public void setScalarValue(String value, Boolean nullElements) {
        setValue(new String(value), nullElements);
    }

    public double getScalarValue() {
        return ((Double) getValue()).doubleValue();
    }

    public String getScalarValueS() {
        return (String) getValue();
    }

    @Override
    public String[] toArray() {
        String[] scalarEventRO = new String[3];
        scalarEventRO[0] = getAttributeCompleteName().trim(); // name
        scalarEventRO[1] = Long.toString(getTimeStamp()).trim(); // time
        if (getValue() instanceof Double) {
            double value = ((Double) getValue()).doubleValue();
            scalarEventRO[2] = Double.toString(value).trim(); // value
        } else {
            scalarEventRO[2] = getScalarValueS().trim(); // value
        }
        return scalarEventRO;

    }

    @Override
    public String toString() {
        StringBuilder scalarEventROString = new StringBuilder();

        scalarEventROString.append("Source : \t").append(getAttributeCompleteName()).append("\r\n")
                .append("TimeSt : \t").append(getTimeStamp()).append("\r\n").append("Value : \t").append(getValue())
                .append("\r\n");

        return scalarEventROString.toString();
    }
}
