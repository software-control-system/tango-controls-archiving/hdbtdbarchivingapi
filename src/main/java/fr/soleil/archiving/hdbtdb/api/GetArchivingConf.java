// +============================================================================
// $ Source: package fr.soleil.TangoArchiving.HdbApi;/GetConf.java
//
// project : ArchivingDev
//
// Description: This class hides
//
// $Author: ounsy $
//
// $Revision: 1.6 $
//
// $Log: GetConf.java,v $
// Revision 1.6 2006/05/04 14:28:09 ounsy
// minor changes (commented useless methods and variables)
//
// Revision 1.5 2005/11/29 17:11:17 chinkumo
// no message
//
// Revision 1.4.10.1 2005/11/15 13:34:38 chinkumo
// no message
//
// Revision 1.4 2005/06/24 12:06:11 chinkumo
// Some constants were moved from
// fr.soleil.hdbtdbArchivingApi.ArchivingApi.ConfigConst to
// fr.soleil.hdbtdbArchivingApi.ArchivingTools.Tools.GlobalConst.

// This change was reported here.
//
// Revision 1.3 2005/06/14 10:12:12 chinkumo
// Branch (tangORBarchiving_1_0_1-branch_0) and HEAD merged.
//
// Revision 1.2.4.2 2005/06/13 15:20:52 chinkumo
// Changes made to improve the management of Exceptions were reported here.
//
// Revision 1.2.4.1 2005/05/12 16:46:18 chinkumo
// A check control is done to be sure that the property being retrieved is
// defined into the static database.
//
// Revision 1.2 2005/01/26 15:35:37 chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1 2004/12/06 17:39:56 chinkumo
// First commit (new API architecture).
//
//
// copyleft :Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
// FRANCE
//
// +============================================================================
/**
 * <B>File</B> : GetConf.java<br/>
 * <B>Project</B> : HDB configuration java classes (hdbconfig package)<br/>
 * <B>Description</B> : This file contains all the values customizd by the
 * cliebt<br/>
 * <B>Original</B> : Mar 4, 2003 - 6:13:47 PM
 * 
 * @author ho
 * @version $Revision: 1.6 $
 */

package fr.soleil.archiving.hdbtdb.api;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.utils.GetConf;

public class GetArchivingConf {

    private static final String TYPE_PROPERTY = "dbType";
    private static final String FACITILY_PROPERTY = "facility";

    /**
     * return the host property define for the given class
     * 
     * @param className
     *            , the name of the class
     * @throws ArchivingException
     */
    public static String getHost(final String className) throws ArchivingException {
        return readStringInDB(className, GetConf.HOST_PROPERTY);
    }

    /**
     * return the name property define for the given class
     * 
     * @param className
     *            , the name of the class
     * @throws ArchivingException
     */
    public static String getName(final String className) throws ArchivingException {
        return readStringInDB(className, GetConf.NAME_PROPERTY);
    }

    /**
     * return the name property define for the given class
     * 
     * @param className
     *            , the name of the class
     * @throws ArchivingException
     */
    public static String getSchema(final String className) throws ArchivingException {
        return readStringInDB(className, GetConf.SCHEMA_PROPERTY);
    }

    public static String readStringInDB(final String className, final String propertyName) throws ArchivingException {
        try {
            return GetConf.readStringInDB(className, propertyName);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static String[] readStringArrayInDB(final String className, final String propertyName)
            throws ArchivingException {
        try {
            return GetConf.readStringArrayInDB(className, propertyName);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static boolean readBooleanInDB(final String className, final String propertyName) throws ArchivingException {
        try {
            return GetConf.readBooleanInDB(className, propertyName);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static boolean readBooleanObjectInDB(final String className, final String propertyName)
            throws ArchivingException {
        try {
            return GetConf.readBooleanObjectInDB(className, propertyName);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static short readShortInDB(final String className, final String propertyName, final short defaultValue)
            throws ArchivingException {
        try {
            return GetConf.readShortInDB(className, propertyName, defaultValue);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static long readLongInDB(final String className, final String propertyName, final long defaultValue)
            throws ArchivingException {
        try {
            return GetConf.readLongInDB(className, propertyName, defaultValue);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    /**
     * return the name property define for the given class
     * 
     * @param className
     *            , the name of the class
     * @throws ArchivingException
     */
    public static int getType(final String className) throws ArchivingException {
        final String propname = TYPE_PROPERTY;
        try {
            final Database dbase = ApiUtil.get_db_obj();
            int property = ConfigConst.BD_MYSQL;
            final DbDatum dbdatum = dbase.get_class_property(className, propname);
            if (!dbdatum.is_empty()) {
                property = dbdatum.extractLong();
            }
            return property;
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    /**
     * return the facility property define for the given class
     * 
     * @param className
     *            , the name of the class
     * @throws ArchivingException
     */
    public static boolean getFacility(final String className) throws ArchivingException {
        String propname = FACITILY_PROPERTY;
        try {
            final Database dbase = ApiUtil.get_db_obj();
            boolean property = false;
            propname = FACITILY_PROPERTY;
            final DbDatum dbdatum = dbase.get_class_property(className, propname);
            if (!dbdatum.is_empty()) {
                property = dbdatum.extractBoolean();
            }
            return property;
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }
}
