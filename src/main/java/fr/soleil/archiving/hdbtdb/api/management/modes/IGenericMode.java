package fr.soleil.archiving.hdbtdb.api.management.modes;

import java.util.Collection;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;

public interface IGenericMode {
    String[] getCurrentArchivedAtt() throws ArchivingException;

    String[] getAttribute(boolean archivingOnly) throws ArchivingException;

    int getCurrentArchivedAttCount() throws ArchivingException;

    String getDeviceInCharge(String attributName) throws ArchivingException;

    boolean isArchived(String attributeName) throws ArchivingException;

    boolean isArchived(String attributeName, String deviceName) throws ArchivingException;

    String getArchiverForAttribute(String attributeName) throws ArchivingException;

    Mode getCurrentArchivingMode(String attributeName) throws ArchivingException;

    Collection<AttributeLightMode> getArchiverCurrentTasks(String archiverName) throws ArchivingException;

    void insertModeRecord(AttributeLightMode attributeLightMode) throws ArchivingException;

    void updateModeRecord(String attributeName) throws ArchivingException;

    void updateModeRecordWithoutStop(final AttributeLightMode attributeLightMode) throws ArchivingException;

}
