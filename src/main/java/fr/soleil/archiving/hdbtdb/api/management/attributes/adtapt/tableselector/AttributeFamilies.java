package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class AttributeFamilies extends AttributesData {

    public AttributeFamilies(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description : </b> Gets all the registered families.
     *
     * @return All the registered families (array of strings).
     * @throws ArchivingException
     */
    public String[] getFamilies() throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = new String[0];
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT DISTINCT ").append(ConfigConst.FAMILY).append(" FROM ")
					.append(connector.getSchema()).append(".").append(ConfigConst.ADT).append(" WHERE ")
                    .append(ConfigConst.FAMILY).append(" IS NOT NULL").toString();
            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    /**
     * <b>Description : </b> Returns the number of distinct registered families.
     *
     * @return the number of distinct registered families.
     * @throws ArchivingException
     */
    public int getFamiliesCount() throws ArchivingException {
        int result;
        if (connector == null) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT COUNT(DISTINCT ").append(ConfigConst.FAMILY)
					.append(") FROM ").append(connector.getSchema()).append(".").append(ConfigConst.ADT)
                    .append(" WHERE ").append(ConfigConst.FAMILY).append(" IS NOT NULL").toString();
            result = getIntAttributeData(sqlStr);
        }
        return result;
    }

    /**
     * <b>Description : </b> Gets all the registered families for the given
     * domain
     *
     * @param domainName the given domain
     * @return array of strings
     * @throws ArchivingException
     */
    public String[] getFamilies(final String domainName) throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = new String[0];
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT DISTINCT ").append(ConfigConst.FAMILY).append(" FROM ")
					.append(connector.getSchema()).append(".").append(ConfigConst.ADT).append(" WHERE ")
					.append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domainName)).toString();
            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    public String[] getFamiliesByCriterion(final String domain_name, final String family_regexp)
            throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = null;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT DISTINCT ").append(ConfigConst.FAMILY).append(" FROM ")
					.append(connector.getSchema()).append(".").append(ConfigConst.ADT)
                    .append(" WHERE ")
                    .append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domain_name))
                    .append(" AND ")
                    .append(compareLikeCaseInsensitive(ConfigConst.FAMILY, family_regexp)).toString();
            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    /**
     * <b>Description : </b> Returns the number of distinct registered families
     * for a given domain.
     *
     * @param domainName the given domain
     * @return the number of distinct registered families for a given domain.
     * @throws ArchivingException
     */
    public int getFamiliesCount(final String domainName) throws ArchivingException {
        int result;
        if (connector == null) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            final StringBuilder sqlStr = new StringBuilder("SELECT COUNT(DISTINCT ").append(ConfigConst.FAMILY)
					.append(") FROM ").append(connector.getSchema()).append(".").append(ConfigConst.ADT)
                    .append(" WHERE ")
            		.append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domainName));
            
            result = getIntAttributeData(sqlStr.toString());
        }
        return result;
    }
}
