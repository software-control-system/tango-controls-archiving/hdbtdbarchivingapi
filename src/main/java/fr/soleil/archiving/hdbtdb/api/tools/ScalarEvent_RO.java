// +======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/ScalarEvent_RO.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ScalarEvent_RO.
//						(Chinkumo Jean) - Mar 10, 2004
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: ScalarEvent_RO.java,v $
// Revision 1.5  2006/10/31 16:54:23  ounsy
// milliseconds and null values management
//
// Revision 1.4  2006/03/10 11:31:00  ounsy
// state and string support
//
// Revision 1.3  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.2.16.2  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.2.16.1  2005/09/09 08:40:09  chinkumo
// The management of the boolean type was added.
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.archiving.hdbtdb.api.tools;

import fr.soleil.archiving.common.api.tools.ArchivingEvent;
import fr.soleil.lib.project.math.MathConst;

public class ScalarEvent_RO extends ArchivingEvent<Boolean> {

    /**
     * Creates a new instance of ScalarEvent_RO
     */
    public ScalarEvent_RO() {
        super();
    }

    /**
     * Creates a new instance of ScalarEvent_RO
     */
    public ScalarEvent_RO(String[] scalarEventRO) {
        this.setAttributeCompleteName(scalarEventRO[0]);
        this.setTimeStamp(Long.parseLong(scalarEventRO[1]));
        this.setScalarValue(scalarEventRO[2] == null ? MathConst.NAN_FOR_NULL : Double.parseDouble(scalarEventRO[2]),
                Boolean.valueOf(scalarEventRO[2] == null));
    }

    public void setScalarValue(double value, Boolean nullElements) {
        setValue(Double.valueOf(value), nullElements);
    }

    public void setScalarValue(boolean value, Boolean nullElements) {
        setValue(Boolean.valueOf(value), nullElements);
    }

    public void setScalarValue(String value, Boolean nullElements) {
        setValue(String.valueOf(value), nullElements);
    }

    public Double getScalarValue() {
        return (Double) getValue();
    }

    public Boolean getScalarValueB() {
        return (Boolean) getValue();
    }

    public String getScalarValueS() {
        return (String) getValue();
    }

    @Override
    public String[] toArray() {
        // double value = ( ( Double ) getValue() ).doubleValue();
        String[] scalarEventRO = new String[3];
        scalarEventRO[0] = getAttributeCompleteName().trim(); // name
        scalarEventRO[1] = Long.toString(getTimeStamp()).trim(); // time
        if (getValue() instanceof Double) {
            scalarEventRO[2] = Double.toString(((Double) getValue()).doubleValue()).trim(); // value
        } else if (getValue() instanceof String) {
            scalarEventRO[2] = ((String) getValue()).trim(); // value
        } else {
            scalarEventRO[2] = getValue().toString().trim(); // value
        }
        return scalarEventRO;
    }

    @Override
    public String toString() {
        StringBuilder scalarEventROString = new StringBuilder();

        scalarEventROString.append("Source : \t").append(getAttributeCompleteName()).append("\r\n")
                .append("TimeSt : \t").append(getTimeStamp()).append("\r\n").append("Value : \t").append(getValue())
                .append("\r\n");

        return scalarEventROString.toString();
    }
}
