/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;

import org.slf4j.Logger;

import fr.esrf.Tango.AttrDataFormat;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.DataExtractor;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.lib.project.date.DateUtil;

/**
 * @author AYADI
 *
 */
public abstract class DataGettersBetweenDates extends DataExtractor {

    private final Logger logger = org.slf4j.LoggerFactory.getLogger(DataGettersBetweenDates.class);

    /**
     * @param con
     * @param ut
     * @param at
     */
    public DataGettersBetweenDates(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    protected abstract String getAttScalarDataRequest(SamplingType samplingType, boolean roFields, String tableName,
            String time0, String time1, int tangoType) throws ArchivingException;

    protected abstract DbData[] getAttSpectrumDataBetweenDates(String attributeName, String time0, String time1,
            SamplingType samplingFactor, DbData... dbData) throws ArchivingException;

    /**
     * <b>Description : </b> Returns the number of data between two dates for a given attribute.
     *
     * @param argin The attribute and dates, organized this way:
     *            <ol start="0">
     *            <li>attribute complete name</li>
     *            <li>beginning date <i><small>(DD-MM-YYYY HH24:MI:SS.FF)</small></i></li>
     *            <li>ending date <i><small>(DD-MM-YYYY HH24:MI:SS.FF)</small></i></li>
     *            </ol>
     *
     * @return The number of data between two dates for given attribute.
     *
     * @throws ArchivingException If a problem occurred.
     */
    public int getAttDataBetweenDatesCount(final String... argin) throws ArchivingException {
        final int result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if (connector == null || dbUtils == null) {
            result = 0;
        } else {
            final String getAttributeDataQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(argin[0].trim()))
                    .append(" WHERE (").append(ConfigConst.TIME).append(" BETWEEN ")
                    .append(dbUtils.toDbTimeString(argin[1].trim())).append(" AND ")
                    .append(dbUtils.toDbTimeString(argin[2].trim())).append(")").toString();

            result = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return result;
    }

    private DbData[] getAttScalarDataBetweenDates(final String attributeName, final String time0, final String time1,
            final SamplingType samplingType, DbData... dbData) throws ArchivingException {
        final long t1 = System.currentTimeMillis();
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (att == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1, dataType = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    dataType = data.getDataType();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;

            // Create and execute the SQL query string
            String query;
            // final String fields = "";
            final String tableName = connector.getSchema() + DB_SEPARATOR + dbUtils.getTableName(attributeName);
            query = getAttScalarDataRequest(samplingType, roFields, tableName, time0, time1, dataType);

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    stmt.setFetchSize(1000);
                    rset = stmt.executeQuery(query);
                    methods.treatStatementResultForGetScalarData(rset, result);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);

            }
        }
        final long t2 = System.currentTimeMillis();
        logger.info(
                DateUtil.elapsedTimeToStringBuilder(new StringBuilder("getAttScalarDataBetweenDates time is "), t2 - t1)
                        .append(" for ").append(attributeName).toString());
        return result;
    }

    public DbData[] getAttImageDataBetweenDates(final String... argin) throws ArchivingException {
        final DbData[] dbData;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final String attributeName = argin[0].trim();
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[1].trim());
            final String time1 = dbUtils.getTime(argin[2].trim());
            final int dataFormat = att.getAttDataFormat(attributeName);
            dbData = DbData.initExtractionResult(attributeName, att.getAttDataWritable(attributeName), dataFormat,
                    att.getAttDataType(attributeName));

            switch (dataFormat) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    methods.makeDataException(dataFormat, IMAGE, SCALAR);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, IMAGE, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    getAttImageDataBetweenDates(attributeName, time0, time1,
                            SamplingType.getSamplingType(SamplingType.ALL), dbData);
                    break;
            }
        }
        return dbData;
    }

    private DbData[] getAttImageDataBetweenDates(final String attributeName, final String time0, final String time1,
            final SamplingType samplingType, final DbData... dbData) throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            StringBuilder queryBuilder;
            final boolean isBothReadAndWrite = !isROFields(writable);
            if (samplingType.hasSampling()) {
                final String format = samplingType.getMySqlFormat();

                queryBuilder = new StringBuilder("SELECT ");
                if (isBothReadAndWrite) {
                    queryBuilder.append(dbUtils.toDbTimeFieldString("T." + ConfigConst.TIME, format))
                            .append(", AVG (T.").append(ConfigConst.DIM_X).append("), AVG (T.")
                            .append(ConfigConst.DIM_Y).append("), to_clob ( MIN ( to_char (T.")
                            .append(ConfigConst.READ_VALUE).append(") ) )");
                    queryBuilder.append(VALUE_SEPARATOR).append("to_clob ( MIN ( to_char (").append("T")
                            .append(DB_SEPARATOR).append(ConfigConst.WRITE_VALUE).append(") ) )");
                } else {
                    queryBuilder.append(dbUtils.toDbTimeFieldString("T." + ConfigConst.TIME, format))
                            .append(", AVG (T.").append(ConfigConst.DIM_X).append("), AVG (T.")
                            .append(ConfigConst.DIM_Y).append("), to_clob ( MIN ( to_char (T.")
                            .append(ConfigConst.VALUE).append(") ) )");
                }
                queryBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                        .append(dbUtils.getTableName(attributeName)).append(" T").append(" WHERE ")
                        .append(ConfigConst.TIME).append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim()))
                        .append(" AND ").append(dbUtils.toDbTimeString(time1.trim())).append(" GROUP BY ")
                        .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format)).append(" ORDER BY ")
                        .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format));
            } else {

                queryBuilder = new StringBuilder("SELECT ");
                if (isBothReadAndWrite) {
                    queryBuilder.append("T.").append(ConfigConst.TIME).append(", T.").append(ConfigConst.DIM_X)
                            .append(", T.").append(ConfigConst.DIM_Y).append(", T.").append(ConfigConst.READ_VALUE);
                    queryBuilder.append(", T.").append(ConfigConst.WRITE_VALUE);
                } else {
                    queryBuilder.append("T.").append(ConfigConst.TIME).append(", T.").append(ConfigConst.DIM_X)
                            .append(", T.").append(ConfigConst.DIM_Y).append(", T.").append(ConfigConst.VALUE);
                }
                queryBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                        .append(dbUtils.getTableName(attributeName)).append(" T").append(" WHERE ").append("(");
                if (isBothReadAndWrite) {
                    queryBuilder.append("T.").append(ConfigConst.TIME).append(" BETWEEN ")
                            .append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                            .append(dbUtils.toDbTimeString(time1.trim()));
                } else {
                    queryBuilder.append("T.").append(ConfigConst.TIME).append(" BETWEEN ")
                            .append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                            .append(dbUtils.toDbTimeString(time1.trim()));
                }
                queryBuilder.append(") ORDER BY time");
            }
            final String query = queryBuilder.toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    rset = stmt.executeQuery(query);
                    result = methods.treatStatementResultForGetImageData(rset, attributeName, result);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;
    }

    /**
     * <b>Description : </b> Retrieves data between two dates, for a given
     * scalar attribute.
     *
     * @param argin The attribute's name, the beginning date (DD-MM-YYYY HH24:MI:SS.FF) and the ending date (DD-MM-YYYY
     *            HH24:MI:SS.FF).
     * @param samplingFactor
     * @return The scalar data for the specified attribute <br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataBetweenDates(final SamplingType samplingFactor, final String... argin)
            throws ArchivingException {
        // final long t1 = System.currentTimeMillis();
        final DbData[] dbData;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        final String attributeName = argin[0].trim();
        if ((connector == null) || (dbUtils == null)) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final String time0 = dbUtils.getTime(argin[1]);
            final String time1 = dbUtils.getTime(argin[2]);

            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);

            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataBetweenDates(attributeName, time0, time1, samplingFactor, dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    getAttSpectrumDataBetweenDates(attributeName, time0, time1, samplingFactor, dbData);
                    break;
                case AttrDataFormat._IMAGE:
                    getAttImageDataBetweenDates(attributeName, time0, time1, samplingFactor, dbData);
                    break;
                default:
                    methods.makeDataException(tfw[1], SCALAR, IMAGE);
            }
        }
        return dbData;
    }

    /**
     *
     * @param attributeName
     * @param time
     * @return
     * @throws ArchivingException
     */
    public double[][] getAttImageDataForDate(final String attributeName, final String time) throws ArchivingException {
        double[][] dvalueArr = null;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if (connector != null && dbUtils != null) {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string

            final String query = new StringBuilder("SELECT ").append(ConfigConst.VALUE).append(" FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE (").append(ConfigConst.TIME).append(" = '").append(time.trim())
                    .append("') ORDER BY time").toString();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(query);
                    if (!isCanceled() && rset.next()) {
                        // Value
                        String valueReadSt = null;
                        StringTokenizer stringTokenizerReadRows = null;
                        if (rset.getObject(1) != null) {
                            valueReadSt = rset.getString(1);
                            if (rset.wasNull()) {
                                valueReadSt = NULL;
                            }
                            stringTokenizerReadRows = new StringTokenizer(valueReadSt,
                                    GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                        }

                        int rowIndex = 0;
                        int numberOfRows = 0;
                        int numberOfCols = 0;

                        if (stringTokenizerReadRows != null) {
                            numberOfRows = stringTokenizerReadRows.countTokens();
                            dvalueArr = new double[numberOfRows][];

                            while (stringTokenizerReadRows.hasMoreTokens()) {
                                final String currentRowRead = stringTokenizerReadRows.nextToken();
                                if (currentRowRead == null || currentRowRead.trim().isEmpty()) {
                                    break;
                                }

                                final StringTokenizer stringTokenizerReadCols = new StringTokenizer(currentRowRead,
                                        GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                                numberOfCols = stringTokenizerReadCols.countTokens();
                                final double[] currentRow = new double[numberOfCols];
                                int colIndex = 0;

                                while (stringTokenizerReadCols.hasMoreTokens()) {
                                    String currentValRead = stringTokenizerReadCols.nextToken();
                                    if (currentValRead == null) {
                                        break;
                                    }
                                    currentValRead = currentValRead.trim();
                                    if (currentValRead.equals("")) {
                                        break;
                                    }

                                    currentRow[colIndex] = Double.parseDouble(currentValRead);
                                    colIndex++;
                                }

                                dvalueArr[rowIndex] = currentRow;
                                rowIndex++;
                            }
                        }
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return dvalueArr;
    }

}
