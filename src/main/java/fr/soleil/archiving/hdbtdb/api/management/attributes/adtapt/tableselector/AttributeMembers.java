package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class AttributeMembers extends AttributesData {

    public AttributeMembers(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description : </b> Gets all the registered members
     *
     * @return array of strings
     * @throws ArchivingException
     */
    public String[] getMembers() throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = null;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT DISTINCT ").append(ConfigConst.MEMBER).append(" FROM ")
					.append(connector.getSchema()).append(".").append(ConfigConst.ADT).append(" WHERE ")
                    .append(ConfigConst.MEMBER).append(" IS NOT NULL").toString();
            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    /**
     * <b>Description : </b> Returns the number of distinct members.
     *
     * @return the number of distinct members.
     * @throws ArchivingException
     */
    public int getMembersCount() throws ArchivingException {
        int result;
        if (connector == null) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT COUNT(DISTINCT ").append(ConfigConst.MEMBER)
					.append(") FROM ").append(connector.getSchema()).append(".").append(ConfigConst.ADT)
                    .append(" WHERE ").append(ConfigConst.MEMBER).append(" IS NOT NULL").toString();
            result = getIntAttributeData(sqlStr);
        }
        return result;
    }

    /**
     * <b>Description : </b> Gets all the registered members for the given
     * domain and family
     *
     * @param domainName the given domain
     * @param familyName the given family
     * @return array of strings
     * @throws ArchivingException
     */
    public String[] getMembers(final String domainName, final String familyName) throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = null;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT DISTINCT ").append(ConfigConst.MEMBER).append(" FROM ")
					.append(connector.getSchema()).append(".").append(ConfigConst.ADT)
                    .append(" WHERE ").append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domainName))
                    .append(" AND ").append(compareLikeCaseInsensitive(ConfigConst.FAMILY, familyName))
                    .append(" AND ").append(ConfigConst.MEMBER).append(" IS NOT NULL").toString();

            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    public String[] getMembersByCriterion(final String domain_name, final String family_name, final String member_regexp)
            throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = null;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT DISTINCT ").append(ConfigConst.MEMBER).append(" FROM ")
					.append(connector.getSchema()).append(".").append(ConfigConst.ADT)
                    .append(" WHERE ").append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domain_name))
                    .append(" AND ").append(compareLikeCaseInsensitive(ConfigConst.FAMILY, family_name))
                    .append(" AND ").append(compareLikeCaseInsensitive(ConfigConst.MEMBER, member_regexp))
                    .toString();
            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    /**
     * <b>Description : </b> Returns the number of distinct registered members
     * for the given domain and family.
     *
     * @param domainName the given domain
     * @param familyName the given family
     * @return the number of distinct registered members for the given domain
     *         and family.
     * @throws ArchivingException
     */
    public int getMembersCount(final String domainName, final String familyName) throws ArchivingException {
        int result;
        if (connector == null) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            String sqlStr;
            sqlStr = new StringBuilder("SELECT COUNT(DISTINCT ").append(ConfigConst.MEMBER).append(") FROM ")
					.append(connector.getSchema()).append(".").append(ConfigConst.ADT).append(" WHERE (")
                    .append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, domainName))
                    .append(" AND ").append(compareLikeCaseInsensitive(ConfigConst.FAMILY, familyName))
                    .append(" AND ").append(ConfigConst.MEMBER).append(" IS NOT NULL").append(")")
                    .toString();

            // Returns the corresponding number
            result = getIntAttributeData(sqlStr);
        }
        return result;
    }

}
