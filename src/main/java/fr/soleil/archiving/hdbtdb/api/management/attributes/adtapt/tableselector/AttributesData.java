package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import java.util.Collection;
import java.util.List;

import fr.soleil.database.connection.AbstractDataBaseConnector;

public class AttributesData {

    protected final AbstractDataBaseConnector connector;

    public AttributesData(final AbstractDataBaseConnector connector) {
        this.connector = connector;
    }

	public Collection<String> getStringVectorAttributeData(final String sqlStr) {
        final List<String> argout_vect;
        if (connector == null) {
            argout_vect = null;
        } else {
			argout_vect = connector.getJdbi().withHandle(handle -> handle.createQuery(sqlStr)
					.map((resultSet, ctx) -> resultSet.getString(1)).list());
        }
        return argout_vect;

    }

	public int getIntAttributeData(final String sqlStr) {
        int counter = 0;
        if (connector != null) {
			counter = connector.getJdbi().withHandle(
					handle -> handle.createQuery(sqlStr).map((resultSet, ctx) -> resultSet.getInt(1)).one());
        }
        return counter;

    }

	protected String compareEqualCaseInsensitive(String column, String value) {
		return " LOWER(" + column + ") = " + (value != null ? "'" + value.toLowerCase() + "'" : "''") + " ";
	}

	protected String compareLikeCaseInsensitive(String column, String value) {
		String accepteNull = "";
		if (value != null && value.trim().equals("*")) {
			accepteNull = " OR " + column + " IS NULL ";
		}
		return "( LOWER(" + column + ") Like "
				+ (value != null ? "'" + value.toLowerCase().replace("*", "%") + "'" : "''") + accepteNull + ") ";
	}

}
