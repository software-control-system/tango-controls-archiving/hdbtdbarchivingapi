package fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.common.api.tools.StringFormater;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.tools.ImageEvent_RO;
import fr.soleil.archiving.hdbtdb.api.tools.ScalarEvent;
import fr.soleil.archiving.hdbtdb.api.tools.SpectrumEvent_RO;
import fr.soleil.archiving.hdbtdb.api.tools.SpectrumEvent_RW;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public abstract class HdbAttributeInsert implements IHdbAttributeInsert, DBExtractionConst {
    protected final AbstractDataBaseConnector connector;
    protected IAdtAptAttributes att;

//    private static final Logger LOGGER = LoggerFactory.getLogger(HdbAttributeInsert.class);

    public HdbAttributeInsert(final AbstractDataBaseConnector connector, final IAdtAptAttributes at) {
        this.connector = connector;
        this.att = at;
    }

    protected abstract void insert_SpectrumData_RO_DataBase(String tableName, StringBuilder tableFields, int dimX,
            Timestamp timeSt, StringBuilder valueStr, String attributeName) throws ArchivingException;

    protected abstract void insert_SpectrumData_RW_DataBase(String tableName, StringBuilder tableFields, int dimX,
            Timestamp timeSt, StringBuilder valueWriteStr, StringBuilder valueReadStr, String attributeName)
            throws ArchivingException;

    protected abstract void insert_ImageData_RO_DataBase(StringBuilder query, StringBuilder tableName,
            StringBuilder tableFields, int dim_x, int dim_y, Timestamp timeSt, double[][] dvalue,
            StringBuilder valueStr, String attributeName) throws ArchivingException;

    @Override
    public abstract void closeConnection();

    /**
     *
     * @param scalarEvent
     * @throws ArchivingException
     */

    @Override
    public void insert_ScalarData(final ScalarEvent scalarEvent) throws ArchivingException {
        if (connector != null) {
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final int writable = scalarEvent.getWritable();
            final String attributeName = scalarEvent.getAttributeCompleteName().trim();
            // Create and execute the SQL query string
            // Build the query string
            final String selectFields = writable == AttrWriteType._READ || writable == AttrWriteType._WRITE ? "?, ?"
                    : "?, ?, ?";
            final StringBuilder tableFields = writable == AttrWriteType._READ || writable == AttrWriteType._WRITE
                    ? new StringBuilder().append(ConfigConst.TIME).append(VALUE_SEPARATOR).append(ConfigConst.VALUE)
                    : new StringBuilder().append(ConfigConst.TIME).append(VALUE_SEPARATOR)
                            .append(ConfigConst.READ_VALUE).append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);

            final StringBuilder tableName = new StringBuilder().append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName));

            final StringBuilder query = new StringBuilder();
            query.append("INSERT INTO ").append(tableName).append(" (").append(tableFields).append(")")
                    .append(" VALUES").append(" (").append(selectFields).append(")");
            final Timestamp timeSt = new Timestamp(scalarEvent.getTimeStamp());
            final Object value = scalarEvent.getValue();
            final int dataType = scalarEvent.getDataType();
            insertScalarDataInDB(tableName.toString(), query, attributeName, writable, timeSt, value, dataType);
        }
    }

    protected void insertScalarDataInDB(final String tableName, final StringBuilder query, final String attributeName,
            final int writable, final Timestamp timeSt, final Object value, final int dataType)
            throws ArchivingException {
        PreparedStatement preparedStatement = null;
        Connection conn = null;
        try {
            conn = connector.getConnection();
            if (conn != null) {
                preparedStatement = conn.prepareStatement(query.toString());
                // dbConn.setLastStatement(preparedStatement);
                ConnectionCommands.prepareSmtScalar(preparedStatement, dataType, writable, value, 2);
                preparedStatement.setTimestamp(1, timeSt);
                preparedStatement.executeUpdate();
            }
        } catch (final SQLException e) {
            throw new ArchivingException(e, query.toString());
        } finally {
            ConnectionCommands.close(preparedStatement);
            connector.closeConnection(conn);
        }
    }

    /**
     * <b>Description : </b> Inserts a spectrum type attribute's data
     *
     * @param hdbSpectrumEventRO
     *            an object that contains the attribute's name, the timestamp's
     *            value and the value.
     */
    @Override
    public void insert_SpectrumData_RO(final SpectrumEvent_RO hdbSpectrumEventRO) throws ArchivingException {
        if (connector != null) {
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String attributeName = hdbSpectrumEventRO.getAttributeCompleteName().trim();
            final StringBuilder tableName = new StringBuilder().append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName));
            final long time = hdbSpectrumEventRO.getTimeStamp();
            final Timestamp timeSt = new Timestamp(time);
            final int dimX = hdbSpectrumEventRO.getDimX();
            int dataType = -1;
            double[] dvalue = null;
            float[] fvalue = null;
            int[] ivalue = null;
            long[] lvalue = null;
            short[] svalue = null;
            byte[] cvalue = null;
            boolean[] bvalue = null;
            String[] stvalue = null;
            if (hdbSpectrumEventRO.getValue() instanceof double[]) {
                dvalue = (double[]) hdbSpectrumEventRO.getValue();
                dataType = TangoConst.Tango_DEV_DOUBLE;
            } else if (hdbSpectrumEventRO.getValue() instanceof float[]) {
                fvalue = (float[]) hdbSpectrumEventRO.getValue();
                dataType = TangoConst.Tango_DEV_FLOAT;
            } else if (hdbSpectrumEventRO.getValue() instanceof int[]) {
                ivalue = (int[]) hdbSpectrumEventRO.getValue();
                dataType = TangoConst.Tango_DEV_LONG;
            } else if (hdbSpectrumEventRO.getValue() instanceof long[]) {
                lvalue = (long[]) hdbSpectrumEventRO.getValue();
                dataType = TangoConst.Tango_DEV_LONG64;
            } else if (hdbSpectrumEventRO.getValue() instanceof short[]) {
                svalue = (short[]) hdbSpectrumEventRO.getValue();
                dataType = TangoConst.Tango_DEV_SHORT;
            } else if (hdbSpectrumEventRO.getValue() instanceof byte[]) {
                cvalue = (byte[]) hdbSpectrumEventRO.getValue();
                dataType = TangoConst.Tango_DEV_CHAR;
            } else if (hdbSpectrumEventRO.getValue() instanceof boolean[]) {
                bvalue = (boolean[]) hdbSpectrumEventRO.getValue();
                dataType = TangoConst.Tango_DEV_BOOLEAN;
            } else if (hdbSpectrumEventRO.getValue() instanceof String[]) {
                stvalue = (String[]) hdbSpectrumEventRO.getValue();
                dataType = TangoConst.Tango_DEV_STRING;
            }
            StringBuilder valueStr = new StringBuilder();

            // First connect with the database
            // if ( dbConn.isAutoConnect() )
            // dbConn.connect();

            // Create and execute the SQL query string
            // Build the query string
            final StringBuilder tableFields = new StringBuilder().append(ConfigConst.TIME).append(VALUE_SEPARATOR)
                    .append(ConfigConst.DIM_X).append(VALUE_SEPARATOR).append(ConfigConst.VALUE);
            switch (dataType) {
                case TangoConst.Tango_DEV_DOUBLE:
                    if (dvalue == null) {
                        valueStr = null;
                    } else {
                        for (int i = 0; i < dvalue.length - 1; i++) {
                            valueStr.append(dvalue[i]).append(GlobalConst.CLOB_SEPARATOR).append(" ");
                        }
                        if (dvalue.length > 0) {
                            valueStr.append(dvalue[dvalue.length - 1]);
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_FLOAT:
                    if (fvalue == null) {
                        valueStr = null;
                    } else {
                        for (int i = 0; i < fvalue.length - 1; i++) {
                            valueStr.append(fvalue[i]).append(GlobalConst.CLOB_SEPARATOR).append(" ");
                        }
                        if (fvalue.length > 0) {
                            valueStr.append(fvalue[fvalue.length - 1]);
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_LONG:
                    if (ivalue == null) {
                        valueStr = null;
                    } else {
                        for (int i = 0; i < ivalue.length - 1; i++) {
                            valueStr.append(ivalue[i]).append(GlobalConst.CLOB_SEPARATOR).append(" ");
                        }
                        if (ivalue.length > 0) {
                            valueStr.append(ivalue[ivalue.length - 1]);
                        }
                    }
                    break;
                case TangoConst.Tango_DEV_LONG64:
                    if (lvalue == null) {
                        valueStr = null;
                    } else {
                        for (int i = 0; i < lvalue.length - 1; i++) {
                            valueStr.append(lvalue[i]).append(GlobalConst.CLOB_SEPARATOR).append(" ");
                        }
                        if (lvalue.length > 0) {
                            valueStr.append(lvalue[lvalue.length - 1]);
                        }
                    }
                    break;
                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                    if (svalue == null) {
                        valueStr = null;
                    } else {
                        for (int i = 0; i < svalue.length - 1; i++) {
                            valueStr.append(svalue[i]).append(GlobalConst.CLOB_SEPARATOR).append(" ");
                        }
                        if (svalue.length > 0) {
                            valueStr.append(svalue[svalue.length - 1]);
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_CHAR:
                    if (cvalue == null) {
                        valueStr = null;
                    } else {
                        for (int i = 0; i < cvalue.length - 1; i++) {
                            valueStr.append(cvalue[i]).append(GlobalConst.CLOB_SEPARATOR).append(" ");
                        }
                        if (cvalue.length > 0) {
                            valueStr.append(cvalue[cvalue.length - 1]);
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_BOOLEAN:
                    if (bvalue == null) {
                        valueStr = null;
                    } else {
                        for (int i = 0; i < bvalue.length - 1; i++) {
                            valueStr.append(bvalue[i]).append(GlobalConst.CLOB_SEPARATOR).append(" ");
                        }
                        if (bvalue.length > 0) {
                            valueStr.append(bvalue[bvalue.length - 1]);
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_STRING:
                    if (stvalue == null) {
                        valueStr = null;
                    } else {
                        for (int i = 0; i < stvalue.length - 1; i++) {
                            valueStr.append(StringFormater.formatStringToWrite(stvalue[i]))
                                    .append(GlobalConst.CLOB_SEPARATOR).append(" ");
                        }
                        if (stvalue.length > 0) {
                            valueStr.append(StringFormater.formatStringToWrite(stvalue[stvalue.length - 1]));
                        }
                    }
                    break;

            }
            insert_SpectrumData_RO_DataBase(tableName.toString(), tableFields, dimX, timeSt, valueStr, attributeName);
        }
    }

    /**
     *
     * @param hdbSpectrumEventRW
     * @throws ArchivingException
     */
    @Override
    public void insert_SpectrumData_RW(final SpectrumEvent_RW hdbSpectrumEventRW) throws ArchivingException {
        if (connector != null) {
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String attributeName = hdbSpectrumEventRW.getAttributeCompleteName().trim();
            final StringBuilder tableName = new StringBuilder().append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName));
            final long time = hdbSpectrumEventRW.getTimeStamp();
            final Timestamp timeSt = new Timestamp(time);
            final int dimX = hdbSpectrumEventRW.getDimX();
            int dataType = -1;
            double[] dvalueRead = null, dvalueWrite = null;
            float[] fvalueRead = null, fvalueWrite = null;
            int[] ivalueRead = null, ivalueWrite = null;
            long[] lvalueRead = null, lvalueWrite = null;
            short[] svalueRead = null, svalueWrite = null;
            byte[] cvalueRead = null, cvalueWrite = null;
            boolean[] bvalueRead = null, bvalueWrite = null;
            String[] stvalueRead = null, stvalueWrite = null;

            if (hdbSpectrumEventRW.getValue() instanceof double[]) {
                dvalueRead = (double[]) hdbSpectrumEventRW.getSpectrumValueRWRead();
                dvalueWrite = (double[]) hdbSpectrumEventRW.getSpectrumValueRWWrite();
                dataType = TangoConst.Tango_DEV_DOUBLE;

            } else if (hdbSpectrumEventRW.getValue() instanceof float[]) {
                fvalueRead = (float[]) hdbSpectrumEventRW.getSpectrumValueRWRead();
                fvalueWrite = (float[]) hdbSpectrumEventRW.getSpectrumValueRWWrite();
                dataType = TangoConst.Tango_DEV_FLOAT;

            } else if (hdbSpectrumEventRW.getValue() instanceof int[]) {
                ivalueRead = (int[]) hdbSpectrumEventRW.getSpectrumValueRWRead();
                ivalueWrite = (int[]) hdbSpectrumEventRW.getSpectrumValueRWWrite();
                dataType = TangoConst.Tango_DEV_LONG;

            } else if (hdbSpectrumEventRW.getValue() instanceof long[]) {
                lvalueRead = (long[]) hdbSpectrumEventRW.getSpectrumValueRWRead();
                lvalueWrite = (long[]) hdbSpectrumEventRW.getSpectrumValueRWWrite();
                dataType = TangoConst.Tango_DEV_LONG64;

            } else if (hdbSpectrumEventRW.getValue() instanceof short[]) {
                svalueRead = (short[]) hdbSpectrumEventRW.getSpectrumValueRWRead();
                svalueWrite = (short[]) hdbSpectrumEventRW.getSpectrumValueRWWrite();
                dataType = TangoConst.Tango_DEV_SHORT;

            } else if (hdbSpectrumEventRW.getValue() instanceof byte[]) {
                cvalueRead = (byte[]) hdbSpectrumEventRW.getSpectrumValueRWRead();
                cvalueWrite = (byte[]) hdbSpectrumEventRW.getSpectrumValueRWWrite();
                dataType = TangoConst.Tango_DEV_CHAR;

            } else if (hdbSpectrumEventRW.getValue() instanceof boolean[]) {
                bvalueRead = (boolean[]) hdbSpectrumEventRW.getSpectrumValueRWRead();
                bvalueWrite = (boolean[]) hdbSpectrumEventRW.getSpectrumValueRWWrite();
                dataType = TangoConst.Tango_DEV_BOOLEAN;

            } else if (hdbSpectrumEventRW.getValue() instanceof String[]) {
                stvalueRead = (String[]) hdbSpectrumEventRW.getSpectrumValueRWRead();
                stvalueWrite = (String[]) hdbSpectrumEventRW.getSpectrumValueRWWrite();
                dataType = TangoConst.Tango_DEV_STRING;

            }

            StringBuilder valueReadStr = new StringBuilder();
            StringBuilder valueWriteStr = new StringBuilder();

            // Create and execute the SQL query string
            // Build the query string
            final StringBuilder tableFields = new StringBuilder().append(ConfigConst.TIME).append(VALUE_SEPARATOR)
                    .append(ConfigConst.DIM_X).append(VALUE_SEPARATOR).append(ConfigConst.READ_VALUE)
                    .append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
            switch (dataType) {
                case TangoConst.Tango_DEV_DOUBLE:
                    if (dvalueRead == null) {
                        valueReadStr = null;
                    } else {
                        for (int i = 0; i < dvalueRead.length; i++) {
                            valueReadStr.append(dvalueRead[i]);
                            if (i < dvalueRead.length - 1) {
                                valueReadStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    if (dvalueWrite == null) {
                        valueWriteStr = null;
                    } else {
                        for (int i = 0; i < dvalueWrite.length; i++) {
                            valueWriteStr.append(dvalueWrite[i]);
                            if (i < dvalueWrite.length - 1) {
                                valueWriteStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_FLOAT:
                    if (fvalueRead == null) {
                        valueReadStr = null;
                    } else {
                        for (int i = 0; i < fvalueRead.length; i++) {
                            valueReadStr.append(fvalueRead[i]);
                            if (i < fvalueRead.length - 1) {
                                valueReadStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    if (fvalueWrite == null) {
                        valueWriteStr = null;
                    } else {
                        for (int i = 0; i < fvalueWrite.length; i++) {
                            valueWriteStr.append(fvalueWrite[i]);
                            if (i < fvalueWrite.length - 1) {
                                valueWriteStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_LONG:
                    if (ivalueRead == null) {
                        valueReadStr = null;
                    } else {
                        for (int i = 0; i < ivalueRead.length; i++) {
                            valueReadStr.append(ivalueRead[i]);
                            if (i < ivalueRead.length - 1) {
                                valueReadStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    if (ivalueWrite == null) {
                        valueWriteStr = null;
                    } else {
                        for (int i = 0; i < ivalueWrite.length; i++) {
                            valueWriteStr.append(ivalueWrite[i]);
                            if (i < ivalueWrite.length - 1) {
                                valueWriteStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }
                    break;
                case TangoConst.Tango_DEV_LONG64:
                    if (lvalueRead == null) {
                        valueReadStr = null;
                    } else {
                        for (int i = 0; i < lvalueRead.length; i++) {
                            valueReadStr.append(lvalueRead[i]);
                            if (i < lvalueRead.length - 1) {
                                valueReadStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    if (lvalueWrite == null) {
                        valueWriteStr = null;
                    } else {
                        for (int i = 0; i < lvalueWrite.length; i++) {
                            valueWriteStr.append(lvalueWrite[i]);
                            if (i < lvalueWrite.length - 1) {
                                valueWriteStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                    if (svalueRead == null) {
                        valueReadStr = null;
                    } else {
                        for (int i = 0; i < svalueRead.length; i++) {
                            valueReadStr.append(svalueRead[i]);
                            if (i < svalueRead.length - 1) {
                                valueReadStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    if (svalueWrite == null) {
                        valueWriteStr = null;
                    } else {
                        for (int i = 0; i < svalueWrite.length; i++) {
                            valueWriteStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            if (i < svalueWrite.length - 1) {
                                valueWriteStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }
                    break;

                case TangoConst.Tango_DEV_CHAR:
                    if (cvalueRead == null) {
                        valueReadStr = null;
                    } else {
                        for (int i = 0; i < cvalueRead.length; i++) {
                            valueReadStr.append(cvalueRead[i]);
                            if (i < cvalueRead.length - 1) {
                                valueReadStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    if (cvalueWrite == null) {
                        valueWriteStr = null;
                    } else {
                        for (int i = 0; i < cvalueWrite.length; i++) {
                            valueWriteStr.append(cvalueWrite[i]);
                            if (i < cvalueWrite.length - 1) {
                                valueWriteStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    break;

                case TangoConst.Tango_DEV_BOOLEAN:
                    if (bvalueRead == null) {
                        valueReadStr = null;
                    } else {
                        for (int i = 0; i < bvalueRead.length; i++) {
                            valueReadStr.append(bvalueRead[i]);
                            if (i < bvalueRead.length - 1) {
                                valueReadStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    if (bvalueWrite == null) {
                        valueWriteStr = null;
                    } else {
                        for (int i = 0; i < bvalueWrite.length; i++) {
                            valueWriteStr.append(bvalueWrite[i]);
                            if (i < bvalueWrite.length - 1) {
                                valueWriteStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    break;

                case TangoConst.Tango_DEV_STRING:
                    if (stvalueRead == null) {
                        valueReadStr = null;
                    } else {
                        for (int i = 0; i < stvalueRead.length; i++) {
                            valueReadStr.append(StringFormater.formatStringToWrite(stvalueRead[i]));
                            if (i < stvalueRead.length - 1) {
                                valueReadStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }

                    if (stvalueWrite == null) {
                        valueWriteStr = null;
                    } else {
                        for (int i = 0; i < stvalueWrite.length; i++) {
                            valueWriteStr.append(stvalueWrite[i]);
                            if (i < stvalueWrite.length - 1) {
                                valueWriteStr.append(GlobalConst.CLOB_SEPARATOR).append(" ");
                            }
                        }
                    }
                    break;
            }

            insert_SpectrumData_RW_DataBase(tableName.toString(), tableFields, dimX, timeSt, valueWriteStr,
                    valueReadStr, attributeName);
        }
    }

    /**
     * <b>Description : </b> Inserts an image type attribute's data
     *
     * @param imageEventRO
     *            an object that contains the attribute's name, the timestamp's
     *            value and the value.
     * @throws ArchivingException
     */
    @Override
    public void insert_ImageData_RO(final ImageEvent_RO imageEventRO)
            throws SQLException, IOException, ArchivingException {
        if (connector != null) {
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String attributeName = imageEventRO.getAttributeCompleteName().trim();
            final StringBuilder tableName = new StringBuilder().append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName));
            final long time = imageEventRO.getTimeStamp();
            final Timestamp timeSt = new Timestamp(time);

            final int dimX = imageEventRO.getDimX();
            final int dimY = imageEventRO.getDimY();

            int data_type = -1;

            double[][] dvalue = null;

            if (imageEventRO.getValue() instanceof double[][]) {
                dvalue = (double[][]) imageEventRO.getValue();
                data_type = TangoConst.Tango_DEV_DOUBLE;

                final StringBuilder valueStr = new StringBuilder();
                final StringBuilder query = new StringBuilder();

                // First connect with the database

                // Create and execute the SQL query string
                // Build the query string
                final StringBuilder tableFields = new StringBuilder().append(ConfigConst.TIME).append(VALUE_SEPARATOR)
                        .append(ConfigConst.DIM_X).append(VALUE_SEPARATOR).append(ConfigConst.DIM_Y)
                        .append(VALUE_SEPARATOR).append(ConfigConst.VALUE);
                if (dvalue != null) {
                    switch (data_type) {
                        case TangoConst.Tango_DEV_DOUBLE:
                            for (int i = 0; i < dimY; i++) {
                                for (int j = 0; j < dimX; j++) {
                                    valueStr.append(dvalue[i][j]);
                                    if (j < dimX - 1) {
                                        valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS).append(" ");
                                    }
                                }

                                if (i < dimY - 1) {
                                    valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS).append(" ");
                                }
                            }
                            break;
                    }
                }

                insert_ImageData_RO_DataBase(query, tableName, tableFields, dimX, dimY, timeSt, dvalue, valueStr,
                        attributeName);
            }

        }
    }

}
