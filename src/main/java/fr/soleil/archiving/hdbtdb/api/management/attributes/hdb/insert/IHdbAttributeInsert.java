/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert;

import java.io.IOException;
import java.sql.SQLException;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.ImageEvent_RO;
import fr.soleil.archiving.hdbtdb.api.tools.ScalarEvent;
import fr.soleil.archiving.hdbtdb.api.tools.SpectrumEvent_RO;
import fr.soleil.archiving.hdbtdb.api.tools.SpectrumEvent_RW;

/**
 * @author AYADI
 *
 */
public interface IHdbAttributeInsert {

    void closeConnection();

    void insert_ScalarData(ScalarEvent scalarEvent) throws ArchivingException;

    void insert_SpectrumData_RO(SpectrumEvent_RO hdbSpectrumEventRO) throws ArchivingException;

    void insert_SpectrumData_RW(SpectrumEvent_RW hdbSpectrumEventRW) throws ArchivingException;

    void insert_ImageData_RO(ImageEvent_RO imageEventRO) throws SQLException, IOException, ArchivingException;
}
