//+============================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Mode/ModeRelatif.java,v $
//
// Project:      Tango Archiving Service
//
// Description: This object is one of the Mode class fields.
//				This class describes the 'relative mode' (the archiving occurs each time the received value is upper a specified proportion of the last archived value).
//
// $Author: ounsy $
//
// $Revision: 1.4 $
//
// $Log: ModeRelatif.java,v $
// Revision 1.4  2006/10/30 14:36:07  ounsy
// added a toStringWatcher method used by the ArchivingWatcher's getAllArchivingAttributes command
//
// Revision 1.3  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.2.16.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.2  2005/01/26 15:35:38  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :   Synchrotron SOLEIL
//			    L'Orme des Merisiers
//			    Saint-Aubin - BP 48
//			    91192 GIF-sur-YVETTE CEDEX
//              FRANCE
//
//+============================================================================

package fr.soleil.archiving.hdbtdb.api.tools.mode;

/**
 * <p/>
 * <B>Description :</B><BR>
 * This object is one of the <I>Mode class</I> fields. This class describes the
 * 'relative mode' (the archiving occurs each time the received value is upper a
 * specified proportion of the last archived value).<BR>
 * </p>
 * 
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version $Revision: 1.4 $
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.Mode
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference
 * @see EventMode
 */

public final class ModeRelatif extends ModeRoot {
    private double prevStoredVal = SLOW_DRIFT_FIRST_VAL;
    /**
     * the <I>lower proportion limit</I> field of the object
     */
    private double percentInf = 0;
    /**
     * the <I>upper proportion limit</I> field of the object
     */
    private double percentSup = 0;
    private boolean slowDrift = false;

    /**
     * Default constructor
     * 
     * @see #ModeRelatif(double, double)
     * @see #ModeRelatif(int, double, double)
     */
    public ModeRelatif() {
    }

    /**
     * This constructor takes two parameters as inputs.
     * 
     * @param inf
     *            the lower proportion limit
     * @param sup
     *            the upper proportion limit
     */
    public ModeRelatif(final double inf, final double sup) {
        percentInf = Math.abs(inf);
        percentSup = Math.abs(sup);
    }

    /**
     * This constructor takes two parameters as inputs.
     * 
     * @param pe
     *            the archiving (polling) period time
     * @param inf
     *            the lower proportion limit
     * @param sup
     *            the upper proportion limit
     */
    public ModeRelatif(final int pe, final double inf, final double sup) {
        super.period = pe;
        percentInf = Math.abs(inf);
        percentSup = Math.abs(sup);
    }

    /**
     * This constructor takes four parameters as inputs.
     * 
     * @param pe
     *            the archiving (polling) period time
     * @param inf
     *            the lower proportion limit
     * @param sup
     *            the upper proportion limit
     * @param sd
     *            the slow drift option
     */
    public ModeRelatif(final int pe, final double inf, final double sup, final boolean sd) {
        super.period = pe;
        percentInf = Math.abs(inf);
        percentSup = Math.abs(sup);
        slowDrift = sd;
    }

    /**
     * Gets the lower proportion limit
     * 
     * @return the lower proportion limit
     */
    public double getPercentInf() {
        return percentInf;
    }

    /**
     * Sets the lower proportion limit
     * 
     * @param inf
     *            the lower proportion limit
     */
    public void setPercentInf(final double inf) {
        percentInf = Math.abs(inf);
    }

    /**
     * Gets the upper proportion limit
     * 
     * @return the upper proportion limit
     */
    public double getPercentSup() {
        return percentSup;
    }

    /**
     * Sets the upper proportion limit
     * 
     * @param sup
     *            the upper proportion limit
     */
    public void setPercentSup(final double sup) {
        percentSup = Math.abs(sup);
    }

    /**
     * Returns a string representation of the object <I>ModeRelatif</I>.
     * 
     * @return a string representation of the object <I>ModeRelatif</I>.
     */
    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder("");
        buf.append("[" + Tag.MODE_R_TAG + "\t\r\n" + "\t(" + Tag.MODE_P0_TAG + " = \"" + this.getPeriod() + "\" "
                + Tag.TIME_UNIT + "\r\n" + "\t" + Tag.MODE_R1_TAG + " = \"" + this.getPercentInf() + "\" \r\n" + "\t"
                + Tag.MODE_R2_TAG + " = \"" + this.getPercentSup() + "\" \r\n" + "\t" + Tag.MODE_R3_TAG + " = \""
                + Boolean.toString(this.isSlow_drift()) + "\" )]\r\n");
        return buf.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ModeRelatif)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        final ModeRelatif modeRelatif = (ModeRelatif) o;

        if (percentInf != modeRelatif.percentInf) {
            return false;
        }
        if (percentSup != modeRelatif.percentSup) {
            return false;
        }
        if (slowDrift != modeRelatif.slowDrift) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(percentInf);
        result = 29 * result + (int) (temp ^ temp >>> 32);
        temp = Double.doubleToLongBits(percentSup);
        result = 29 * result + (int) (temp ^ temp >>> 32);
        return result;
    }

    public String toStringWatcher() {
        return "MODE_R + " + super.getPeriod() + " + " + this.getPercentInf() + " + " + this.getPercentSup() + " + "
                + Boolean.toString(this.isSlow_drift());
    }

    public double getPrev_stored_val() {
        return prevStoredVal;
    }

    public void setPrev_stored_val(final double prev_stored_val) {
        this.prevStoredVal = prev_stored_val;
    }

    public boolean isSlow_drift() {
        return slowDrift;
    }

    public void setSlow_drift(final boolean slow_drift) {
        this.slowDrift = slow_drift;
    }
}