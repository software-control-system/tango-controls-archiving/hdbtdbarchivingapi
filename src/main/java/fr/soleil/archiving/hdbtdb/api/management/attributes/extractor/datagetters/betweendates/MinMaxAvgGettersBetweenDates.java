/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates;

import fr.esrf.Tango.AttrDataFormat;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.DataExtractor;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class MinMaxAvgGettersBetweenDates extends DataExtractor {

    private static final String IMAGE = "Image";
    private static final String SPECTRUM = "Spectrum";
    private static final String SCALAR = "Scalar";

    /**
     * @param con
     * @param ut
     * @param at
     */
    public MinMaxAvgGettersBetweenDates(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description : </b> Returns the lower value recorded for the given
     * attribute and between two dates (date_1 & date_2).
     * 
     * @param argin
     *            The attribute's name, the beginning date (DD-MM-YYYY
     *            HH24:MI:SS.FF) and the ending date (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The lower scalar data for the specified attribute and for the
     *         specified period
     * @throws ArchivingException
     */
    public double getAttDataMinBetweenDates(final String[] argin) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        double min = 0;
        if (att != null) {
            final String attributeName = argin[0].trim();
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[1].trim());
            final String time1 = dbUtils.getTime(argin[2].trim());

            final int[] tfw = att.getAttTFWData(attributeName);
            // int data_type = tfw[0];
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    min = getAttScalarDataMinBetweenDates(attributeName, time0, time1, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
            }
        }
        return min;
    }

    /**
     * <b>Description : </b> Returns the upper value recorded for the given
     * attribute during a given period. and between two dates (date_1 & date_2).
     * 
     * @param argin
     *            The attribute's name, the beginning date (DD-MM-YYYY
     *            HH24:MI:SS.FF) and the ending date (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The upper scalar data for the specified attribute and for the
     *         specified period.
     * @throws ArchivingException
     */
    public double getAttDataMaxBetweenDates(final String[] argin) throws ArchivingException {
        double max = 0;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (att != null) {
            final String attributeName = argin[0].trim();
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[1].trim());
            final String time1 = dbUtils.getTime(argin[2].trim());
            final int[] tfw = att.getAttTFWData(attributeName);
            // int data_type = tfw[0];
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    max = getAttScalarDataMaxBetweenDates(attributeName, time0, time1, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
            }
        }
        return max;
    }

    /**
     * <b>Description : </b> Returns the mean value for the given attribute and
     * during a given period. and between two dates (date_1 & date_2).
     * 
     * @param argin
     *            The attribute's name, the beginning date (DD-MM-YYYY
     *            HH24:MI:SS.FF) and the ending date (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The mean scalar data for the specified attribute and for the
     *         specified period.
     * @throws ArchivingException
     */
    public double getAttDataAvgBetweenDates(final String[] argin) throws ArchivingException {
        double avg = 0;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (att != null) {
            final String attributeName = argin[0].trim();
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[1].trim());
            final String time1 = dbUtils.getTime(argin[2].trim());
            final int[] tfw = att.getAttTFWData(attributeName);
            // int data_type = tfw[0];
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    avg = getAttScalarDataAvgBetweenDates(attributeName, time0, time1, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
            }
        }
        return avg;
    }

    /*
     * 
     */
    private double getAttScalarDataMaxBetweenDates(final String attributeName, final String time0, final String time1,
            final int writable) throws ArchivingException {
        return methods.getAttScalarDataMinMaxAvgBetweenDates(attributeName, time0, time1, writable, "MAX");
    }

    /*
     * 
     */
    private double getAttScalarDataMinBetweenDates(final String attributeName, final String time0, final String time1,
            final int writable) throws ArchivingException {
        return methods.getAttScalarDataMinMaxAvgBetweenDates(attributeName, time0, time1, writable, "MIN");
    }

    /*
     * 
     */
    private double getAttScalarDataAvgBetweenDates(final String attributeName, final String time0, final String time1,
            final int writable) throws ArchivingException {
        return methods.getAttScalarDataMinMaxAvgBetweenDates(attributeName, time0, time1, writable, "AVG");
    }

}
