//+======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/AttributeSupport.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  AttributeSupport.
//						(chinkumo) - 24 ao�t 2005
//
// $Author: ounsy $
//
// $Revision: 1.10 $
//
// $Log: AttributeSupport.java,v $
// Revision 1.10  2006/07/24 07:36:04  ounsy
// image support
//
// Revision 1.9  2006/06/16 08:48:36  ounsy
// exceptions messages easier to understand
//
// Revision 1.8  2006/04/05 13:50:50  ounsy
// new types full support
//
// Revision 1.7  2006/03/13 14:41:19  ounsy
// code factoring
//
// Revision 1.6  2006/03/10 11:31:00  ounsy
// state and string support
//
// Revision 1.5  2006/02/24 12:50:35  ounsy
// float ok
//
// Revision 1.4  2006/02/06 12:22:28  ounsy
// added spectrum support for basic types
//
// Revision 1.3  2006/01/23 10:31:56  ounsy
// spectrum management
//
// Revision 1.2  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.1.2.2  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.1.2.1  2005/09/09 08:21:24  chinkumo
// First commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.archiving.hdbtdb.api.tools;

import java.util.Arrays;
import java.util.List;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.GlobalConst;

public final class AttributeSupport {

	public static final List<Integer> supportedDataType = Arrays.asList(TangoConst.Tango_DEV_SHORT,
			TangoConst.Tango_DEV_USHORT, TangoConst.Tango_DEV_LONG, TangoConst.Tango_DEV_ULONG,
			TangoConst.Tango_DEV_DOUBLE, TangoConst.Tango_DEV_FLOAT, TangoConst.Tango_DEV_BOOLEAN,
			TangoConst.Tango_DEV_STRING, TangoConst.Tango_DEV_STATE, TangoConst.Tango_DEV_UCHAR,
			TangoConst.Tango_DEV_ULONG64, TangoConst.Tango_DEV_LONG64);

	public static final List<Integer> supportedWriteType = Arrays.asList(AttrWriteType._READ, AttrWriteType._WRITE,
			AttrWriteType._READ_WRITE, AttrWriteType._READ_WITH_WRITE);

	public static final List<Integer> supportedDataFormat = Arrays.asList(AttrDataFormat._SCALAR,
			AttrDataFormat._SPECTRUM, AttrDataFormat._IMAGE);

    private AttributeSupport() {

    }

    public static boolean checkAttributeSupport(final String name, final int dataType, final int dataFormat,
			final int writable) throws ArchivingException {
		if (!supportedDataType.contains(dataType)) {
			throw generateException(GlobalConst.DATA_TYPE_EXCEPTION, dataType, name);
		}
		if (!supportedWriteType.contains(writable)) {
			throw generateException(GlobalConst.DATA_WRITABLE_EXCEPTION, writable, name);
		}
		if (!supportedDataFormat.contains(dataFormat)) {
			throw generateException(GlobalConst.DATA_FORMAT_EXCEPTION, dataFormat, name);
		}
		switch (dataFormat) {
		case AttrDataFormat._SCALAR:
		case AttrDataFormat._SPECTRUM:
			if (writable == AttrWriteType._READ) {
				return true;
			} else {
				if (TangoConst.Tango_DEV_STATE != dataType) {
					return true;
				} else {
					throw generateException(GlobalConst.DATA_TYPE_EXCEPTION, dataType, name);
				}
			}
		case AttrDataFormat._IMAGE:
			if (writable == AttrWriteType._READ) {
				if (TangoConst.Tango_DEV_STATE != dataType) {
					return false;
				} else {
					throw generateException(GlobalConst.DATA_TYPE_EXCEPTION, dataType, name);
				}
			} else {
				throw generateException(GlobalConst.DATA_WRITABLE_EXCEPTION, writable, name);
			}
		}
		return false;
    }

    private static ArchivingException generateException(final String cause, final int causeValue, final String name) {
	final String message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + cause;
	final String reason = "Failed while executing AttributeSupport.checkAttributeSupport()...";
	String desc = new String(cause + " (");
	if (GlobalConst.DATA_WRITABLE_EXCEPTION.equals(cause)) {
	    switch (causeValue) {
	    case AttrWriteType._READ:
		desc += "READ";
		break;
	    case AttrWriteType._WRITE:
		desc += "WRITE";
		break;
	    case AttrWriteType._READ_WITH_WRITE:
		desc += "READ WITH WRITE";
		break;
	    case AttrWriteType._READ_WRITE:
		desc += "READ WRITE";
		break;
	    default:
		desc += causeValue;
	    }
	} else if (GlobalConst.DATA_FORMAT_EXCEPTION.equals(cause)) {
	    switch (causeValue) {
	    case AttrDataFormat._SCALAR:
		desc += "SCALAR";
		break;
	    case AttrDataFormat._SPECTRUM:
		desc += "SPECTRUM";
		break;
	    case AttrDataFormat._IMAGE:
		desc += "IMAGE";
		break;
	    default:
		desc += causeValue;
	    }
	} else if (GlobalConst.DATA_TYPE_EXCEPTION.equals(cause)) {
	    switch (causeValue) {
	    case TangoConst.Tango_DEV_SHORT:
		desc += "SHORT";
		break;
	    case TangoConst.Tango_DEV_USHORT:
		desc += "UNSIGNED SHORT";
		break;
	    case TangoConst.Tango_DEV_LONG:
		desc += "LONG";
		break;
	    case TangoConst.Tango_DEV_ULONG:
		desc += "UNSIGNED LONG";
		break;
	    case TangoConst.Tango_DEV_DOUBLE:
		desc += "DOUBLE";
		break;
	    case TangoConst.Tango_DEV_FLOAT:
		desc += "FLOAT";
		break;
	    case TangoConst.Tango_DEV_BOOLEAN:
		desc += "BOOLEAN";
		break;
	    case TangoConst.Tango_DEV_CHAR:
		desc += "CHAR";
		break;
	    case TangoConst.Tango_DEV_UCHAR:
		desc += "UNSIGNED CHAR";
		break;
	    case TangoConst.Tango_DEV_STATE:
		desc += "STATE";
		break;
	    case TangoConst.Tango_DEV_STRING:
		desc += "STRING";
		break;
	    default:
		desc += causeValue;
	    }
	} else {
	    desc += causeValue;
	}
	desc += ") not supported !! [" + name + "]";
	// desc = cause + " (" + cause_value + ") not supported !! [" + name +
	// "]";
	return new ArchivingException(message, reason, ErrSeverity.WARN, desc, "");
    }
}
