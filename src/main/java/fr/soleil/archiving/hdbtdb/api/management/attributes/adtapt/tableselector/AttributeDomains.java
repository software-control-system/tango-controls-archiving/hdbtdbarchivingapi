package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class AttributeDomains extends AttributesData {

    public AttributeDomains(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description : </b> Gets all the registered domains.
     *
     * @return all the registered domains (array of strings)
     * @throws ArchivingException
     */
    public String[] getDomains() throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = new String[0];
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT DISTINCT ").append(ConfigConst.DOMAIN).append(" FROM ")
					.append(connector.getSchema()).append(".").append(ConfigConst.ADT).toString();
            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    /**
     *
     * @param dom_regexp
     * @return
     * @throws ArchivingException
     */
    public String[] getDomainsByCriterion(final String dom_regexp) throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = new String[0];
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT DISTINCT ").append(ConfigConst.DOMAIN).append(" FROM ")
					.append(connector.getSchema()).append(".").append(ConfigConst.ADT).append(" WHERE ")
					.append(compareLikeCaseInsensitive(ConfigConst.DOMAIN, dom_regexp)).toString();
            result = DbUtils.toStringArray(getStringVectorAttributeData(sqlStr));
        }
        return result;
    }

    /**
     * <b>Description : </b> Returns the number of distinct registered domains.
     *
     *
     * @return the number of distinct registered domains.
     * @throws ArchivingException
     */
    public int getDomainsCount() throws ArchivingException {
        int result;
        if (connector == null) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            final String sqlStr = new StringBuilder("SELECT COUNT(DISTINCT ").append(ConfigConst.DOMAIN)
					.append(") FROM ").append(connector.getSchema()).append(".").append(ConfigConst.ADT).toString();
            result = getIntAttributeData(sqlStr);
        }
        return result;
    }

}
