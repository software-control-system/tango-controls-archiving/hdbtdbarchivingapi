/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.manager;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.archiving.hdbtdb.api.tools.ArchivingMessConfig;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;

/**
 * @author AYADI
 *
 */
public interface IArchivingManagerApiRef {

    boolean getFacility();

    int getArchiverListSize();

    String getStatus(String attributeName) throws ArchivingException;

    void archivingStart(ArchivingMessConfig archivingMessConfig) throws ArchivingException;

    void archivingStop(String... attributeNameList) throws ArchivingException;

    void archivingStopConf(String... attributeNameList) throws ArchivingException;

    Mode getArchivingMode(String attributeName) throws ArchivingException;

    boolean isArchived(String attributeName) throws ArchivingException;

    void archivingConfigure() throws ArchivingException;

    // DB connection
    void archivingConfigureWithoutArchiverListInit() throws ArchivingException;

    DataBaseManager getDataBase();

    boolean isDbConnected();

    boolean isMFacility();

    void setMFacility(boolean facility);

    String[] getMExportedArchiverList();

    void setMExportedArchiverList(String... exportedArchiverList);

    String[] getMNotExportedArchiverList();

    void setMNotExportedArchiverList(String... notExportedArchiverList);

}
