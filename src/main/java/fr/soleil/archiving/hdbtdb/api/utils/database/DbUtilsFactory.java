/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.utils.database;

import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.H2DataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public final class DbUtilsFactory {

    /**
	 * 
	 */
    private DbUtilsFactory() {
    }

    /**
     * 
     * @param type
     * @return
     */
    public static IDbUtils getInstance(final AbstractDataBaseConnector connector) {
        IDbUtils result;
        if (connector == null) {
            result = null;
		} else if (connector instanceof OracleDataBaseConnector || connector instanceof H2DataBaseConnector) {
            result = new OracleDbUtils(connector);
        } else {
            result = new MySqlDbUtils(connector);
        }
        return result;
    }
}
