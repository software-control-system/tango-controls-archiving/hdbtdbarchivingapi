package fr.soleil.archiving.hdbtdb.api.management.database.commands;

import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.StringFormater;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import oracle.jdbc.OraclePreparedStatement;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author AYADI
 */
public final class ConnectionCommands {
    private ConnectionCommands() {

    }

    public static void commit(final Connection conn, final IDbUtils dbUtils) throws ArchivingException {
        try {
            conn.commit();
        } catch (final SQLException e) {
            throw new ArchivingException(e, "commit in ConnectionCommands");
        }

    }

    public static void minimumRequest(final AbstractDataBaseConnector connector) throws ArchivingException {
        if (connector != null) {
            final String tableName = connector.getSchema() + ".adt";
            final String query = "SELECT NULL FROM " + tableName + " WHERE ID=1";
            Connection conn = null;
            Statement stmt = null;
            ResultSet res = null;
            try {
                conn = connector.getConnection();
                stmt = conn.createStatement();
                // dbConn.setLastStatement(stmt);
                // ResultSet rset =
                res = stmt.executeQuery(query);
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                close(res);
                close(stmt);
                connector.closeConnection(conn);
            }
        }

    }

    public static void close(final ResultSet rset) {
        if (rset != null) {
            try {
                rset.close();
            } catch (final SQLException e) {
                // ignore
            }
        }
    }

    public static void close(final Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (final SQLException e) {
            }
        }

    }

    public static void rollback(final Connection conn, final IDbUtils dbUtils) throws ArchivingException {
        try {
            conn.rollback();
        } catch (final SQLException e) {
            throw new ArchivingException(e, "rollback in ConnectionCommands");
        }

    }

    public static void prepareSmtScalar(final PreparedStatement preparedStatement, final int dataType,
                                        final int writeType, final Object value, final int pos) throws SQLException, ArchivingException {
        if (value == null) {
            if (dataType == TangoConst.Tango_DEV_STRING) {
                preparedStatement.setNull(pos, java.sql.Types.VARCHAR);
                if (writeType == AttrWriteType._READ_WRITE) {
                    preparedStatement.setNull(pos + 1, java.sql.Types.VARCHAR);
                }
            } else {
                preparedStatement.setNull(pos, java.sql.Types.NUMERIC);
                if (writeType == AttrWriteType._READ_WRITE) {
                    preparedStatement.setNull(pos + 1, java.sql.Types.NUMERIC);
                }
            }
        } else {
            final Class<?> clazz = value.getClass();

            if (clazz.isArray()) { // read or write attribute
                prepareSmt(preparedStatement, Array.get(value, 0), pos);
                prepareSmt(preparedStatement, Array.get(value, 1), pos + 1);
            } else {
                prepareSmt(preparedStatement, value, pos);
            }
        }
    }

    private static void prepareSmt(final PreparedStatement preparedStatement, final Object value, final int pos)
            throws SQLException, ArchivingException {
        final Class<?> clazz = value.getClass();
        if (clazz.equals(String.class)) {
            preparedStatement.setString(pos, StringFormater.formatStringToWrite((String) value));
        } else if (clazz.equals(Byte.class)) {
            preparedStatement.setByte(pos, (Byte) value);
        } else if (clazz.equals(DevState.class)) {
            preparedStatement.setInt(pos, ((DevState) value).value());
        } else if (clazz.equals(Integer.class)) {
            preparedStatement.setInt(pos, (Integer) value);
        } else if (clazz.equals(Boolean.class)) {
            preparedStatement.setBoolean(pos, (Boolean) value);
        } else if (clazz.equals(Long.class)) {
            preparedStatement.setLong(pos, (Long) value);
        } else if (clazz.equals(Short.class)) {
            preparedStatement.setShort(pos, (Short) value);
        } else if (clazz.equals(Float.class)) {
            if (Float.isNaN((Float) value)) {
                preparedStatement.setNull(pos, java.sql.Types.NUMERIC);
            } else {
                preparedStatement.setFloat(pos, (Float) value);
            }
        } else if (clazz.equals(Double.class)) {
            if (Double.isNaN((Double) value)) {
                preparedStatement.setNull(pos, java.sql.Types.NUMERIC);
            } else {
                preparedStatement.setDouble(pos, (Double) value);
            }
        } else if (clazz.equals(BigInteger.class)) {
            preparedStatement.setObject(pos, value);
        } else {
            final String desc = "Failed while executing ConectionCommands.prepareSmtForInsertScalar1ValNotNull() method : Invalid DATA_TYPE ";
            throw new ArchivingException(desc);
        }

    }

    public static void prepareSmtScalarOracle(final PreparedStatement preparedStatement, final int dataType,
                                              final int writeType, final Object value, final int pos) throws SQLException, ArchivingException {
        if (value == null) {
            if (dataType == TangoConst.Tango_DEV_STRING) {
                preparedStatement.setNull(pos, java.sql.Types.VARCHAR);
                if (writeType == AttrWriteType._READ_WRITE) {
                    preparedStatement.setNull(pos + 1, java.sql.Types.VARCHAR);
                }
            } else {
                preparedStatement.setNull(pos, java.sql.Types.NUMERIC);
                if (writeType == AttrWriteType._READ_WRITE) {
                    preparedStatement.setNull(pos + 1, java.sql.Types.NUMERIC);
                }
            }
        } else {
            final Class<?> clazz = value.getClass();
            if (clazz.isArray()) { // read or write attribute
                final Object readValue = Array.get(value, 0);
                if (readValue.getClass().equals(Double.class) && Double.isNaN((Double) readValue)) {
                    insertNaNForOracle(preparedStatement, readValue, pos);
                } else if (readValue.getClass().equals(Float.class) && Float.isNaN((Float) readValue)) {
                    insertNaNForOracle(preparedStatement, readValue, pos);
                } else {
                    prepareSmt(preparedStatement, readValue, pos);
                }
                final Object writeValue = Array.get(value, 1);
                if (writeValue.getClass().equals(Double.class) && Double.isNaN((Double) writeValue)) {
                    insertNaNForOracle(preparedStatement, writeValue, pos + 1);
                } else if (writeValue.getClass().equals(Float.class) && Float.isNaN((Float) writeValue)) {
                    insertNaNForOracle(preparedStatement, writeValue, pos + 1);
                } else {
                    prepareSmt(preparedStatement, writeValue, pos + 1);
                }
            } else {
                if (value.getClass().equals(Double.class) && Double.isNaN((Double) value)) {
                    insertNaNForOracle(preparedStatement, value, pos);
                } else if (value.getClass().equals(Float.class) && Float.isNaN((Float) value)) {
                    insertNaNForOracle(preparedStatement, value, pos);
                } else {
                    prepareSmt(preparedStatement, value, pos);
                }
            }
        }
    }

    /**
     * Special case for Oracle. Double values are inserted in db as binary
     * double which support NaN values.
     *
     * @param preparedStatement
     * @param value
     * @param pos
     * @throws SQLException
     */
    private static void insertNaNForOracle(final PreparedStatement preparedStatement, final Object value, final int pos)
            throws SQLException {
        try {
            if (preparedStatement instanceof OraclePreparedStatement) {
                ((OraclePreparedStatement) preparedStatement).setBinaryDouble(pos, Double.NaN);
            } else { // NaN not supported
                preparedStatement.setNull(pos, java.sql.Types.NUMERIC);
            }
        } catch (final SQLException e) {
            throw e;
        } catch (final Throwable e) {
            // if oracle is not in the classpath, ignore error
            preparedStatement.setNull(pos, java.sql.Types.NUMERIC);
        }
    }

    public static void cancelStatement(Statement stmt) throws ArchivingException {
        if (stmt != null) {
            try {
                stmt.cancel();
            } catch (SQLException e) {
                boolean canThrow;
                try {
                    canThrow = !stmt.isClosed();
                } catch (SQLException e1) {
                    canThrow = true;
                }
                if (canThrow) {
                    throw new ArchivingException(e, "cancel statement");
                }
            }
        }
    }
}
