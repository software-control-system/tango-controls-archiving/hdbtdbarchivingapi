package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.esrf.Tango.AttrWriteType;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.archiving.hdbtdb.api.utils.database.NullableData;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class MySQlExtractorMethods extends GenericExtractorMethods {

    public MySQlExtractorMethods(AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    public DbData[] treatStatementResultForGetSpectData(final ResultSet rset, final String attributeName,
            final DbData... dbData) throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1, dataType = -1;
            for (DbData data : result) {
                if (data != null) {
                    writable = data.getWritable();
                    dataType = data.getDataType();
                    break;
                }
            }
            try {
                List<NullableTimedData> readList = new ArrayList<>(), writeList = new ArrayList<>();
                final boolean roFields = writable == AttrWriteType._READ || writable == AttrWriteType._WRITE;
                for (DbData data : result) {
                    if (data != null) {
                        data.setMaxX(0);
                    }
                }
                while (rset.next()) {
                    if (isCanceled()) {
                        break;
                    }
                    long time = DateUtil.stringToMilli(rset.getString(1));
                    int dimX = rset.getInt(2);
                    NullableTimedData readData;
                    NullableTimedData writeData;

                    if (result[READ_INDEX] == null) {
                        readData = null;
                    } else {
                        readData = DbData.initNullableTimedData(dataType, time, dimX);
                        if (readData.getX() > result[READ_INDEX].getMaxX()) {
                            result[READ_INDEX].setMaxX(readData.getX());
                        }
                    }
                    if (result[WRITE_INDEX] == null) {
                        writeData = null;
                    } else {
                        writeData = DbData.initNullableTimedData(dataType, time, dimX);
                        if (writeData.getX() > result[WRITE_INDEX].getMaxX()) {
                            result[WRITE_INDEX].setMaxX(writeData.getX());
                        }
                    }

                    // Value
                    String valueReadSt = null;

                    valueReadSt = rset.getString(3);
                    if (rset.wasNull()) {
                        valueReadSt = NULL;
                    }

                    String valueWriteSt = null;
                    if (!roFields) {
                        valueWriteSt = rset.getString(4);
                        if (rset.wasNull()) {
                            valueWriteSt = NULL;
                        }
                    }
                    final NullableData<boolean[]>[] temp;
                    if (writable == AttrWriteType._WRITE) {
                        temp = dbUtils.getSpectrumValue(null, valueReadSt, dataType);
                    } else {
                        temp = dbUtils.getSpectrumValue(valueReadSt, valueWriteSt, dataType);
                    }
                    NullableData.fillTimedData(temp, readData, writeData);
                    if (readData != null) {
                        readList.add(readData);
                    }
                    if (writeData != null) {
                        writeList.add(writeData);
                    }
                } // end while (rset.next())
                DbData.afterExtraction(readList, writeList, result);
            } catch (final SQLException e) {
                throw new ArchivingException(e, "MySqlDataGetters#treatStatementResultForGetSpectData");
            }

        }
        return dbData;
    }

    @Override
    public DbData[] treatStatementResultForGetImageData(final ResultSet rset, final String attributeName,
            final DbData... dbData) throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (att == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1, dataType = -1;
            for (DbData data : result) {
                if (data != null) {
                    writable = data.getWritable();
                    dataType = data.getDataType();
                    break;
                }
            }
            if (writable == AttrWriteType._READ_WITH_WRITE || writable == AttrWriteType._READ_WRITE) {
                throw new ArchivingException("RW image attributes are not supported");
            }
            try {
                List<NullableTimedData> readList = new ArrayList<>(), writeList = new ArrayList<>();
                while (rset.next()) {
                    if (isCanceled()) {
                        break;
                    }
                    long time = DateUtil.stringToMilli(rset.getString(1));
                    int dimX = rset.getInt(2), dimY = rset.getInt(3);

                    NullableTimedData readData;
                    NullableTimedData writeData;

                    if (result[READ_INDEX] == null) {
                        readData = null;
                    } else {
                        readData = DbData.initNullableTimedData(dataType, time, dimX, dimY);
                    }
                    if (result[WRITE_INDEX] == null) {
                        writeData = null;
                    } else {
                        writeData = DbData.initNullableTimedData(dataType, time, dimX, dimY);
                    }

                    // Value
                    String valueReadSt;
                    if (writable == AttrWriteType._WRITE) {
                        valueReadSt = null;
                    } else {
                        valueReadSt = rset.getString(4);
                        if (rset.wasNull()) {
                            valueReadSt = NULL;
                        }
                    }
                    String valueWriteSt;
                    if (writable == AttrWriteType._READ) {
                        valueWriteSt = null;
                    } else if (writable == AttrWriteType._WRITE) {
                        valueWriteSt = rset.getString(4);
                        if (rset.wasNull()) {
                            valueWriteSt = NULL;
                        }
                    } else {
                        valueWriteSt = rset.getString(5);
                        if (rset.wasNull()) {
                            valueWriteSt = NULL;
                        }
                    }
                    final NullableData<boolean[][]>[] temp;
                    temp = dbUtils.getImageValue(valueReadSt, valueWriteSt, dataType);
                    NullableData.fillTimedData(temp, readData, writeData);
                    if (readData != null) {
                        readList.add(readData);
                    }
                    if (writeData != null) {
                        writeList.add(writeData);
                    }
                } // end while (rset.next())
                DbData.afterExtraction(readList, writeList, result);
            } catch (final SQLException e) {
                throw new ArchivingException(e, "MySqlDataGetters#treatStatementResultForGetImageData");
            }
        }
        return dbData;
    }

}
