/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 *
 */
public class OracleDataGettersBetweenDates extends DataGettersBetweenDates {

    /**
     * @param con
     * @param ut
     * @param at
     */
    public OracleDataGettersBetweenDates(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    protected String getAttScalarDataRequest(final SamplingType samplingType, final boolean roFields,
            final String tableName, final String time0, final String time1, final int tangoType)
            throws ArchivingException {
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        final String query;
        if (connector == null || dbUtils == null) {
            query = null;
        } else {
            StringBuilder queryBuilder;
            if (samplingType.hasSampling()) {
                if (samplingType.hasAdditionalFiltering()) {
                    final String format = samplingType.getOneLevelHigherFormat(false);
                    final String fullFormat = SamplingType.getSamplingType(SamplingType.SECOND).getFormat(false);
                    final String groupingNormalisationType = SamplingType.getGroupingNormalisationType(tangoType);

                    final String minTime = "MIN(" + ConfigConst.TIME + ")";
                    final String convertedMinTime = dbUtils.toDbTimeFieldString(minTime, fullFormat);

                    queryBuilder = new StringBuilder("SELECT ");
                    if (roFields) {
                        queryBuilder.append(convertedMinTime).append(VALUE_SEPARATOR).append(groupingNormalisationType)
                                .append("(").append(ConfigConst.VALUE).append(")");
                    } else {
                        queryBuilder.append(convertedMinTime).append(VALUE_SEPARATOR).append(groupingNormalisationType)
                                .append("(").append(ConfigConst.READ_VALUE).append(") , ")
                                .append(groupingNormalisationType).append("(").append(ConfigConst.WRITE_VALUE)
                                .append(")");
                    }
                    queryBuilder.append(" FROM ").append(tableName).append(" WHERE ").append(ConfigConst.TIME)
                            .append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                            .append(dbUtils.toDbTimeString(time1.trim())).append(" GROUP BY ")
                            .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format))
                            .append(samplingType.getAdditionalFilteringClause(false, ConfigConst.TIME))
                            .append(" ORDER BY MIN(").append(ConfigConst.TIME).append(")");
                } else {
                    final String format = samplingType.getOracleFormat();
                    final String groupingNormalisationType = SamplingType.getGroupingNormalisationType(tangoType);

                    queryBuilder = new StringBuilder("SELECT ");
                    if (roFields) {
                        queryBuilder.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format))
                                .append(VALUE_SEPARATOR).append(groupingNormalisationType).append("(")
                                .append(ConfigConst.VALUE).append(")");
                    } else {
                        queryBuilder.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format))
                                .append(VALUE_SEPARATOR).append(groupingNormalisationType).append("(")
                                .append(ConfigConst.READ_VALUE).append(") , ").append(groupingNormalisationType)
                                .append("(").append(ConfigConst.WRITE_VALUE).append(")");
                    }
                    queryBuilder.append(" FROM ").append(tableName).append(" WHERE ").append(ConfigConst.TIME)
                            .append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                            .append(dbUtils.toDbTimeString(time1.trim())).append(" GROUP BY ")
                            .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format)).append(" ORDER BY ")
                            .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format));
                }
            } else {
                queryBuilder = new StringBuilder("SELECT ");
                if (roFields) {
                    queryBuilder.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                            .append(ConfigConst.VALUE);
                } else {
                    queryBuilder.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                            .append(ConfigConst.READ_VALUE).append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
                }
                queryBuilder.append(" FROM ").append(tableName).append(" WHERE (").append(ConfigConst.TIME)
                        .append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                        .append(dbUtils.toDbTimeString(time1.trim())).append(") ORDER BY time");
            }
            query = queryBuilder.toString();
        }
        return query;

    }

    @Override
    public DbData[] getAttSpectrumDataBetweenDates(final String attributeName, final String time0, final String time1,
            final SamplingType samplingType, final DbData... dbData) throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (att == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            final boolean isBothReadAndWrite = !isROFields(writable);
            final StringBuilder queryBuilder;

            if (samplingType.hasSampling()) {
                final String groupingNormalisationType = "MIN"; // can't average a
                // spectrum
                if (!samplingType.hasAdditionalFiltering()) {
                    final String format = samplingType.getOracleFormat();
                    queryBuilder = new StringBuilder("SELECT ");
                    if (isBothReadAndWrite) {
                        queryBuilder.append(dbUtils.toDbTimeFieldString("T." + ConfigConst.TIME, format))
                                .append(", AVG (T.").append(ConfigConst.DIM_X).append("), ").append("to_clob ( ")
                                .append(groupingNormalisationType).append(" ( to_char (T.")
                                .append(ConfigConst.READ_VALUE).append(") ) )");
                        queryBuilder.append(", to_clob ( ").append(groupingNormalisationType).append(" ( to_char (T.")
                                .append(ConfigConst.WRITE_VALUE).append(") ) )");
                    } else {
                        queryBuilder.append(dbUtils.toDbTimeFieldString("T" + DB_SEPARATOR + ConfigConst.TIME, format))
                                .append(", AVG (T.").append(ConfigConst.DIM_X).append("), ").append("to_clob ( ")
                                .append(groupingNormalisationType).append(" ( to_char (T.").append(ConfigConst.VALUE)
                                .append(") ) )");
                    }
                    queryBuilder.append(" FROM ")
                            .append(connector.getSchema() + DB_SEPARATOR + dbUtils.getTableName(attributeName) + " T")
                            .append(" WHERE ").append(ConfigConst.TIME).append(" BETWEEN ")
                            .append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                            .append(dbUtils.toDbTimeString(time1.trim())).append(" GROUP BY ")
                            .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format)).append(" ORDER BY ")
                            .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format));
                } else {
                    final String format = samplingType.getOneLevelHigherFormat(false);
                    final String fullFormat = SamplingType.getSamplingType(SamplingType.SECOND).getFormat(false);

                    final String minTime = "MIN(" + ConfigConst.TIME + ")";

                    queryBuilder = new StringBuilder("SELECT ");
                    if (isBothReadAndWrite) {
                        queryBuilder.append(dbUtils.toDbTimeFieldString(minTime, fullFormat)).append(", AVG (T.")
                                .append(ConfigConst.DIM_X).append("), to_clob ( ").append(groupingNormalisationType)
                                .append(" ( to_char (T.").append(ConfigConst.READ_VALUE).append(") ) ), to_clob ( ")
                                .append(groupingNormalisationType).append(" ( to_char (T.")
                                .append(ConfigConst.WRITE_VALUE).append(") ) )");
                    } else {
                        queryBuilder.append(dbUtils.toDbTimeFieldString(minTime, fullFormat)).append(", AVG (T.")
                                .append(ConfigConst.DIM_X).append("), to_clob ( ").append(groupingNormalisationType)
                                .append(" ( to_char (T.").append(ConfigConst.VALUE).append(") ) )");
                    }
                    queryBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                            .append(dbUtils.getTableName(attributeName)).append(" T WHERE ").append(ConfigConst.TIME)
                            .append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                            .append(dbUtils.toDbTimeString(time1.trim())).append(" GROUP BY ")
                            .append(dbUtils.toDbTimeFieldString(ConfigConst.TIME, format))
                            .append(samplingType.getAdditionalFilteringClause(false, ConfigConst.TIME))
                            .append(" ORDER BY MIN(").append(ConfigConst.TIME).append(")");
                }
            } else {
                queryBuilder = new StringBuilder("SELECT ");
                if (isBothReadAndWrite) {
                    queryBuilder.append(dbUtils.toDbTimeFieldString("T." + ConfigConst.TIME)).append(", T.")
                            .append(ConfigConst.DIM_X).append(", T.").append(ConfigConst.READ_VALUE);
                    queryBuilder.append(", T.").append(ConfigConst.WRITE_VALUE);
                } else {
                    queryBuilder.append(dbUtils.toDbTimeFieldString("T." + ConfigConst.TIME)).append(", T.")
                            .append(ConfigConst.DIM_X).append(", T.").append(ConfigConst.VALUE);
                }
                queryBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                        .append(dbUtils.getTableName(attributeName)).append(" T WHERE (");
                if (isBothReadAndWrite) {
                    queryBuilder.append("T.").append(ConfigConst.TIME);
                } else {
                    queryBuilder.append("T.").append(ConfigConst.TIME);
                }
                queryBuilder.append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                        .append(dbUtils.toDbTimeString(time1.trim()));
                queryBuilder.append(") ORDER BY time");
            }
            final String query = queryBuilder.toString();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    // dbConn.setLastStatement(stmt);
                    stmt.setFetchSize(1000);
                    rset = stmt.executeQuery(query);
                    methods.treatStatementResultForGetSpectData(rset, attributeName, result);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;
    }

}
