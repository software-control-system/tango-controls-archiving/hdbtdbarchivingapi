/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.esrf.Tango.AttrDataFormat;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 *
 */
public class MySqlDataGetters extends DataGetters {

    public MySqlDataGetters(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    protected String getDbScalarLastNRequest(final String tableName, final String attributeName, final int number,
            final boolean roFields, final String fields) throws ArchivingException {
        String result;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (att == null) {
            result = null;
        } else {
            final int total_count = att.getAttRecordCount(attributeName);
            int limInf = total_count - number;
            if (limInf < 0) {
                limInf = 0;
            }
            final int limSup = total_count;
            result = new StringBuilder("SELECT ").append(fields).append(" FROM ").append(tableName)
                    .append(" ORDER BY time ").append(" LIMIT ").append(limInf).append(VALUE_SEPARATOR).append(limSup)
                    .toString();
        }
        return result;
    }

    @Override
    public DbData[] getAttSpectrumDataLastN(final String attributeName, final int number, final DbData... dbData)
            throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (att == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);

            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string

            final int totalCount = att.getAttRecordCount(attributeName);
            int limInf = totalCount - number;
            if (limInf < 0) {
                limInf = 0;
            }
            final int limSup = totalCount;
            final StringBuilder queryBuilder = new StringBuilder("SELECT ");
            if (roFields) {
                queryBuilder.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.DIM_X).append(VALUE_SEPARATOR).append(ConfigConst.VALUE);
            } else {
                queryBuilder.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.DIM_X).append(VALUE_SEPARATOR).append(ConfigConst.READ_VALUE)
                        .append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
            }
            queryBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(" ORDER BY time ").append(" LIMIT ")
                    .append(limInf).append(VALUE_SEPARATOR).append(limSup);
            final String query = queryBuilder.toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(query);
                    methods.treatStatementResultForGetSpectData(rset, attributeName, result);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;
    }

    @Override
    public void buildAttributeTab(final String tableName, final int dataType, final int dataFormat, final int writable)
            throws ArchivingException {
        switch (dataFormat) {
            case fr.esrf.Tango.AttrDataFormat._SCALAR:
                switch (writable) {
                    case fr.esrf.Tango.AttrWriteType._READ:
                        buildAttributeScalarTab_R(tableName, dataType);
                        break;
                    case fr.esrf.Tango.AttrWriteType._READ_WITH_WRITE:
                        buildAttributeScalarTab_RW(tableName, dataType);
                        break;
                    case fr.esrf.Tango.AttrWriteType._WRITE:
                        buildAttributeScalarTab_W(tableName, dataType);
                        break;
                    case fr.esrf.Tango.AttrWriteType._READ_WRITE:
                        buildAttributeScalarTab_RW(tableName, dataType);
                        break;
                }
                break;
            case fr.esrf.Tango.AttrDataFormat._SPECTRUM:
                switch (writable) {
                    case fr.esrf.Tango.AttrWriteType._READ:
                        buildAttributeSpectrumTab_R(tableName, dataType);
                        break;
                    case fr.esrf.Tango.AttrWriteType._READ_WITH_WRITE:
                        buildAttributeSpectrumTab_RW(tableName, dataType);
                        break;
                    case fr.esrf.Tango.AttrWriteType._WRITE:
                        // buildAttributeSpectrumTab_W(tableName , data_type);
                        break;
                    case fr.esrf.Tango.AttrWriteType._READ_WRITE:
                        buildAttributeSpectrumTab_RW(tableName, dataType);
                        break;
                }
                break;
            case fr.esrf.Tango.AttrDataFormat._IMAGE:
                switch (writable) {
                    case fr.esrf.Tango.AttrWriteType._READ:
                        buildAttributeImageTab_R(tableName, dataType);
                        break;
                    case fr.esrf.Tango.AttrWriteType._READ_WITH_WRITE:
                        buildAttributeImageTab_RW(tableName, dataType);
                        break;
                    case fr.esrf.Tango.AttrWriteType._WRITE:
                        // buildAttributeImageTab_W(tableName , data_type);
                        break;
                    case fr.esrf.Tango.AttrWriteType._READ_WRITE:
                        buildAttributeImageTab_RW(tableName, dataType);
                        break;
                }
                break;
        }
    }

    private void buildAttributeScalarTab_R(final String attributeID, final int dataType) throws ArchivingException {
        methods.buildAttributeScalarTab(attributeID, dataType, ConfigConst.TAB_SCALAR_RO);
    }

    private void buildAttributeScalarTab_RW(final String attributeID, final int dataType) throws ArchivingException {
        methods.buildAttributeScalarTab(attributeID, dataType, ConfigConst.TAB_SCALAR_RW);
    }

    private void buildAttributeScalarTab_W(final String attributeID, final int dataType) throws ArchivingException {
        methods.buildAttributeScalarTab(attributeID, dataType, ConfigConst.TAB_SCALAR_WO);
    }

    private void buildAttributeSpectrumTab_R(final String attributeID, final int dataType) throws ArchivingException {
        methods.buildAttributeSpectrumTab(attributeID, ConfigConst.TAB_SPECTRUM_RO, dataType);
    }

    private void buildAttributeSpectrumTab_RW(final String attributeID, final int dataType) throws ArchivingException {
        methods.buildAttributeSpectrumTab(attributeID, ConfigConst.TAB_SPECTRUM_RW, dataType);
    }

    private void buildAttributeImageTab_R(final String attributeID, final int dataType) throws ArchivingException {
        methods.buildAttributeImageTab(attributeID, ConfigConst.TAB_IMAGE_RO, dataType);
    }

    private void buildAttributeImageTab_RW(final String att_id, final int data_type) throws ArchivingException {
        methods.buildAttributeImageTab(att_id, ConfigConst.TAB_IMAGE_RW, data_type);
    }

    @Override
    public String getNearestValueQuery(final String attributeName, final String timestamp) throws ArchivingException {
        final String query;
        if (connector == null) {
            query = null;
        } else {
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String tableName = connector.getSchema() + DB_SEPARATOR + dbUtils.getTableName(attributeName);
            final int[] tfw = AdtAptAttributesFactory.getInstance(connector).getAttTFWData(attributeName);
            final int writeType = tfw[2];
            final String time = ConfigConst.TIME;
            // SELECT time, value from hdb.att_14
            // WHERE
            // ( ABS ( TO_DATE( '11-04-2011 16:34:43', 'DD-MM-YYYY HH24:MI:SS') -
            // TO_DATE(to_char(time, 'DD-MM-YYYY HH24:MI:SS'), 'DD-MM-YYYY
            // HH24:MI:SS') ))
            // =
            // (
            // SELECT min(ABS( TO_DATE ( '11-04-2011 16:34:43', 'DD-MM-YYYY
            // HH24:MI:SS') - TO_DATE (to_char(time, 'DD-MM-YYYY HH24:MI:SS'),
            // 'DD-MM-YYYY HH24:MI:SS') )) from hdb.att_14
            // )
            final String dateFormat = "'%d-%m-%Y %H:%i:%s'";
            final StringBuilder queryBuilder = new StringBuilder("SELECT ");

            if (isROFields(writeType)) {
                switch (tfw[1]) {
                    case AttrDataFormat._SCALAR:
                        queryBuilder.append(String.join(VALUE_SEPARATOR, ConfigConst.TAB_SCALAR_RO));
                        break;
                    case AttrDataFormat._SPECTRUM:
                        queryBuilder.append(String.join(VALUE_SEPARATOR, ConfigConst.TAB_SPECTRUM_RO));
                        break;
                    case AttrDataFormat._IMAGE:
                        queryBuilder.append(String.join(VALUE_SEPARATOR, ConfigConst.TAB_IMAGE_RO));
                        break;
                }
            } else {
                switch (tfw[1]) {
                    case AttrDataFormat._SCALAR:
                        queryBuilder.append(String.join(VALUE_SEPARATOR, ConfigConst.TAB_SCALAR_RW));
                        break;
                    case AttrDataFormat._SPECTRUM:
                        queryBuilder.append(String.join(VALUE_SEPARATOR, ConfigConst.TAB_SPECTRUM_RW));
                        break;
                    case AttrDataFormat._IMAGE:
                        queryBuilder.append(String.join(VALUE_SEPARATOR, ConfigConst.TAB_IMAGE_RW));
                        break;
                }
            }

            queryBuilder.append(" FROM ").append(tableName).append(" WHERE ( ABS ( TIMEDIFF(STR_TO_DATE('")
                    .append(timestamp).append("',").append(dateFormat).append(") ,").append(time)
                    .append("))) = (SELECT MIN(ABS ( TIMEDIFF(STR_TO_DATE ('").append(timestamp).append("',")
                    .append(dateFormat).append("),").append(time).append(") )) FROM ").append(tableName).append(")");
            query = queryBuilder.toString();
        }
        return query;
    }

}
