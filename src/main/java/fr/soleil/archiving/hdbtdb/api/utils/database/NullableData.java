package fr.soleil.archiving.hdbtdb.api.utils.database;

import java.util.Arrays;

import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.database.DBExtractionConst;

public class NullableData<T> implements DBExtractionConst {

    private Object value;
    private T nullElements;

    public NullableData(Object value, T nullElements) {
        super();
        this.value = value;
        this.nullElements = nullElements;
    }

    public Object getValue() {
        return value;
    }

    public T getNullElements() {
        return nullElements;
    }

    @Override
    protected void finalize() {
        value = null;
        nullElements = null;
    }

    public static void fillTimedData(NullableData<?>[] nullableData, NullableTimedData readData,
            NullableTimedData writeData) {
        if ((nullableData == null) || (nullableData.length < 2)) {
            if (readData != null) {
                readData.setValue(null, null);
            }
            if (writeData != null) {
                writeData.setValue(null, null);
            }
        } else {
            if (nullableData[READ_INDEX] == null) {
                if (readData != null) {
                    readData.setValue(null, null);
                }
            } else if (readData != null) {
                readData.setValue(nullableData[READ_INDEX].getValue(), nullableData[READ_INDEX].getNullElements());
            }
            if (nullableData[WRITE_INDEX] == null) {
                if (writeData != null) {
                    writeData.setValue(null, null);
                }
            } else if (writeData != null) {
                writeData.setValue(nullableData[WRITE_INDEX].getValue(), nullableData[WRITE_INDEX].getNullElements());
            }
        }
        if (nullableData != null) {
            Arrays.fill(nullableData, null);
        }
    }

}
