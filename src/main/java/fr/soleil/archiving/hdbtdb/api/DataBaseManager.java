package fr.soleil.archiving.hdbtdb.api;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.AttributeExtractor;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.AttributeExtractorFactroy;
import fr.soleil.archiving.hdbtdb.api.management.modes.IGenericMode;
import fr.soleil.archiving.hdbtdb.api.management.modes.ModeFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.MySQLDataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

public class DataBaseManager {
    // private IDBConnection dbConn;
    protected AbstractDataBaseConnector connector;
    protected boolean hdb;

    public DataBaseManager(final AbstractDataBaseConnector connector, final boolean hdb) {
        this.connector = connector;
        this.hdb = hdb;
    }

    public IDbUtils getDbUtil() throws ArchivingException {
        return DbUtilsFactory.getInstance(connector);
    }

    public IAdtAptAttributes getAttribute() throws ArchivingException {
        return AdtAptAttributesFactory.getInstance(connector);

    }

    public AttributeExtractor getExtractor() throws ArchivingException {
        return AttributeExtractorFactroy.getAttributeExtractor(connector);

    }

    public IGenericMode getMode() throws ArchivingException {
        return ModeFactory.getInstance(connector, hdb);
    }

    public boolean isOracle() {
        return connector instanceof OracleDataBaseConnector;
    }

    public boolean isMySQL() {
        return connector instanceof MySQLDataBaseConnector;
    }

    public String getConnectionInfo() {
        return connector.getParams().toString();
    }

    public String getHost() {
        return connector == null ? null : connector.getParams().getHost();
    }

    public String getUser() {
        return connector == null ? null : connector.getParams().getUser();
    }

}
