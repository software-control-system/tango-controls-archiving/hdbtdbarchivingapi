/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.modes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.EventMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeCalcul;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeSeuil;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtils;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

/**
 * @author AYADI
 *
 */
public abstract class GenericMode implements IGenericMode, DBExtractionConst {

    protected final AbstractDataBaseConnector connector;
    protected boolean caseSensitiveDb;

    public GenericMode(final AbstractDataBaseConnector connector) {
        this.connector = connector;
        this.caseSensitiveDb = connector instanceof OracleDataBaseConnector;
    }

    abstract protected StringBuilder updateSelectField(StringBuilder selectFieldBuilder);

    abstract protected void setSpec(Mode mode, ResultSet rset) throws SQLException;

    abstract protected void setSpecificStatementForInsertMode(PreparedStatement preparedStatement,
            AttributeLightMode attributeLightMode) throws SQLException;

    abstract protected void appendQuery(StringBuilder query, StringBuilder tableName, StringBuilder selectField);

    /**
     *
     * @return
     * @throws ArchivingException
     */
    @Override
    public String[] getCurrentArchivedAtt() throws ArchivingException {
        return getAttribute(true);
    }

    /**
     *
     * @param archivingOnly
     * @return
     * @throws ArchivingException
     */
    @Override
    public String[] getAttribute(final boolean archivingOnly) throws ArchivingException {
        String[] nameArr;
        if (connector == null) {
            nameArr = null;
        } else {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            String getAttributeDataQuery = null;
            try {
                final Collection<String> nameVect = new ArrayList<String>();
                // First connect with the database
                // if ( dbConn.isAutoConnect() )
                // {
                // dbConn.connect();
                // }

                // Create and execute the SQL query string
                // Build the query string
                final String selectField = new StringBuilder(ConfigConst.ADT).append(DB_SEPARATOR)
                        .append(ConfigConst.FULL_NAME).toString();
                final String table1 = new StringBuilder(connector.getSchema()).append(DB_SEPARATOR)
                        .append(ConfigConst.ADT).toString();
                final String table2 = new StringBuilder(connector.getSchema()).append(DB_SEPARATOR)
                        .append(ConfigConst.AMT).toString();
                final String tables = new StringBuilder(table1).append(VALUE_SEPARATOR).append(table2).toString();
                final String clause1 = new StringBuilder(ConfigConst.AMT).append(DB_SEPARATOR)
                        .append(ConfigConst.STOP_DATE).append(" IS NULL").toString();
                final String clause2 = new StringBuilder(ConfigConst.AMT).append(DB_SEPARATOR).append(ConfigConst.ID)
                        .append(" = ").append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.ID).toString();

                final String whereClause = archivingOnly
                        ? new StringBuilder("(").append(clause1).append(" AND ").append(clause2).append(")").toString()
                        : clause2;

                if (archivingOnly) {
                    getAttributeDataQuery = new StringBuilder("SELECT ").append(selectField).append(" FROM ")
                            .append(tables).append(" WHERE ").append(whereClause).toString();
                } else {
                    getAttributeDataQuery = new StringBuilder("SELECT ").append(selectField).append(" FROM ")
                            .append(table1).toString();
                }
                conn = connector.getConnection();
                if (conn == null) {
                    nameArr = null;
                } else {
                    stmt = conn.createStatement();
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(getAttributeDataQuery);
                    while (rset.next()) {
                        final String next = rset.getString(1);
                        if (next != null) {
                            nameVect.add(next);
                        }
                    }
                    nameArr = DbUtils.toStringArray(nameVect);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getAttributeDataQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }

        // Returns the names list
        return nameArr;
    }

    /**
     * <b>Description : </b> Gets the number of attributes that are being
     * archived in <I>HDB</I>
     *
     * @return An integer which is the number of attributes being archived in
     *         <I>HDB</I>
     * @throws ArchivingException
     */
    @Override
    public int getCurrentArchivedAttCount() throws ArchivingException {
        int activeSimpleSignalCount = 0;
        if (connector != null) {
            // todo test
            Statement stmt = null;
            ResultSet rset = null;

            // First connect with the database
            // (if not already done)
            // if ( dbConn.isAutoConnect() )
            // dbConn.connect();

            // Create and execute the SQL query string
            final String getAttributeDataQuery = new StringBuilder("SELECT DISTINCT(COUNT(*)) FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.AMT).append(" WHERE (")
                    .append(ConfigConst.AMT).append(DB_SEPARATOR).append(ConfigConst.START_DATE)
                    .append(" IS NOT NULL AND ").append(ConfigConst.AMT).append(DB_SEPARATOR)
                    .append(ConfigConst.STOP_DATE).append(" IS NULL)").toString();
            Connection conn = null;
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(getAttributeDataQuery);
                    // Gets the result of the query
                    if (rset.next()) {
                        activeSimpleSignalCount = rset.getInt(1);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getAttributeDataQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        // Returns the number of active simple signals defined in HDB
        return activeSimpleSignalCount;
    }

    /**
     * <b>Description : </b> Gets the name of the device in charge of the
     * archiving of the given attribute.
     *
     * @return the name of the device in charge of the archiving of the given
     *         attribute.
     * @throws ArchivingException
     */
    @Override
    public String getDeviceInCharge(final String attributeName) throws ArchivingException {
        String deviceInCharge;
        if (connector == null) {
            deviceInCharge = null;
        } else {
            deviceInCharge = "";
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            String getDeviceInChargeQuery = "";
            final StringBuilder selectField = new StringBuilder("SELECT ").append(ConfigConst.AMT).append(DB_SEPARATOR)
                    .append(ConfigConst.ARCHIVER);

            selectField.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.AMT)
                    .append(VALUE_SEPARATOR).append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.ADT)
                    .append(" WHERE (").append("(").append(ConfigConst.AMT).append(DB_SEPARATOR).append(ConfigConst.ID)
                    .append(" = ").append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.ID).append(")")
                    .append(" AND ");
            if (caseSensitiveDb) {
                // XXX Use "LOWER" to avoid case sensitivity problems. Might reduce performances.
                selectField.append("(LOWER(").append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.FULL_NAME)
                        .append(")= LOWER(?))");
            } else {
                selectField.append("(").append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.FULL_NAME)
                        .append("= ?)");
            }
            selectField.append(" AND (").append(ConfigConst.AMT).append(DB_SEPARATOR).append(ConfigConst.STOP_DATE)
                    .append(" IS NULL ").append(")").append(") ");

            getDeviceInChargeQuery = selectField.toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(getDeviceInChargeQuery);
                    final String attrName = attributeName.trim();
                    getDeviceInChargeQuery = getDeviceInChargeQuery.replaceFirst("\\?", attrName);
                    preparedStatement.setString(1, attrName);
                    rset = preparedStatement.executeQuery();
                    while (rset.next()) {
                        deviceInCharge = rset.getString(ConfigConst.ARCHIVER);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getDeviceInChargeQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
        // Returns the names list
        return deviceInCharge;
    }

    /**
     * <b>Description : </b>
     *
     * @param attributeName
     *            The name of the attribute
     * @return a boolean : "true" if the attribute of given name is currently
     *         archived, "false" otherwise.
     * @throws ArchivingException
     */
    @Override
    public boolean isArchived(final String attributeName) throws ArchivingException {
        return isArchived(attributeName, null);
    }

    /**
     * <b>Description : </b>
     *
     * @param attributeName
     *            The name of the attribute
     * @param deviceName
     *            The name of the device in charge
     * @return a boolean : "true" if the attribute named att_name is currently
     *         archived by the device named device_name, "false" otherwise.
     * @throws ArchivingException
     */
    @Override
    public boolean isArchived(final String attributeName, final String deviceName) throws ArchivingException {
        boolean ret = false;
        if (connector != null) {
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            ResultSet rset = null;

            // Build the query string
            // SELECT amt.ID FROM hdb.amt, hdb.adt WHERE ((amt.ID = adt.ID) AND (LOWER(adt.full_name) = LOWER(?))
            final StringBuilder queryBuilder = new StringBuilder("SELECT ");
            queryBuilder.append(ConfigConst.AMT_ID).append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.AMT).append(VALUE_SEPARATOR).append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.ADT).append(" WHERE ((").append(ConfigConst.AMT).append(DB_SEPARATOR)
                    .append(ConfigConst.ID).append(" = ").append(ConfigConst.ADT).append(DB_SEPARATOR + ConfigConst.ID)
                    .append(") AND ");
            if (caseSensitiveDb) {
                // XXX Use LOWER for case sensitivity problems. Might reduce performances.
                queryBuilder.append("(LOWER(").append(ConfigConst.ADT).append(DB_SEPARATOR)
                        .append(ConfigConst.FULL_NAME).append(") = LOWER(?))");
            } else {
                queryBuilder.append("(").append(ConfigConst.ADT).append(DB_SEPARATOR).append(ConfigConst.FULL_NAME)
                        .append(" = ?)");
            }

            if (deviceName != null) {
                queryBuilder.append(" AND (").append(ConfigConst.AMT_ARCHIVER).append(" =  ?)");
            }
            queryBuilder.append(" AND (").append(ConfigConst.AMT_STOP_DATE).append(" IS NULL ))");
            /*
             * Complete query: "[]" for optional part
             * SELECT amt.ID FROM hdb.amt, hdb.adt WHERE ((amt.ID = adt.ID) AND (LOWER(adt.full_name) = LOWER(?)) [AND (amt.archiver = ?)] AND (amt.stop_date IS NULL ))
             */
            String isArchivedQuery = queryBuilder.toString();

            try {
                conn = connector.getConnection();
                final String attrName = attributeName.trim();
                preparedStatement = conn.prepareStatement(isArchivedQuery);
                isArchivedQuery = isArchivedQuery.replaceFirst("\\?", attrName);
                preparedStatement.setString(1, attrName);
                if (deviceName != null) {
                    final String devName = deviceName.trim();
                    isArchivedQuery = isArchivedQuery.replaceFirst("\\?", devName);
                    preparedStatement.setString(2, devName);
                }
                rset = preparedStatement.executeQuery();
                if (rset.next()) {
                    ret = true;
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, isArchivedQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }

        }
        // Close the connection with the database
        return ret;
    }

    /**
     * <b>Description : </b>
     *
     * @param attributeName
     *            The name of the attribute
     * @return a String : The name of the corresponding archiver if the
     *         attribute is beeing archived, an empty String otherwise
     * @throws ArchivingException
     */
    @Override
    public String getArchiverForAttribute(final String attributeName) throws ArchivingException {
        String result;
        if (connector == null) {
            result = null;
        } else {

            final List<String> archivVect = new ArrayList<String>();
            int res = 0;
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            ResultSet rset = null;
            // Build the query string
            final StringBuilder selectFieldBuilder = new StringBuilder("SELECT ").append(ConfigConst.AMT)
                    .append(DB_SEPARATOR).append(ConfigConst.ARCHIVER);

            selectFieldBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.AMT).append(VALUE_SEPARATOR).append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.ADT).append(" WHERE ((").append(ConfigConst.AMT).append(DB_SEPARATOR)
                    .append(ConfigConst.ID).append(" = ").append(ConfigConst.ADT).append(DB_SEPARATOR)
                    .append(ConfigConst.ID).append(") AND ");
            if (caseSensitiveDb) {
                // XXX Use "LOWER" to avoid case sensitivity problems. Might reduce performances.
                selectFieldBuilder.append("(LOWER(").append(ConfigConst.ADT).append(DB_SEPARATOR)
                        .append(ConfigConst.FULL_NAME).append(") = LOWER(?))");
            } else {
                selectFieldBuilder.append("(").append(ConfigConst.ADT).append(DB_SEPARATOR)
                        .append(ConfigConst.FULL_NAME).append(" = ?)");
            }
            selectFieldBuilder.append(" AND (").append(ConfigConst.AMT_STOP_DATE).append(" IS NULL ))");
            String isArchivedQuery = selectFieldBuilder.toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(isArchivedQuery);
                    final String attrName = attributeName.trim();
                    isArchivedQuery = isArchivedQuery.replaceFirst("\\?", attrName);
                    preparedStatement.setString(1, attrName);
                    rset = preparedStatement.executeQuery();
                    while (rset.next()) {
                        archivVect.add(rset.getString(1));
                        res++;
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, isArchivedQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
            // Close the connection with the database
            if (res > 0) {
                result = archivVect.get(0);
            } else {
                result = "";
            }
        }
        return result;
    }

    /**
     * <b>Description : </b> Gets the current archiving mode for a given
     * attribute name.
     *
     * @return An array of string containing all the current mode's informations
     *         for a given attibute's name
     * @throws ArchivingException
     */
    @Override
    public Mode getCurrentArchivingMode(final String attributeName) throws ArchivingException {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        ResultSet rset = null;
        // boolean isArchived = false;
        final Mode mode = new Mode();
        String getCurrentArchivingModeQuery = null;

        if (connector != null) {
            try {

                // Create and execute the SQL query string
                // Build the query string
                StringBuilder selectFieldBuilder = new StringBuilder("SELECT ").append(ConfigConst.AMT_PERDIODIC_MODE)
                        .append(VALUE_SEPARATOR).append(ConfigConst.AMT_PERDIODIC_MODE_PERIOD).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_ABSOLUTE_MODE).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_ABSOLUTE_MODE_PERIOD).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_ABSOLUTE_MODE_INF).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_ABSOLUTE_MODE_SUP).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_RELATIVE_MODE).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_RELATIVE_MODE_PERIOD).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_RELATIVE_MODE_INF).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_RELATIVE_MODE_SUP).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_THRESHOLD_MODE).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_THRESHOLD_MODE_PERIOD).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_THRESHOLD_MODE_INF).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_THRESHOLD_MODE_SUP).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_CALC_MODE).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_CALC_MODE_PERIOD).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_CALC_MODE_VAL).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_CALC_MODE_TYPE).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_CALC_MODE_ALGO).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_DIFF_MODE).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_DIFF_MODE_PERIOD).append(VALUE_SEPARATOR)
                        .append(ConfigConst.AMT_EXT_MODE);

                selectFieldBuilder = updateSelectField(selectFieldBuilder);

                selectFieldBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                        .append(ConfigConst.AMT).append(VALUE_SEPARATOR).append(connector.getSchema())
                        .append(DB_SEPARATOR).append(ConfigConst.ADT).append(" WHERE ((").append(ConfigConst.AMT)
                        .append(DB_SEPARATOR).append(ConfigConst.ID).append(" = ").append(ConfigConst.ADT)
                        .append(DB_SEPARATOR).append(ConfigConst.ID).append(") AND ");
                if (caseSensitiveDb) {
                    // XXX Use "LOWER" to avoid case sensitivity problems. Might reduce performances.
                    selectFieldBuilder.append("(LOWER(").append(ConfigConst.ADT).append(DB_SEPARATOR)
                            .append(ConfigConst.FULL_NAME).append(") = LOWER(?))");
                } else {
                    selectFieldBuilder.append("(").append(ConfigConst.ADT).append(DB_SEPARATOR)
                            .append(ConfigConst.FULL_NAME).append(" = ?)");
                }
                selectFieldBuilder.append(" AND (").append(ConfigConst.AMT).append(DB_SEPARATOR)
                        .append(ConfigConst.STOP_DATE).append(" IS NULL ").append(")").append(") ");
                getCurrentArchivingModeQuery = selectFieldBuilder.toString();
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(getCurrentArchivingModeQuery);
                    // dbConn.setLastStatement(preparedStatement);
                    final String attrName = attributeName.trim();
                    getCurrentArchivingModeQuery = getCurrentArchivingModeQuery.replaceFirst("\\?", attrName);
                    preparedStatement.setString(1, attrName);
                    rset = preparedStatement.executeQuery();
                    while (rset.next()) {
                        // isArchived = true;
                        if (rset.getInt(ConfigConst.PERIODIC_MODE) == 1) {
                            final ModePeriode modePeriode = new ModePeriode(
                                    rset.getInt(ConfigConst.PERIODIC_MODE_PERIOD));
                            mode.setModeP(modePeriode);
                        }
                        if (rset.getInt(ConfigConst.ABSOLUTE_MODE) == 1) {
                            final ModeAbsolu modeAbsolu = new ModeAbsolu(rset.getInt(ConfigConst.ABSOLUTE_MODE_PERIOD),
                                    rset.getDouble(ConfigConst.ABSOLUTE_MODE_INF),
                                    rset.getDouble(ConfigConst.ABSOLUTE_MODE_SUP), false);
                            mode.setModeA(modeAbsolu);
                        } else if (rset.getInt(ConfigConst.ABSOLUTE_MODE) == 2) {
                            final ModeAbsolu modeAbsolu = new ModeAbsolu(rset.getInt(ConfigConst.ABSOLUTE_MODE_PERIOD),
                                    rset.getDouble(ConfigConst.ABSOLUTE_MODE_INF),
                                    rset.getDouble(ConfigConst.ABSOLUTE_MODE_SUP), true);
                            mode.setModeA(modeAbsolu);
                        }
                        if (rset.getInt(ConfigConst.RELATIVE_MODE) == 1) {
                            final ModeRelatif modeRelatif = new ModeRelatif(
                                    rset.getInt(ConfigConst.RELATIVE_MODE_PERIOD),
                                    rset.getDouble(ConfigConst.RELATIVE_MODE_INF),
                                    rset.getDouble(ConfigConst.RELATIVE_MODE_SUP), false);
                            mode.setModeR(modeRelatif);
                        } else if (rset.getInt(ConfigConst.RELATIVE_MODE) == 2) {
                            final ModeRelatif modeRelatif = new ModeRelatif(
                                    rset.getInt(ConfigConst.RELATIVE_MODE_PERIOD),
                                    rset.getDouble(ConfigConst.RELATIVE_MODE_INF),
                                    rset.getDouble(ConfigConst.RELATIVE_MODE_SUP), true);
                            mode.setModeR(modeRelatif);
                        }
                        if (rset.getInt(ConfigConst.THRESHOLD_MODE) == 1) {
                            final ModeSeuil modeSeuil = new ModeSeuil(rset.getInt(ConfigConst.THRESHOLD_MODE_PERIOD),
                                    rset.getDouble(ConfigConst.THRESHOLD_MODE_INF),
                                    rset.getDouble(ConfigConst.THRESHOLD_MODE_SUP));
                            mode.setModeT(modeSeuil);
                        }
                        if (rset.getInt(ConfigConst.CALC_MODE) == 1) {
                            final ModeCalcul modeCalcul = new ModeCalcul(rset.getInt(ConfigConst.CALC_MODE_PERIOD),
                                    rset.getInt(ConfigConst.CALC_MODE_VAL), rset.getInt(ConfigConst.CALC_MODE_TYPE));
                            mode.setModeC(modeCalcul);
                            // Warning Field 18 is not used yet ...
                        }
                        if (rset.getInt(ConfigConst.DIFF_MODE) == 1) {
                            final ModeDifference modeDifference = new ModeDifference(
                                    rset.getInt(ConfigConst.DIFF_MODE_PERIOD));
                            mode.setModeD(modeDifference);
                        }
                        if (rset.getInt(ConfigConst.EXT_MODE) == 1) {
                            final EventMode eventMode = new EventMode();
                            mode.setEventMode(eventMode);
                        }
                        setSpec(mode, rset);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getCurrentArchivingModeQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }

        return mode;
    }

    /**
     * This method retrieves a given archiver's current tasks and for a given
     * archiving type.
     *
     * @param archiverName
     * @param historic
     * @return The current task for a given archiver and for a given archiving
     *         type.
     * @throws ArchivingException
     */
    @Override
    public Collection<AttributeLightMode> getArchiverCurrentTasks(final String archiverName) throws ArchivingException {
        final Collection<AttributeLightMode> attributeListConfig;
        if (connector == null) {
            attributeListConfig = null;
        } else {
            attributeListConfig = new ArrayList<AttributeLightMode>();
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            String getArchiverCurrentTasksQuery = "";
            // ADT
            StringBuilder selectFieldBuilder = new StringBuilder("SELECT ").append(ConfigConst.ADT).append(DB_SEPARATOR)
                    .append(ConfigConst.FULL_NAME).append(VALUE_SEPARATOR).append(// fullname
                            ConfigConst.ADT)
                    .append(DB_SEPARATOR).append(ConfigConst.DATA_TYPE).append(VALUE_SEPARATOR).append(// data_type
                            ConfigConst.ADT)
                    .append(DB_SEPARATOR).append(ConfigConst.DATA_FORMAT).append(VALUE_SEPARATOR).append(// data_format
                            ConfigConst.ADT)
                    .append(DB_SEPARATOR).append(ConfigConst.WRITABLE).append(VALUE_SEPARATOR); // writable
            // AMT
            selectFieldBuilder = selectFieldBuilder.append(ConfigConst.AMT_PERDIODIC_MODE).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_PERDIODIC_MODE_PERIOD).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_ABSOLUTE_MODE).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_ABSOLUTE_MODE_PERIOD).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_ABSOLUTE_MODE_INF).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_ABSOLUTE_MODE_SUP).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_RELATIVE_MODE).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_RELATIVE_MODE_PERIOD).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_RELATIVE_MODE_INF).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_RELATIVE_MODE_SUP).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_THRESHOLD_MODE).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_THRESHOLD_MODE_PERIOD).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_THRESHOLD_MODE_INF).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_THRESHOLD_MODE_SUP).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_CALC_MODE).append(VALUE_SEPARATOR).append(ConfigConst.AMT_CALC_MODE_PERIOD)
                    .append(VALUE_SEPARATOR).append(ConfigConst.AMT_CALC_MODE_VAL).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_CALC_MODE_TYPE).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_CALC_MODE_ALGO).append(VALUE_SEPARATOR).append(ConfigConst.AMT_DIFF_MODE)
                    .append(VALUE_SEPARATOR).append(ConfigConst.AMT_DIFF_MODE_PERIOD).append(VALUE_SEPARATOR)
                    .append(ConfigConst.AMT_EXT_MODE);
            selectFieldBuilder = updateSelectField(selectFieldBuilder);

            // String clause_2 = ConfigConst.TABS[ 2 ] + DB_SEPARATOR + ConfigConst.TAB_MOD[
            // 1 ] + " LIKE " + "?";

            selectFieldBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.ADT).append(VALUE_SEPARATOR).append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.AMT).append(" WHERE ((").append(ConfigConst.ADT).append(DB_SEPARATOR)
                    .append(ConfigConst.ID).append(" = ").append(ConfigConst.AMT).append(DB_SEPARATOR)
                    .append(ConfigConst.ID).append(") AND (").append(ConfigConst.AMT).append(DB_SEPARATOR)
                    .append(ConfigConst.ARCHIVER).append(" = ?) AND (").append(ConfigConst.AMT).append(DB_SEPARATOR)
                    .append(ConfigConst.STOP_DATE).append(" IS NULL )) ");

            getArchiverCurrentTasksQuery = selectFieldBuilder.toString();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(getArchiverCurrentTasksQuery);
                    final String archiver = archiverName.trim();
                    getArchiverCurrentTasksQuery = getArchiverCurrentTasksQuery.replaceFirst("\\?", archiver);
                    preparedStatement.setString(1, archiver);
                    rset = preparedStatement.executeQuery();
                    while (rset.next()) {
                        boolean sd;
                        final AttributeLightMode attributeLightMode = new AttributeLightMode();

                        attributeLightMode.setAttributeCompleteName(rset.getString(1));
                        attributeLightMode.setDataType(rset.getInt(2));
                        attributeLightMode.setDataFormat(rset.getInt(3));
                        attributeLightMode.setWritable(rset.getInt(4));
                        attributeLightMode.setDevice_in_charge(archiverName);
                        // Mode
                        final Mode mode = new Mode();
                        if (rset.getInt(ConfigConst.PERIODIC_MODE) != 0) {
                            final ModePeriode modePeriode = new ModePeriode(
                                    rset.getInt(ConfigConst.PERIODIC_MODE_PERIOD));
                            mode.setModeP(modePeriode);
                        }
                        if (rset.getInt(ConfigConst.ABSOLUTE_MODE) != 0) {
                            sd = rset.getInt(ConfigConst.ABSOLUTE_MODE) == 2;
                            final ModeAbsolu modeAbsolu = new ModeAbsolu(rset.getInt(ConfigConst.ABSOLUTE_MODE_PERIOD),
                                    rset.getDouble(ConfigConst.ABSOLUTE_MODE_INF),
                                    rset.getDouble(ConfigConst.ABSOLUTE_MODE_SUP), sd);
                            mode.setModeA(modeAbsolu);
                        }
                        if (rset.getInt(ConfigConst.RELATIVE_MODE) != 0) {
                            sd = rset.getInt(ConfigConst.RELATIVE_MODE) == 2;
                            final ModeRelatif modeRelatif = new ModeRelatif(
                                    rset.getInt(ConfigConst.RELATIVE_MODE_PERIOD),
                                    rset.getDouble(ConfigConst.RELATIVE_MODE_INF),
                                    rset.getDouble(ConfigConst.RELATIVE_MODE_SUP), sd);
                            mode.setModeR(modeRelatif);
                        }
                        if (rset.getInt(ConfigConst.THRESHOLD_MODE) != 0) {
                            final ModeSeuil modeSeuil = new ModeSeuil(rset.getInt(ConfigConst.THRESHOLD_MODE_PERIOD),
                                    rset.getDouble(ConfigConst.THRESHOLD_MODE_INF),
                                    rset.getDouble(ConfigConst.THRESHOLD_MODE_SUP));
                            mode.setModeT(modeSeuil);
                        }
                        if (rset.getInt(ConfigConst.CALC_MODE) != 0) {
                            final ModeCalcul modeCalcul = new ModeCalcul(rset.getInt(ConfigConst.CALC_MODE_PERIOD),
                                    rset.getInt(ConfigConst.CALC_MODE_VAL), rset.getInt(ConfigConst.CALC_MODE_TYPE));
                            mode.setModeC(modeCalcul);
                        }
                        if (rset.getInt(ConfigConst.DIFF_MODE) != 0) {
                            final ModeDifference modeDifference = new ModeDifference(
                                    rset.getInt(ConfigConst.DIFF_MODE_PERIOD));
                            mode.setModeD(modeDifference);
                        }
                        if (rset.getInt(ConfigConst.EXT_MODE) != 0) {
                            final EventMode eventMode = new EventMode();
                            mode.setEventMode(eventMode);
                        }
                        setSpec(mode, rset);
                        attributeLightMode.setMode(mode);
                        attributeListConfig.add(attributeLightMode);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getArchiverCurrentTasksQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
        return attributeListConfig;
    }

    /**
     * <b>Description : </b> Inserts a record in the "Attribut Mode Table"
     * <I>(mySQL only)</I>. Each time that the archiving of an attribute is
     * triggered, this table is fielded.
     */
    @Override
    public void insertModeRecord(final AttributeLightMode attributeLightMode) throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            final int id = AdtAptAttributesFactory.getInstance(connector).getIds()
                    .getAttID(attributeLightMode.getAttributeCompleteName(), caseSensitiveDb);
            final StringBuilder tableName = new StringBuilder().append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.AMT);

            final StringBuilder selectField = new StringBuilder();
            selectField.append(ConfigConst.AMT_ID).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_ARCHIVER).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_START_DATE).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_PERDIODIC_MODE).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_PERDIODIC_MODE_PERIOD).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_ABSOLUTE_MODE).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_ABSOLUTE_MODE_PERIOD).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_ABSOLUTE_MODE_INF).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_ABSOLUTE_MODE_SUP).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_RELATIVE_MODE).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_RELATIVE_MODE_PERIOD).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_RELATIVE_MODE_INF).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_RELATIVE_MODE_SUP).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_THRESHOLD_MODE).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_THRESHOLD_MODE_PERIOD).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_THRESHOLD_MODE_INF).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_THRESHOLD_MODE_SUP).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_CALC_MODE).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_CALC_MODE_PERIOD).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_CALC_MODE_VAL).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_CALC_MODE_TYPE).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_CALC_MODE_ALGO).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_DIFF_MODE).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_DIFF_MODE_PERIOD).append(VALUE_SEPARATOR);
            selectField.append(ConfigConst.AMT_EXT_MODE);

            updateSelectField(selectField);
            final StringBuilder formatselectField = new StringBuilder();
            formatselectField.append(selectField.toString().replaceAll("amt\\.", ""));
            // Create and execute the SQL query string
            // Build the query string
            final StringBuilder queryBuilder = new StringBuilder();
            appendQuery(queryBuilder, tableName, formatselectField);

            String query = queryBuilder.toString();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(query);

                    query = query.replaceFirst("\\?", Integer.toString(id));
                    preparedStatement.setInt(1, id);
                    query = query.replaceFirst("\\?", attributeLightMode.getDevice_in_charge());
                    preparedStatement.setString(2, attributeLightMode.getDevice_in_charge());
                    final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    query = query.replaceFirst("\\?", timestamp.toString());
                    preparedStatement.setTimestamp(3, timestamp);
                    // the field named "stop_date (3) is not included"

                    // Periodical Mode
                    if (attributeLightMode.getMode().getModeP() != null) {
                        query = query.replaceFirst("\\?", "1");
                        preparedStatement.setInt(4, 1);
                        final int period = attributeLightMode.getMode().getModeP().getPeriod();
                        query = query.replaceFirst("\\?", Integer.toString(period));
                        preparedStatement.setInt(5, period);
                    } else {
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(4, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(5, 0);
                    }
                    // Absolute Mode
                    if (attributeLightMode.getMode().getModeA() != null) {
                        if (attributeLightMode.getMode().getModeA().isSlow_drift()) {
                            query = query.replaceFirst("\\?", "2");
                            preparedStatement.setInt(6, 2);
                        } else {
                            query = query.replaceFirst("\\?", "1");
                            preparedStatement.setInt(6, 1);
                        }
                        final int period = attributeLightMode.getMode().getModeA().getPeriod();
                        query = query.replaceFirst("\\?", Integer.toString(period));
                        preparedStatement.setInt(7, period);
                        final double inf = attributeLightMode.getMode().getModeA().getValInf();
                        query = query.replaceFirst("\\?", Double.toString(inf));
                        preparedStatement.setDouble(8, inf);
                        final double sup = attributeLightMode.getMode().getModeA().getValSup();
                        query = query.replaceFirst("\\?", Double.toString(sup));
                        preparedStatement.setDouble(9, sup);
                    } else {
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(6, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(7, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(8, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(9, 0);
                    }
                    // Relative Mode
                    if (attributeLightMode.getMode().getModeR() != null) {
                        if (attributeLightMode.getMode().getModeR().isSlow_drift()) {
                            query = query.replaceFirst("\\?", "2");
                            preparedStatement.setInt(10, 2);
                        } else {
                            query = query.replaceFirst("\\?", "1");
                            preparedStatement.setInt(10, 1);
                        }
                        final int period = attributeLightMode.getMode().getModeR().getPeriod();
                        query = query.replaceFirst("\\?", Integer.toString(period));
                        preparedStatement.setInt(11, period);
                        final double inf = attributeLightMode.getMode().getModeR().getPercentInf();
                        query = query.replaceFirst("\\?", Double.toString(inf));
                        preparedStatement.setDouble(12, inf);
                        final double sup = attributeLightMode.getMode().getModeR().getPercentSup();
                        query = query.replaceFirst("\\?", Double.toString(sup));
                        preparedStatement.setDouble(13, sup);
                    } else {
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(10, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(11, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(12, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(13, 0);
                    }
                    // Threshold Mode
                    if (attributeLightMode.getMode().getModeT() != null) {
                        query = query.replaceFirst("\\?", "1");
                        preparedStatement.setInt(14, 1);
                        final int period = attributeLightMode.getMode().getModeT().getPeriod();
                        query = query.replaceFirst("\\?", Integer.toString(period));
                        preparedStatement.setInt(15, period);
                        final double inf = attributeLightMode.getMode().getModeT().getThresholdInf();
                        query = query.replaceFirst("\\?", Double.toString(inf));
                        preparedStatement.setDouble(16, inf);
                        final double sup = attributeLightMode.getMode().getModeT().getThresholdSup();
                        query = query.replaceFirst("\\?", Double.toString(sup));
                        preparedStatement.setDouble(17, sup);
                    } else {
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(14, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(15, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(16, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(17, 0);
                    }
                    // On Calculation Mode
                    if (attributeLightMode.getMode().getModeC() != null) {
                        query = query.replaceFirst("\\?", "1");
                        preparedStatement.setInt(18, 1);
                        final int period = attributeLightMode.getMode().getModeC().getPeriod();
                        query = query.replaceFirst("\\?", Integer.toString(period));
                        preparedStatement.setInt(19, period);
                        final int range = attributeLightMode.getMode().getModeC().getRange();
                        query = query.replaceFirst("\\?", Integer.toString(range));
                        preparedStatement.setInt(20, range);
                        final int type = attributeLightMode.getMode().getModeC().getTypeCalcul();
                        query = query.replaceFirst("\\?", Integer.toString(type));
                        preparedStatement.setInt(21, type);
                        query = query.replaceFirst("\\?", "''");
                        preparedStatement.setString(22, "");
                    } else {
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(18, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(19, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(20, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(21, 0);
                        query = query.replaceFirst("\\?", "''");
                        preparedStatement.setString(22, "");
                    }
                    // On Difference Mode
                    if (attributeLightMode.getMode().getModeD() != null) {
                        query = query.replaceFirst("\\?", "1");
                        preparedStatement.setInt(23, 1);
                        final int period = attributeLightMode.getMode().getModeD().getPeriod();
                        query = query.replaceFirst("\\?", Integer.toString(period));
                        preparedStatement.setInt(24, period);
                    } else {
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(23, 0);
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(24, 0);
                    }
                    // On External Mode
                    if (attributeLightMode.getMode().getEventMode() != null) {
                        query = query.replaceFirst("\\?", "1");
                        preparedStatement.setInt(25, 1);
                    } else {
                        query = query.replaceFirst("\\?", "0");
                        preparedStatement.setInt(25, 0);
                    }

                    // Specif Tdb

                    setSpecificStatementForInsertMode(preparedStatement, attributeLightMode);
                    preparedStatement.executeUpdate();
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
    }

    /**
     * Method which updates a record in the "Attribute Mode Table" Each time
     * that the archiving of an attribute is stopped, one of this table's row is
     * fielded.
     */

    @Override
    public void updateModeRecord(final String attributeName) throws ArchivingException {
        // DbModeFactory.getInstance(arch_type).updateModeRecord(attribute_name);
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (connector != null && att != null) {
            final int attID = att.getIds().getAttID(attributeName, caseSensitiveDb);
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            // Create and execute the SQL query string
            // Build the query string
            final String updateQuery = new StringBuilder().append("UPDATE ").append(connector.getSchema())
                    .append(DB_SEPARATOR).append(ConfigConst.AMT).append(" SET ").append(ConfigConst.STOP_DATE)
                    .append(" = ? WHERE ((").append(ConfigConst.AMT_ID).append(" = ").append(attID).append(") AND (")
                    .append(ConfigConst.AMT_STOP_DATE).append(" IS NULL ))").toString();
            try {
                conn = connector.getConnection();
                preparedStatement = conn.prepareStatement(updateQuery);
                final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                preparedStatement.setTimestamp(1, timestamp);
                preparedStatement.executeUpdate();
            } catch (final SQLException e) {
                throw new ArchivingException(e, updateQuery);
            } finally {
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
    }

    @Override
    public void updateModeRecordWithoutStop(final AttributeLightMode attributeLightMode) throws ArchivingException {
        if (connector != null) {
            final int id = AdtAptAttributesFactory.getInstance(connector).getIds()
                    .getAttID(attributeLightMode.getAttributeCompleteName(), caseSensitiveDb);
            final StringBuilder tableName = new StringBuilder().append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(ConfigConst.AMT);

            final StringBuilder query = new StringBuilder();
            query.append("UPDATE ").append(tableName).append(" SET ");
            query.append(ConfigConst.AMT_ARCHIVER).append("=?,");
            query.append(ConfigConst.AMT_PERDIODIC_MODE).append("=?,");
            query.append(ConfigConst.AMT_PERDIODIC_MODE_PERIOD).append("=?,");
            query.append(ConfigConst.AMT_ABSOLUTE_MODE).append("=?,");
            query.append(ConfigConst.AMT_ABSOLUTE_MODE_PERIOD).append("=?,");
            query.append(ConfigConst.AMT_ABSOLUTE_MODE_INF).append("=?,");
            query.append(ConfigConst.AMT_ABSOLUTE_MODE_SUP).append("=?,");
            query.append(ConfigConst.AMT_RELATIVE_MODE).append("=?,");
            query.append(ConfigConst.AMT_RELATIVE_MODE_PERIOD).append("=?,");
            query.append(ConfigConst.AMT_RELATIVE_MODE_INF).append("=?,");
            query.append(ConfigConst.AMT_RELATIVE_MODE_SUP).append("=?,");
            query.append(ConfigConst.AMT_THRESHOLD_MODE).append("=?,");
            query.append(ConfigConst.AMT_THRESHOLD_MODE_PERIOD).append("=?,");
            query.append(ConfigConst.AMT_THRESHOLD_MODE_INF).append("=?,");
            query.append(ConfigConst.AMT_THRESHOLD_MODE_SUP).append("=?,");
            query.append(ConfigConst.AMT_CALC_MODE).append("=?,");
            query.append(ConfigConst.AMT_CALC_MODE_PERIOD).append("=?,");
            query.append(ConfigConst.AMT_CALC_MODE_VAL).append("=?,");
            query.append(ConfigConst.AMT_CALC_MODE_TYPE).append("=?,");
            query.append(ConfigConst.AMT_CALC_MODE_ALGO).append("=?,");
            query.append(ConfigConst.AMT_DIFF_MODE).append("=?,");
            query.append(ConfigConst.AMT_DIFF_MODE_PERIOD).append("=?,");
            query.append(ConfigConst.AMT_EXT_MODE).append(" = ? WHERE ");
            query.append(ConfigConst.AMT_ID).append(" = ? AND ");
            query.append(ConfigConst.AMT_STOP_DATE).append(" is null");
            updateSelectField(query);

            // Create and execute the SQL query string
            // Build the query string
            PreparedStatement preparedStatement = null;
            Connection conn = null;
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(query.toString());
                    int i = 1;
                    preparedStatement.setString(i++, attributeLightMode.getDevice_in_charge());

                    // Periodical Mode
                    if (attributeLightMode.getMode().getModeP() != null) {
                        preparedStatement.setInt(i++, 1);
                        preparedStatement.setInt(i++, attributeLightMode.getMode().getModeP().getPeriod());
                    } else {
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                    }
                    // Absolute Mode
                    if (attributeLightMode.getMode().getModeA() != null) {
                        if (attributeLightMode.getMode().getModeA().isSlow_drift()) {
                            preparedStatement.setInt(i++, 2);
                        } else {
                            preparedStatement.setInt(i++, 1);
                        }
                        preparedStatement.setInt(i++, attributeLightMode.getMode().getModeA().getPeriod());
                        preparedStatement.setDouble(i++, attributeLightMode.getMode().getModeA().getValInf());
                        preparedStatement.setDouble(i++, attributeLightMode.getMode().getModeA().getValSup());
                    } else {
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                    }
                    // Relative Mode
                    if (attributeLightMode.getMode().getModeR() != null) {
                        if (attributeLightMode.getMode().getModeR().isSlow_drift()) {
                            preparedStatement.setInt(i++, 2);
                        } else {
                            preparedStatement.setInt(i++, 1);
                        }
                        preparedStatement.setInt(i++, attributeLightMode.getMode().getModeR().getPeriod());
                        preparedStatement.setDouble(i++, attributeLightMode.getMode().getModeR().getPercentInf());
                        preparedStatement.setDouble(i++, attributeLightMode.getMode().getModeR().getPercentSup());
                    } else {
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                    }
                    // Threshold Mode
                    if (attributeLightMode.getMode().getModeT() != null) {
                        preparedStatement.setInt(i++, 1);
                        preparedStatement.setInt(i++, attributeLightMode.getMode().getModeT().getPeriod());
                        preparedStatement.setDouble(i++, attributeLightMode.getMode().getModeT().getThresholdInf());
                        preparedStatement.setDouble(i++, attributeLightMode.getMode().getModeT().getThresholdSup());
                    } else {
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                    }
                    // On Calculation Mode
                    if (attributeLightMode.getMode().getModeC() != null) {
                        preparedStatement.setInt(i++, 1);
                        preparedStatement.setInt(i++, attributeLightMode.getMode().getModeC().getPeriod());
                        preparedStatement.setInt(i++, attributeLightMode.getMode().getModeC().getRange());
                        preparedStatement.setInt(i++, attributeLightMode.getMode().getModeC().getTypeCalcul());
                        preparedStatement.setString(i++, "");
                    } else {
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setString(i++, "");
                    }
                    // On Difference Mode
                    if (attributeLightMode.getMode().getModeD() != null) {
                        preparedStatement.setInt(i++, 1);
                        preparedStatement.setInt(i++, attributeLightMode.getMode().getModeD().getPeriod());
                    } else {
                        preparedStatement.setInt(i++, 0);
                        preparedStatement.setInt(i++, 0);
                    }
                    // On External Mode
                    if (attributeLightMode.getMode().getEventMode() != null) {
                        preparedStatement.setInt(i++, 1);
                    } else {
                        preparedStatement.setInt(i++, 0);
                    }
                    // ID
                    preparedStatement.setInt(i++, id);

                    // Specif Tdb

                    setSpecificStatementForInsertMode(preparedStatement, attributeLightMode);
                    preparedStatement.executeUpdate();
                }
            } catch (final SQLException e) {

                throw new ArchivingException(e, query.toString());
            } finally {
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
    }
}
