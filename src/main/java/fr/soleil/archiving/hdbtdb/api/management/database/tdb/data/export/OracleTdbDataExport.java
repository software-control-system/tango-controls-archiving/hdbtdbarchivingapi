/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.database.tdb.data.export;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 *
 */
public class OracleTdbDataExport extends TdbDataExport {
	 final static Logger logger = LoggerFactory.getLogger(OracleTdbDataExport.class);
	 
    public OracleTdbDataExport(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    public/* synchronized */void exportToDB_Data(final String remoteDir, final String fileName,
            final String tableName, final int writable) throws ArchivingException {
        if (connector != null) {
            CallableStatement callableStatement = null;
            final StringBuilder query = new StringBuilder().append("{call ").append(connector.getSchema())
                    .append(".crcontrolfile(?, ?, ?)}");
            Connection conn = null;
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    callableStatement = conn.prepareCall(query.toString());
                    callableStatement.setString(1, remoteDir);
                    callableStatement.setString(2, fileName);
                    callableStatement.setString(3, tableName);

                    callableStatement.executeUpdate();
                }
            } catch (final SQLException e) {
                query.append('(').append(remoteDir).append("),(").append(fileName).append("),(").append(tableName)
                .append(')');
                logger.error("QUERY: " + query);
                e.printStackTrace();
                throw new ArchivingException(e, query.toString());
            } finally {
                ConnectionCommands.close(callableStatement);
                connector.closeConnection(conn);
            }
        }
    }

    /**
     *
     * @param tableName
     * @throws ArchivingException
     */
    @Override
    public void forceDatabaseToImportFile(final String tableName) throws ArchivingException {
        if (connector != null) {
            CallableStatement callableStatement = null;
            final StringBuilder query = new StringBuilder().append("{call ").append(connector.getSchema())
                    .append(".force_load_data(?)}");
            Connection conn = null;
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    callableStatement = conn.prepareCall(query.toString());
                    callableStatement.setString(1, tableName);
                    callableStatement.executeUpdate();
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query.toString());
            } finally {
                ConnectionCommands.close(callableStatement);
                connector.closeConnection(conn);
            }
        }
    }

}
