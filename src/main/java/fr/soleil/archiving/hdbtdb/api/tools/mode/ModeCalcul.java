//+============================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Mode/ModeCalcul.java,v $
//
// Project:      Tango Archiving Service
//
// Description: This object is one of the Mode class fields.
//				This class describes the 'on calculation mode' (the archived value is the result of one or several treated values).
//
// $Author: chinkumo $
//
// $Revision: 1.3 $
//
// $Log: ModeCalcul.java,v $
// Revision 1.3  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.2.16.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.2  2005/01/26 15:35:38  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :   Synchrotron SOLEIL
//			    L'Orme des Merisiers
//			    Saint-Aubin - BP 48
//			    91192 GIF-sur-YVETTE CEDEX
//              FRANCE
//
//+============================================================================

package fr.soleil.archiving.hdbtdb.api.tools.mode;

/**
 * <p/>
 * <B>Description :</B><BR>
 * This object is one of the <I>Mode class</I> fields. This class describes the
 * 'on calculation mode' (the archived value is the result of one or several
 * treated values).<BR>
 * </p>
 * 
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version $Revision: 1.3 $
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.Mode
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif
 * @see fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference
 * @see EventMode
 */
public final class ModeCalcul extends ModeRoot {
    private int range;
    private int calculType;

    /**
     * Default constructor
     * 
     * @see #ModeCalcul(int, int)
     * @see #ModeCalcul(int, int, int)
     */
    public ModeCalcul() {
    }

    /**
     * This constructor takes two parameters as inputs.
     * 
     * @param ra
     *            Number of events taken into account to archive one value
     * @param ct
     *            Type of calculation applied to the data
     * @see #ModeCalcul()
     * @see #ModeCalcul(int, int, int)
     */
    public ModeCalcul(final int ra, final int ct) {
        range = ra;
        calculType = ct;
    }

    /**
     * This constructor takes three parameters as inputs :
     * 
     * @param pe
     *            Archiving (polling) pe time
     * @param ra
     *            Number of events taken into account to archive one value
     * @param ct
     *            Type of calculation applied to the data
     * @see #ModeCalcul()
     * @see #ModeCalcul(int, int)
     */
    public ModeCalcul(final int pe, final int ra, final int ct) {
        super.period = pe;
        range = ra;
        calculType = ct;
    }

    /**
     * Gets the number of events taken into account to archive one value
     * 
     * @return the number of events taken into account to archive one value
     */
    public int getRange() {
        return range;
    }

    /**
     * Sets the number of events taken into account to archive one value
     * 
     * @param ra
     *            the number of events taken into account to archive one value
     */
    public void setRange(final int ra) {
        range = ra;
    }

    /**
     * Gets the calculation type applied to the data
     * 
     * @return the calculation type applied to the data
     */
    public int getTypeCalcul() {
        return calculType;
    }

    /**
     * Sets the calculation type applied to the data
     * 
     * @param ct
     *            the calculation type applied to the data
     */
    public void setTypeCalcul(final int ct) {
        calculType = ct;
    }

    /**
     * Returns a string representation of the object <I>ModeCalcul</I>.
     * 
     * @return a string representation of the object <I>ModeCalcul</I>.
     */
    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder("");
        buf.append(Tag.MODE_C_TAG + "\r\n" + "\t" + Tag.MODE_P0_TAG + " = \"" + this.getPeriod() + "\" "
                + Tag.TIME_UNIT + "\r\n" + "\t" + Tag.MODE_C1_TAG + " = \"" + this.getRange() + "\" \r\n" + "\t"
                + Tag.MODE_C2_TAG + " = \"" + this.getTypeCalcul() + "\"");
        return buf.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ModeCalcul)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        final ModeCalcul modeCalcul = (ModeCalcul) o;

        if (calculType != modeCalcul.calculType) {
            return false;
        }
        if (range != modeCalcul.range) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 29 * result + range;
        result = 29 * result + calculType;
        return result;
    }
}