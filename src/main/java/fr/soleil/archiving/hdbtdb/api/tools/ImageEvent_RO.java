// +======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/ImageEvent_RO.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ImageEvent_RO.
//						(Chinkumo Jean) - Mar 10, 2004
//
// $Author: ounsy $
//
// $Revision: 1.7 $
//
// $Log: ImageEvent_RO.java,v $
// Revision 1.7  2006/10/31 16:54:23  ounsy
// milliseconds and null values management
//
// Revision 1.6  2006/07/24 07:36:33  ounsy
// getValue_AsString() method added
//
// Revision 1.5  2006/07/18 10:42:56  ounsy
// better image support
//
// Revision 1.4  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.3.12.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.3  2005/06/14 10:12:11  chinkumo
// Branch (tangORBarchiving_1_0_1-branch_0)  and HEAD merged.
//
// Revision 1.2.4.1  2005/06/13 15:08:16  chinkumo
// Minor changes made to improve image event management efficiency
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.archiving.hdbtdb.api.tools;

import fr.soleil.archiving.common.api.tools.ArchivingEvent;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.lib.project.math.MathConst;

public class ImageEvent_RO extends ArchivingEvent<boolean[][]> {
    private int dimX;
    private int dimY;

    /**
     * Creates a new instance of DhdbEvent
     */
    public ImageEvent_RO() {
        super();
    }

    /**
     * Creates a new instance of DhdbEvent
     */
    public ImageEvent_RO(final String[] hdbImageEvent_RO) {
        setAttributeCompleteName(hdbImageEvent_RO[0]);
        setTimeStamp(Long.parseLong(hdbImageEvent_RO[1]));
        setDimX(Integer.parseInt(hdbImageEvent_RO[2]));
        setDimY(Integer.parseInt(hdbImageEvent_RO[3]));

        final double[][] value = new double[dimY][dimX];
        final boolean[][] nullElements = new boolean[dimY][dimX];
        int k = 4;
        for (int i = 0; i < dimY; i++) {
            for (int j = 0; j < dimX; j++) {
                if (hdbImageEvent_RO[k] == null || "".equals(hdbImageEvent_RO[k]) || "null".equals(hdbImageEvent_RO[k])) {
                    value[i][j] = MathConst.NAN_FOR_NULL;
                    nullElements[i][j] = true;
                } else {
                    value[i][j] = Double.parseDouble(hdbImageEvent_RO[k]);
                }
                k++;
            }
        }
        this.setImageValueRO(value, nullElements);
    }

    public int getDimX() {
        return dimX;
    }

    public void setDimX(final int dimX) {
        this.dimX = dimX;
    }

    public int getDimY() {
        return dimY;
    }

    public void setDimY(final int dimY) {
        this.dimY = dimY;
    }

    public double[][] getImageValueRO() {
        return (double[][]) getValue();
    }

    public void setImageValueRO(final double[][] value, final boolean[][] nullElements) {
        setValue(value, nullElements);
    }

    /**
     * Returns an array representation of the object <I>ImageEvent_RO</I>.
     * 
     * @return an array representation of the object <I>ImageEvent_RO</I>.
     */
    @Override
    public String[] toArray() {
        final double[][] value = (double[][]) getValue();
        final int dimX = value[0].length;
        final int dimY = value.length;
        final String[] hdbImageEventRO = new String[4 + dimX * dimY];

        hdbImageEventRO[0] = getAttributeCompleteName(); // name
        hdbImageEventRO[1] = Long.toString(getTimeStamp()).trim(); // time
        hdbImageEventRO[2] = Integer.toString(dimX); // dim_x
        hdbImageEventRO[3] = Integer.toString(dimY); // dim_y

        int k = 4;
        for (int i = 0; i < dimY; i++) {
            for (int j = 0; j < dimX; j++) {
                hdbImageEventRO[k] = "" + value[i][j];
                k++;
            }
        }
        return hdbImageEventRO;
    }

    /**
     * This method returns the value of this spectrum event. The returned value
     * is then formated as a String.
     * 
     * @return the value of this spectrum event.
     */
    public String getImageValueRO_AsString() {
        final double[][] value = getImageValueRO();
        final StringBuilder valueStr = new StringBuilder();
        for (int i = 0; i < value.length; i++) { // lignes
            for (int j = 0; j < value[i].length - 1; j++) { // colonnes
                valueStr.append(toString(value[i][j])).append("\t");
            }
            valueStr.append(toString(value[i][value[i].length - 1])).append("\r\n");

        }
        return valueStr.toString();
    }

    /**
     * This method returns the value of this image event. The returned value is
     * then formated as a String.
     * 
     * @return the value of this image event.
     */
    public String getValueAsString() {
        final StringBuilder valueStr = new StringBuilder();
        final Object value = getValue();
        if (value != null) {
            if (value instanceof double[][] && ((double[][]) value).length != 0) {
                double[][] dval = (double[][]) value;
                for (int i = 0; i < dval.length; i++) {
                    for (int j = 0; j < dval[0].length; j++) {
                        valueStr.append(toString(dval[i][j]));
                        if (j < dval[0].length - 1) {
                            valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                        }
                    }
                    if (i < dval.length - 1) {
                        valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                    }
                }
            } else if (value instanceof float[][] && ((float[][]) value).length != 0) {
                float[][] fval = (float[][]) value;
                for (int i = 0; i < fval.length; i++) {
                    for (int j = 0; j < fval[0].length; j++) {
                        valueStr.append(toString(fval[i][j]));
                        if (j < fval[0].length - 1) {
                            valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                        }
                    }
                    if (i < fval.length - 1) {
                        valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                    }
                }
            } else if (value instanceof long[][] && ((long[][]) value).length != 0) {
                long[][] lval = (long[][]) value;
                for (int i = 0; i < lval.length; i++) {
                    for (int j = 0; j < lval[0].length; j++) {
                        valueStr.append(toString(lval[i][j]));
                        if (j < lval[0].length - 1) {
                            valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                        }
                    }
                    if (i < lval.length - 1) {
                        valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                    }
                }
            } else if (value instanceof int[][] && ((int[][]) value).length != 0) {
                int[][] ival = (int[][]) value;
                for (int i = 0; i < ival.length; i++) {
                    for (int j = 0; j < ival[0].length; j++) {
                        valueStr.append(toString(ival[i][j]));
                        if (j < ival[0].length - 1) {
                            valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                        }
                    }
                    if (i < ival.length - 1) {
                        valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                    }
                }
            } else if (value instanceof short[][] && ((short[][]) value).length != 0) {
                short[][] sval = (short[][]) value;
                for (int i = 0; i < sval.length; i++) {
                    for (int j = 0; j < sval[0].length; j++) {
                        valueStr.append(toString(sval[i][j]));
                        if (j < sval[0].length - 1) {
                            valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                        }
                    }
                    if (i < sval.length - 1) {
                        valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                    }
                }
            } else if (value instanceof byte[][] && ((byte[][]) value).length != 0) {
                byte[][] bval = (byte[][]) value;
                for (int i = 0; i < bval.length; i++) {
                    for (int j = 0; j < bval[0].length; j++) {
                        valueStr.append(toString(bval[i][j]));
                        if (j < bval[0].length - 1) {
                            valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                        }
                    }
                    if (i < bval.length - 1) {
                        valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                    }
                }
            } else if (value instanceof boolean[][] && ((boolean[][]) value).length != 0) {
                boolean[][] bval = (boolean[][]) value;
                for (int i = 0; i < bval.length; i++) {
                    for (int j = 0; j < bval[0].length; j++) {
                        valueStr.append(bval[i][j]);
                        if (j < bval[0].length - 1) {
                            valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                        }
                    }
                    if (i < bval.length - 1) {
                        valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                    }
                }
            } else if (value instanceof String[][] && ((String[][]) value).length != 0) {
                String[][] sval = (String[][]) value;
                for (int i = 0; i < sval.length; i++) {
                    for (int j = 0; j < sval[0].length; j++) {
                        valueStr.append(sval[i][j]);
                        if (j < sval[0].length - 1) {
                            valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
                        }
                    }
                    if (i < sval.length - 1) {
                        valueStr.append(GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
                    }
                }
            } else {
                valueStr.append(value.toString());
            }
        }
        return valueStr.toString();
    }

    @Override
    public String toString() {
        final StringBuilder event_String = new StringBuilder();
        event_String.append("Source : \t").append(getAttributeCompleteName()).append("\r\n");
        event_String.append("TimeSt : \t").append(getTimeStamp()).append("\r\n");
        event_String.append("Value :  \t...").append("\r\n");
        return event_String.toString();
    }
}
