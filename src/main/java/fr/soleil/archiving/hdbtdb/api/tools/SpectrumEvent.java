package fr.soleil.archiving.hdbtdb.api.tools;

import fr.soleil.archiving.common.api.tools.ArchivingEvent;

public abstract class SpectrumEvent extends ArchivingEvent<boolean[]> {
	protected int dimX;
	protected final int dimY = 0;

	public int getDimX() {
		return dimX;
	}

	public void setDimX(int dim_x) {
		this.dimX = dim_x;
	}

	public int getDimY() {
		return dimY;
	}
}
