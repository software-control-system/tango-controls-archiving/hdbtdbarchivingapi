// +======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/SpectrumEvent_RO.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SpectrumEvent_RO.
//						(Chinkumo Jean) - Mar 10, 2004
//
// $Author: ounsy $
//
// $Revision: 1.14 $
//
// $Log: SpectrumEvent_RO.java,v $
// Revision 1.14  2007/02/27 15:36:34  ounsy
// corrected a severe bug in  getValue_AsString()  (the value wasn't filled)
//
// Revision 1.13  2007/02/06 09:03:47  ounsy
// corrected a bug in getValue_AsString for empty values
//
// Revision 1.12  2006/10/31 16:54:23  ounsy
// milliseconds and null values management
//
// Revision 1.11  2006/08/23 09:52:17  ounsy
// null value represented as an empty String in getValue_AsString() method
//
// Revision 1.10  2006/05/12 09:23:10  ounsy
// CLOB_SEPARATOR in GlobalConst
//
// Revision 1.9  2006/05/04 14:30:41  ounsy
// CLOB_SEPARATOR centralized in ConfigConst
//
// Revision 1.8  2006/03/27 15:19:30  ounsy
// new spectrum types support + better spectrum management
//
// Revision 1.7  2006/02/28 17:05:58  chinkumo
// no message
//
// Revision 1.6  2006/02/24 12:05:06  ounsy
// replaced hard-coded "," value to CLOB_SEPARATOR
//
// Revision 1.5  2006/02/15 11:07:34  chinkumo
// Minor changes made to optimize streams when sending data to DB.
//
// Revision 1.4  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.3.12.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.3  2005/06/14 10:12:11  chinkumo
// Branch (tangORBarchiving_1_0_1-branch_0)  and HEAD merged.
//
// Revision 1.2.4.1  2005/06/13 14:55:38  chinkumo
// Minor changes made to improve spectrum event management efficiency.
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.archiving.hdbtdb.api.tools;

import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.lib.project.math.MathConst;

import java.lang.reflect.Array;

public class SpectrumEvent_RO extends SpectrumEvent {


    /**
     * Creates a new instance of Spectrum Event
     */
    public SpectrumEvent_RO() {
        super();
    }


    /**
     * Creates a new instance of Spectrum Event
     */
    public SpectrumEvent_RO(final String[] hdbSpectrumEventRO) {
        this.setAttributeCompleteName(hdbSpectrumEventRO[0]);
        this.setTimeStamp(Long.parseLong(hdbSpectrumEventRO[1]));
        this.setDimX(Integer.parseInt(hdbSpectrumEventRO[2]));
        // dim_y = 0;
        final double[] value = new double[hdbSpectrumEventRO.length - 4];
        final boolean[] nullElements = new boolean[value.length];
        for (int i = 0; i < value.length; i++) {
            if (hdbSpectrumEventRO[i + 4] == null || "".equals(hdbSpectrumEventRO[i + 4])
                    || "null".equals(hdbSpectrumEventRO[i + 4])) {
                value[i] = MathConst.NAN_FOR_NULL;
                nullElements[i] = true;
            } else {
                value[i] = Double.parseDouble(hdbSpectrumEventRO[i + 4]);
            }
        }
        this.setValue(value, nullElements);
    }

    /**
     * This method returns the value of this spectrum event. The returned value
     * is then formated as a String.
     *
     * @return the value of this spectrum event.
     */
    public String getValueAsString() {
        final StringBuilder valueStr = new StringBuilder();
        final Object value = getValue();
        if (value == null) {
            valueStr.append(GlobalConst.ARCHIVER_NULL_VALUE);
        } else if (Array.getLength(value) > 0) {
            if (value instanceof double[]) {
                double[] dval = (double[]) value;
                for (double d : dval) {
                    valueStr.append(toString(d)).append(GlobalConst.CLOB_SEPARATOR);
                }
                if (dval.length > 0) {
                    valueStr.delete(valueStr.lastIndexOf(GlobalConst.CLOB_SEPARATOR), valueStr.length());
                }
            } else if (value instanceof byte[]) {
                byte[] bval = (byte[]) value;
                for (byte b : bval) {
                    valueStr.append(toString(b)).append(GlobalConst.CLOB_SEPARATOR);
                }
                if (bval.length > 0) {
                    valueStr.delete(valueStr.lastIndexOf(GlobalConst.CLOB_SEPARATOR), valueStr.length());
                }
            } else if (value instanceof short[]) {
                short[] sval = (short[]) value;
                for (short s : sval) {
                    valueStr.append(toString(s)).append(GlobalConst.CLOB_SEPARATOR);
                }
                if (sval.length > 0) {
                    valueStr.delete(valueStr.lastIndexOf(GlobalConst.CLOB_SEPARATOR), valueStr.length());
                }
            } else if (value instanceof int[]) {
                int[] ival = (int[]) value;
                for (int i : ival) {
                    valueStr.append(toString(i)).append(GlobalConst.CLOB_SEPARATOR);
                }
                if (ival.length > 0) {
                    valueStr.delete(valueStr.lastIndexOf(GlobalConst.CLOB_SEPARATOR), valueStr.length());
                }
            } else if (value instanceof float[]) {
                float[] fval = (float[]) value;
                for (float f : fval) {
                    valueStr.append(toString(f)).append(GlobalConst.CLOB_SEPARATOR);
                }
                if (fval.length > 0) {
                    valueStr.delete(valueStr.lastIndexOf(GlobalConst.CLOB_SEPARATOR), valueStr.length());
                }
            } else if (value instanceof boolean[]) {
                boolean[] bval = (boolean[]) value;
                for (boolean b : bval) {
                    valueStr.append(b).append(GlobalConst.CLOB_SEPARATOR);
                }
                if (bval.length > 0) {
                    valueStr.delete(valueStr.lastIndexOf(GlobalConst.CLOB_SEPARATOR), valueStr.length());
                }
            } else if (value instanceof String[]) {
                for (int i = 0; i < ((String[]) value).length - 1; i++) {
                    valueStr.append(((String[]) value)[i]).append(GlobalConst.CLOB_SEPARATOR);
                }
                valueStr.append(((String[]) value)[((String[]) value).length - 1]);
            } else if (value instanceof long[]) {
                long[] lval = (long[]) value;
                for (long l : lval) {
                    valueStr.append(toString(l)).append(GlobalConst.CLOB_SEPARATOR);
                }
                if (lval.length > 0) {
                    valueStr.delete(valueStr.lastIndexOf(GlobalConst.CLOB_SEPARATOR), valueStr.length());
                }
            } else {
                valueStr.append(value.toString());
            }
        }

        return valueStr.toString();
    }

    @Override
    public String[] toArray() {
        final double[] value = (double[]) getValue();
        final String[] spectrumEvent_ro = new String[4 + value.length];
        spectrumEvent_ro[0] = getAttributeCompleteName(); // name
        spectrumEvent_ro[1] = Long.toString(getTimeStamp()).trim(); // time
        spectrumEvent_ro[2] = Integer.toString(value.length);
        spectrumEvent_ro[3] = "0";
        for (int i = 0; i < value.length; i++) {
            spectrumEvent_ro[i + 4] = String.valueOf(value[i]);
        }
        return spectrumEvent_ro;

    }

    @Override
    public String toString() {
        final StringBuilder event_String = new StringBuilder();
        event_String.append("SpectrumEvent_RO : ").append(getAttributeCompleteName());
        event_String.append(", timestamp = ").append(getTimeStamp());
        return event_String.toString();
    }
}
