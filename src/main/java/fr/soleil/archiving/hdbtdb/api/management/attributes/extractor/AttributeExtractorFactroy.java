/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor;

import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.DataGettersFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.InfSupGetters;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.MinMaxAvgGetters;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates.DataGettersBetweenDatesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates.InfSupGettersBetweenDates;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates.MinMaxAvgGettersBetweenDates;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class AttributeExtractorFactroy {

    /**
	 * 
	 */
    private AttributeExtractorFactroy() {
    }

    public static AttributeExtractor getAttributeExtractor(final AbstractDataBaseConnector connector) {
        final AttributeExtractor extractor = new AttributeExtractor(connector);
        extractor.dataGetters = DataGettersFactory.getDataGatters(connector);
        extractor.dataGettersBetweenDates = DataGettersBetweenDatesFactory.getDataGatters(connector);
        extractor.infSupGetters = new InfSupGetters(connector);
        extractor.infSupGettersBetweenDates = new InfSupGettersBetweenDates(connector);
        extractor.minMaxAvgGetters = new MinMaxAvgGetters(connector);
        extractor.minMaxAvgGettersBetweenDates = new MinMaxAvgGettersBetweenDates(connector);
        return extractor;
    }
}
