package fr.soleil.archiving.hdbtdb.api.tools;

import java.util.ArrayList;
import java.util.Collection;

import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.common.api.exception.ArchivingException;

/**
 * Class dedicated to trace which attributes have caused this ArchivingException
 * to be thrown
 * 
 * @author awo
 */
public final class AttributesArchivingException extends ArchivingException {

    private static final long serialVersionUID = 2681822737857412440L;
    // List of attributes that have caused this exception
    private Collection<FaultingAttribute> faultingAttributes = new ArrayList<FaultingAttribute>();

    // Default constructor
    public AttributesArchivingException() {
        super();
    }

    // Same constructor as superclass, for backward-compatibility
    public AttributesArchivingException(final String message, final String reason, final ErrSeverity archSeverity,
            final String desc, final String origin) {
        super(message, reason, archSeverity, desc, origin);
    }

    public Collection<FaultingAttribute> getFaultingAttributes() {
        return faultingAttributes;
    }

    public void setFaultingAttributes(final Collection<FaultingAttribute> faultingAttributes) {
        this.faultingAttributes = faultingAttributes;
    }
}
