// +======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/SpectrumEvent_RW.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  SpectrumEvent_RW.
//						(Chinkumo Jean) -
//
// $Author: ounsy $
//
// $Revision: 1.13 $
//
// $Log: SpectrumEvent_RW.java,v $
// Revision 1.13  2006/10/31 16:54:23  ounsy
// milliseconds and null values management
//
// Revision 1.12  2006/08/23 09:52:42  ounsy
// null value represented as an empty String in getValue_AsString() method
//
// Revision 1.11  2006/05/12 09:23:10  ounsy
// CLOB_SEPARATOR in GlobalConst
//
// Revision 1.10  2006/05/04 14:30:41  ounsy
// CLOB_SEPARATOR centralized in ConfigConst
//
// Revision 1.9  2006/04/11 14:34:54  ounsy
// avoiding negative array size exception
//
// Revision 1.8  2006/03/27 15:19:31  ounsy
// new spectrum types support + better spectrum management
//
// Revision 1.7  2006/02/28 17:05:58  chinkumo
// no message
//
// Revision 1.6  2006/02/24 12:05:18  ounsy
// replaced hard-coded "," value to CLOB_SEPARATOR
//
// Revision 1.5  2006/02/15 11:07:34  chinkumo
// Minor changes made to optimize streams when sending data to DB.
//
// Revision 1.4  2006/02/08 15:42:10  chinkumo
// minor change
//
// Revision 1.3  2006/02/08 13:27:07  chinkumo
// spectrum management enhanced for the getSpectrumValueRWWrite and getSpectrumValueRWRead methods
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.archiving.hdbtdb.api.tools;

import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.lib.project.math.MathConst;

public class SpectrumEvent_RW extends SpectrumEvent {

    /**
     * Creates a new instance of Spectrum Event
     */
    public SpectrumEvent_RW() {
        super();
    }


    /**
     * Creates a new instance of Spectrum Event
     */
    public SpectrumEvent_RW(String[] hdbSpectrumEventRO) {
        this.setAttributeCompleteName(hdbSpectrumEventRO[0]);
        this.setTimeStamp(Long.parseLong(hdbSpectrumEventRO[1]));
        this.setDimX(Integer.parseInt(hdbSpectrumEventRO[2]));
        // dim_y = 0;
        double[] value = new double[hdbSpectrumEventRO.length - 4];
        boolean[] nullElements = new boolean[value.length];
        for (int i = 0; i < value.length; i++) {
            if (hdbSpectrumEventRO[i + 4] == null || hdbSpectrumEventRO[i + 4].isEmpty()
                    || "null".equals(hdbSpectrumEventRO[i + 4])) {
                value[i] = MathConst.NAN_FOR_NULL;
                nullElements[i] = true;
            } else {
                value[i] = Double.parseDouble(hdbSpectrumEventRO[i + 4]);
            }
        }
        this.setValue(value, nullElements);
    }

    /**
     * This method returns the value of this spectrum event. The returned value
     * is then formated as a String.
     * 
     * @return the value of this spectrum event.
     */
    public String getValueAsString() {
        Object value = getValue();
        if (value == null)
            return GlobalConst.ARCHIVER_NULL_VALUE;
        StringBuilder valueStr = new StringBuilder();
        if (value instanceof double[]) {
            double[] dval = (double[]) value;
            for (int i = 0; i < dval.length - 1; i++) {
                valueStr.append(toString(dval[i])).append(GlobalConst.CLOB_SEPARATOR);
            }
            valueStr.append(toString(dval[dval.length - 1]));
        } else if (value instanceof byte[]) {
            byte[] bval = (byte[]) value;
            for (int i = 0; i < bval.length - 1; i++) {
                valueStr.append(toString(bval[i])).append(GlobalConst.CLOB_SEPARATOR);
            }
            valueStr.append(toString(bval[bval.length - 1]));
        } else if (value instanceof short[]) {
            short[] sval = (short[]) value;
            for (int i = 0; i < sval.length - 1; i++) {
                valueStr.append(toString(sval[i])).append(GlobalConst.CLOB_SEPARATOR);
            }
            valueStr.append(toString(sval[sval.length - 1]));
        } else if (value instanceof int[]) {
            int[] ival = (int[]) value;
            for (int i = 0; i < ival.length - 1; i++) {
                valueStr.append(toString(ival[i])).append(GlobalConst.CLOB_SEPARATOR);
            }
            valueStr.append(toString(ival[ival.length - 1]));
        } else if (value instanceof long[]) {
            long[] lval = (long[]) value;
            for (int i = 0; i < lval.length - 1; i++) {
                valueStr.append(toString(lval[i])).append(GlobalConst.CLOB_SEPARATOR);
            }
            valueStr.append(toString(lval[lval.length - 1]));
        } else if (value instanceof float[]) {
            float[] fval = (float[]) value;
            for (int i = 0; i < fval.length - 1; i++) {
                valueStr.append(toString(fval[i])).append(GlobalConst.CLOB_SEPARATOR);
            }
            valueStr.append(toString(fval[fval.length - 1]));
        } else if (value instanceof boolean[]) {
            boolean[] bval = (boolean[]) value;
            for (int i = 0; i < bval.length - 1; i++) {
                valueStr.append(bval[i]).append(GlobalConst.CLOB_SEPARATOR);
            }
            valueStr.append(bval[bval.length - 1]);
        } else if (value instanceof String[]) {
            String[] sval = (String[]) value;
            for (int i = 0; i < sval.length - 1; i++) {
                valueStr.append(sval[i]).append(GlobalConst.CLOB_SEPARATOR);
            }
            valueStr.append(sval[sval.length - 1]);
        } else {
            valueStr.append(value.toString());
        }
        return valueStr.toString();
    }

    /**
     * Returns an array representation of the object <I>SpectrumEvent_RO</I>.
     * 
     * @return an array representation of the object <I>SpectrumEvent_RO</I>.
     */
    @Override
    public String[] toArray() {
        double[] value = (double[]) getValue();
        String[] hdbSpectrumEventRO = new String[4 + value.length];
        hdbSpectrumEventRO[0] = getAttributeCompleteName(); // name
        hdbSpectrumEventRO[1] = Long.toString(getTimeStamp()).trim(); // time
        hdbSpectrumEventRO[2] = Integer.toString(value.length);
        hdbSpectrumEventRO[3] = "0";
        for (int i = 0; i < value.length; i++) {
            hdbSpectrumEventRO[i + 4] = "" + value[i];
        }
        return hdbSpectrumEventRO;

    }

    @Override
    public String toString() {
        StringBuilder eventString = new StringBuilder();
        eventString.append("Source : \t").append(getAttributeCompleteName()).append("\r\n");
        eventString.append("TimeSt : \t").append(getTimeStamp()).append("\r\n");
        eventString.append("Value :  \t...").append("\r\n");
        return eventString.toString();
    }

    /**
     * @return
     */
    public Object getSpectrumValueRWRead() {
        Object value = getValue();
        if (value == null) {
            return null;
        }
        int len = this.dimX;
        if (value instanceof double[]) {
            double[] ret = new double[len];
            for (int i = 0; i < len; i++) {
                ret[i] = ((double[]) value)[i];
            }
			return ret;
        } else if (value instanceof byte[]) {
            byte[] ret = new byte[len];
            for (int i = 0; i < len; i++) {
                ret[i] = ((byte[]) value)[i];
            }
            return ret;
        } else if (value instanceof short[]) {
            short[] ret = new short[len];
            for (int i = 0; i < len; i++) {
                ret[i] = ((short[]) value)[i];
            }
            return ret;
        } else if (value instanceof int[]) {
            int[] ret = new int[len];
            for (int i = 0; i < len; i++) {
                ret[i] = ((int[]) value)[i];
            }
            return ret;
		} else if (value instanceof long[]) {
			long[] ret = new long[len];
			for (int i = 0; i < len; i++) {
				ret[i] = ((long[]) value)[i];
			}
			return ret;
        } else if (value instanceof float[]) {
            float[] ret = new float[len];
            for (int i = 0; i < len; i++) {
                ret[i] = ((float[]) value)[i];
            }
            return ret;
        } else if (value instanceof boolean[]) {
            boolean[] ret = new boolean[len];
            for (int i = 0; i < len; i++) {
                ret[i] = ((boolean[]) value)[i];
            }
            return ret;
        } else if (value instanceof String[]) {
            String[] ret = new String[len];
            for (int i = 0; i < len; i++) {
                ret[i] = ((String[]) value)[i];
            }
            return ret;
        } else {
            return null;
        }
    }

    public Object getSpectrumValueRWWrite() {
        Object value = getValue();
        if (value == null) {
            return null;
        }
        if (value instanceof double[]) {
            int len = ((double[]) value).length - this.dimX;
            if (len < 0)
                len = 0;
            double[] ret = new double[len];
            for (int i = this.dimX; i < ((double[]) value).length; i++) {
                ret[i - this.dimX] = ((double[]) value)[i];
            }

            return ret;
        } else if (value instanceof byte[]) {
            int len = ((byte[]) value).length - this.dimX;
            if (len < 0)
                len = 0;
            byte[] ret = new byte[len];
            for (int i = this.dimX; i < ((byte[]) value).length; i++) {
                ret[i - this.dimX] = ((byte[]) value)[i];
            }

            return ret;
        } else if (value instanceof short[]) {
            int len = ((short[]) value).length - this.dimX;
            if (len < 0)
                len = 0;
            short[] ret = new short[len];
            for (int i = this.dimX; i < ((short[]) value).length; i++) {
                ret[i - this.dimX] = ((short[]) value)[i];
            }

            return ret;
        } else if (value instanceof int[]) {
            int len = ((int[]) value).length - this.dimX;
            if (len < 0)
                len = 0;
            int[] ret = new int[len];
            for (int i = this.dimX; i < ((int[]) value).length; i++) {
                ret[i - this.dimX] = ((int[]) value)[i];
            }

            return ret;
		} else if (value instanceof long[]) {
			int len = ((long[]) value).length - this.dimX;
			if (len < 0)
				len = 0;
			long[] ret = new long[len];
			for (int i = this.dimX; i < ((long[]) value).length; i++) {
				ret[i - this.dimX] = ((long[]) value)[i];
			}

			return ret;
		} else if (value instanceof float[]) {
            int len = ((float[]) value).length - this.dimX;
            if (len < 0)
                len = 0;
            float[] ret = new float[len];
            for (int i = this.dimX; i < ((float[]) value).length; i++) {
                ret[i - this.dimX] = ((float[]) value)[i];
            }

            return ret;
        } else if (value instanceof boolean[]) {
            int len = ((boolean[]) value).length - this.dimX;
            if (len < 0)
                len = 0;
            boolean[] ret = new boolean[len];
            for (int i = this.dimX; i < ((boolean[]) value).length; i++) {
                ret[i - this.dimX] = ((boolean[]) value)[i];
            }

            return ret;
        } else if (value instanceof String[]) {
            int len = ((String[]) value).length - this.dimX;
            if (len < 0)
                len = 0;
            String[] ret = new String[len];
            for (int i = this.dimX; i < ((String[]) value).length; i++) {
                ret[i - this.dimX] = ((String[]) value)[i];
            }

            return ret;
        } else {
            return null;
        }
    }

    /**
     * @return
     */
    public String getSpectrumValueRW_AsString_Read() {
        Object value = getSpectrumValueRWRead();
        if (value == null) {
            return GlobalConst.ARCHIVER_NULL_VALUE;
        }
        if (value instanceof double[]) {
            return convertDoubleTabToString((double[]) value);
        } else if (value instanceof byte[]) {
            return convertByteTabToString((byte[]) value);
        } else if (value instanceof short[]) {
            return convertShortTabToString((short[]) value);
        } else if (value instanceof int[]) {
            return convertIntTabToString((int[]) value);
		} else if (value instanceof long[]) {
			return convertLongTabToString((long[]) value);
        } else if (value instanceof float[]) {
            return convertFloatTabToString((float[]) value);
        } else if (value instanceof boolean[]) {
            return convertBooleanTabToString((boolean[]) value);
        } else if (value instanceof String[]) {
            return convertStringTabToString((String[]) value);
        } else {
            return GlobalConst.ARCHIVER_NULL_VALUE;
        }
    }

    /**
     * @return
     */
    public String getSpectrumValueRW_AsString_Write() {
        Object value = getSpectrumValueRWWrite();
        if (value == null) {
            return GlobalConst.ARCHIVER_NULL_VALUE;
        }
        if (value instanceof double[]) {
            return convertDoubleTabToString((double[]) value);
        } else if (value instanceof byte[]) {
            return convertByteTabToString((byte[]) value);
        } else if (value instanceof short[]) {
            return convertShortTabToString((short[]) value);
        } else if (value instanceof int[]) {
            return convertIntTabToString((int[]) value);
		} else if (value instanceof long[]) {
			return convertLongTabToString((long[]) value);
        } else if (value instanceof float[]) {
            return convertFloatTabToString((float[]) value);
        } else if (value instanceof boolean[]) {
            return convertBooleanTabToString((boolean[]) value);
        } else if (value instanceof String[]) {
            return convertStringTabToString((String[]) value);
        } else {
            return GlobalConst.ARCHIVER_NULL_VALUE;
        }
    }

    /**
     * 
     * @param val
     * @return
     */
    private String convertDoubleTabToString(double[] val) {
        if (val == null) {
            return null;
        }
        StringBuilder valueStr = new StringBuilder();

        for (int j = 0; j < val.length; j++) {
            valueStr.append(toString(val[j]));
            if (j < val.length - 1) {
                valueStr.append(GlobalConst.CLOB_SEPARATOR);
            }
        }

        return valueStr.toString();
    }

    /**
     * 
     * @param val
     * @return
     */
    private String convertByteTabToString(byte[] val) {
        if (val == null) {
            return null;
        }
        StringBuilder valueStr = new StringBuilder();

        for (int j = 0; j < val.length; j++) {
            valueStr.append(toString(val[j]));
            if (j < val.length - 1) {
                valueStr.append(GlobalConst.CLOB_SEPARATOR);
            }
        }

        return valueStr.toString();
    }

    /**
     * 
     * @param val
     * @return
     */
    private String convertIntTabToString(int[] val) {
        if (val == null) {
            return null;
        }
        StringBuilder valueStr = new StringBuilder();

        for (int j = 0; j < val.length; j++) {
            valueStr.append(toString(val[j]));
            if (j < val.length - 1) {
                valueStr.append(GlobalConst.CLOB_SEPARATOR);
            }
        }

        return valueStr.toString();
    }

	/**
	 * 
	 * @param val
	 * @return
	 */
	private String convertLongTabToString(long[] val) {
		if (val == null) {
			return null;
		}
		StringBuilder valueStr = new StringBuilder();

		for (int j = 0; j < val.length; j++) {
			valueStr.append(toString(val[j]));
			if (j < val.length - 1) {
				valueStr.append(GlobalConst.CLOB_SEPARATOR);
			}
		}

		return valueStr.toString();
	}

    /**
     * 
     * @param val
     * @return
     */
    private String convertShortTabToString(short[] val) {
        if (val == null) {
            return null;
        }
        StringBuilder valueStr = new StringBuilder();

        for (int j = 0; j < val.length; j++) {
            valueStr.append(toString(val[j]));
            if (j < val.length - 1) {
                valueStr.append(GlobalConst.CLOB_SEPARATOR);
            }
        }

        return valueStr.toString();
    }

    /**
     * 
     * @param val
     * @return
     */
    private String convertFloatTabToString(float[] val) {
        if (val == null) {
            return null;
        }
        StringBuilder valueStr = new StringBuilder();

        for (int j = 0; j < val.length; j++) {
            valueStr.append(toString(val[j]));
            if (j < val.length - 1) {
                valueStr.append(GlobalConst.CLOB_SEPARATOR);
            }
        }

        return valueStr.toString();
    }

    /**
     * 
     * @param val
     * @return
     */
    private String convertBooleanTabToString(boolean[] val) {
        if (val == null) {
            return null;
        }
        StringBuilder valueStr = new StringBuilder();

        for (int j = 0; j < val.length; j++) {
            valueStr.append(toString(val[j]));
            if (j < val.length - 1) {
                valueStr.append(GlobalConst.CLOB_SEPARATOR);
            }
        }

        return valueStr.toString();
    }

    /**
     * 
     * @param val
     * @return
     */
    private String convertStringTabToString(String[] val) {
        if (val == null) {
            return null;
        }
        StringBuilder valueStr = new StringBuilder();

        for (int j = 0; j < val.length; j++) {
            valueStr.append(toString(val[j]));
            if (j < val.length - 1) {
                valueStr.append(GlobalConst.CLOB_SEPARATOR);
            }
        }

        return valueStr.toString();
    }

}
