/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.lib.project.ObjectUtils;

/**
 * @author AYADI
 *
 */
public class OracleDataGetters extends DataGetters {

    public OracleDataGetters(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    protected String getDbScalarLastNRequest(final String tableName, final String attributeName, final int number,
            final boolean roFields, final String fields) throws ArchivingException {
        final String orderField = roFields ? ConfigConst.TIME : ConfigConst.TIME;
        return new StringBuilder("SELECT ").append(fields).append(" FROM ").append("(").append("SELECT * FROM ")
                .append(tableName).append(" ORDER BY ").append(orderField).append(" DESC").append(")")
                .append(" WHERE rownum <= ").append(number).append(" ORDER BY  ").append(orderField).append(" ASC")
                .toString();
    }

    @Override
    public DbData[] getAttSpectrumDataLastN(final String attributeName, final int number, final DbData... dbData)
            throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (att == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string

            final StringBuilder queryBuilder = new StringBuilder("SELECT ");
            if (roFields) {
                queryBuilder.append(dbUtils.toDbTimeFieldString("T." + ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append("T.").append(ConfigConst.DIM_X).append(VALUE_SEPARATOR).append("T.")
                        .append(ConfigConst.VALUE);
            } else {
                queryBuilder.append(dbUtils.toDbTimeFieldString("T." + ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append("T.").append(ConfigConst.DIM_X).append(VALUE_SEPARATOR).append("T.")
                        .append(ConfigConst.READ_VALUE);
                queryBuilder.append(VALUE_SEPARATOR).append("T.").append(ConfigConst.WRITE_VALUE);
            }

            queryBuilder.append(" FROM (SELECT * FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(" ORDER BY ").append(ConfigConst.TIME)
                    .append(" DESC) T WHERE rownum <= ").append(number).append(" ORDER BY  ").append(ConfigConst.TIME)
                    .append(" ASC");
            final String query = queryBuilder.toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    rset = stmt.executeQuery(query);
                    methods.treatStatementResultForGetSpectData(rset, attributeName, result);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;
    }

    @Override
    public void buildAttributeTab(final String tableName, final int data_type, final int data_format,
            final int writable) throws ArchivingException {
        // TODO Auto-generated method stub

    }

    @Override
    public String getNearestValueQuery(final String attributeName, final String timestamp) throws ArchivingException {
        String query;
        if (connector == null) {
            query = null;
        } else {
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final int[] tfw = AdtAptAttributesFactory.getInstance(connector).getAttTFWData(attributeName);
            final int writeType = tfw[2];
            final boolean roFields = writeType == AttrWriteType._READ || writeType == AttrWriteType._WRITE;
            String selectFields = ObjectUtils.EMPTY_STRING;
            if (roFields) {
                switch (tfw[1]) {
                    case AttrDataFormat._SCALAR:
                        selectFields = String.join(VALUE_SEPARATOR, ConfigConst.TAB_SCALAR_RO);
                        break;
                    case AttrDataFormat._SPECTRUM:
                        selectFields = String.join(VALUE_SEPARATOR, ConfigConst.TAB_SPECTRUM_RO);
                        break;
                    case AttrDataFormat._IMAGE:
                        selectFields = String.join(VALUE_SEPARATOR, ConfigConst.TAB_IMAGE_RO);
                        break;
                }
            } else {
                switch (tfw[1]) {
                    case AttrDataFormat._SCALAR:
                        selectFields = String.join(VALUE_SEPARATOR, ConfigConst.TAB_SCALAR_RW);
                        break;
                    case AttrDataFormat._SPECTRUM:
                        selectFields = String.join(VALUE_SEPARATOR, ConfigConst.TAB_SPECTRUM_RW);
                        break;
                    case AttrDataFormat._IMAGE:
                        selectFields = String.join(VALUE_SEPARATOR, ConfigConst.TAB_IMAGE_RW);
                        break;
                }
            }

            // ************************************************ //
            final String refDate = dbUtils.toDbTimeString(timestamp.trim());
            final String select = "SELECT ";
            final String from = " FROM ";
            final String where = " WHERE ";
            // Query to search nearest date before (or equal to) given date
            final StringBuilder nearestBeforeBuilder = new StringBuilder(select);
            nearestBeforeBuilder.append(selectFields).append(", (").append(refDate).append(" - time) as diff");
            nearestBeforeBuilder.append(from).append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(where).append("time = (SELECT MAX(time)");
            nearestBeforeBuilder.append(from).append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(where).append("time <= ").append(refDate)
                    .append(")");
            // Query to search nearest date after (or equal to) given date
            final StringBuilder nearestAfterBuilder = new StringBuilder(select);
            nearestAfterBuilder.append(selectFields).append(", (time - ").append(refDate).append(") as diff");
            nearestAfterBuilder.append(from).append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(where).append("time = (SELECT MIN(time)");
            nearestAfterBuilder.append(from).append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(where).append("time >= ").append(refDate)
                    .append(")");

            final StringBuilder queryBuilder = new StringBuilder(select);
            queryBuilder.append(selectFields).append(from).append("( ");
            queryBuilder.append(select).append(selectFields).append(from).append("( ");
            queryBuilder.append(nearestBeforeBuilder)
                    .append(" UNION " + (tfw[1] != AttrDataFormat._SCALAR ? "ALL " : ObjectUtils.EMPTY_STRING))
                    .append(nearestAfterBuilder);
            queryBuilder.append(" ) ORDER BY diff ASC ) WHERE ROWNUM = 1");

            query = queryBuilder.toString();
        }
        return query;
    }
}
