package fr.soleil.archiving.hdbtdb.api.management.attributes.tdb.delete;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class TdbAttributeDelete {
    private final Logger logger = LoggerFactory.getLogger(TdbAttributeDelete.class);
    private final AbstractDataBaseConnector connector;

    public TdbAttributeDelete(final AbstractDataBaseConnector connector) {
        this.connector = connector;
    }

    public void deleteOldRecords(final long keepedPeriod, final String[] attributeList) throws ArchivingException {
        final long currentDate = System.currentTimeMillis();
        final long keepedDate = currentDate - keepedPeriod;
        final long time = keepedDate;
        final Timestamp timeSt = new Timestamp(time);
        logger.debug("removing data older than {}", timeSt);
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if (connector != null && dbUtils != null) {
            Connection conn = null;
            PreparedStatement psDelete = null;
            for (int i = 0; i < attributeList.length; i++) {
                try {
                    conn = connector.getConnection();
                    final String name = attributeList[i];
                    final String deleteString = new StringBuilder("DELETE FROM  ").append(connector.getSchema())
                            .append(".").append(dbUtils.getTableName(name)).append(" WHERE ").append(ConfigConst.TIME)
                            .append(" <= ?").toString();
                    final String truncateString = new StringBuilder("TRUNCATE TABLE  ").append(connector.getSchema())
                            .append(".").append(dbUtils.getTableName(name)).toString();
                    boolean everythingIsOld = false;
                    final Timestamp lastInsert = dbUtils.getTimeOfLastInsert(name, true);

                    if (lastInsert != null) {
                        everythingIsOld = lastInsert.getTime() - timeSt.getTime() < 0;
                    }

                    final String query = everythingIsOld ? truncateString : deleteString;
                    psDelete = conn.prepareStatement(query);
                    // dbConn.setLastStatement(ps_delete);
                    if (!everythingIsOld) {
                        psDelete.setTimestamp(1, timeSt);
                    }
                    psDelete.executeUpdate();
                } catch (final SQLException e) {
                    e.printStackTrace();
                    logger.error("SQLException received (go to the next element) : ", e);
                    continue;
                } catch (final ArchivingException e) {
                    e.printStackTrace();
                    logger.error("ArchivingException received (go to the next element) : ", e);
                    continue;
                } catch (final Exception e) {
                    e.printStackTrace();
                    logger.error("Unknown Exception received (go to the next element) : ", e);
                    continue;
                } finally {
                    ConnectionCommands.close(psDelete);
                    connector.closeConnection(conn);
                }

            }

        }
    }

}
