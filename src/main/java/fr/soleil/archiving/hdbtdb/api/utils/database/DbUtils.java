package fr.soleil.archiving.hdbtdb.api.utils.database;

import java.io.File;
import java.lang.reflect.Array;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.common.api.tools.StringFormater;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.database.DBExtractionConst;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.math.MathConst;

public abstract class DbUtils implements IDbUtils, DBExtractionConst {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbUtils.class);

    protected final AbstractDataBaseConnector connector;

    public DbUtils(final AbstractDataBaseConnector connector) {
        this.connector = connector;
    }

    @Override
    public abstract String getFormat(SamplingType samplingType);

    @Override
    public abstract String toDbTimeString(String timeField);

    @Override
    public abstract String toDbTimeFieldString(String timeField);

    @Override
    public abstract String toDbTimeFieldString(String timeField, String format);

    @Override
    public abstract String getTime(String string) throws ArchivingException;

    @Override
    public abstract String getTableName(int index);

    protected abstract String getRequest();

    protected abstract String getFormattedTimeField(String maxOrMin);

    /*
     *
     */
    @Override
    public Timestamp getTimeOfLastInsert(final String completeName, final boolean max) throws ArchivingException {
        Timestamp ret = null;
        if (connector != null) {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final String maxOrMin = max ? "MAX" : "MIN";
            final String field = getFormattedTimeField(maxOrMin);
            final String query = new StringBuilder("select ").append(field).append(" from ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(getTableName(completeName)).toString();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();

                    rset = stmt.executeQuery(query);
                    rset.next();
                    final String rawDate = rset.getString(1);
                    if (rawDate == null) {
                        return null;
                    }

                    final long stringToMilli = DateUtil.stringToMilli(rawDate);
                    ret = new Timestamp(stringToMilli);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return ret;
    }

    /**
     *
     * @param manager
     * @return
     * @throws ArchivingException
     * @throws SQLException
     */
    public void deleteOldRecords(final long keepedPeriod, final String[] attributeList)
            throws ArchivingException, SQLException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (connector != null && att != null) {
            final long currentDate = System.currentTimeMillis();
            final long keepedDate = currentDate - keepedPeriod;
            final long time = keepedDate;
            // long time = current_date;
            final Timestamp timeSt = new Timestamp(time);

            Connection conn = null;
            PreparedStatement psDelete = null;
            try {
                conn = connector.getConnection();
                if (conn != null) {

                    for (final String name : attributeList) {

                        try {
                            final String tableName = connector.getSchema() + DB_SEPARATOR + getTableName(name);
                            final String tableField = ConfigConst.TIME;

                            final String deleteString = "DELETE FROM  " + tableName + " WHERE " + tableField + " <= ?";
                            final String truncateString = "TRUNCATE TABLE  " + tableName;
                            boolean everythingIsOld = false;

                            final Timestamp lastInsert = getTimeOfLastInsert(name, true);

                            if (lastInsert != null) {
                                everythingIsOld = lastInsert.getTime() - timeSt.getTime() < 0;
                            }

                            final String query = everythingIsOld ? truncateString : deleteString;

                            psDelete = conn.prepareStatement(query);

                            if (!everythingIsOld) {
                                psDelete.setTimestamp(1, timeSt);
                            }

                            psDelete.executeUpdate();
                            ConnectionCommands.close(psDelete);
                        } catch (final SQLException e) {
                            ConnectionCommands.close(psDelete);
                            e.printStackTrace();
                            LOGGER.error("SQLException received (go to the next element) : " + e);
                            continue;
                        } catch (final ArchivingException e) {
                            ConnectionCommands.close(psDelete);
                            e.printStackTrace();
                            LOGGER.error("ArchivingException received (go to the next element) : " + e);
                            continue;
                        } catch (final Exception e) {
                            ConnectionCommands.close(psDelete);
                            e.printStackTrace();
                            LOGGER.error("Unknown Exception received (go to the next element) : " + e);
                            continue;
                        }
                    }
                }
            } finally {
                ConnectionCommands.close(psDelete);
                connector.closeConnection(conn);
            }
        }
    }

    public Timestamp now() throws ArchivingException {
        Timestamp date = null;
        if (connector != null) {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            final String sqlStr = getRequest();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();

                    rset = stmt.executeQuery(sqlStr);
                    rset.next();
                    // Gets the result of the query
                    date = rset.getTimestamp(1);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, sqlStr);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return date;
    }

    // --------------------------------- Archiving Watcher Report
    // ---------------------------------
    /**
     * Return a Watcher Report: 1/archived attributes number 2/number of KO and
     * OK attributes 3/List of KO attribute's name 4/List Of partitions for
     * Oracle database 5/List of job's Status 6/List of job's errors
     */

    // --------------------------------- Archiving Watcher Report
    // ---------------------------------
    /**
     * Start watcher report in real time
     */
    @Override
    public void startWatcherReport() throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            CallableStatement stmt = null;
            final String startAdminReportQuery = "CALL " + connector.getSchema() + ".FEEDALIVE()";
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.prepareCall(startAdminReportQuery);
                    // dbConn.setLastStatement(stmt);
                    stmt.execute();
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, startAdminReportQuery);
            } finally {
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
    }

    /**
     * return % of procedure call progression
     */
    @Override
    public int getFeedAliveProgression() throws ArchivingException {
        int result = 0;
        if (connector != null) {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final String getProgressionLevelQuery = new StringBuilder("SELECT TRUNC ( COUNT(").append(ConfigConst.ID)
                    .append(")/").append("(SELECT COUNT(*) FROM ").append(ConfigConst.AMT).append(" WHERE ")
                    .append(ConfigConst.STOP_DATE).append(" IS NULL)").append(") FROM ").append(connector.getSchema())
                    .append(DB_SEPARATOR).append(ConfigConst.IS_ALIVED).toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(getProgressionLevelQuery);
                    // Gets the result of the query
                    if (rset.next()) {
                        result = rset.getInt(1);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getProgressionLevelQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;

    }

    /**
     * return the number of KO and OK attributes
     */
    @Override
    public int getAttributesCountOkOrKo(final boolean isOKStatus) throws ArchivingException {
        int attributesCount = 0;
        if (connector != null) {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final String getAttributesCountDataQuery = new StringBuilder("SELECT DISTINCT(COUNT(*)) FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.IS_ALIVED).append(" WHERE ")
                    .append("(").append(ConfigConst.IS_ALIVED).append(DB_SEPARATOR).append(ConfigConst.STATUS)
                    .append(" = ").append(isOKStatus ? "\'OK\'" : "\'KO\'").append(")").toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    rset = stmt.executeQuery(getAttributesCountDataQuery);
                    // Gets the result of the query
                    if (rset.next()) {
                        attributesCount = rset.getInt(1);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getAttributesCountDataQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return attributesCount;

    }

    @Override
    public String[] getKOAttrCountByDevice() throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = null;
        } else {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final String getKOAttributesByDeviceQuery = new StringBuilder("SELECT * FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.IS_ALIVED2).toString();
            final Collection<String> res = new ArrayList<String>();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    rset = stmt.executeQuery(getKOAttributesByDeviceQuery);
                    // Gets the result of the query
                    res.add("\n---------- Ko attributes count by device: ");
                    res.add("Archiver  NBR");
                    while (rset.next()) {
                        final String elt = rset.getString(1) + "  " + String.valueOf(rset.getInt(2));
                        res.add(elt);
                    }
                }
            } catch (final SQLException e) {
                String message = ObjectUtils.EMPTY_STRING;
                if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                        || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.ADB_CONNECTION_FAILURE;
                } else {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.STATEMENT_FAILURE;
                }

                final String reason = GlobalConst.QUERY_FAILURE;
                final String desc = "Failed while executing DbUtils.getKOAttrCountByDevice() method...";
                throw new ArchivingException(message, reason, getKOAttributesByDeviceQuery, ErrSeverity.WARN, desc,
                        this.getClass().getName(), e);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
            // Returns the number of active simple signals defined in HDB
            result = res.size() > 2 ? toStringArray(res) : null;
        }
        return result;

    }

    /**
     * return the number of KO and OK attributes
     */
    @Override
    public String[] getKoAttributes() throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = null;
        } else {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final Collection<String> koAttributes = new ArrayList<String>();
            final String getKOAttributesQuery = new StringBuilder("SELECT * FROM ").append(connector.getSchema())
                    .append(DB_SEPARATOR).append(ConfigConst.IS_ALIVED).append(" WHERE (").append(ConfigConst.IS_ALIVED)
                    .append(DB_SEPARATOR).append(ConfigConst.STATUS).append(" = \'KO\') ORDER BY ")
                    .append(ConfigConst.IS_ALIVED).append(DB_SEPARATOR).append(ConfigConst.MAX_TIME).toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    rset = stmt.executeQuery(getKOAttributesQuery);
                    // Gets the result of the query
                    koAttributes.add("\n---------- List of Ko attributes: ");
                    koAttributes.add("Status  Full_Name  ID  Archiver  maxTime");
                    while (rset.next()) {
                        String elt = rset.getString(1) + "  " + rset.getString(2) + "  "
                                + String.valueOf(rset.getInt(3)) + "  " + rset.getString(4) + "  ";
                        elt += rset.getDate(5) == null ? NULL : rset.getDate(5);
                        koAttributes.add(elt);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getKOAttributesQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
            // Returns the number of active simple signals defined in HDB
            result = koAttributes.size() > 2 ? toStringArray(koAttributes) : null;
        }
        return result;
    }

    /**
     * return the list of job's status
     */
    @Override
    public String[] getListOfJobStatus() throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = null;
        } else {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final Collection<String> jobStatusVector = new ArrayList<String>();

            final String getListOfJobStatusQuery = new StringBuilder("SELECT ").append(ConfigConst.OWNER)
                    .append(VALUE_SEPARATOR).append(ConfigConst.JOB_NAME).append(VALUE_SEPARATOR)
                    .append(ConfigConst.STATUS).append(", to_char(").append(ConfigConst.LOG_DATE)
                    .append(",\'YYYY-MM-DD HH24:MI:SS\') time FROM ").append(ConfigConst.DBA_SCHEDULER_JOB_LOG)
                    .append(" WHERE ").append(ConfigConst.LOG_DATE).append(" IN (SELECT MAX(")
                    .append(ConfigConst.LOG_DATE).append(") FROM ").append(ConfigConst.DBA_SCHEDULER_JOB_LOG)
                    .append(" GROUP BY ").append(ConfigConst.JOB_NAME).append(") AND ").append(ConfigConst.OWNER)
                    .append(" IN (\'SYSTEM\', \'").append(connector.getSchema().toUpperCase())
                    .append("\', \'ADMINISTRATOR\') ORDER BY 2,1").toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    rset = stmt.executeQuery(getListOfJobStatusQuery);

                    // Gets the result of the query
                    jobStatusVector.add("\n---------- List of job status: ");
                    jobStatusVector.add("OWNER  JOB_NAME  STATUS  LOG_DATE");
                    while (rset.next()) {
                        String elt = rset.getString(1) + "  " + rset.getString(2) + "  " + rset.getString(3) + "  ";
                        elt += rset.getTimestamp(4) == null ? NULL : rset.getTimestamp(4).toString();
                        jobStatusVector.add(elt);
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getListOfJobStatusQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
            // Returns the number of active simple signals defined in HDB
            result = jobStatusVector.size() > 2 ? toStringArray(jobStatusVector) : null;
        }
        return result;
    }

    /**
     * return the list of job's status
     */
    @Override
    public String[] getListOfJobErrors() throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = null;
        } else {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final Collection<String> jobErrorsVector = new ArrayList<String>();

            String getJobErrorsQuery = new StringBuilder("SELECT to_char(").append(ConfigConst.LOG_DATE)
                    .append(",\'YYYY-MM-DD HH24:MI:SS\') time , ").append(ConfigConst.JOB_NAME).append(VALUE_SEPARATOR)
                    .append(ConfigConst.STATUS).append(" ,").append(ConfigConst.ADDITIONAL_INFO).append(" FROM ")
                    .append(ConfigConst.DBA_SCHEDULER_JOB_RUN_DETAILS).append(" WHERE ")
                    .append(ConfigConst.ADDITIONAL_INFO).append(" IS NOT NULL  AND ").append(ConfigConst.LOG_DATE)
                    .append(" > SYSDATE - 5 ORDER BY 1").toString();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    getJobErrorsQuery = conn.nativeSQL(getJobErrorsQuery);
                    stmt = conn.createStatement();
                    rset = stmt.executeQuery(getJobErrorsQuery);
                    // Gets the result of the query
                    jobErrorsVector.add("\n---------- List of job Errors : ");
                    jobErrorsVector.add("LOG_DATE  JOB_NAME  STATUS  ADDITIONAL_INFO");
                    while (rset.next()) {
                        jobErrorsVector.add(rset.getTimestamp(1).toString() + "  " + rset.getString(2) + "  "
                                + rset.getString(3) + "  " + rset.getString(4));
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getJobErrorsQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
            // Returns the number of active simple signals defined in HDB
            result = jobErrorsVector.size() > 2 ? toStringArray(jobErrorsVector) : null;
        }
        return result;
    }

    // ---------------------------------------------------------------------------------------

    /**
     * <b>Description : </b> Build a array of Double with the given Double
     * Collection
     *
     * @param vector
     *            The given Double Collection
     * @return a Double type array that contains the different vector's Double
     *         type elements <br>
     */
    public static double[] toDoubleArray(final Collection<Double> vector) {
        double[] array;
        array = new double[vector.size()];
        int index = 0;
        for (final Double element : vector) {
            array[index++] = element == null ? MathConst.NAN_FOR_NULL : element.doubleValue();
        }
        return array;
    }

    /**
     * <b>Description : </b> Build a array of Double with the two given Double
     * Collections
     *
     * @param vector1
     *            The first given Double Collection
     * @param vector2
     *            The second given Double Collection
     * @return a Double type array that contains the first and the second Double
     *         elements <br>
     */
    public static double[] toDoubleArray(final Collection<Double> vector1, final Collection<Double> vector2) {
        for (final Double element : vector2) {
            vector1.add(element);
        }
        return toDoubleArray(vector1);
    }

    /**
     * <b>Description : </b> Build a array of String with the given String
     * Collection
     *
     * @param vector
     *            The given String Collection
     * @return a String type array that contains the different vector's String
     *         type elements <br>
     */
    public static String[] toStringArray(final Collection<String> vector) {
        return vector.toArray(new String[vector.size()]);
    }

    /**
     * <b>Description : </b> Build a array of String with the two String Double
     * Collections
     *
     * @param vector1
     *            The first given String Collection
     * @param vector2
     *            The second given String Collection
     * @return a String type array that contains the first and the second String
     *         elements <br>
     */
    public static String[] toStringArray(final Collection<String> vector1, final Collection<String> vector2) {
        for (final String element : vector2) {
            vector1.add(element);
        }
        return toStringArray(vector1);
    }

    /**
     * This method returns a String which contains the value columnn name
     *
     * @param completeName
     *            : attribute full_name
     * @param dataFormat
     *            : AttrDataFormat._SCALAR or AttrDataFormat._SPECTRUM ot
     *            AttrDataFormat._IMAGE
     * @param writable
     *            AttrWriteType._READ_WRITE or AttrWriteType._READ or
     *            AttrWriteType._WRITE
     * @return corresponding column name
     */
    private String getValueColumnName(final int dataFormat, final int writable) {

        if (dataFormat == AttrDataFormat._SCALAR) {
            if (writable == AttrWriteType._READ_WRITE) {
                return ConfigConst.READ_VALUE;
            } else {
                return ConfigConst.VALUE;
            }
        } else if (dataFormat == AttrDataFormat._SPECTRUM) {
            if (writable == AttrWriteType._READ_WRITE) {
                return ConfigConst.READ_VALUE;
            } else {
                return ConfigConst.VALUE;
            }
        } else {
            if (writable == AttrWriteType._READ_WRITE) {
                return ConfigConst.READ_VALUE;
            } else {
                return ConfigConst.VALUE;
            }
        }
    }

    /**
     *
     * @param attDirPath
     * @param prefix
     * @param nbFileMax
     * @return
     */
    public int getRelativePathIndex(final String attDirPath, final String prefix, final int nbFileMax) {
        final File attDirectory = new File(attDirPath);
        // Le repertoire est il vide ?
        if (!attDirectory.exists()) {
            // Si 'oui', je le cree et je renvoi 1
            attDirectory.mkdirs();
            return 1;
        } else {
            // Si 'non' je choisi le reperoire courant (d'index maximum) : je
            // compte pour celui le nombre de sous repertoire
            final int currentIndexDir = attDirectory.listFiles().length;
            final String currentPath = attDirPath + File.separator + prefix + currentIndexDir;
            final File attCurrentDirectory = new File(currentPath);
            // Si le nombre de fichier du repertoire courant est inferieur au
            // nombre max... je retourne le nombre courant
            if (attCurrentDirectory.listFiles().length < nbFileMax) {
                return currentIndexDir;
            } else {
                return currentIndexDir + 1;
            }
        }
    }

    /**
     * This method returns the name of the table associated (table in which will host its archived values) to the given
     * attribute
     *
     * @param attributeName the attribute's name (cf. ADT in HDB).
     * @return the name of the table associated (table in which will host its archived values) to the given attribute.
     */
    @Override
    public String getTableName(final String attributeName) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final int id = att.getIds().getAttID(attributeName, att.isCaseSentitiveDatabase());
        if (id <= 0) {
            throw new ArchivingException("Invalid attribute: " + attributeName, "Invalid attribute: " + attributeName,
                    ErrSeverity.WARN,
                    "No database connection or \"" + attributeName + "\" attribute not found in database",
                    this.getClass().getName());
        }
        return getTableName(id);
    }

    protected boolean isNull(final String string, final boolean testNaN) {
        boolean isNull;
        if (string == null) {
            isNull = true;
        } else {
            final String trimmed = string.trim();
            if (trimmed.isEmpty() || NULL.equals(trimmed)) {
                isNull = true;
            } else if (testNaN && "NaN".equalsIgnoreCase(trimmed)) {
                isNull = true;
            } else {
                isNull = false;
            }
        }
        return isNull;
    }

    protected boolean isNull(final String string) {
        return isNull(string, false);
    }

    protected boolean isNullOrNaN(final String string) {
        return isNull(string, true);
    }

    public static Object defaultValue(final int dataType) {
        switch (dataType) {
            case TangoConst.Tango_DEV_BOOLEAN:
                return false;
            case TangoConst.Tango_DEV_STATE:
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
                return 0;
            case TangoConst.Tango_DEV_LONG64:
            case TangoConst.Tango_DEV_ULONG64:
                return 0l;
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_UCHAR:
                return (short) 0;
            case TangoConst.Tango_DEV_FLOAT:
                return Float.NaN;
            case TangoConst.Tango_DEV_DOUBLE:
                return MathConst.NAN_FOR_NULL;
            default:
                return null;
        }
    }

    public static Object cast(int dataType, double d) {
        switch (dataType) {
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
            case TangoConst.Tango_DEV_UCHAR:
                return (short) d;
            case TangoConst.Tango_DEV_STATE:
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
                return (int) d;
            case TangoConst.Tango_DEV_LONG64:
            case TangoConst.Tango_DEV_ULONG64:
                return (long) d;
            case TangoConst.Tango_DEV_DOUBLE:
                return d;
            case TangoConst.Tango_DEV_FLOAT:
                return (float) d;
            case TangoConst.Tango_DEV_BOOLEAN:
                return !Double.isNaN(d) && d != 0;
            default:
                return d;
        }
    }

    public static Object cast(final int dataType, String currentValRead) {
        try {
            switch (dataType) {
                case TangoConst.Tango_DEV_BOOLEAN:
                    return (int) Double.parseDouble(currentValRead) != 0;
                case TangoConst.Tango_DEV_STATE:
                case TangoConst.Tango_DEV_LONG:
                case TangoConst.Tango_DEV_ULONG:
                    return Integer.parseInt(currentValRead);
                case TangoConst.Tango_DEV_LONG64:
                case TangoConst.Tango_DEV_ULONG64:
                    return Long.parseLong(currentValRead);
                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                case TangoConst.Tango_DEV_UCHAR:
                    return Short.parseShort(currentValRead);
                case TangoConst.Tango_DEV_FLOAT:
                    return Float.parseFloat(currentValRead);
                case TangoConst.Tango_DEV_DOUBLE:
                    return Double.parseDouble(currentValRead);
                case TangoConst.Tango_DEV_STRING:
                    return StringFormater.formatStringToRead(currentValRead);
                default:
                    return null;
            }
        } catch (NumberFormatException e) {
            switch (dataType) {
                case TangoConst.Tango_DEV_BOOLEAN:
                    return "true".equalsIgnoreCase(currentValRead.trim());
                case TangoConst.Tango_DEV_STATE:
                case TangoConst.Tango_DEV_LONG:
                case TangoConst.Tango_DEV_ULONG:
                    return (int) Double.parseDouble(currentValRead);
                case TangoConst.Tango_DEV_LONG64:
                case TangoConst.Tango_DEV_ULONG64:
                    return (long) Double.parseDouble(currentValRead);
                case TangoConst.Tango_DEV_SHORT:
                case TangoConst.Tango_DEV_USHORT:
                case TangoConst.Tango_DEV_UCHAR:
                    return (short) Double.parseDouble(currentValRead);
                default:
                    return null;
            }
        }

    }

    protected void fillScalarValue(NullableData<boolean[]>[] array, int index, int dataType, String dbString) {
        Object value = DbData.initPrimitiveArray(dataType, 1);
        final boolean[] nullElements = new boolean[1];
        if (isNullOrNaN(dbString)) {
            nullElements[0] = true;
            Array.set(value, 0, defaultValue(dataType));
        } else {
            Array.set(value, 0, cast(dataType, dbString));
        }
        array[index] = new NullableData<boolean[]>(value, nullElements);
    }

    @Override
    public NullableData<boolean[]>[] getScalarValue(final String readString, final String writeString,
            final int dataType, final int writable) {
        NullableData<boolean[]>[] result;
        if ((readString == null) && (writeString == null)) {
            result = null;
        } else {
            @SuppressWarnings("unchecked")
            NullableData<boolean[]>[] tmp = new NullableData[2];
            result = tmp;

            if (writable != AttrWriteType._WRITE) {
                fillScalarValue(result, READ_INDEX, dataType, readString);
            }
            if (writable != AttrWriteType._READ) {
                fillScalarValue(result, WRITE_INDEX, dataType, writeString);
            }
        }
        return result;
    }

    protected StringTokenizer initSpectrumTokenizer(String dbString) {
        StringTokenizer tokenizer;
        if (isNull(dbString)) {
            tokenizer = null;
        } else {
            tokenizer = new StringTokenizer(dbString, GlobalConst.CLOB_SEPARATOR);
        }
        return tokenizer;
    }

    protected int getSize(final StringTokenizer tokenizer, final String dbString, final int dataType) {
        int size = 0;
        if (tokenizer != null) {
            size += tokenizer.countTokens();
            if (dbString.startsWith(GlobalConst.CLOB_SEPARATOR) && dataType == TangoConst.Tango_DEV_STRING) {
                size++;
            }
        }
        return size;
    }

    protected void fillSpectrumValue(NullableData<boolean[]>[] array, int index, int dataType, String dbString,
            StringTokenizer tokenizer) {
        int i = 0;
        int size = getSize(tokenizer, dbString, dataType);
        Object value = DbData.initPrimitiveArray(dataType, size);
        final boolean[] nullElements = new boolean[size];
        if (dbString.startsWith(GlobalConst.CLOB_SEPARATOR) && dataType == TangoConst.Tango_DEV_STRING) {
            nullElements[i] = true;
            i++;
        }
        while (tokenizer.hasMoreTokens()) {
            final String currentVal = tokenizer.nextToken();
            if (isNullOrNaN(currentVal)) {
                nullElements[i] = true;
                Array.set(value, i, defaultValue(dataType));
            } else {
                Array.set(value, i, cast(dataType, currentVal));
            }
            i++;
        }
        array[index] = new NullableData<boolean[]>(value, nullElements);
    }

    @Override
    public NullableData<boolean[]>[] getSpectrumValue(final String readString, final String writeString,
            final int dataType) {
        NullableData<boolean[]>[] result;
        StringTokenizer readTokenizer = initSpectrumTokenizer(readString);
        StringTokenizer writeTokenizer = initSpectrumTokenizer(writeString);
        if ((readTokenizer == null) && (writeTokenizer == null)) {
            result = null;
        } else {
            @SuppressWarnings("unchecked")
            NullableData<boolean[]>[] tmp = new NullableData[2];
            result = tmp;

            if (readTokenizer != null) {
                fillSpectrumValue(result, READ_INDEX, dataType, readString, readTokenizer);
            }

            if (writeTokenizer != null) {
                fillSpectrumValue(result, WRITE_INDEX, dataType, writeString, writeTokenizer);
            }
        }
        return result;
    }

    protected StringTokenizer initImageTokenizer(String dbString) {
        StringTokenizer tokenizer;
        if (isNull(dbString)) {
            tokenizer = null;
        } else {
            tokenizer = new StringTokenizer(
                    dbString.replace("[", ObjectUtils.EMPTY_STRING).replace("]", ObjectUtils.EMPTY_STRING),
                    GlobalConst.CLOB_SEPARATOR_IMAGE_ROWS);
        }
        return tokenizer;
    }

    protected void fillImageValue(final NullableData<boolean[][]>[] array, final int index, final int dataType,
            final StringTokenizer readTokenizer) {
        Object[] valArray = null;
        int rowSize = 0, colSize = 0;
        if (readTokenizer != null) {
            rowSize = readTokenizer.countTokens();
            valArray = new Object[rowSize];
            int i = 0;
            while (readTokenizer.hasMoreTokens()) {
                valArray[i++] = readTokenizer.nextToken().trim().split(GlobalConst.CLOB_SEPARATOR_IMAGE_COLS);
            }
            if (rowSize > 0) {
                colSize = ((String[]) valArray[0]).length;
            }
        }
        final boolean[][] nullElements = new boolean[rowSize][colSize];
        switch (dataType) {
            case TangoConst.Tango_DEV_BOOLEAN:
                boolean[][] bvalueArr = new boolean[rowSize][colSize];
                if (rowSize > 0 && colSize > 0) {
                    for (int i = 0; i < rowSize; i++) {
                        for (int j = 0; j < colSize; j++) {
                            try {
                                if (isNullOrNaN(((String[]) valArray[i])[j])) {
                                    bvalueArr[i][j] = false;
                                    nullElements[i][j] = true;
                                } else {
                                    bvalueArr[i][j] = (int) Double.parseDouble(((String[]) valArray[i])[j].trim()) != 0;
                                }
                            } catch (final NumberFormatException n) {
                                bvalueArr[i][j] = "true".equalsIgnoreCase(((String[]) valArray[i])[j].trim());
                            }
                        }
                    }
                }
                array[index] = new NullableData<boolean[][]>(bvalueArr, nullElements);
                bvalueArr = null;
                break;
            case TangoConst.Tango_DEV_CHAR:
            case TangoConst.Tango_DEV_UCHAR:
                byte[][] cvalueArr = new byte[rowSize][colSize];
                if (rowSize > 0 && colSize > 0) {
                    for (int i = 0; i < valArray.length; i++) {
                        for (int j = 0; j < colSize; j++) {
                            try {
                                if (isNullOrNaN(((String[]) valArray[i])[j])) {
                                    cvalueArr[i][j] = 0;
                                    nullElements[i][j] = true;
                                } else {
                                    cvalueArr[i][j] = Byte.parseByte(((String[]) valArray[i])[j].trim());
                                }
                            } catch (final NumberFormatException n) {
                                cvalueArr[i][j] = (byte) Double.parseDouble(((String[]) valArray[i])[j].trim());
                            }
                        }
                    }
                }
                array[index] = new NullableData<boolean[][]>(cvalueArr, nullElements);
                cvalueArr = null;
                break;
            case TangoConst.Tango_DEV_STATE:
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
                int[][] lvalueArr = new int[rowSize][colSize];
                if (rowSize > 0 && colSize > 0) {
                    for (int i = 0; i < valArray.length; i++) {
                        for (int j = 0; j < colSize; j++) {
                            try {
                                if (isNullOrNaN(((String[]) valArray[i])[j])) {
                                    lvalueArr[i][j] = 0;
                                    nullElements[i][j] = true;
                                } else {
                                    lvalueArr[i][j] = Integer.parseInt(((String[]) valArray[i])[j].trim());
                                }
                            } catch (final NumberFormatException n) {
                                lvalueArr[i][j] = (int) Double.parseDouble(((String[]) valArray[i])[j].trim());
                            }
                        }
                    }
                }
                array[index] = new NullableData<boolean[][]>(lvalueArr, nullElements);
                lvalueArr = null;
                break;
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
                short[][] svalueArr = new short[rowSize][colSize];
                if (rowSize > 0 && colSize > 0) {
                    for (int i = 0; i < valArray.length; i++) {
                        for (int j = 0; j < colSize; j++) {
                            try {
                                if (isNullOrNaN(((String[]) valArray[i])[j])) {
                                    svalueArr[i][j] = 0;
                                    nullElements[i][j] = true;
                                } else {
                                    svalueArr[i][j] = Short.parseShort(((String[]) valArray[i])[j].trim());
                                }
                            } catch (final NumberFormatException n) {
                                svalueArr[i][j] = (short) Double.parseDouble(((String[]) valArray[i])[j].trim());
                            }
                        }
                    }
                }
                array[index] = new NullableData<boolean[][]>(svalueArr, nullElements);
                svalueArr = null;
                break;
            case TangoConst.Tango_DEV_FLOAT:
                float[][] fvalueArr = new float[rowSize][colSize];
                if (rowSize > 0 && colSize > 0) {
                    for (int i = 0; i < valArray.length; i++) {
                        for (int j = 0; j < colSize; j++) {
                            try {
                                if (isNullOrNaN(((String[]) valArray[i])[j])) {
                                    fvalueArr[i][j] = Float.NaN;
                                    nullElements[i][j] = true;
                                } else {
                                    fvalueArr[i][j] = Float.parseFloat(((String[]) valArray[i])[j].trim());
                                }
                            } catch (final NumberFormatException n) {
                                fvalueArr[i][j] = (float) Double.parseDouble(((String[]) valArray[i])[j].trim());
                            }
                        }
                    }
                }
                array[index] = new NullableData<boolean[][]>(fvalueArr, nullElements);
                fvalueArr = null;
                break;
            case TangoConst.Tango_DEV_STRING:
                String[][] stvalueArr = new String[rowSize][colSize];
                if (rowSize > 0 && colSize > 0) {
                    for (int i = 0; i < valArray.length; i++) {
                        for (int j = 0; j < colSize; j++) {
                            if (isNullOrNaN(((String[]) valArray[i])[j])) {
                                stvalueArr[i][j] = null;
                                nullElements[i][j] = true;
                            } else {
                                stvalueArr[i][j] = StringFormater
                                        .formatStringToRead(((String[]) valArray[i])[j].trim());
                            }
                        }
                    }
                }
                array[index] = new NullableData<boolean[][]>(stvalueArr, nullElements);
                stvalueArr = null;
                break;
            case TangoConst.Tango_DEV_DOUBLE:
            default:
                double[][] dvalueArr = new double[rowSize][colSize];
                if (rowSize > 0 && colSize > 0) {
                    for (int i = 0; i < valArray.length; i++) {
                        for (int j = 0; j < colSize; j++) {
                            if (isNullOrNaN(((String[]) valArray[i])[j])) {
                                dvalueArr[i][j] = MathConst.NAN_FOR_NULL;
                                nullElements[i][j] = true;
                            } else {
                                dvalueArr[i][j] = Double.parseDouble(((String[]) valArray[i])[j].trim());
                            }
                        }
                    }
                }
                array[index] = new NullableData<boolean[][]>(dvalueArr, nullElements);
                dvalueArr = null;
                break;
        }
    }

    @Override
    public NullableData<boolean[][]>[] getImageValue(final String readValue, final String writeValue,
            final int dataType) {
        NullableData<boolean[][]>[] result;
        StringTokenizer readTokenizer = initImageTokenizer(readValue);
        StringTokenizer writeTokenizer = initImageTokenizer(writeValue);
        if ((readTokenizer == null) && (writeTokenizer == null)) {
            result = null;
        } else {
            @SuppressWarnings("unchecked")
            NullableData<boolean[][]>[] tmp = new NullableData[2];
            result = tmp;
            fillImageValue(result, READ_INDEX, dataType, readTokenizer);
            fillImageValue(result, WRITE_INDEX, dataType, writeTokenizer);
        }
        return result;
    }

    /*
     *
     */
    @Override
    public boolean isLastDataNull(final String attributName) throws ArchivingException {
        boolean ret;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (connector == null || att == null) {
            ret = false;
        } else {
            final int[] tfw = att.getAttTFWData(attributName);
            final int format = tfw[1];
            final int writable = tfw[2];
            final boolean roFields = writable == AttrWriteType._READ || writable == AttrWriteType._WRITE;

            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // int data_type = tfw [0];

            // Create and execute the SQL query string
            // Build the query string

            final StringBuilder queryBuilder = new StringBuilder("SELECT ");
            if (roFields) {
                queryBuilder.append(toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.VALUE);
            } else {
                queryBuilder.append(toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.READ_VALUE).append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
            }
            queryBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(getTableName(attributName));
            // + " LIMIT 1 "
            final String query = queryBuilder.toString();

            try {
                conn = connector.getConnection();
                if (conn == null) {
                    ret = false;
                } else {
                    stmt = conn.createStatement();
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(query);
                    rset.next();

                    if (roFields) {
                        if (format == AttrDataFormat._SCALAR) {
                            ret = rset.getString(2) == null || rset.getString(2).isEmpty();
                        } else {
                            ret = rset.getClob(2) == null || rset.getClob(2).equals(ObjectUtils.EMPTY_STRING);
                        }
                    } else {
                        if (format == AttrDataFormat._SCALAR) {
                            ret = rset.getString(2) == null || rset.getString(2).isEmpty();
                            ret = ret && (rset.getString(3) == null || rset.getString(3).isEmpty());
                        } else {
                            ret = rset.getClob(2) == null || rset.getClob(2).equals(ObjectUtils.EMPTY_STRING);
                            ret = ret && (rset.getClob(3) == null || rset.getClob(3).equals(ObjectUtils.EMPTY_STRING));
                        }
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return ret;
    }

    /**
     * This method returns a String array which contains as first element the
     * time value and the second element indicates if the corresponding value is
     * null or not
     *
     * @param completeName
     *            : attribute full_name
     * @param dataFormat
     *            : AttrDataFormat._SCALAR or AttrDataFormat._SPECTRUM ot
     *            AttrDataFormat._IMAGE
     * @param writable
     *            AttrWriteType._READ_WRITE or AttrWriteType._READ or
     *            AttrWriteType._WRITE
     * @return couple of string with the maximum time value and the value equal
     *         to null or notnull
     * @throws ArchivingException
     */
    @Override
    public String[] getTimeValueNullOrNotOfLastInsert(final String completeName, final int dataFormat,
            final int writable) throws ArchivingException {
        String ret[] = null;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (connector != null || att != null) {
            String tableName;
            try {
                tableName = connector.getSchema() + DB_SEPARATOR + getTableName(completeName);
            } catch (final ArchivingException e) {
                tableName = null;
            }
            if (tableName != null) {
                final String query = new StringBuilder("select ")
                        .append(getFormattedTimeField(ObjectUtils.EMPTY_STRING)).append(",")
                        .append(getValueColumnName(dataFormat, writable)).append(" from ").append(tableName)
                        .append("  where time = (select MAX(time) from ").append(tableName).append(")").toString();

                // Tools.trace("DataBaseApi.getTimeValueNullOrNotOfLastInsert - query:"
                // + query + "for " + completeName,
                // Warnable.LOG_LEVEL_DEBUG);

                Connection conn = null;
                Statement stmt = null;
                ResultSet rset = null;

                try {
                    conn = connector.getConnection();
                    if (conn != null) {
                        stmt = conn.createStatement();
                        rset = stmt.executeQuery(query);

                        if (rset.next()) { // Otherwise it indicates that the ResultSet is empty
                            final String rawDate = rset.getString(1);
                            if (!rset.wasNull() && !isNull(rawDate)) {
                                ret = new String[2];
                                ret[0] = rawDate;
                                final String readString = rset.getString(2);
                                if (isNull(readString)) {
                                    ret[1] = NULL;
                                } else {
                                    ret[1] = NOT_NULL;
                                }
                            }
                        }
                    }
                } catch (final SQLException e) {
                    throw new ArchivingException(e, query);
                } finally {
                    ConnectionCommands.close(rset);
                    ConnectionCommands.close(stmt);
                    connector.closeConnection(conn);
                }
            }
        }
        return ret;
    }

    /**
     *
     * @param type
     * @return
     */
    public static int getHdbTdbType(final int type) {
        switch (type) {
            case ConfigConst.TDB_MYSQL:
            case ConfigConst.TDB_ORACLE:
                return ConfigConst.TDB;
            case ConfigConst.HDB_MYSQL:
            case ConfigConst.HDB_ORACLE:
                return ConfigConst.HDB;
            default:
                return -1;
        }
    }

    /**
     *
     * @param type
     * @return
     */
    public static int getDbType(final int type) {
        switch (type) {
            case ConfigConst.TDB_MYSQL:
            case ConfigConst.HDB_MYSQL:
                return ConfigConst.BD_MYSQL;
            case ConfigConst.TDB_ORACLE:
            case ConfigConst.HDB_ORACLE:
                return ConfigConst.BD_ORACLE;
            default:
                return -1;
        }
    }

    public static int getArchivingType(final int type, final int bd) {
        if (type == ConfigConst.HDB) {
            return bd == ConfigConst.BD_MYSQL ? ConfigConst.HDB_MYSQL : ConfigConst.HDB_ORACLE;
        } else {
            return bd == ConfigConst.BD_MYSQL ? ConfigConst.TDB_MYSQL : ConfigConst.TDB_ORACLE;
        }
    }

    public static String getPoolName(final int archType, final String user) {
        if (archType == ConfigConst.HDB) {
            return "HDB_" + user;
        } else {
            return "TDB_" + user;
        }
    }

}
