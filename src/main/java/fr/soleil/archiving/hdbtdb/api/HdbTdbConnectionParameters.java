package fr.soleil.archiving.hdbtdb.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DbDevice;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.utils.DbConnectionInfo;

public class HdbTdbConnectionParameters {
	
	 final static Logger logger = LoggerFactory.getLogger(HdbTdbConnectionParameters.class);

    private static final DbConnectionInfo hdbInfoConnector = new DbConnectionInfo(ConfigConst.HDB_CLASS_DEVICE);
    private static final DbConnectionInfo tdbInfoConnector = new DbConnectionInfo(ConfigConst.TDB_CLASS_DEVICE);

    public static final String HDB_AVAILABLE = "hdb";
    public static final String TDB_AVAILABLE = "tdb";
    public static final String BOTH_AVAILABLE = "both";

    private static String dbAvailable = BOTH_AVAILABLE;

    public static boolean isHdbAvailable() {
        return dbAvailable.equals(HDB_AVAILABLE) || dbAvailable.equals(BOTH_AVAILABLE);
    }

    public static boolean isTdbAvailable() {
        return dbAvailable.equals(TDB_AVAILABLE) || dbAvailable.equals(BOTH_AVAILABLE);
    }

    public static String getHDBUser() {
        return hdbInfoConnector.getDbUser();
    }

    public static void setHDBUser(String hDBUser, String origin) {
        hdbInfoConnector.setDbUser(hDBUser, origin);
    }

    public static String getHDBPassword() {
        return hdbInfoConnector.getDbPassword();
    }

    public static void setHDBPassword(String hDBPassword, String origin) {
        hdbInfoConnector.setDbPassword(hDBPassword, origin);
    }

    public static String getHDbHost() {
        return hdbInfoConnector.getDbHost();
    }

    public static void setHDbHost(String hDbHost, String origin) {
        hdbInfoConnector.setDbHost(hDbHost, origin);
    }

    public static String getHDbName() {
        return hdbInfoConnector.getDbName();
    }

    public static void setHDbName(String hDbName, String origin) {
        hdbInfoConnector.setDbName(hDbName, origin);
    }

    public static String getHDbSchema() {
        return hdbInfoConnector.getDbSchema();
    }

    public static void setHDbSchema(String hDbSchema, String origin) {
        hdbInfoConnector.setDbSchema(hDbSchema, origin);
    }

    public static boolean isHDbRac() {
        return Boolean.parseBoolean(hdbInfoConnector.isDbRac());
    }

    public static void setHDbRac(String hDbRac, String origin) {
        hdbInfoConnector.setDbRac(hDbRac, origin);
    }

    public static String getTDBUser() {
        return tdbInfoConnector.getDbUser();
    }

    public static void setTDBUser(String hDBUser, String origin) {
        tdbInfoConnector.setDbUser(hDBUser, origin);
    }

    public static String getTDBPassword() {
        return tdbInfoConnector.getDbPassword();
    }

    public static void setTDBPassword(String hDBPassword, String origin) {
        tdbInfoConnector.setDbPassword(hDBPassword, origin);
    }

    public static String getTDbHost() {
        return tdbInfoConnector.getDbHost();
    }

    public static void setTDbHost(String hDbHost, String origin) {
        tdbInfoConnector.setDbHost(hDbHost, origin);
    }

    public static String getTDbName() {
        return tdbInfoConnector.getDbName();
    }

    public static void setTDbName(String hDbName, String origin) {
        tdbInfoConnector.setDbName(hDbName, origin);
    }

    public static String getTDbSchema() {
        return tdbInfoConnector.getDbSchema();
    }

    public static void setTDbSchema(String hDbSchema, String origin) {
        tdbInfoConnector.setDbSchema(hDbSchema, origin);
    }

    public static boolean isTDBRac() {
        return Boolean.parseBoolean(tdbInfoConnector.isDbRac());
    }

    public static void setTDbRac(String hDbRac, String origin) {
        tdbInfoConnector.setDbRac(hDbRac, origin);
    }

    public static void setDbAvailable(String available) {
        dbAvailable = available;
    }

    public static void initDbAvailable() {
        setDbAvailable(System.getProperty("fr.soleil.db.active", "both"));
    }

    // Init with properties

    public static void initHdbFromDeviceProperties(final DbDevice device) throws ArchivingException {
        try {
            // If the device has special connection properties
            String[] alternateProperties = { "HdbHost", "HdbName", "HdbSchema", "HdbUser", "HdbPwd",
                    "HdbRacConnection" };
            hdbInfoConnector.initFromDevicePropertiesList(device, alternateProperties);
            // Else we get default properties
            hdbInfoConnector.initFromDeviceProperties(device);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static void initTdbFromDeviceProperties(final DbDevice device) throws ArchivingException {
        try {
            // If the device has special connection properties
            String[] alternateProperties = { "TdbHost", "TdbName", "TdbSchema", "TdbUser", "TdbPwd",
                    "TdbRacConnection" };
            tdbInfoConnector.initFromDevicePropertiesList(device, alternateProperties);
            // Else we get default properties
            tdbInfoConnector.initFromDeviceProperties(device);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static void initHdbFromClassProperties() throws ArchivingException {
        try {
            hdbInfoConnector.initFromClassProperties();
        } catch (final DevFailed devFailed) {
            devFailed.printStackTrace();
            throw new ArchivingException(devFailed);
        }
    }

    public static void initTdbFromClassProperties() throws ArchivingException {
        try {
            tdbInfoConnector.initFromClassProperties();
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
    }

    public static void initHdbFromDefaultProperties() {
        hdbInfoConnector.initFromDefaultProperties(ConfigConst.HDB_HOST, ConfigConst.HDB_SCHEMA_NAME,
                ConfigConst.HDB_SCHEMA_NAME, ConfigConst.HDB_BROWSER_USER, ConfigConst.HDB_BROWSER_PASSWORD, "false");
    }

    public static void initTdbFromDefaultProperties() {
        tdbInfoConnector.initFromDefaultProperties(ConfigConst.TDB_HOST, ConfigConst.TDB_SCHEMA_NAME,
                ConfigConst.TDB_SCHEMA_NAME, ConfigConst.TDB_BROWSER_USER, ConfigConst.TDB_BROWSER_PASSWORD, "false");
    }

    public static void initHdbFromSystemProperties() {
        hdbInfoConnector.initFromSystemProperties("HDB_HOST", "HDB_NAME", "HDB_SCHEMA", "HDB_USER", "HDB_PASS",
                "HDB_RAC");
    }

    public static void initTdbFromSystemProperties() {
        tdbInfoConnector.initFromSystemProperties("TDB_HOST", "TDB_NAME", "TDB_SCHEMA", "TDB_USER", "TDB_PASS",
                "TDB_RAC");
    }

    public static void initHdbFromArgumentsProperties(final String host, final String name, final String schema,
            final String user, final String pass, final String isRac) {
        hdbInfoConnector.initFromArgumentsProperties(host, name, schema, user, pass, isRac);
    }

    public static void initTdbFromArgumentsProperties(final String host, final String name, final String schema,
            final String user, final String pass, final String isRac) {
        tdbInfoConnector.initFromArgumentsProperties(host, name, schema, user, pass, isRac);
    }

    /**
     * Try to initialize information with all known methods. The search is
     * interrupted when a value is find for an information. <br>
     * Order of search :
     * <ul>
     * <li>Given arguments</li>
     * <li>System properties</li>
     * <li>Device Properties</li>
     * <li>Class Properties From HdbArchiver</li>
     * <li>Default registered properties (only for test database)</li>
     * </ul>
     */
    public static void performAllHDBInit(final DbDevice device, final String host, final String name,
            final String schema, final String user, final String pass, final String isRac) throws ArchivingException {
        initDbAvailable();
        if (isHdbAvailable()) {
            initHdbFromArgumentsProperties(host, name, schema, user, pass, isRac);
            initHdbFromSystemProperties();
            if (device != null) {
                initHdbFromDeviceProperties(device);
            }
            initHdbFromClassProperties();
            initHdbFromDefaultProperties();
        }
    }

    /**
     * Try to initialize information with all known methods. The search is
     * interrupted when a value is find for an information. If an argument is
     * <code>null</code>, then the related search method won't be called (if
     * className is <code>null</code> for example, we won't search for Class
     * Properties).
     * 
     * <br>
     * Order of search :
     * <ul>
     * <li>Given arguments</li>
     * <li>System properties</li>
     * <li>Device Properties</li>
     * <li>Class Properties From TdbArchiver</li>
     * <li>Default registered properties (only for test database)</li>
     * </ul>
     */
    public static void performAllTDBInit(final DbDevice device, final String host, final String name,
            final String schema, final String user, final String pass, final String isRac) throws ArchivingException {
        initDbAvailable();
        if (isTdbAvailable()) {
            initTdbFromArgumentsProperties(host, name, schema, user, pass, isRac);
            initTdbFromSystemProperties();
            if (device != null) {
                initTdbFromDeviceProperties(device);
            }
            initTdbFromClassProperties();
            initTdbFromDefaultProperties();
        }
    }

    /**
     * Try to initialize information with all methods, except arguments. The
     * search is interrupted when a value is find for an information. If an
     * argument is <code>null</code>, then the related search method won't be
     * called (if className is <code>null</code> for example, we won't search
     * for Class Properties). <br>
     * Order of search :
     * <ul>
     * <li>System properties</li>
     * <li>Device Properties</li>
     * <li>Class Properties From HdbArchiver</li>
     * <li>Default registered properties (only for test database)</li>
     * </ul>
     */
    public static void performHDBInit(final DbDevice device, boolean searchSystemProperties,
            boolean searchClassProperties) throws ArchivingException {

        if (searchSystemProperties) {
            initHdbFromSystemProperties();
        }
        if (device != null) {
            initHdbFromDeviceProperties(device);
        }
        if (searchClassProperties) {
            initHdbFromClassProperties();
        }
        initHdbFromDefaultProperties();
    }

    /**
     * Try to initialize information with all methods, except arguments. The
     * search is interrupted when a value is find for an information. If an
     * argument is <code>null</code>, then the related search method won't be
     * called (if className is <code>null</code> for example, we won't search
     * for Class Properties). <br>
     * Order of search :
     * <ul>
     * <li>System properties</li>
     * <li>Device Properties</li>
     * <li>Class Properties From TdbArchiver</li>
     * <li>Default registered properties (only for test database)</li>
     * </ul>
     */
    public static void performTDBInit(final DbDevice device, boolean searchSystemProperties,
            boolean searchClassProperties) throws ArchivingException {

        if (searchSystemProperties) {
            initTdbFromSystemProperties();
        }
        if (device != null) {
            initTdbFromDeviceProperties(device);
        }
        if (searchClassProperties) {
            initTdbFromClassProperties();
        }
        initTdbFromDefaultProperties();
    }

    public static void printHDBConnectionInfoLog() {
        if (isHdbAvailable()) {
            logger.info(hdbInfoConnector.appendLoggerTrace(null).toString());
        } else {
           logger.error("\nNO HDB CONNECTION\n");
        }
    }

    public static void printTDBConnectionInfoLog() {
        if (isTdbAvailable()) {
        	logger.info(tdbInfoConnector.appendLoggerTrace(null).toString());
        } else {
        	logger.error("\nNO TDB CONNECTION\n");
        }
    }
}
