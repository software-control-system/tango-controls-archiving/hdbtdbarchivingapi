/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt;

import java.util.Collection;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.AttributeHeavy;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeDomains;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeFamilies;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeIds;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeMembers;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeNames;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeProperties;

/**
 * @author AYADI
 * 
 */
public interface IAdtAptAttributes {

    boolean isCaseSentitiveDatabase();

    AttributeFamilies getFamilies();

    AttributeMembers getMembers();

    AttributeNames getNames();

    AttributeDomains getDomains();

    AttributeIds getIds();

    AttributeProperties getProperties();

    int[] getAttTFWData(String attributeName) throws ArchivingException;

    int getAttDataWritable(String attributeName) throws ArchivingException;

    int getAttDataFormat(String attributeName) throws ArchivingException;

    int getAttDataType(String attributeName) throws ArchivingException;

    int getAttRecordCount(String attributeName) throws ArchivingException;

    boolean isRegisteredADT(String attributeName) throws ArchivingException;

    void registerAttribute(AttributeHeavy attributeHeavy) throws ArchivingException;

    void createAttributeTableIfNotExist(String attributeName, AttributeHeavy attr) throws ArchivingException;

    Collection<String> getAttDefinitionData(String attributeName) throws ArchivingException;
}
