package fr.soleil.archiving.hdbtdb.api.tools.mode;

import fr.soleil.archiving.common.api.exception.ArchivingException;

import java.util.HashMap;
import java.util.Map;

/**
 * Handles comparisons between the initialized list of short period attributes,
 * and other attributes
 *
 * @author awo
 */
public class ShortPeriodAttributesManager {

    /**
     * Map containing short period attributes: key: the full name of the
     * attribute value: the period in seconds
     */
    private static Map<String, Integer> shortPeriodAttributes = new HashMap<>();

    /**
     * Return true if the input attribute name is in the shortPeriodAttributes
     * map
     */
    public static boolean isShortPeriodAttribute(final String attributeName) throws ArchivingException {
        if (attributeName == null) {
            return false;
        }
        return shortPeriodAttributes.containsKey(attributeName.toLowerCase());
    }

    /**
     * Returns the corresponding period value to the input attribute name
     */
    public static Integer getPeriodFromShortAttributeName(final String attributeName) throws ArchivingException {
        if (attributeName == null) {
            return null;
        }
        return shortPeriodAttributes.get(attributeName.toLowerCase());
    }

    public static Map<String, Integer> getShortPeriodAttributes() {
        return shortPeriodAttributes;
    }

    public static void setShortPeriodAttributes(final Map<String, Integer> shortPeriodAttributes) {
        ShortPeriodAttributesManager.shortPeriodAttributes = shortPeriodAttributes;
    }
}
