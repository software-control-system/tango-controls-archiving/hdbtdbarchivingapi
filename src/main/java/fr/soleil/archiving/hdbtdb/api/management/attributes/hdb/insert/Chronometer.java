package fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert;

/**
 * Calculates elapsed time
 * 
 * @author ABEILLE
 */
public class Chronometer {

    // 1mn contains 60000ms
    private static final int TO_MIN = 60000;
    private long startTime;
    private long duration;
    private volatile boolean isOver;

    /**
     * Starts the chronometer
     * 
     * @param duration The duration in minutes
     */
    public void start(final long duration) {
        startTime = System.currentTimeMillis();
        this.duration = duration * TO_MIN;
        isOver = false;
    }

    /**
     * Stops the chronometer
     */
    public void stop() {
        isOver = true;
    }

    /**
     * Checks if the started duration is over
     * 
     * @return A <code>boolean</code>
     */
    public boolean isOver() {
        if (!isOver) {
            final long now = System.currentTimeMillis();
            if (now - startTime > duration * TO_MIN) {
                isOver = true;
            }
        }
        return isOver;
    }
}
