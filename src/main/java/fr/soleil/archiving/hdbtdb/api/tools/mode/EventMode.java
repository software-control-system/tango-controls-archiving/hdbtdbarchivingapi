//+============================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Mode/ModeExterne.java,v $
//
// Project:      Tango Archiving Service
//
// Description: This object is one of the Mode class fields.
//				This class describes the 'external mode' (the archiving is not triggered by the filing service).
//
// $Author: chinkumo $
//
// $Revision: 1.3 $
//
// $Log: ModeExterne.java,v $
// Revision 1.3  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.2.16.1  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.2  2005/01/26 15:35:38  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :   Synchrotron SOLEIL
//			    L'Orme des Merisiers
//			    Saint-Aubin - BP 48
//			    91192 GIF-sur-YVETTE CEDEX
//              FRANCE
//
//+============================================================================

package fr.soleil.archiving.hdbtdb.api.tools.mode;

public final class EventMode extends ModeRoot {
    private String description = "tango event mode";

    /**
     * Default constructor
     */
    public EventMode() {
    }

    /**
     * Gets the description of this object.
     *
     * @return the description of this object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of this object.
     *
     * @param desc the description of this object.
     */
    public void setDescription(final String desc) {
        description = desc;
    }

    /**
     * Returns a string representation of the object <I>ModeExterne</I>.
     *
     * @return a string representation of the object <I>ModeExterne</I>.
     */
    @Override
    public String toString() {
        final StringBuilder buf = new StringBuilder("");
        buf.append(Tag.MODE_E_TAG + "\r\n" + "\t" + Tag.MODE_E1_TAG + " = \"" + getDescription() + "\"");
        return buf.toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EventMode)) {
            return false;
        }

        final EventMode eventMode = (EventMode) o;

        if (!description.equals(eventMode.description)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return description.hashCode();
    }
}