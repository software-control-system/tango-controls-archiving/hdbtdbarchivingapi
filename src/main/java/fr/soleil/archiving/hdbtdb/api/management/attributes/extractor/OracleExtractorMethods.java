package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.esrf.Tango.AttrWriteType;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.archiving.hdbtdb.api.utils.database.NullableData;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class OracleExtractorMethods extends GenericExtractorMethods {

    public OracleExtractorMethods(AbstractDataBaseConnector connector) {
        super(connector);
    }

    protected String getReadString(ResultSet rset, int index, int writable) throws SQLException {
        String readString = null;
        if (writable != AttrWriteType._WRITE) {
            Clob readClob = rset.getClob(index);
            if (rset.wasNull()) {
                readString = NULL;
            } else {
                readString = readClob.getSubString(1, (int) readClob.length());
            }
        }
        return readString;
    }

    protected String getWriteString(ResultSet rset, int index, int writable) throws SQLException {
        String writeString = null;
        if (writable != AttrWriteType._READ) {
            Clob writeClob = null;
            if (writable == AttrWriteType._WRITE) {
                writeClob = rset.getClob(index);
            } else {
                writeClob = rset.getClob(index + 1);
            }
            if (rset.wasNull()) {
                writeString = NULL;
            } else {
                writeString = writeClob.getSubString(1, (int) writeClob.length());
            }
        }
        return writeString;
    }

    @Override
    public DbData[] treatStatementResultForGetSpectData(final ResultSet rset, final String attributeName,
            final DbData... dbData) throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1, dataType = -1;
            for (DbData data : result) {
                if (data != null) {
                    writable = data.getWritable();
                    dataType = data.getDataType();
                    break;
                }
            }
            try {
                List<NullableTimedData> readList = new ArrayList<>(), writeList = new ArrayList<>();
                for (DbData data : result) {
                    if (data != null) {
                        data.setMaxX(0);
                    }
                }
                while (rset.next()) {
                    if (isCanceled()) {
                        break;
                    }
                    long time = DateUtil.stringToMilli(rset.getString(1));
                    int dimX = rset.getInt(2);
                    NullableTimedData readData;
                    NullableTimedData writeData;

                    if (result[READ_INDEX] == null) {
                        readData = null;
                    } else {
                        readData = DbData.initNullableTimedData(dataType, time, dimX);
                        if (readData.getX() > result[READ_INDEX].getMaxX()) {
                            result[READ_INDEX].setMaxX(readData.getX());
                        }
                    }
                    if (result[WRITE_INDEX] == null) {
                        writeData = null;
                    } else {
                        writeData = DbData.initNullableTimedData(dataType, time, dimX);
                        if (writeData.getX() > result[WRITE_INDEX].getMaxX()) {
                            result[WRITE_INDEX].setMaxX(writeData.getX());
                        }
                    }
                    // Value
                    String readString = getReadString(rset, 3, writable);
                    String writeString = getWriteString(rset, 3, writable);

                    final NullableData<boolean[]>[] tmp = dbUtils.getSpectrumValue(readString, writeString, dataType);
                    NullableData.fillTimedData(tmp, readData, writeData);
                    if (readData != null) {
                        readList.add(readData);
                    }
                    if (writeData != null) {
                        writeList.add(writeData);
                    }
                } // end while (rset.next())
                DbData.afterExtraction(readList, writeList, result);
            } catch (final SQLException e) {
                throw new ArchivingException(e, "see OracleDataGetters#treatStatementResultForGetSpectData");
            }
        }
        return dbData;
    }

    @Override
    public DbData[] treatStatementResultForGetImageData(final ResultSet rset, final String attributeName,
            final DbData... dbData) throws ArchivingException {
        DbData[] result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null) || (att == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1, dataType = -1;
            for (DbData data : result) {
                if (data != null) {
                    writable = data.getWritable();
                    dataType = data.getDataType();
                    break;
                }
            }
            if (writable == AttrWriteType._READ_WITH_WRITE || writable == AttrWriteType._READ_WRITE) {
                throw new ArchivingException("RW image attributes are not supported");
            }
            try {
                List<NullableTimedData> readList = new ArrayList<>(), writeList = new ArrayList<>();
                while (rset.next()) {
                    if (isCanceled()) {
                        break;
                    }
                    long time = DateUtil.stringToMilli(rset.getString(1));
                    int dimX = rset.getInt(2), dimY = rset.getInt(3);

                    NullableTimedData readData;
                    NullableTimedData writeData;

                    if (result[READ_INDEX] == null) {
                        readData = null;
                    } else {
                        readData = DbData.initNullableTimedData(dataType, time, dimX, dimY);
                    }
                    if (result[WRITE_INDEX] == null) {
                        writeData = null;
                    } else {
                        writeData = DbData.initNullableTimedData(dataType, time, dimX, dimY);
                    }

                    String readString = getReadString(rset, 4, writable);
                    String writeString = getWriteString(rset, 4, writable);

                    final NullableData<boolean[][]>[] temp = dbUtils.getImageValue(readString, writeString, dataType);
                    NullableData.fillTimedData(temp, readData, writeData);
                    if (readData != null) {
                        readList.add(readData);
                    }
                    if (writeData != null) {
                        writeList.add(writeData);
                    }
                } // end while (rset.next())
                DbData.afterExtraction(readList, writeList, result);
            } catch (final SQLException e) {
                throw new ArchivingException(e, "OracleDataGettersBetweenDates#getAttImageDataBetweenDatesRequest");
            }
        }
        return dbData;
    }

}
