// +============================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingApi/ConfigConst.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  This file contains all the constants and functions used by all other classes of the package.
//						(Chinkumo Jean) - Dec 1, 2002
//
// $Author: pierrejoseph $
//
// $Revision: 1.16 $
//
// $Log: ConfigConst.java,v $
// Revision 1.16  2007/03/22 16:21:36  pierrejoseph
// DataBaseApi Refactoring
//
// Revision 1.15  2006/11/16 10:52:14  ounsy
// no autocommit for oracle database
//
// Revision 1.14  2006/05/12 09:22:06  ounsy
// CLOB_SEPARATOR in GlobalConst
//
// Revision 1.13  2006/05/04 14:27:30  ounsy
// minor changes
//
// Revision 1.12  2006/02/24 12:03:14  ounsy
// CLOB_SEPARATOR_MYSQL and CLOB_SEPARATOR_ORACLE are merged
// back into CLOB_SEPARATOR
//
// Revision 1.11  2006/02/20 14:15:06  ounsy
// clob separator oracle/mysql
//
// Revision 1.10  2006/02/16 14:31:58  chinkumo
// The spectrums and images database table's fields were renamed.
// This was reported here.
//
// Revision 1.9  2006/02/08 14:22:16  ounsy
// added a CLOB_SEPARATOR=; constant
//
// Revision 1.8  2006/02/08 12:42:55  chinkumo
// The 'dim1' parameter was renamed into 'dim' (TAB_SPECTRUM_RW).
//
// Revision 1.7  2006/02/07 10:03:16  chinkumo
// AUTO_COMMIT_DEFAULT parameter added.
//
// Revision 1.6  2005/11/29 17:11:16  chinkumo
// no message
//
// Revision 1.5.10.1  2005/11/15 13:34:37  chinkumo
// no message
//
// Revision 1.5  2005/06/24 12:03:34  chinkumo
// All constants related to errors were moved from fr.soleil.hdbtdbArchivingApi.ArchivingApi.ConfigConst to fr.soleil.hdbtdbArchivingApi.ArchivingTools.Tools.GlobalConst.
//
// Revision 1.4  2005/06/14 10:12:12  chinkumo
// Branch (tangORBarchiving_1_0_1-branch_0)  and HEAD merged.
//
// Revision 1.3.4.2  2005/06/13 15:31:45  chinkumo
// New constants were created since exceptions are now managed by the archiving service.
//
// Revision 1.3.4.1  2005/05/03 16:37:01  chinkumo
// Constants were renamed and put upper case.
//
// Revision 1.3  2005/03/07 16:26:35  chinkumo
// Minor change (tag renamed)
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :   Synchrotron SOLEIL
//			    L'Orme des Merisiers
//			    Saint-Aubin - BP 48
//			    91192 GIF-sur-YVETTE CEDEX
//              FRANCE
//
//+============================================================================

package fr.soleil.archiving.hdbtdb.api;

import fr.soleil.database.DBExtractionConst;

/**
 * <B>File</B> : ConfigConst.java<br/>
 * <B>Project</B> : HDB configuration java classes (hdbconfig package)<br/>
 * <B>Description</B> : This file contains all the constants and functions used
 * by all other classes of the package<br/>
 * <B>Original</B> : Mar 4, 2003 - 6:13:47 PM
 * 
 * @author Jean CHINKUMO - Synchrotron SOLEIL
 * @version $Revision: 1.16 $
 */
public class ConfigConst implements DBExtractionConst {

    private ConfigConst() {

    }

    /*
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * |||||||||||||||||| PART 1: DataBase defaults parameters ||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     */

    // ----------------------------------- >> Historical DataBase
    /**
     * Parameter that represents the default database host
     */
    public static final String HDB_HOST = "localhost";
    /**
     * Parameter that represents the default database names
     */
    public static final String HDB_SCHEMA_NAME = "hdb";
    /**
     * Parameter that represents the default database manager user id
     * (operators...)
     */
    public static final String HDB_MANAGER_USER = "manager";
    /**
     * Parameter that represents the default database manager user password
     */
    public static final String HDB_MANAGER_PASSWORD = "manager";
    /**
     * Parameter that represents the default database archiver user id
     * (archivers...)
     */
    public static final String HDB_ARCHIVER_USER = "archiver";
    /**
     * Parameter that represents the default database archiver user password
     */
    public static final String HDB_ARCHIVER_PASSWORD = "archiver";
    /**
     * Parameter that represents the default database browser user id
     */
    public static final String HDB_BROWSER_USER = "browser";
    /**
     * Parameter that represents the default database browser user password (for
     * the default user...)
     */
    public static final String HDB_BROWSER_PASSWORD = "browser";

    /**
     * Parameter that represents the HDB device class name
     */
    public static final String HDB_CLASS_DEVICE = "HdbArchiver";

    // ----------------------------------- >> Temporary DataBase
    /**
     * Parameter that represents the default database host
     */
    public static final String TDB_HOST = "localhost";
    /**
     * Parameter that represents the default database names
     */
    public static final String TDB_SCHEMA_NAME = "tdb";
    /**
     * Parameter that represents the default database manager user id
     * (operators...)
     */
    public static final String TDB_MANAGER_USER = "manager";
    /**
     * Parameter that represents the default database manager user password
     */
    public static final String TDB_MANAGER_PASSWORD = "manager";
    /**
     * Parameter that represents the default database archiver user id
     * (archivers...)
     */
    public static final String TDB_ARCHIVER_USER = "archiver";
    /**
     * Parameter that represents the default database archiver user password
     */
    public static final String TDB_ARCHIVER_PASSWORD = "archiver";
    /**
     * Parameter that represents the default database browser user id
     */
    public static final String TDB_BROWSER_USER = "browser";
    /**
     * Parameter that represents the default database browser user password (for
     * the default user...)
     */
    public static final String TDB_BROWSER_PASSWORD = "browser";
    /**
     * Parameter that represents the TDB device class name
     */
    public static final String TDB_CLASS_DEVICE = "TdbArchiver";

    // ----------------------------------- >> DataBase Type
    /**
     * Parameter that represents the MySQL database type
     */
    public static final int BD_MYSQL = 0;
    /**
     * Parameter that represents the Oracle database type
     */
    public static final int BD_ORACLE = 1;
    /**
     * Parameter that represents the PostGreSQL database type
     */
    public static final int BD_POSTGRESQL = 2;

    // ----------------------------------- >> Drivers Types
    /**
     * Parameter that represents the MySQL database driver
     */
    public static final String DRIVER_MYSQL = "jdbc:mysql";
    /**
     * Parameter that represents the Oracle database driver
     */
    public static final String DRIVER_ORACLE = "jdbc:oracle:thin";
    /**
     * Port number for the connection
     */
    public static final String ORACLE_PORT = "1521";

    /**
     * Auto commit database default value
     */
    public static final boolean AUTO_COMMIT_DEFAULT = true;

    // /**
    // * Auto commit database for mysql
    // */
    // public final static boolean AUTO_COMMIT_MYSQL = true;
    //
    // /**
    // * Auto commit database for oracle
    // */
    // public final static boolean AUTO_COMMIT_ORACLE = false;

    /*
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||| PART 2: Special constants used to describe the database structure ||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     */

    /**
     * Array that contains the main database table's name
     */
    public static final String ADT = "adt";
    public static final String APT = "apt";
    public static final String AMT = "amt";
    public static final String IS_ALIVED = "isalived";
    public static final String STAT_PART = "statpart";
    public static final String DBA_SCHEDULER_JOB_LOG = "dba_scheduler_job_log";
    public static final String DBA_SCHEDULER_JOB_RUN_DETAILS = "dba_scheduler_job_run_details";
    public static final String IS_ALIVED2 = "isalived2";
//    public static final String[] TABS = { ADT, APT, AMT, IS_ALIVED, STAT_PART, // 0 -> 4
//            DBA_SCHEDULER_JOB_LOG, DBA_SCHEDULER_JOB_RUN_DETAILS, IS_ALIVED2 }; // 5 -> 7

    /**
     * Array that contains the name's fields for the database Definition Table
     */
    public static final String[] TAB_DEF = { "ID", "time", "full_name", "device", // 0 -> 3
            "domain", "family", "member", "att_name", // 4 -> 7
            "data_type", "data_format", "writable", // 8 -> 10
            "max_dim_x", "max_dim_y", // 11 -> 12
            "levelg", "facility", "archivable", "substitute" }; // 13 -> 16

    /**
     * Array that contains the name's fields for the database Property Table
     */
    // TODO add the "optional_properties" field !!!
    public static final String[] TAB_PROP = { "ID", "time", // 0 -> 1
            "description", "label", // 2 -> 3
            "unit", "standard_unit", "display_unit", "format", // 4 -> 7
            "min_value", "max_value", "min_alarm", "max_alarm" }; // 8 -> 11

    /**
     * Array that contains the name's fields for the database Mode Table If
     * database is historic, its length will be 23 If database is temporary, its
     * length will be 25
     */

    public static final String ID = "ID";
    public static final String TIME = "time";

    public static final String FULL_NAME = "full_name";
    public static final String DEVICE = "device";
    public static final String DOMAIN = "domain";
    public static final String FAMILY = "family";
    public static final String MEMBER = "member";
    public static final String ATT_NAME = "att_name";
    public static final String DATA_TYPE = "data_type";
    public static final String DATA_FORMAT = "data_format";
    public static final String WRITABLE = "writable";
    public static final String FACILITY = "facility";

    public static final String FORMAT = "format";

    public static final String STATUS = "status";
    public static final String MAX_TIME = "maxtime";

    public static final String ARCHIVER = "archiver";
    public static final String START_DATE = "start_date";
    public static final String STOP_DATE = "stop_date";
    public static final String PERIODIC_MODE = "per_mod";
    public static final String PERIODIC_MODE_PERIOD = "per_per_mod";
    public static final String ABSOLUTE_MODE = "abs_mod";
    public static final String ABSOLUTE_MODE_PERIOD = "per_abs_mod";
    public static final String ABSOLUTE_MODE_INF = "dec_del_abs_mod";
    public static final String ABSOLUTE_MODE_SUP = "gro_del_abs_mod";
    public static final String RELATIVE_MODE = "rel_mod";
    public static final String RELATIVE_MODE_PERIOD = "per_rel_mod";
    public static final String RELATIVE_MODE_INF = "n_percent_rel_mod";
    public static final String RELATIVE_MODE_SUP = "p_percent_rel_mod";
    public static final String THRESHOLD_MODE = "thr_mod";
    public static final String THRESHOLD_MODE_PERIOD = "per_thr_mod";
    public static final String THRESHOLD_MODE_INF = "min_val_thr_mod";
    public static final String THRESHOLD_MODE_SUP = "max_val_thr_mod";
    public static final String CALC_MODE = "cal_mod";
    public static final String CALC_MODE_PERIOD = "per_cal_mod";
    public static final String CALC_MODE_VAL = "val_cal_mod";
    public static final String CALC_MODE_TYPE = "type_cal_mod";
    public static final String CALC_MODE_ALGO = "algo_cal_mod";
    public static final String DIFF_MODE = "dif_mod";
    public static final String DIFF_MODE_PERIOD = "per_dif_mod";
    public static final String EXT_MODE = "ext_mod";
    public static final String TDB_EXPORT_PERIOD = "export_per";
    public static final String TDB_KEEPING_PERIOD = "keeping_per";

    public static final String LOG_DATE = "log_date";
    public static final String OWNER = "owner";
    public static final String JOB_NAME = "job_name";
    public static final String ADDITIONAL_INFO = "additional_info";

    public static final String AMT_ID = AMT + DB_SEPARATOR + ID;
    public static final String AMT_ARCHIVER = AMT + DB_SEPARATOR + ARCHIVER;
    public static final String AMT_START_DATE = AMT + DB_SEPARATOR + START_DATE;
    public static final String AMT_STOP_DATE = AMT + DB_SEPARATOR + STOP_DATE;
    public static final String AMT_PERDIODIC_MODE = AMT + DB_SEPARATOR + PERIODIC_MODE;
    public static final String AMT_PERDIODIC_MODE_PERIOD = AMT + DB_SEPARATOR + PERIODIC_MODE_PERIOD;
    public static final String AMT_ABSOLUTE_MODE = AMT + DB_SEPARATOR + ABSOLUTE_MODE;
    public static final String AMT_ABSOLUTE_MODE_PERIOD = AMT + DB_SEPARATOR + ABSOLUTE_MODE_PERIOD;
    public static final String AMT_ABSOLUTE_MODE_INF = AMT + DB_SEPARATOR + ABSOLUTE_MODE_INF;
    public static final String AMT_ABSOLUTE_MODE_SUP = AMT + DB_SEPARATOR + ABSOLUTE_MODE_SUP;
    public static final String AMT_RELATIVE_MODE = AMT + DB_SEPARATOR + RELATIVE_MODE;
    public static final String AMT_RELATIVE_MODE_PERIOD = AMT + DB_SEPARATOR + RELATIVE_MODE_PERIOD;
    public static final String AMT_RELATIVE_MODE_INF = AMT + DB_SEPARATOR + RELATIVE_MODE_INF;
    public static final String AMT_RELATIVE_MODE_SUP = AMT + DB_SEPARATOR + RELATIVE_MODE_SUP;
    public static final String AMT_THRESHOLD_MODE = AMT + DB_SEPARATOR + THRESHOLD_MODE;
    public static final String AMT_THRESHOLD_MODE_PERIOD = AMT + DB_SEPARATOR + THRESHOLD_MODE_PERIOD;
    public static final String AMT_THRESHOLD_MODE_INF = AMT + DB_SEPARATOR + THRESHOLD_MODE_INF;
    public static final String AMT_THRESHOLD_MODE_SUP = AMT + DB_SEPARATOR + THRESHOLD_MODE_SUP;
    public static final String AMT_CALC_MODE = AMT + DB_SEPARATOR + CALC_MODE;
    public static final String AMT_CALC_MODE_PERIOD = AMT + DB_SEPARATOR + CALC_MODE_PERIOD;
    public static final String AMT_CALC_MODE_VAL = AMT + DB_SEPARATOR + CALC_MODE_VAL;
    public static final String AMT_CALC_MODE_TYPE = AMT + DB_SEPARATOR + CALC_MODE_TYPE;
    public static final String AMT_CALC_MODE_ALGO = AMT + DB_SEPARATOR + CALC_MODE_ALGO;
    public static final String AMT_DIFF_MODE = AMT + DB_SEPARATOR + DIFF_MODE;
    public static final String AMT_DIFF_MODE_PERIOD = AMT + DB_SEPARATOR + DIFF_MODE_PERIOD;
    public static final String AMT_EXT_MODE = AMT + DB_SEPARATOR + EXT_MODE;
    public static final String AMT_TDB_EXPORT_PERIOD = AMT + DB_SEPARATOR + TDB_EXPORT_PERIOD;
    public static final String AMT_TDB_KEEPING_PERIOD = AMT + DB_SEPARATOR + TDB_KEEPING_PERIOD;

    /**
     * Parameter that represents the prefix substring for attributes tables
     */
    public static final String TAB_PREF = "att_";

    public static final String VALUE = "value";
    public static final String READ_VALUE = "read_value";
    public static final String WRITE_VALUE = "write_value";
    public static final String DIM_X = "dim_x";
    public static final String DIM_Y = "dim_y";
    /**
     * Array that contains the name's fields for the Scalar_Read Attribute's
     * Table
     */
    public static final String[] TAB_SCALAR_RO = { TIME, VALUE };
    /**
     * Array that contains the name's fields for the Scalar_Write Attribute's
     * Table
     */
    public static final String[] TAB_SCALAR_WO = { TIME, VALUE };
    /**
     * Array that contains the name's fields for the Scalar_ReadWrite
     * Attribute's Table
     */
    public static final String[] TAB_SCALAR_RW = { TIME, READ_VALUE, WRITE_VALUE };
    /**
     * Array that contains the name's fields for the Spectrum_Read Attribute's
     * Table
     */
    public static final String[] TAB_SPECTRUM_RO = { TIME, DIM_X, VALUE };
    /**
     * Array that contains the name's fields for the Spectrum_ReadWrite
     * Attribute's Table
     */
    public static final String[] TAB_SPECTRUM_RW = { TIME, DIM_X, READ_VALUE, WRITE_VALUE };
    /**
     * Array that contains the name's fields for the Image_Read Attribute's
     * Table
     */
    public static final String[] TAB_IMAGE_RO = { TIME, DIM_X, DIM_Y, VALUE };
    /**
     * Array that contains the name's fields for the Image_ReadWrite Attribute's
     * Table
     */
    public static final String[] TAB_IMAGE_RW = { TIME, DIM_X, DIM_Y, READ_VALUE, WRITE_VALUE };
    public static final int HDB = 0;
    public static final int TDB = 1;

    public static final int HDB_MYSQL = 0;
    public static final int HDB_ORACLE = 1;
    public static final int TDB_MYSQL = 2;
    public static final int TDB_ORACLE = 3;

    /*
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * |||||||||||||||| PART 3: Miscellaneous global constants ||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     * ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
     */

//    public static boolean FACILITY_NAMED = false;

//    public static String common_dir_path = "D:" + File.separator + "Temporaire" + File.separator + "partage";

    public static final String NEW_LINE = "\r\n";
    public static final String FIELDS_LIMIT = ",\n";
    public static final String LINES_LIMIT = "\n" + "%%%" + "\n";

}
