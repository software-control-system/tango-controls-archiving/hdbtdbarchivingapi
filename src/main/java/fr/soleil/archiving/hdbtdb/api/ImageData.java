/*	Synchrotron Soleil 
 *  
 *   File          :  ImageData.java
 *  
 *   Project       :  javaapi_IMAGES
 *  
 *   Description   :  
 *  
 *   Author        :  CLAISSE
 *  
 *   Original      :  25 avr. 2006 
 *  
 *   Revision:  					Author:  
 *   Date: 							State:  
 *  
 *   Log: ImageData.java,v 
 *
 */
/*
 * Created on 25 avr. 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.archiving.hdbtdb.api;

import java.sql.Timestamp;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.AttributeExtractor;

/**
 * 
 * @author CLAISSE
 */
public final class ImageData {
    /*
     * TODO : TANGO WEB WARNING : We need to implement the java.io.serializable
     * interface AND We need to correct the load() method
     */

    private Timestamp date;
    private int dimX;
    private int dimY;
    private String name;
    private boolean isHistoric;
    private final AttributeExtractor databaseExtractor;

    private double[][] value;

    /**
     * 
     */
    public ImageData(final AttributeExtractor ext) {
	super();
	this.databaseExtractor = ext;
    }

    /**
     * @return Returns the date.
     */
    public Timestamp getDate() {
	return date;
    }

    /**
     * @param date
     *            The date to set.
     */
    public void setDate(final Timestamp date) {
	this.date = date;
    }

    /**
     * @return Returns the dimX.
     */
    public int getDimX() {
	return dimX;
    }

    /**
     * @param dimX
     *            The dimX to set.
     */
    public void setDimX(final int dimX) {
	this.dimX = dimX;
    }

    /**
     * @return Returns the dimY.
     */
    public int getDimY() {
	return dimY;
    }

    /**
     * @param dimY
     *            The dimY to set.
     */
    public void setDimY(final int dimY) {
	this.dimY = dimY;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
	return name;
    }

    /**
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
	this.name = name;
    }

    /**
     * @return Returns the isHistoric.
     */
    public boolean isHistoric() {
	return isHistoric;
    }

    /**
     * @param isHistoric
     *            The isHistoric to set.
     */
    public void setHistoric(final boolean isHistoric) {
	this.isHistoric = isHistoric;
    }

    /**
     * @throws ArchivingException
     * 
     */
    public void load() throws ArchivingException {
	/*
	 * TODO Correct this method to use the same system of Mambo's life cycle
	 * with an interface and two implementation (classic and web) Data and
	 * treatment will be in different class
	 */
	final double[][] _value = databaseExtractor.getAttImageDataForDate(this.name, this.date.toString());
	this.value = _value;
    }

    /**
     * @return Returns the value.
     */
    public double[][] getValue() {
	return value;
    }

    /**
     * @param value
     *            The value to set.
     */
    public void setValue(final double[][] value) {
	this.value = value;
    }
}
