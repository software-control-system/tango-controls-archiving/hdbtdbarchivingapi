// +======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/ScalarEvent_RW.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ScalarEvent_RW.
//						(Chinkumo Jean) - Mar 10, 2004
//
// $Author: ounsy $
//
// $Revision: 1.5 $
//
// $Log: ScalarEvent_RW.java,v $
// Revision 1.5  2006/10/31 16:54:23  ounsy
// milliseconds and null values management
//
// Revision 1.4  2006/03/10 11:31:00  ounsy
// state and string support
//
// Revision 1.3  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.2.16.2  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.2.16.1  2005/09/09 08:41:08  chinkumo
// The management of the boolean type was added.
//
// Revision 1.2  2005/01/26 15:35:37  chinkumo
// Ultimate synchronization before real sharing.
//
// Revision 1.1  2004/12/06 17:39:56  chinkumo
// First commit (new API architecture).
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.archiving.hdbtdb.api.tools;

import fr.soleil.archiving.common.api.tools.ArchivingEvent;
import fr.soleil.lib.project.math.MathConst;

public class ScalarEvent_RW extends ArchivingEvent<boolean[]> {

    /**
     * Creates a new instance of ScalarEvent_RW
     */
    public ScalarEvent_RW() {
        super();
    }

    /**
     * Creates a new instance of ScalarEvent_RW
     */
    public ScalarEvent_RW(final String[] scalarEventRW) {
        this.setAttributeCompleteName(scalarEventRW[0]);
        this.setTimeStamp(Long.parseLong(scalarEventRW[1]));

        final double[] value = new double[2];
        final boolean[] nullElements = new boolean[2];
        if (scalarEventRW[2] == null || scalarEventRW[2].isEmpty()) {
            value[0] = MathConst.NAN_FOR_NULL;
            nullElements[0] = true;
        } else {
            value[0] = Double.parseDouble(scalarEventRW[2]);
        }
        if (scalarEventRW[3] == null || scalarEventRW[3].isEmpty()) {
            value[1] = MathConst.NAN_FOR_NULL;
            nullElements[1] = true;
        } else {
            value[1] = Double.parseDouble(scalarEventRW[3]);
        }
        this.setScalarValueRW(value, nullElements);
    }

    public void setScalarValueRW(final double[] value, boolean[] nullElements) {
        setValue(value, nullElements);
    }

    public void setScalarValueRW(final boolean[] bvalue, boolean[] nullElements) {
        final double[] dvalue = new double[2];
        dvalue[0] = bvalue[0] ? 1.0 : 0;
        dvalue[1] = bvalue[1] ? 1.0 : 0;
        setValue(dvalue, nullElements);
    }

    public void setScalarValueRW(final float[] fvalue, boolean[] nullElements) {
        final double[] dvalue = new double[2];
        dvalue[0] = fvalue[0];
        dvalue[1] = fvalue[1];
        setValue(dvalue, nullElements);
    }

    public void setScalarValueRW(final int[] ivalue, boolean[] nullElements) {
        final double[] dvalue = new double[2];
        dvalue[0] = ivalue[0];
        dvalue[1] = ivalue[1];
        setValue(dvalue, nullElements);
    }

    public void setScalarValueRW(final short[] svalue, boolean[] nullElements) {
        final double[] dvalue = new double[2];
        dvalue[0] = svalue[0];
        dvalue[1] = svalue[1];
        setValue(dvalue, nullElements);
    }

    public void setScalarValueRW(final String[] svalue, boolean[] nullElements) {
        setValue(svalue, nullElements);
    }

    public double[] getScalarValueRW() {
        return (double[]) getValue();
    }

    public String[] getScalarValueRWS() {
        return (String[]) getValue();
    }

    @Override
    public String[] toArray() {
        double[] value;
        final String[] scalarEventRW = new String[4];
        scalarEventRW[0] = getAttributeCompleteName().trim(); // name
        scalarEventRW[1] = Long.toString(getTimeStamp()).trim(); // time
        if (getValue() instanceof double[]) {
            value = getScalarValueRW();
            scalarEventRW[2] = Double.toString(value[0]); // value READ
            scalarEventRW[3] = Double.toString(value[1]); // value WRITE
        } else {
            scalarEventRW[2] = getScalarValueRWS()[0]; // value READ
            scalarEventRW[3] = getScalarValueRWS()[1]; // value WRITE
        }
        return scalarEventRW;

    }

    @Override
    public String toString() {
        StringBuilder scalarEvent_RW_String = new StringBuilder();
        scalarEvent_RW_String.append("Source : \t").append(getAttributeCompleteName()).append("\r\n")
                .append("TimeSt : \t").append(getTimeStamp()).append("\r\n");
        if (getValue() instanceof double[]) {
            double[] value = getScalarValueRW();
            scalarEvent_RW_String.append("Value READ: \t").append(value[0]).append("\r\n").append("Value WRITE: \t")
                    .append(value[1]).append("\r\n");
        } else {
            String[] value = getScalarValueRWS();
            scalarEvent_RW_String.append("Value READ: \t").append(value[0]).append("\r\n").append("Value WRITE: \t")
                    .append(value[1]).append("\r\n");
        }
        return scalarEvent_RW_String.toString();
    }
}
