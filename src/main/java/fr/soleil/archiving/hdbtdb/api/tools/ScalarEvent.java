// +======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/ScalarEvent.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ScalarEvent.
//						(Chinkumo Jean) - Aug 29, 2005
//
// $Author: pierrejoseph $
//
// $Revision: 1.11 $
//
// $Log: ScalarEvent.java,v $
// Revision 1.11  2007/06/04 09:03:04  pierrejoseph
// New contructor + value = null treatement in various methods
//
// Revision 1.10  2007/03/20 12:50:09  ounsy
// 1.4 compatibility
//
// Revision 1.9  2007/03/14 09:19:14  ounsy
// added a method avoidUnderFlow ()
//
// Revision 1.8  2006/10/31 16:54:24  ounsy
// milliseconds and null values management
//
// Revision 1.7  2006/07/20 09:24:15  ounsy
// minor changes
//
// Revision 1.6  2006/05/12 09:23:10  ounsy
// CLOB_SEPARATOR in GlobalConst
//
// Revision 1.5  2006/05/04 14:30:41  ounsy
// CLOB_SEPARATOR centralized in ConfigConst
//
// Revision 1.4  2006/03/13 14:46:45  ounsy
// State as an int management
// Long as an int management
//
// Revision 1.3  2006/03/10 11:31:00  ounsy
// state and string support
//
// Revision 1.2  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.1.2.2  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.1.2.1  2005/09/09 08:21:24  chinkumo
// First commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package fr.soleil.archiving.hdbtdb.api.tools;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.ArchivingEvent;
import fr.soleil.archiving.common.api.tools.GlobalConst;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScalarEvent extends ArchivingEvent<boolean[]> {
    private static final String MIN_VALUE = "1e-100";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private static double minAbsoluteValue = Double.parseDouble(MIN_VALUE);

    /**
     * Creates a new instance of DhdbEvent
     */
    public ScalarEvent() {
        super();
    }

    public ScalarEvent(final String attributeName, final int dataType, final int writable, final long timestamp,
                       final Object value, final boolean[] nullElements) {
        super();
        setAttributeCompleteName(attributeName);
        setDataFormat(AttrDataFormat._SCALAR);
        setDataType(dataType);
        setWritable(writable);
        setTimeStamp(timestamp);
        setValue(value, nullElements);
    }

    public Object getReadValue() {
        Object read_value = null;
        switch (getWritable()) {
            case AttrWriteType._READ:
            case AttrWriteType._WRITE:
                read_value = getValue();
                break;
            case AttrWriteType._READ_WITH_WRITE:
            case AttrWriteType._READ_WRITE:
                if (getValue() != null) {
                    read_value = Array.get(getValue(), 0);
                }
                break;
        }
        return read_value;
    }

    @Override
    public String[] toArray() {
        String[] scalarEvent;
        scalarEvent = new String[8];
        scalarEvent[0] = getAttributeCompleteName();
        scalarEvent[1] = Integer.toString(getDataFormat());
        scalarEvent[2] = Integer.toString(getDataType());
        scalarEvent[3] = Integer.toString(getWritable());
        scalarEvent[4] = Long.toString(getTimeStamp());
        scalarEvent[5] = getTableName();
        scalarEvent[6] = valueToString(0);
        scalarEvent[7] = valueToString(1);
        return scalarEvent;
    }

    public String valueToString(final int pos) {
        String valueToString;
        Object value = getValue();
        if (value == null) {
            valueToString = GlobalConst.ARCHIVER_NULL_VALUE;
        } else if ((value instanceof Object[]) && ((Object[]) value)[pos] == null) {
            valueToString = GlobalConst.ARCHIVER_NULL_VALUE;
        } else {
            valueToString = GlobalConst.ARCHIVER_NULL_VALUE;
            switch (getDataFormat()) {
                case AttrDataFormat._SCALAR:
                    switch (getWritable()) {
                        case AttrWriteType._READ:
                        case AttrWriteType._WRITE:
                            if (value instanceof DevState) {
                                valueToString = toString(((DevState) value).value());
                            } else {
                                valueToString = toString(value);
                            }
                            break;
                        case AttrWriteType._READ_WITH_WRITE:
                        case AttrWriteType._READ_WRITE:
                            valueToString = toString(Array.get(value, pos));
                            break;
                    }
                    break;
                case AttrDataFormat._SPECTRUM:
                case AttrDataFormat._IMAGE:
                    valueToString = toString(value);
                    break;
            }
        }
        return valueToString;
    }

    @Override
    public String toString() {
        StringBuilder scEvSt = new StringBuilder();

        scEvSt.append(getAttributeCompleteName()).append("[timestamp: ")
                .append(DATE_FORMAT.format(new Date(getTimeStamp()))).append(" - value: ");

        if (getWritable() == AttrWriteType._READ || getWritable() == AttrWriteType._READ_WITH_WRITE
                || getWritable() == AttrWriteType._READ_WRITE) {
            scEvSt.append("read value :  ").append(valueToString(0));
        }
        if (getWritable() == AttrWriteType._WRITE || getWritable() == AttrWriteType._READ_WITH_WRITE
                || getWritable() == AttrWriteType._READ_WRITE) {
            scEvSt.append("write value : ").append(valueToString(1));
        }

        scEvSt.append("]");
        return scEvSt.toString();
    }

    public void avoidUnderFlow() {
        if ((getDataType() == TangoConst.Tango_DEV_DOUBLE) && (getValue() != null)) {
            switch (super.getWritable()) {
                case AttrWriteType._READ:
                case AttrWriteType._WRITE:
                    Double valueEither = (Double) getValue();
                    if (valueEither != null) {
                        valueEither = avoidUnderFlow(valueEither);
                    }
                    super.setValue(valueEither, getNullElements());
                    break;

                case AttrWriteType._READ_WRITE:
                case AttrWriteType._READ_WITH_WRITE:
                    final double[] valueBoth = (double[]) getValue();
                    valueBoth[0] = avoidUnderFlow(valueBoth[0]);
                    valueBoth[1] = avoidUnderFlow(valueBoth[1]);
                    super.setValue(valueBoth, getNullElements());
                    break;
            }
        }
    }

    private double avoidUnderFlow(final double value) {
        final double absoluteValue = Math.abs(value);
        double ret;
        if (absoluteValue < minAbsoluteValue) {
            ret = 0.0;
        } else {
            ret = value;
        }
        return ret;
    }
}
