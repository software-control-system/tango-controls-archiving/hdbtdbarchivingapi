package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters;

import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

public final class DataGettersFactory {

    private DataGettersFactory() {
    }

    public static DataGetters getDataGatters(final AbstractDataBaseConnector connector) {
        DataGetters getters;
        if (connector == null) {
            getters = null;
        } else if (connector instanceof OracleDataBaseConnector) {
            getters = new OracleDataGetters(connector);
        } else {
            getters = new MySqlDataGetters(connector);
        }
        return getters;
    }

}
