/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;

import fr.esrf.Tango.AttrWriteType;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.GetArchivingConf;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import org.slf4j.LoggerFactory;

/**
 * @author AYADI
 *
 */
public class HDBMySqlAttributeInsert extends HdbAttributeInsert {
    private static final int DEFAULT_BULK_SIZE = 1;
    private int insertBulkSize = DEFAULT_BULK_SIZE;
    private static final String BULK_SIZE_PROP_NAME = "InsertBulkSize";

    private static final long DEFAULT_BULK_TIME = TimeUnit.SECONDS.toMillis(10);
    private long insertBulkTime = DEFAULT_BULK_TIME;
    private static final String BULK_TIME_PROP_NAME = "InsertBulkTime";
    private static int MAX_STRING_SIZE = 255;
    private final Logger logger = LoggerFactory.getLogger(HDBMySqlAttributeInsert.class);

    private final Map<String, ScalarGroupInserter> statements = Collections
            .synchronizedMap(new HashMap<String, ScalarGroupInserter>());
    private final Map<String, SpectrumGroupInserter> spectrumStatements = Collections
            .synchronizedMap(new HashMap<String, SpectrumGroupInserter>());

    /**
     * @param con
     * @param ut
     * @param at
     * @throws ArchivingException
     */
    public HDBMySqlAttributeInsert(final AbstractDataBaseConnector connector, final IAdtAptAttributes at) throws ArchivingException {
        super(connector, at);
        try {
            insertBulkSize = Math.abs(Integer.parseInt(GetArchivingConf.readStringInDB(ConfigConst.HDB_CLASS_DEVICE,
                    BULK_SIZE_PROP_NAME)));
        } catch (final Exception e) {
            insertBulkSize = DEFAULT_BULK_SIZE;
        }
        try {
            insertBulkTime = Math.abs(Integer.parseInt(GetArchivingConf.readStringInDB(ConfigConst.HDB_CLASS_DEVICE,
                    BULK_TIME_PROP_NAME)));
        } catch (final NumberFormatException e) {
            insertBulkTime = DEFAULT_BULK_TIME;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @seefr.soleil.hdbtdbArchivingApi.ArchivingApi.AttributesManagement.
     * AttributeExtractor.HDBExtractor#insert_ImageData_RO_DataBase()
     */
    @Override
    protected void insert_ImageData_RO_DataBase(final StringBuilder query, final StringBuilder tableName,
            final StringBuilder tableFields, final int dimX, final int dimY, final Timestamp timeSt,
            final double[][] dvalue, final StringBuilder valueStr, final String attributeName)
                    throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            query.append("INSERT INTO ").append(tableName).append(" (").append(tableFields).append(")")
            .append(" VALUES (?, ?, ?, ?)");
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    preparedStatement = conn.prepareStatement(query.toString());
                    // dbConn.setLastStatement(preparedStatement);
                    preparedStatement.setTimestamp(1, timeSt);
                    preparedStatement.setInt(2, dimX);
                    preparedStatement.setInt(3, dimY);
                    if (dvalue == null) {
                        preparedStatement.setNull(4, java.sql.Types.BLOB);
                    } else {
                        preparedStatement.setString(4, valueStr.toString());
                    }
                    preparedStatement.executeUpdate();
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query.toString());
            } finally {
                ConnectionCommands.close(preparedStatement);
                connector.closeConnection(conn);
            }
        }
    }

    @Override
    protected void insert_SpectrumData_RO_DataBase(final String tableName, final StringBuilder tableFields,
            final int dim_x, final Timestamp timeSt, final StringBuilder valueStr, final String att_name)
                    throws ArchivingException {
        final StringBuilder query = new StringBuilder();

        query.append("INSERT INTO ").append(tableName).append(" (").append(tableFields).append(")").append(" VALUES ");
        try {
            if (spectrumStatements.containsKey(tableName)) {
                spectrumStatements.get(tableName).addSpectrumROValue(valueStr, dim_x, timeSt);
            } else {
                final SpectrumGroupInserter inserter = new SpectrumGroupInserter(query.toString());
                spectrumStatements.put(tableName, inserter);
                inserter.addSpectrumROValue(valueStr, dim_x, timeSt);
            }
        } catch (final SQLException e) {
            throw new ArchivingException(e, query.toString());
        }
    }

    @Override
    protected void insert_SpectrumData_RW_DataBase(final String tableName, final StringBuilder tableFields,
            final int dimX, final Timestamp timeSt, final StringBuilder valueWriteStr,
            final StringBuilder valueReadStr, final String attributeName) throws ArchivingException {
        if (connector != null) {
            final StringBuilder query = new StringBuilder();
            query.append("INSERT INTO ").append(tableName).append(" (").append(tableFields).append(")")
            .append(" VALUES");
            try {
                if (spectrumStatements.containsKey(tableName)) {
                    spectrumStatements.get(tableName).addSpectrumRWValue(valueReadStr, valueWriteStr, dimX, timeSt);
                } else {
                    final SpectrumGroupInserter inserter = new SpectrumGroupInserter(query.toString());
                    spectrumStatements.put(tableName, inserter);
                    inserter.addSpectrumRWValue(valueReadStr, valueWriteStr, dimX, timeSt);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query.toString());
            }
        }
    }

    @Override
    protected synchronized void insertScalarDataInDB(final String tableName, final StringBuilder query,
            final String attributeName, final int writable, final Timestamp timeSt, final Object value,
            final int dataType) throws ArchivingException {
        try {
            Object dataToInsert = value;
            // MYSQL max size of VARCHAR is 255
            if (value instanceof String) {
                final String string = (String) value;
                if (string.length() > MAX_STRING_SIZE) {
                    dataToInsert = string.substring(0, MAX_STRING_SIZE);
                }
            }
            if (statements.containsKey(tableName)) {
                statements.get(tableName).addScalarValue(dataToInsert, dataType, writable, timeSt);
            } else {
                final ScalarGroupInserter inserter = new ScalarGroupInserter(query.toString());
                statements.put(tableName, inserter);
                inserter.addScalarValue(dataToInsert, dataType, writable, timeSt);
            }
        } catch (final SQLException e) {
            logger.error("SqlException has been raised insertScalarData()");
            throw new ArchivingException(e, query.toString());
        }
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    /**
     * Manage bulk insertion for scalar attributes. Configured by tango class
     * properties of HdbArchiver
     *
     * @author ABEILLE
     *
     */
    private class ScalarGroupInserter {
        private class InsertData {
            public InsertData(final Object value, final int dataType, final int writable, final Timestamp timestamp) {
                super();
                this.value = value;
                this.dataType = dataType;
                this.writable = writable;
                this.timestamp = timestamp;
            }

            private final Object value;
            private final int dataType;
            private final int writable;
            private final Timestamp timestamp;

            public Object getValue() {
                return value;
            }

            public int getDataType() {
                return dataType;
            }

            public int getWritable() {
                return writable;
            }

            public Timestamp getTimestamp() {
                return timestamp;
            }
        }

        private int currentBulkSize = 0;
        private long currentBulkStartTime = 0;
        private final String query;
        private StringBuilder queryBuffer;
        private final List<InsertData> dataToInsert = new ArrayList<InsertData>();

        public ScalarGroupInserter(final String query) throws ArchivingException, SQLException {
            this.query = query;
            queryBuffer = new StringBuilder(query);
        }

        public void addScalarValue(final Object value, final int dataType, final int writable, final Timestamp timestamp)
                throws SQLException, ArchivingException {
            if (currentBulkSize > 0) {
                if (writable == AttrWriteType._READ_WRITE) {
                    queryBuffer.append(", (?, ?, ?)");
                } else {
                    queryBuffer.append(", (?, ?)");
                }
            }
            dataToInsert.add(new InsertData(value, dataType, writable, timestamp));
            long currentBulkTime = 0;
            if (currentBulkStartTime == 0) {
                currentBulkStartTime = timestamp.getTime();
            } else {
                currentBulkTime = timestamp.getTime() - currentBulkStartTime;
            }
            currentBulkSize++;

            if (currentBulkSize >= insertBulkSize || currentBulkTime >= insertBulkTime) {
                Connection conn = null;
                PreparedStatement stmt = null;
                try {
                    conn = connector.getConnection();
                    if (conn != null) {
                        stmt = conn.prepareStatement(queryBuffer.toString());
                        int i = 1;
                        for (final InsertData data : dataToInsert) {
                            stmt.setTimestamp(i++, data.getTimestamp());
                            ConnectionCommands.prepareSmtScalar(stmt, data.getDataType(), data.getWritable(),
                                    data.getValue(), i++);
                            if (writable == AttrWriteType._READ_WRITE) {
                                i++;
                            }
                        }
                        stmt.execute();
                    }
                } finally {
                    currentBulkStartTime = 0;
                    currentBulkSize = 0;
                    dataToInsert.clear();
                    queryBuffer = new StringBuilder(query);
                    ConnectionCommands.close(stmt);
                    connector.closeConnection(conn);
                }
            }
        }

    }

    private class SpectrumGroupInserter {

        private class InsertData {
            public InsertData(final StringBuilder readValue, final StringBuilder writeValue, final int dimX,
                    final Timestamp timestamp) {
                super();
                this.readValue = readValue;
                this.writeValue = writeValue;
                this.dimX = dimX;
                this.timestamp = timestamp;
            }

            private final StringBuilder readValue;
            private final StringBuilder writeValue;

            public StringBuilder getWriteValue() {
                return writeValue;
            }

            private final int dimX;
            private final Timestamp timestamp;

            public StringBuilder getReadValue() {
                return readValue;
            }

            public int getDimX() {
                return dimX;
            }

            public Timestamp getTimestamp() {
                return timestamp;
            }
        }

        private int currentBulkSize = 0;
        private long currentBulkStartTime = 0;
        private final String query;
        private StringBuilder queryBuffer;
        private final List<InsertData> dataToInsert = new ArrayList<InsertData>();

        public SpectrumGroupInserter(final String query) throws ArchivingException, SQLException {
            this.query = query;
            queryBuffer = new StringBuilder(query);
        }

        private void addSpectrum(final StringBuilder readValue, final StringBuilder writeValue, final int dimX,
                final Timestamp timestamp, final boolean isReadWrite) throws ArchivingException, SQLException {
            dataToInsert.add(new InsertData(readValue, writeValue, dimX, timestamp));
            long currentBulkTime = 0;
            if (currentBulkStartTime == 0) {
                currentBulkStartTime = timestamp.getTime();
            } else {
                currentBulkTime = timestamp.getTime() - currentBulkStartTime;
            }
            currentBulkSize++;

            if (currentBulkSize >= insertBulkSize || currentBulkTime >= insertBulkTime) {
                Connection conn = null;
                PreparedStatement stmt = null;
                try {
                    conn = connector.getConnection();
                    stmt = conn.prepareStatement(queryBuffer.toString());
                    int i = 1;
                    for (final InsertData data : dataToInsert) {
                        stmt.setTimestamp(i++, data.getTimestamp());
                        stmt.setInt(i++, data.getDimX());
                        if (data.getReadValue() == null) {
                            stmt.setNull(i++, java.sql.Types.BLOB);
                        } else {
                            stmt.setString(i++, data.getReadValue().toString());
                        }
                        if (isReadWrite) {
                            if (data.getWriteValue() == null) {
                                stmt.setNull(i++, java.sql.Types.BLOB);
                            } else {
                                stmt.setString(i++, data.getWriteValue().toString());
                            }
                        }
                    }
                    stmt.execute();
                } finally {
                    currentBulkStartTime = 0;
                    currentBulkSize = 0;
                    dataToInsert.clear();
                    queryBuffer = new StringBuilder(query);
                    ConnectionCommands.close(stmt);
                    connector.closeConnection(conn);
                }
            }
        }

        public void addSpectrumROValue(final StringBuilder value, final int dimX, final Timestamp timestamp)
                throws SQLException, ArchivingException {
            if (currentBulkSize == 0) {
                queryBuffer.append("(?, ?, ?)");
            } else {
                queryBuffer.append(", (?, ?, ?)");
            }
            addSpectrum(value, null, dimX, timestamp, false);
        }

        public void addSpectrumRWValue(final StringBuilder readValue, final StringBuilder writeValue, final int dimX,
                final Timestamp timestamp) throws SQLException, ArchivingException {
            if (currentBulkSize == 0) {
                queryBuffer.append("(?, ?, ?, ?)");
            } else {
                queryBuffer.append(" ,(?, ?, ?, ?)");
            }
            addSpectrum(readValue, writeValue, dimX, timestamp, true);
        }
    }

    @Override
    public void closeConnection() {
        // do nothing

    }

}
