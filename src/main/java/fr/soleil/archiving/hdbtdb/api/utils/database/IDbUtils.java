/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.utils.database;

import java.sql.Timestamp;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;

/**
 * @author AYADI
 * 
 */
public interface IDbUtils {

    public String getTableName(String attributeName) throws ArchivingException;

    public String getFormat(SamplingType samplingType);

    public String toDbTimeString(String timeField);

    public String toDbTimeFieldString(String timeField);

    public String toDbTimeFieldString(String timeField, String format);

    public String getTime(String string) throws ArchivingException;

    public String getTableName(int index);

    public NullableData<boolean[]>[] getScalarValue(String readString, String writeString, int dataType, int writable);

    public NullableData<boolean[]>[] getSpectrumValue(String readString, String writeString, int dataType);

    public NullableData<boolean[][]>[] getImageValue(String readValue, String writeValue, int dataType);

    public Timestamp getTimeOfLastInsert(String completeName, boolean max) throws ArchivingException;

    public String[] getTimeValueNullOrNotOfLastInsert(String completeName, int dataFormat, int writable)
            throws ArchivingException;

    public void startWatcherReport() throws ArchivingException;

    public int getAttributesCountOkOrKo(boolean isOKStatus) throws ArchivingException;

    public String[] getKoAttributes() throws ArchivingException;

    public String[] getListOfPartitions() throws ArchivingException;

    public String[] getListOfJobStatus() throws ArchivingException;

    public String[] getListOfJobErrors() throws ArchivingException;

    public String[] getKOAttrCountByDevice() throws ArchivingException;

    public int getFeedAliveProgression() throws ArchivingException;

    public boolean isLastDataNull(String attributeName) throws ArchivingException;

}
