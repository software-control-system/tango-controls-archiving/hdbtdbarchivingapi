package fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.H2DataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

public final class HdbAttributeInsertFactory {

    private HdbAttributeInsertFactory() {
    }

    public synchronized static IHdbAttributeInsert getInstance(final AbstractDataBaseConnector connector,
            final IAdtAptAttributes attr) throws ArchivingException {
        IHdbAttributeInsert result;
        if (connector == null) {
            result = null;
        } else if (connector instanceof OracleDataBaseConnector || connector instanceof H2DataBaseConnector) {
            result = new HDBOracleAttributeInsert(connector, attr);
        } else {
            result = new HDBMySqlAttributeInsert(connector, attr);
        }
        return result;
    }
}
