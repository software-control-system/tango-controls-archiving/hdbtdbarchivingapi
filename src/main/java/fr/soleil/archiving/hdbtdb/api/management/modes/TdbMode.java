/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.modes;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.TdbSpec;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class TdbMode extends GenericMode {

    /**
     * @param c
     */
    public TdbMode(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    public StringBuilder updateSelectField(final StringBuilder selectFieldBuilder) {
        return selectFieldBuilder.append(VALUE_SEPARATOR).append(ConfigConst.AMT).append(DB_SEPARATOR)
                .append(ConfigConst.TDB_EXPORT_PERIOD).append(VALUE_SEPARATOR).append(ConfigConst.AMT)
                .append(DB_SEPARATOR).append(ConfigConst.TDB_KEEPING_PERIOD);
    }

    @Override
    public void setSpec(final Mode mode, final ResultSet rset) throws SQLException {
        TdbSpec tdbSpec;
        tdbSpec = new TdbSpec(rset.getInt(ConfigConst.TDB_EXPORT_PERIOD), rset.getInt(ConfigConst.TDB_KEEPING_PERIOD));
        mode.setTdbSpec(tdbSpec);
    }

    @Override
    public void appendQuery(final StringBuilder query, final StringBuilder tableName,
            final StringBuilder select_field) {
        query.append("INSERT INTO ").append(tableName).append(" (").append(select_field).append(")")
                .append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    }

    public String[] getArchivedAtt() throws ArchivingException {
        return getAttribute(false);
    }

    /**
     * @throws SQLException
     *             *
     * 
     */
    @Override
    protected void setSpecificStatementForInsertMode(final PreparedStatement preparedStatement,
            final AttributeLightMode attributeLightMode) throws SQLException {
        if (attributeLightMode.getMode().getTdbSpec() != null) {
            preparedStatement.setLong(26, attributeLightMode.getMode().getTdbSpec().getExportPeriod());
            preparedStatement.setLong(27, attributeLightMode.getMode().getTdbSpec().getKeepingPeriod());
        }

    }

}
