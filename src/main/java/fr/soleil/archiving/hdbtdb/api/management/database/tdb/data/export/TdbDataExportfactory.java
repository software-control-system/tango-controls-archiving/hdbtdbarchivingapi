/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.database.tdb.data.export;

import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.OracleDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class TdbDataExportfactory {

    /**
	 * 
	 */
    private TdbDataExportfactory() {
    }

    /***
     * 
     * @param dbType
     * @return
     */
    public static ITdbDataExport getInstance(final AbstractDataBaseConnector connector) {
        ITdbDataExport result;
        if (connector == null) {
            result = null;
        } else if (connector instanceof OracleDataBaseConnector) {
            result = new OracleTdbDataExport(connector);
        } else {
            result = new MySqlTdbDataExport(connector);
        }
        return result;
    }
}
