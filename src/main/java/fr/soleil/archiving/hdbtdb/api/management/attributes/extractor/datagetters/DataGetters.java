/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.DataExtractor;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.archiving.hdbtdb.api.utils.database.NullableData;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.lib.project.ObjectUtils;

/**
 * @author AYADI
 *
 */
public abstract class DataGetters extends DataExtractor {

    public DataGetters(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    public abstract void buildAttributeTab(String tableName, int dataType, int dataFormat, int writable)
            throws ArchivingException;

    protected abstract String getDbScalarLastNRequest(String tableName, String attributeName, int number,
            boolean roFields, String fields) throws ArchivingException;

    protected abstract DbData[] getAttSpectrumDataLastN(String attributeName, int number, DbData... dbData)
            throws ArchivingException;

    /**
     * Get the newest inserted value for an attribute. Returns READ part only.
     *
     * @param attributeName The attribute name
     * @return a DbData that contains a timestamp and a value.(DbData.getData_timed()[0].value[0] to retrieve value as a
     *         string even for spectrum)
     * @throws ArchivingException
     */
    public DbData getNewestValue(final String attributeName) throws ArchivingException {
        final DbData result = new DbData(attributeName);
        if (connector != null) {
            final int[] tfw = AdtAptAttributesFactory.getInstance(connector).getAttTFWData(attributeName);
            result.setDataType(tfw[0]);
            result.setDataFormat(tfw[1]);
            result.setWritable(tfw[2]);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String tableName = connector.getSchema() + DB_SEPARATOR + dbUtils.getTableName(attributeName);
            final boolean roFields = tfw[2] == AttrWriteType._READ || tfw[2] == AttrWriteType._WRITE;
            String selectFields;
            if (roFields) {
                selectFields = ConfigConst.TIME + VALUE_SEPARATOR + ConfigConst.VALUE;
            } else {
                selectFields = ConfigConst.TIME + VALUE_SEPARATOR + ConfigConst.READ_VALUE;
            }
            // select value, time from att_15 where time=(select max(time) from
            // att_15)
            final String query = "SELECT " + selectFields + " FROM " + tableName + " WHERE " + ConfigConst.TIME
                    + " = (SELECT MAX(" + ConfigConst.TIME + ") FROM " + tableName + ")";

            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    rset = stmt.executeQuery(query);
                    while (rset.next()) {
                        if (isCanceled()) {
                            break;
                        }
                        final NullableTimedData data = new NullableTimedData();
                        data.setDataType(result.getDataType());
                        data.setTime(rset.getTimestamp(1).getTime());
                        data.setX(1);
                        data.setValue(new String[] { rset.getString(2) }, null);
                        result.setTimedData(data);
                        break;
                    } // end while (rset.next())
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;
    }

    public abstract String getNearestValueQuery(String attributeName, String timestamp) throws ArchivingException;

    protected String getReadString(ResultSet rset, int index, int writable) throws SQLException {
        String readString;
        if (writable == AttrWriteType._WRITE) {
            readString = null;
        } else {
            readString = rset.getString(index);
        }
        return readString;
    }

    protected String getWriteString(ResultSet rset, int index, int writable) throws SQLException {
        String writeString;
        if (writable == AttrWriteType._READ) {
            writeString = null;
        } else if (writable == AttrWriteType._WRITE) {
            writeString = rset.getString(index);
        } else {
            writeString = rset.getString(index + 1);
        }
        return writeString;
    }

    /**
     * Get the nearest value and timestamp of an attribute for a given timestamp.
     *
     * @param attributeName
     * @param timestamp DD-MM-YYYY HH24:MI:SS
     * @return
     * @throws ArchivingException
     */
    public DbData[] getNearestValue(final String attributeName, final String timestamp) throws ArchivingException {
        final DbData[] result;
        if (connector == null) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            final int[] tfw = AdtAptAttributesFactory.getInstance(connector).getAttTFWData(attributeName);
            result = DbData.initExtractionResult(attributeName, tfw);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time = dbUtils.getTime(timestamp);
            final String query = getNearestValueQuery(attributeName, time);
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    if (tfw[1] == AttrDataFormat._SPECTRUM) {
                        stmt.setFetchSize(1000);
                    }
                    rset = stmt.executeQuery(query);
                    while (rset.next()) {
                        if (isCanceled()) {
                            break;
                        }
                        long timeL = rset.getTimestamp(1).getTime();
                        final NullableTimedData readData = new NullableTimedData();
                        readData.setDataType(tfw[0]);
                        readData.setTime(timeL);
                        readData.setX(1);
                        final NullableTimedData writeData = new NullableTimedData();
                        writeData.setDataType(tfw[0]);
                        writeData.setTime(timeL);
                        writeData.setX(1);
                        switch (tfw[1]) {
                            case AttrDataFormat._SCALAR: {
                                final NullableData<boolean[]>[] temp = dbUtils.getScalarValue(
                                        getReadString(rset, 2, tfw[2]), getWriteString(rset, 2, tfw[2]), tfw[0],
                                        tfw[2]);
                                NullableData.fillTimedData(temp, readData, writeData);
                                break;
                            }
                            case AttrDataFormat._SPECTRUM: {
                                int x = rset.getInt(2);
                                readData.setX(x);
                                writeData.setX(x);
                                final NullableData<boolean[]>[] temp = dbUtils.getSpectrumValue(
                                        getReadString(rset, 3, tfw[2]), getWriteString(rset, 3, tfw[2]), tfw[0]);
                                NullableData.fillTimedData(temp, readData, writeData);
                                break;
                            }
                            case AttrDataFormat._IMAGE: {
                                int x = rset.getInt(2), y = rset.getInt(3);
                                readData.setX(x);
                                readData.setY(y);
                                writeData.setX(x);
                                writeData.setY(y);
                                final NullableData<boolean[][]>[] temp = dbUtils.getImageValue(
                                        getReadString(rset, 4, tfw[2]), getWriteString(rset, 4, tfw[2]), tfw[0]);
                                NullableData.fillTimedData(temp, readData, writeData);
                                break;
                            }
                        }
                        if (result[READ_INDEX] != null) {
                            result[READ_INDEX].setTimedData(readData);
                        }
                        if (result[WRITE_INDEX] != null) {
                            result[WRITE_INDEX].setTimedData(writeData);
                        }
                        break;
                    } // end while (rset.next())
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;
    }

    public DbData[] getAttData(final String attributeName) throws ArchivingException {
        final DbData[] result;
        if (connector == null) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            final int[] tfw = AdtAptAttributesFactory.getInstance(connector).getAttTFWData(attributeName);
            result = DbData.initExtractionResult(attributeName, tfw);
            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarData(attributeName, result);
                    break;
                case AttrDataFormat._SPECTRUM:
                    getAttSpectrumData(attributeName, result);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(tfw[1], "Scalar", "Image");
                    break;
            }
        }
        return result;
    }

    private DbData[] getAttScalarData(final String attributeName, DbData... dbData) throws ArchivingException {
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        DbData[] result;
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData == null) || (dbData.length != 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = null;
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;

            // Create and execute the SQL query string
            // Build the query string
            final String tableName = connector.getSchema() + DB_SEPARATOR + dbUtils.getTableName(attributeName);
            final String fields = isROFields(writable)
                    ? dbUtils.toDbTimeFieldString(ConfigConst.TIME) + VALUE_SEPARATOR + ConfigConst.VALUE
                    : dbUtils.toDbTimeFieldString(ConfigConst.TIME) + VALUE_SEPARATOR + ConfigConst.READ_VALUE
                            + VALUE_SEPARATOR + ConfigConst.WRITE_VALUE;

            // My statement
            final String query = "SELECT " + fields + " FROM " + tableName + " ORDER BY time";

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(query);
                    methods.treatStatementResultForGetScalarData(rset, dbData);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;
    }

    private DbData[] getAttSpectrumData(final String attributeName, DbData... dbData) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if (connector == null || att == null || dbUtils == null || dbData == null) {
            return null;
        } else {

            final int writable = att.getAttDataWritable(attributeName);
            final int dataFormat = att.getAttDataFormat(attributeName);

            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            // Create and execute the SQL query string
            // Build the query string
            String selectField0 = ObjectUtils.EMPTY_STRING;
            String selectField1 = ObjectUtils.EMPTY_STRING;
            String selectField2 = ObjectUtils.EMPTY_STRING;
            String selectField3 = null;
            String message = ObjectUtils.EMPTY_STRING, reason = ObjectUtils.EMPTY_STRING,
                    desc = ObjectUtils.EMPTY_STRING;
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    message = "Failed retrieving data ! ";
                    reason = "The attribute should be Spectrum ";
                    desc = "The attribute format is  not Spectrum : " + dataFormat + " (Scalar) !!";
                    throw new ArchivingException(message, reason, null, desc, this.getClass().getName());
                case AttrDataFormat._SPECTRUM:
                    final boolean isBothReadAndWrite = !(writable == AttrWriteType._READ
                            || writable == AttrWriteType._WRITE);
                    if (!isBothReadAndWrite) {
                        selectField0 = ConfigConst.TIME;
                        selectField1 = ConfigConst.DIM_X;
                        selectField2 = ConfigConst.VALUE;
                    } else {
                        selectField0 = ConfigConst.TIME;
                        selectField1 = ConfigConst.DIM_X;
                        selectField2 = ConfigConst.READ_VALUE;
                        selectField3 = ConfigConst.WRITE_VALUE;
                    }

                    final StringBuilder queryBuilder = new StringBuilder("SELECT ");

                    queryBuilder.append(dbUtils.toDbTimeFieldString(selectField0)).append(VALUE_SEPARATOR)
                            .append(selectField1).append(VALUE_SEPARATOR).append(selectField2);
                    if (isBothReadAndWrite) {
                        queryBuilder.append(VALUE_SEPARATOR).append(selectField3);
                    }
                    queryBuilder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                            .append(dbUtils.getTableName(attributeName)).append(" ORDER BY time");
                    final String getAttributeDataQuery = queryBuilder.toString();

                    try {
                        conn = connector.getConnection();
                        if (conn != null) {
                            stmt = conn.createStatement();
                            lastStatement = stmt;
                            rset = stmt.executeQuery(getAttributeDataQuery);
                            methods.treatStatementResultForGetSpectData(rset, attributeName, dbData);
                        }
                    } catch (final SQLException e) {
                        throw new ArchivingException(e, getAttributeDataQuery);
                    } finally {
                        ConnectionCommands.close(rset);
                        ConnectionCommands.close(stmt);
                        connector.closeConnection(conn);
                    }
                    break;
                case AttrDataFormat._IMAGE:
                    message = "Failed retrieving data ! ";
                    reason = "The attribute should be Spectrum ";
                    desc = "The attribute format is  not Spectrum : " + dataFormat + " (Image) !!";
                    throw new ArchivingException(message, reason, null, desc, this.getClass().getName());
            }
        }
        return dbData;
    }

    /**
     * <b>Description : </b> Returns the number of the data archieved for an
     * attribute
     *
     * @return The number of the data archieved for an attribute<br>
     * @throws ArchivingException
     */
    public int getAttDataCount(final String attributeName) throws ArchivingException {
        int count;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if (connector == null || dbUtils == null) {
            count = 0;
        } else {
            // Create and execute the SQL query string
            final String tableName = connector.getSchema() + DB_SEPARATOR + dbUtils.getTableName(attributeName.trim());
            // MySQL and Oracle querry are the same in this case : no test
            final String getAttributeDataQuery = "SELECT " + " COUNT(*) " + " FROM " + tableName;
            count = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return count;
    }

    /**
     * <b>Description : </b> Retrieves n last data, for a given scalar
     * attribute.
     *
     * @param argin The attribute's name and the number which specifies the number of desired data.
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataLastN(final String... argin) throws ArchivingException {
        final DbData[] dbData;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final String attributeName = argin[0];
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final int number = Integer.parseInt(argin[1]);
            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);
            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataLastN(attributeName, number, dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    getAttSpectrumDataLastN(attributeName, number, dbData);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(tfw[1], "Scalar", "Image");
                    break;
            }
        }
        // Returns the names list
        return dbData;
    }

    public DbData[] getAttScalarDataLastN(final String attributeName, final int number, DbData... dbData)
            throws ArchivingException {
        DbData[] result;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (att == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : result) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final String fields = roFields
                    ? dbUtils.toDbTimeFieldString(ConfigConst.TIME) + VALUE_SEPARATOR + ConfigConst.VALUE
                    : dbUtils.toDbTimeFieldString(ConfigConst.TIME) + VALUE_SEPARATOR + ConfigConst.READ_VALUE
                            + VALUE_SEPARATOR + ConfigConst.WRITE_VALUE; // if

            // Create and execute the SQL query string
            // Build the query string
            final String tableName = connector.getSchema() + DB_SEPARATOR + dbUtils.getTableName(attributeName);
            final String query = getDbScalarLastNRequest(tableName, attributeName, number, roFields, fields);
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    lastStatement = stmt;
                    rset = stmt.executeQuery(query);
                    methods.treatStatementResultForGetScalarData(rset, result);
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, query);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return result;
    }
}
