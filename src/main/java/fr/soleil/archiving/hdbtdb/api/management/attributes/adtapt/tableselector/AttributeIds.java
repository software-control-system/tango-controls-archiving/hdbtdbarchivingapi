package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class AttributeIds extends AttributesData {

    /**
     * Map of cache per database
     */
    private static final Map<String, IDCache> ID_CACHES = new HashMap<String, IDCache>();

    private final Logger logger = LoggerFactory.getLogger(AttributeIds.class);
    private final IDCache idCache;

    public AttributeIds(final AbstractDataBaseConnector connector) {
        super(connector);
        synchronized (ID_CACHES) {
            final String key = connector.getParams().getUniqueKey();
            IDCache tmpCache = ID_CACHES.get(key);
            if (tmpCache == null) {
                logger.debug("new attribute id cache for {}", key);
                tmpCache = new IDCache(key);
                ID_CACHES.put(key, tmpCache);
            }
            this.idCache = tmpCache;
        }
    }

    /**
     * <b>Description : </b> Gets for a specified attribute its ID as defined in
     * HDB
     *
     * @param attributeName The attribute's name
     * @param caseSensitiveDb Whether database is case sensitive
     * @return The <I>HDB</I>'s ID that caracterize the given attribute
     * @throws ArchivingException
     */
    public int getAttID(final String attributeName, final boolean caseSensitiveDb) throws ArchivingException {
        int attributeID = idCache.getId(attributeName);
        if (attributeID == 0) {
            logger.debug("get attribute id from db for cache {}", idCache.getKey());
            // DBA-1178: reduce the attribute id recovery request
            if (connector != null) {
                ResultSet rset = null;
                Connection conn = null;
                PreparedStatement getAttID = null;
                final StringBuilder builder = new StringBuilder("SELECT ").append(ConfigConst.ID)
						.append(" FROM ").append(connector.getSchema()).append(".").append(ConfigConst.ADT);
                if (caseSensitiveDb) {
                    // XXX use "LOWER" to avoid case sensitivity problems. Might reduce performances.
                    builder.append(" WHERE LOWER(").append(ConfigConst.FULL_NAME).append(") = LOWER(?)");
                } else {
                    builder.append(" WHERE ").append(ConfigConst.FULL_NAME).append(" = ?");
                }
                String selectString = builder.toString();
                try {
                    conn = connector.getConnection();
                    if (conn != null) {
                        getAttID = conn.prepareStatement(selectString);
                        // dbConn.setLastStatement(ps_get_att_id);
                        final String field1 = attributeName.trim();
                        selectString = selectString.replaceFirst("\\?", field1);
                        getAttID.setString(1, field1);
                        rset = getAttID.executeQuery();
                        // Gets the result of the query
                        if (rset.next()) {
                            attributeID = rset.getInt(1);
                        }
                        putAttributeID(attributeName, attributeID);
                    }
                } catch (final SQLException e) {
                    throw new ArchivingException(e, selectString);
                } finally {
                    ConnectionCommands.close(rset);
                    ConnectionCommands.close(getAttID);
                    connector.closeConnection(conn);
                }
            }
        }
		if (attributeID == 0) {
			throw new ArchivingException("Id not found");
		}
        return attributeID;
    }

    /**
     * <b>Description : </b> Returns the new id for the attribute being referenced in <I>HDB</I>.<I>(mySQL only)</I>
     *
     * @return the new id for the attribute being referenced in <I>HDB</I>
     * @throws ArchivingException
     */
    public int getMaxID() throws ArchivingException {
        int res;
        if (connector == null) {
            res = 0;
        } else {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final int new_ID = 10000;
            res = 10000;
            // Create and execute the SQL query string
            // Build the query string
            final String getMaxIdQuery = new StringBuilder("SELECT MAX(").append(ConfigConst.ID)
					.append(") FROM ").append(connector.getSchema()).append(".").append(ConfigConst.ADT).toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    rset = stmt.executeQuery(getMaxIdQuery);
                    // Gets the result of the query
                    if (rset.next()) {
                        res = rset.getInt("max(ID)");
                    }
                    if (res < new_ID) {
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getMaxIdQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
        }
        return res;
    }

    protected void putAttributeID(final String attributeName, final int attributeID) {
        if (attributeName != null) {
            idCache.putId(attributeName, attributeID);
        }
    }

    public void clear() {
        logger.debug("clear cache");
        idCache.clear();
    }

    /**
     * Maintains a cache of attribute id
     *
     * @author thiamm
     */
    private static class IDCache {
        private final ConcurrentMap<String, Integer> attributeIdCache = new ConcurrentHashMap<String, Integer>();
        private final String key;
        private final Logger logger = LoggerFactory.getLogger(IDCache.class);;

        public IDCache(final String key) {
            this.key = key;
        }

        public void putId(final String attributeName, final int id) {
            if (attributeName != null) {
                logger.debug("put attribute in cache {}={}", attributeName, id);
                attributeIdCache.putIfAbsent(attributeName.toLowerCase(), id);
            }
            logger.debug("cache size is {} ", attributeIdCache.size());
        }

        public int getId(final String attributeName) {
            int id;
            if (attributeName == null) {
                id = 0;
            } else {
                final Integer temp = attributeIdCache.get(attributeName.toLowerCase());
                if (temp == null) {
                    id = 0;
                } else {
                    id = temp;
                }
            }
            logger.debug("get attribute from cache {}={}", attributeName, id);
            return id;
        }

        public void clear() {
            attributeIdCache.clear();
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }

        public String getKey() {
            return key;
        }

    }

}
