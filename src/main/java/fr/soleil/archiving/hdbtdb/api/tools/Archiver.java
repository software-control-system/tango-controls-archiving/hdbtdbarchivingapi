/*
 * Synchrotron Soleil
 * 
 * File : Archiver.java
 * 
 * Project : mambo
 * 
 * Description :
 * 
 * Author : CLAISSE
 * 
 * Original : 6 juin 2006
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: Archiver.java,v
 *
 */
/*
 * Created on 6 juin 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package fr.soleil.archiving.hdbtdb.api.tools;

import java.awt.Color;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.archiving.common.api.exception.ArchivingException;

public final class Archiver {
    private String name;
    private boolean dedicated;
    private String[] reservedAttributes;
    private boolean loaded = false;
    private boolean historic = false;
    private boolean exported;

    private static final Map<String, Archiver> ATTRIBUTES_TO_HDB_DEDICATED_ARCHIVERS = new ConcurrentHashMap<>();
    private static final Map<String, Archiver> ATTRIBUTES_TO_TDB_DEDICATED_ARCHIVERS = new ConcurrentHashMap<>();
    private static final Map<String, Archiver> ARCHIVERS = new ConcurrentHashMap<>();

    public static final int NON_DEDICATED_ARCHIVER_RIGHT = 1;
    public static final int DEDICATED_ARCHIVER_RIGHT = 2;
    public static final int DEDICATED_ARCHIVER_WRONG = 3;
    public static final int NON_DEDICATED_ARCHIVER_WRONG = 4;

    private static final String RESERVED_ATTRIBUTES = "reservedAttributes";
    private static final String IS_DEDICATED = "isDedicated";

    public static final Color DARKER_GREEN = new Color(0, 170, 0);
    public static final Color DARKER_RED = new Color(200, 0, 0);

    public Archiver(final String name, final boolean historic) {
        this.name = name;
        this.historic = historic;
    }

    public void load() throws ArchivingException {
        if (!loaded) {
            try {
                final Archiver loaded = getArchiver(name, historic);
                setDedicated(loaded.isDedicated());
                setReservedAttributes(loaded.getReservedAttributes());
            } finally {
                loaded = true;
            }
        }
    }

    public Color getAssociationColor(final String attributeName) throws ArchivingException {
        final int colorCase = hasReservedAttribute(attributeName);
        Color color;
        switch (colorCase) {
            case Archiver.DEDICATED_ARCHIVER_RIGHT:
                color = DARKER_GREEN;
                break;

            case Archiver.DEDICATED_ARCHIVER_WRONG:
                color = DARKER_RED;
                break;
            default:
                color = Color.BLACK;
        }

        return color;
    }

    private int hasReservedAttribute(final String attributeName) throws ArchivingException {
        load();
        final int ret;
        if (!dedicated) {
            ret = nonDedicated(attributeName);
        } else if (reservedAttributes == null || reservedAttributes.length == 0) {
            ret = nonDedicated(attributeName);
        } else if (attributeName == null || attributeName.isEmpty()) {
            ret = NON_DEDICATED_ARCHIVER_RIGHT;
        } else {
            boolean hasAttribute = false;
            for (int i = 0; i < reservedAttributes.length; i++) {
                final String nextAttribute = reservedAttributes[i];
                if (nextAttribute == null) {
                    continue;
                }

                if (nextAttribute.equals(attributeName)) {
                    hasAttribute = true;
                    break;
                }
            }
            ret = hasAttribute ? DEDICATED_ARCHIVER_RIGHT : DEDICATED_ARCHIVER_WRONG;
        }
        return ret;
    }

    private int nonDedicated(final String attributeName) {
        final Map<String, Archiver> attributesToDedicatedArchiver = historic ? ATTRIBUTES_TO_HDB_DEDICATED_ARCHIVERS
                : ATTRIBUTES_TO_TDB_DEDICATED_ARCHIVERS;
        final int ret;
        if (attributesToDedicatedArchiver == null || attributeName == null) {
            ret = NON_DEDICATED_ARCHIVER_RIGHT;
        } else if (!attributesToDedicatedArchiver.containsKey(attributeName)) {
            ret = NON_DEDICATED_ARCHIVER_RIGHT;
        } else {
            ret = NON_DEDICATED_ARCHIVER_WRONG;
        }
        return ret;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            The name to set.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return Returns the isDedicated.
     */
    public boolean isDedicated() {
        return dedicated;
    }

    /**
     * @param dedicated The isDedicated to set.
     */
    public void setDedicated(final boolean dedicated) {
        this.dedicated = dedicated;
    }

    /**
     * @return Returns the reservedAttributes.
     */
    public String[] getReservedAttributes() {
        return reservedAttributes;
    }

    /**
     * @param reservedAttributes
     *            The reservedAttributes to set.
     */
    public void setReservedAttributes(final String[] reservedAttributes) {
        this.reservedAttributes = reservedAttributes;
    }

    public void setExported(final boolean exported) {
        this.exported = exported;
    }

    /**
     * @return Returns the isExported.
     */
    public boolean isExported() {
        return exported;
    }

    protected static void updateMap(Map<String, Archiver> source, Map<String, Archiver> dest) {
        dest.clear();
        if (source != null) {
            for (Entry<String, Archiver> entry : source.entrySet()) {
                String attribute = entry.getKey();
                Archiver archiver = entry.getValue();
                if ((attribute != null) && (archiver != null)) {
                    dest.put(attribute, archiver);
                }
            }
        }
    }

    /**
     * @param attributesToHdbDedicatedArchiver
     *            The deviceDefinedAttributesToDedicatedArchiver to set.
     */
    public static void setAttributesToDedicatedArchiver(final boolean historic,
            final Map<String, Archiver> deviceDefinedAttributesToDedicatedArchiver) {
        if (historic) {
            updateMap(deviceDefinedAttributesToDedicatedArchiver, ATTRIBUTES_TO_HDB_DEDICATED_ARCHIVERS);
        } else {
            updateMap(deviceDefinedAttributesToDedicatedArchiver, ATTRIBUTES_TO_TDB_DEDICATED_ARCHIVERS);
        }
    }

    private static Archiver getArchiver(final String my_archiver, final boolean historic) throws ArchivingException {
        final Archiver ret = new Archiver(my_archiver, historic);

        DeviceProxy archiverProxy = null;
        try {
            archiverProxy = new DeviceProxy(my_archiver);

            final DbDatum isDedicatedRawValue = archiverProxy.get_property(IS_DEDICATED);
            boolean isDedicated = false;

            final DbDatum reservedAttributesRawValue = archiverProxy.get_property(RESERVED_ATTRIBUTES);
            String[] reservedAttributes = null;

            try {
                isDedicated = isDedicatedRawValue.extractBoolean();
            } catch (final Exception e) {
                // do nothing, no such attribute or empty value
            }
            try {
                reservedAttributes = reservedAttributesRawValue.extractStringArray();
            } catch (final Exception e) {
                // do nothing, no such attribute or empty value
            }

            ret.setDedicated(isDedicated);
            ret.setReservedAttributes(reservedAttributes);

            ARCHIVERS.put(ret.getName(), ret);
        } catch (final DevFailed devFailed) {
            throw new ArchivingException(devFailed);
        }
        return ret;
    }

    public static Archiver findArchiver(final String name, final boolean historic) throws ArchivingException {
        final Archiver ret;
        if (name == null) {
            ret = null;
        } else {
            final Archiver archiver = ARCHIVERS.get(name);
            if (archiver == null) {
                ret = getArchiver(name, historic);
            } else {
                ret = archiver;
            }
        }
        return ret;
    }

    /**
     * @return Returns the attributesToDedicatedArchiver.
     */
    public static Map<String, Archiver> getAttributesToDedicatedArchiver(final boolean historic) {
        return historic ? ATTRIBUTES_TO_HDB_DEDICATED_ARCHIVERS : ATTRIBUTES_TO_TDB_DEDICATED_ARCHIVERS;
    }

}
