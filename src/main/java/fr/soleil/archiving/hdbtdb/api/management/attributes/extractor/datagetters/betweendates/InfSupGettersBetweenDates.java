/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters.betweendates;

import fr.esrf.Tango.AttrDataFormat;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.DataExtractor;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtilsFactory;
import fr.soleil.archiving.hdbtdb.api.utils.database.IDbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class InfSupGettersBetweenDates extends DataExtractor {

    public InfSupGettersBetweenDates(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description : </b> Retrieves data between two dates (date_1 & date_2)
     * - Data are lower than the given value x.
     * 
     * @param argin The attribute's name, the lower limit, the upper limit, the beginning date (DD-MM-YYYY
     *            HH24:MI:SS.FF) and the ending date (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataInfThanBetweenDates(final String... argin) throws ArchivingException {
        final DbData[] dbData;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final String attributeName = argin[0];
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final int upperValue = Integer.parseInt(argin[1]);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[2]);
            final String time1 = dbUtils.getTime(argin[3]);
            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);
            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataInfThanBetweenDates(attributeName, upperValue, time0, time1, dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(tfw[1], SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(tfw[1], SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the names list
        return dbData;
    }

    /**
     * <b>Description : </b> Returns the number data lower than the given value x, and between two dates (date_1 &
     * date_2).
     * 
     * @param argin The attribute's name, the lower limit, the beginning date (DD-MM-YYYY HH24:MI:SS.FF) and the ending
     *            date (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The number data lower than the given value x, and between two dates (date_1 & date_2).<br>
     * @throws ArchivingException
     */
    public int getAttDataInfThanBetweenDatesCount(final String... argin) throws ArchivingException {
        int valuesCount = 0;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (att != null) {
            final String attributeName = argin[0];
            final int upperValue = Integer.parseInt(argin[1]);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[2].trim());
            final String time1 = dbUtils.getTime(argin[3].trim());

            // Retrieve informations on format and writable
            final int[] tfw = att.getAttTFWData(attributeName);
            // int data_type = tfw[0];
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    valuesCount = getAttScalarDataInfThanBetweenDatesCount(attributeName, upperValue, time0, time1,
                            writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the number of records
        return valuesCount;
    }

    /**
     * <b>Description : </b> Retrieves data between two dates (date_1 & date_2)
     * - Data are higher than the given value y.
     * 
     * @param argin The attribute's name, the lower limit, the beginning date (DD-MM-YYYY HH24:MI:SS.FF) and the ending
     *            date (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataSupThanBetweenDates(final String... argin) throws ArchivingException {
        final DbData[] dbData;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final String attributeName = argin[0];
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final int lowerValue = Integer.parseInt(argin[1]);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[2].trim());
            final String time1 = dbUtils.getTime(argin[3].trim());
            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);
            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataSupThanBetweenDates(attributeName, lowerValue, time0, time1, dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(tfw[1], SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(tfw[1], SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the names list
        return dbData;
    }

    /**
     * <b>Description : </b> Returns the number of data higher than the given value y, and between two dates (date_1 &
     * date_2).
     * 
     * @param argin The attribute's name, the lower limit, the beginning date (DD-MM-YYYY HH24:MI:SS.FF) and the ending
     *            date (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The number of data higher than the given value y, and between two dates (date_1 & date_2).<br>
     * @throws ArchivingException
     */
    public int getAttDataSupThanBetweenDatesCount(final String... argin) throws ArchivingException {
        int valuesCount = 0;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (att != null) {
            final String attributeName = argin[0];
            final int lowerValue = Integer.parseInt(argin[1]);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[2].trim());
            final String time1 = dbUtils.getTime(argin[3].trim());

            // Retreive informations on format and writable
            final int[] tfw = att.getAttTFWData(attributeName);
            // int data_type = tfw[0];
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    valuesCount = getAttScalarDataSupThanBetweenDatesCount(attributeName, lowerValue, time0, time1,
                            writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the number of records
        return valuesCount;
    }

    /**
     * <b>Description : </b> Retrieves data between two dates (date_1 & date_2).
     * Data are lower than the given value x OR higher than the given value y.
     * 
     * @param argin The attribute's name, the lower limit, the upper limit, the beginning date (DD-MM-YYYY
     *            HH24:MI:SS.FF) and the ending date (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataInfOrSupThanBetweenDates(final String... argin) throws ArchivingException {
        final DbData[] dbData;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final String attributeName = argin[0];
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final int lowerValue = Integer.parseInt(argin[1]);
            final int upperValue = Integer.parseInt(argin[2]);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[3].trim());
            final String time1 = dbUtils.getTime(argin[4].trim());
            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);

            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataInfOrSupThanBetweenDates(attributeName, lowerValue, upperValue, time0, time1,
                            dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(tfw[1], SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(tfw[1], SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the names list
        return dbData;
    }

    public int getAttDataInfOrSupThanBetweenDatesCount(final String... argin) throws ArchivingException {
        int valuesCount = 0;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (att != null) {
            final String attributeName = argin[0];
            final int lowerValue = Integer.parseInt(argin[1]);
            final int upperValue = Integer.parseInt(argin[2]);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[3].trim());
            final String time1 = dbUtils.getTime(argin[4].trim());

            // Retreive informations on format and writable
            final int[] tfw = att.getAttTFWData(attributeName);
            // int data_type = tfw[0];
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    valuesCount = getAttScalarDataInfOrSupThanBetweenDatesCount(attributeName, lowerValue, upperValue,
                            time0, time1, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the number of records
        return valuesCount;
    }

    /**
     * <b>Description : </b> Retrieves data between two dates (date_1 & date_2)
     * - Data are higher than the given value x AND lower than the given value y.
     * 
     * @param argin The attribute's name, the lower limit, the upper limit, the beginning date (DD-MM-YYYY
     *            HH24:MI:SS.FF) and the ending date (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The scalar data for the specified attribute<br>
     * @throws ArchivingException
     */
    public DbData[] getAttDataSupAndInfThanBetweenDates(final String... argin) throws ArchivingException {
        final DbData[] dbData;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        final String attributeName = argin[0];
        if (att == null) {
            dbData = DbData.initExtractionResult(attributeName);
        } else {
            final int lowerValue = Integer.parseInt(argin[1]);
            final int upperValue = Integer.parseInt(argin[2]);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[3].trim());
            final String time1 = dbUtils.getTime(argin[4].trim());
            final int[] tfw = att.getAttTFWData(attributeName);
            dbData = DbData.initExtractionResult(attributeName, tfw);
            switch (tfw[1]) { // data format: [0 -> SCALAR] [1 -> SPECTRUM] [2 -> IMAGE]
                case AttrDataFormat._SCALAR:
                    getAttScalarDataSupAndInfThanBetweenDates(attributeName, lowerValue, upperValue, time0, time1,
                            dbData);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(tfw[1], SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(tfw[1], SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the names list
        return dbData;
    }

    /**
     * <b>Description : </b> Returns the number of data higher than the given
     * value x, (AND) lower than the given value y, and between two dates
     * (date_1 & date_2).
     * 
     * @param argin
     *            The attribute's name, the lower limit, the upper limit, the
     *            beginning date (DD-MM-YYYY HH24:MI:SS.FF) and the ending date
     *            (DD-MM-YYYY HH24:MI:SS.FF).
     * @return The number of data higher than the given value x, (AND) lower
     *         than the given value y, and between two dates (date_1 & date_2).<br>
     * @throws ArchivingException
     */
    public int getAttDataSupAndInfThanBetweenDatesCount(final String... argin) throws ArchivingException {
        int valuesCount = 0;
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        if (att != null) {
            final String attributeName = argin[0];
            final int lowerValue = Integer.parseInt(argin[1]);
            final int upperValue = Integer.parseInt(argin[2]);
            final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
            final String time0 = dbUtils.getTime(argin[3].trim());
            final String time1 = dbUtils.getTime(argin[4].trim());

            // Retreive informations on format and writable
            final int[] tfw = att.getAttTFWData(attributeName);
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    valuesCount = getAttScalarDataSupAndInfThanBetweenDatesCount(attributeName, lowerValue, upperValue,
                            time0, time1, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                default:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
                    break;
            }
        }
        // Returns the number of records
        return valuesCount;
    }

    private DbData[] getAttScalarDataInfThanBetweenDates(final String attributeName, final int upperValue,
            final String time0, final String time1, final DbData... dbData) throws ArchivingException {
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        DbData[] result;
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);

            // Create and execute the SQL query string
            // Build the query string
            final String selectField = roFields ? ConfigConst.VALUE : ConfigConst.READ_VALUE;
            final String dateClause = ConfigConst.TIME + " BETWEEN " + dbUtils.toDbTimeString(time0.trim()) + " AND "
                    + dbUtils.toDbTimeString(time1.trim());
            // My statement
            final StringBuilder query = new StringBuilder("SELECT ");
            if (roFields) {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.VALUE);
            } else {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.READ_VALUE).append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
            }
            query.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(" WHERE ").append("(").append("(")
                    .append(selectField).append(" < ").append(upperValue).append(")").append(" AND ").append("(")
                    .append(dateClause).append(")").append(")").append(" ORDER BY time");

            result = methods.getAttScalarDataForQuery(query.toString(), result);
        }
        return result;
    }

    private DbData[] getAttScalarDataSupThanBetweenDates(final String attributeName, final int lowerValue,
            final String time0, final String time1, final DbData... dbData) throws ArchivingException {
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        DbData[] result;
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);

            // Create and execute the SQL query string
            // Build the query string
            final StringBuilder query = new StringBuilder("SELECT ");
            if (roFields) {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.VALUE);
            } else {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.READ_VALUE).append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
            }
            query.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(" WHERE ((");
            if (roFields) {
                query.append(ConfigConst.VALUE);
            } else {
                query.append(ConfigConst.READ_VALUE);
            }
            query.append(" > ").append(lowerValue).append(") AND (").append(ConfigConst.TIME).append(" BETWEEN ")
                    .append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                    .append(dbUtils.toDbTimeString(time1.trim())).append(")) ORDER BY time");
            result = methods.getAttScalarDataForQuery(query.toString(), result);
        }
        return result;
    }

    private DbData[] getAttScalarDataInfOrSupThanBetweenDates(final String attributeName, final int lowerValue,
            final int upperValue, final String time0, final String time1, final DbData... dbData)
            throws ArchivingException {
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        DbData[] result;
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);

            // Create and execute the SQL query string
            // Build the query string
            final String selectField = roFields ? ConfigConst.VALUE : ConfigConst.READ_VALUE;
            final StringBuilder query = new StringBuilder("SELECT ");
            if (roFields) {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.VALUE);
            } else {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.READ_VALUE).append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
            }
            query.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(" WHERE ((").append(selectField).append(" < ")
                    .append(lowerValue).append(" OR ").append(selectField).append(" > ").append(upperValue)
                    .append(") AND (").append(ConfigConst.TIME).append(" BETWEEN ")
                    .append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                    .append(dbUtils.toDbTimeString(time1.trim())).append(")) ORDER BY time");
            result = methods.getAttScalarDataForQuery(query.toString(), result);
        }
        return result;
    }

    private DbData[] getAttScalarDataSupAndInfThanBetweenDates(final String attributeName, final int lowerValue,
            final int upperValue, final String time0, final String time1, final DbData... dbData)
            throws ArchivingException {
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        DbData[] result;
        if ((connector == null) || (dbUtils == null) || (dbData == null) || (dbData.length < 2)
                || ((dbData[0] == null) && (dbData[1] == null))) {
            result = DbData.initExtractionResult(attributeName);
        } else {
            result = dbData;
            int writable = -1;
            for (DbData data : dbData) {
                if (data != null) {
                    writable = data.getWritable();
                    break;
                }
            }
            final boolean roFields = isROFields(writable);
            // Create and execute the SQL query string
            // Build the query string
            final String selectField = roFields ? ConfigConst.VALUE : ConfigConst.READ_VALUE;
            // My statement
            // String query = "SELECT " + fields + " FROM " + tableName + " WHERE "
            // + "(" + "(" + selectField + " < " + lower_value + " OR " +
            // selectField + " > " + upper_value + ")" + " AND " + "(" + dateClause
            // + ")" + ")";
            final StringBuilder query = new StringBuilder("SELECT ");
            if (roFields) {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.VALUE);
            } else {
                query.append(dbUtils.toDbTimeFieldString(ConfigConst.TIME)).append(VALUE_SEPARATOR)
                        .append(ConfigConst.READ_VALUE).append(VALUE_SEPARATOR).append(ConfigConst.WRITE_VALUE);
            }
            query.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR)
                    .append(dbUtils.getTableName(attributeName)).append(" WHERE ((").append(selectField).append(" > ")
                    .append(lowerValue).append(" AND ").append(selectField).append(" < ").append(upperValue)
                    .append(") AND (").append(ConfigConst.TIME).append(" BETWEEN ")
                    .append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                    .append(dbUtils.toDbTimeString(time1.trim())).append("))").append(" ORDER BY time");
            result = methods.getAttScalarDataForQuery(query.toString(), result);
        }
        return result;
    }

    /*
     * 
     */
    private int getAttScalarDataSupThanBetweenDatesCount(final String attributeName, final int lowerValue,
            final String time0, final String time1, final int writable) throws ArchivingException {
        final int result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null)) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            // Build the query string
            final String selectField0;
            final String selectField1;
            if (isROFields(writable)) {
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.VALUE;
            } else {
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.READ_VALUE;
            }

            final String getAttributeDataQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE ((").append(selectField1).append(" > ").append(lowerValue).append(") AND (")
                    .append(selectField0).append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim()))
                    .append(" AND ").append(dbUtils.toDbTimeString(time1.trim())).append("))").toString();
            result = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return result;
    }

    /*
     * 
     */
    private int getAttScalarDataInfThanBetweenDatesCount(final String attributeName, final int upperValue,
            final String time0, final String time1, final int writable) throws ArchivingException {
        final int result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null)) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            // Build the query string
            final String selectField0;
            final String selectField1;
            final String tableName = connector.getSchema() + DB_SEPARATOR + dbUtils.getTableName(attributeName);
            if (isROFields(writable)) {
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.VALUE;
            } else {
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.READ_VALUE;
            }
            final String getAttributeDataQuery = new StringBuilder("SELECT COUNT(*) FROM ").append(tableName)
                    .append(" WHERE ((").append(selectField1).append(" < ").append(upperValue).append(") AND (")
                    .append(selectField0).append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim()))
                    .append(" AND ").append(dbUtils.toDbTimeString(time1.trim())).append("))").toString();

            result = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return result;
    }

    /*
     * 
     */
    private int getAttScalarDataInfOrSupThanBetweenDatesCount(final String attributeName, final int lowerValue,
            final int upperValue, final String time0, final String time1, final int writable)
            throws ArchivingException {
        final int result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null)) {
            result = 0;
        } else {
            // Create and execute the SQL query string
            // Build the query string
            final String selectField0;
            final String selectField1;
            if (isROFields(writable)) {
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.VALUE;
            } else { // if (writable == AttrWriteType._READ_WITH_WRITE || writable
                // == AttrWriteType._READ_WRITE)
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.READ_VALUE;
            }

            final String getAttributeDataQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE ((").append(selectField1).append(" < ").append(lowerValue).append(" OR ")
                    .append(selectField1).append(" > ").append(upperValue).append(") AND (").append(selectField0)
                    .append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                    .append(dbUtils.toDbTimeString(time1.trim())).append("))").toString();
            result = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return result;
    }

    /**
     * 
     * @param attributeName
     * @param lowerValue
     * @param upperValue
     * @param time0
     * @param time1
     * @param writable
     * @return
     * @throws ArchivingException
     */
    private int getAttScalarDataSupAndInfThanBetweenDatesCount(final String attributeName, final int lowerValue,
            final int upperValue, final String time0, final String time1, final int writable)
            throws ArchivingException {
        // Create and execute the SQL query string
        // Build the query string
        final int result;
        final IDbUtils dbUtils = DbUtilsFactory.getInstance(connector);
        if ((connector == null) || (dbUtils == null)) {
            result = 0;
        } else {
            final String selectField0;
            final String selectField1;
            if (isROFields(writable)) {
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.VALUE;
            } else { // if (writable == AttrWriteType._READ_WITH_WRITE || writable
                // == AttrWriteType._READ_WRITE)
                selectField0 = ConfigConst.TIME;
                selectField1 = ConfigConst.READ_VALUE;
            }

            final String getAttributeDataQuery = new StringBuilder("SELECT COUNT(*) FROM ")
                    .append(connector.getSchema()).append(DB_SEPARATOR).append(dbUtils.getTableName(attributeName))
                    .append(" WHERE ((").append(selectField1).append(" > ").append(lowerValue).append(" AND ")
                    .append(selectField1).append(" < ").append(upperValue).append(") AND (").append(selectField0)
                    .append(" BETWEEN ").append(dbUtils.toDbTimeString(time0.trim())).append(" AND ")
                    .append(dbUtils.toDbTimeString(time1.trim())).append("))").toString();
            result = methods.getDataCountFromQuery(getAttributeDataQuery);
        }
        return result;
    }

}
