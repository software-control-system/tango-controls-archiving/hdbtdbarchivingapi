/**
 * 
 */
package fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.datagetters;

import fr.esrf.Tango.AttrDataFormat;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.AdtAptAttributesFactory;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.DataExtractor;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 * 
 */
public class MinMaxAvgGetters extends DataExtractor {

    private static final String IMAGE = "Image";
    private static final String SPECTRUM = "Spectrum";
    private static final String SCALAR = "Scalar";

    /**
     * @param con
     * @param ut
     * @param at
     */
    public MinMaxAvgGetters(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    /**
     * <b>Description</b> Returns the lower value recorded for the given
     * attribute
     * 
     * @param attributeName
     *            The attribute's name
     * @return The lower scalar data for the specified attribute
     * @throws ArchivingException
     */
    public double getAttDataMin(final String attributeName) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        double min = 0;
        if (att != null) {
            final int[] tfw = att.getAttTFWData(attributeName);
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    min = getAttScalarDataMin(attributeName, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
                    break;
            }
        }
        return min;
    }

    /**
     * <b>Description : </b> Returns the biggest value generated by the given
     * attribute
     * 
     * @param attributeName
     *            The attribute's name
     * @return The biggest value generated by the attribute
     * @throws ArchivingException
     */
    public double getAttDataMax(final String attributeName) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        double max = 0;
        if (att != null) {
            final int[] tfw = att.getAttTFWData(attributeName);
            final int dataFormat = tfw[1];
            final int writable = tfw[2];
            switch (dataFormat) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    max = getAttScalarDataMax(attributeName, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(dataFormat, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(dataFormat, SCALAR, IMAGE);
                    break;
            }
        }
        return max;
    }

    /**
     * <b>Description : </b> Returns the mean value for the given attribute
     * 
     * @param attributeName
     *            The attribute's name
     * @return The mean scalar data for the specified attribute
     * @throws ArchivingException
     */
    public double getAttDataAvg(final String attributeName) throws ArchivingException {
        final IAdtAptAttributes att = AdtAptAttributesFactory.getInstance(connector);
        double avg = 0;
        if (att != null) {
            final int[] tfw = att.getAttTFWData(attributeName);
            final int data_format = tfw[1];
            final int writable = tfw[2];
            switch (data_format) { // [0 - > SCALAR] [1 - > SPECTRUM] [2 - > IMAGE]
                case AttrDataFormat._SCALAR:
                    avg = getAttScalarDataAvg(attributeName, writable);
                    break;
                case AttrDataFormat._SPECTRUM:
                    methods.makeDataException(data_format, SCALAR, SPECTRUM);
                    break;
                case AttrDataFormat._IMAGE:
                    methods.makeDataException(data_format, SCALAR, IMAGE);
                    break;
            }
        }
        return avg;
    }

    /*
     * 
     */
    public double getAttScalarDataMin(final String attributeName, final int writable) throws ArchivingException {
        return methods.getAttScalarDataMinMaxAvg(attributeName, writable, "MIN");
    }

    /*
     * 
     */
    public double getAttScalarDataMax(final String attributeName, final int writable) throws ArchivingException {
        return methods.getAttScalarDataMinMaxAvg(attributeName, writable, "MAX");
    }

    /*
     * 
     */
    public double getAttScalarDataAvg(final String attributeName, final int writable) throws ArchivingException {
        return methods.getAttScalarDataMinMaxAvg(attributeName, writable, "AVG");
    }

}
