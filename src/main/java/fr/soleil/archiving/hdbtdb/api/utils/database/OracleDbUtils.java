/**
 *
 */
package fr.soleil.archiving.hdbtdb.api.utils.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.utils.DateUtil;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.database.connection.AbstractDataBaseConnector;

/**
 * @author AYADI
 *
 */
public class OracleDbUtils extends DbUtils {

    public OracleDbUtils(final AbstractDataBaseConnector connector) {
        super(connector);
    }

    @Override
    public String getFormat(final SamplingType samplingType) {
        return samplingType.getOracleFormat();
    }

    @Override
    public String toDbTimeFieldString(final String timeField) {
        return "to_char(" + timeField + " ,'DD-MM-YYYY HH24:MI:SS.FF')";
    }

    @Override
    public String toDbTimeFieldString(final String timeField, final String format) {
        return "to_char(" + timeField + ", '" + format + "')";
    }

    @Override
    public String toDbTimeString(final String timeField) {
		return "to_timestamp(\'" + timeField + "" + "\',\'DD-MM-YYYY HH24:MI:SS.FF\')";
    }

    @Override
    protected String getRequest() {
        return "SELECT SYSDATE FROM DUAL";
    }

    @Override
    public String getTableName(final int index) {
        return ConfigConst.TAB_PREF + index;
    }

    @Override
    protected String getFormattedTimeField(final String maxOrMin) {
        return toDbTimeFieldString(maxOrMin + "(" + ConfigConst.TIME + ")");
    }

    @Override
    public String getTime(final String string) throws ArchivingException {
        return DateUtil.stringToDisplayString(string);
    }

    @Override
    public String[] getListOfPartitions() throws ArchivingException {
        String[] result;
        if (connector == null) {
            result = null;
        } else {
            Connection conn = null;
            Statement stmt = null;
            ResultSet rset = null;
            final List<String> partitions = new ArrayList<String>();
            final String getPartitionQuery = new StringBuilder("SELECT * FROM ").append(connector.getSchema())
					.append(".").append(ConfigConst.STAT_PART).append(" WHERE ( ROWNUM < 5 )").toString();
            try {
                conn = connector.getConnection();
                if (conn != null) {
                    stmt = conn.createStatement();
                    // dbConn.setLastStatement(stmt);
                    rset = stmt.executeQuery(getPartitionQuery);
                    // Gets the result of the query
                    partitions.add("---------- List Of Partitions : ");
                    partitions.add("NBR  Partition_Name");
                    while (rset.next()) {
                        partitions.add(String.valueOf(rset.getInt(1)) + "  " + rset.getString(2));
                    }
                }
            } catch (final SQLException e) {
                throw new ArchivingException(e, getPartitionQuery);
            } finally {
                ConnectionCommands.close(rset);
                ConnectionCommands.close(stmt);
                connector.closeConnection(conn);
            }
            // Returns the number of active simple signals defined in HDB
            result = partitions.size() > 2 ? toStringArray(partitions) : null;
        }
        return result;
    }

}
