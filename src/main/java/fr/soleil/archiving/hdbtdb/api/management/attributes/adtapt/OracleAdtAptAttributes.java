package fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.AttributeHeavy;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.tableselector.AttributeIds;
import fr.soleil.archiving.hdbtdb.api.management.database.commands.ConnectionCommands;
import fr.soleil.database.connection.AbstractDataBaseConnector;

public class OracleAdtAptAttributes extends AdtAptAttributes {

    public OracleAdtAptAttributes(final AbstractDataBaseConnector connector, final AttributeIds ids) {
        super(connector, ids);
    }

    @Override
    public boolean isCaseSentitiveDatabase() {
        return true;
    }

    @Override
    protected String getRequest(final String attributeName) throws ArchivingException {
        String str;
        if (connector == null) {
            str = null;
        } else {
            final StringBuilder builder = new StringBuilder("SELECT ");
            for (int i = 0; i < ConfigConst.TAB_DEF.length - 1; i++) {
                builder.append("to_char(").append(ConfigConst.TAB_DEF[i]).append(")").append(VALUE_SEPARATOR);
            }
            builder.append("to_char(").append(ConfigConst.TAB_DEF[ConfigConst.TAB_DEF.length - 1]).append(")");
            builder.append(" FROM ").append(connector.getSchema()).append(DB_SEPARATOR).append(ConfigConst.ADT)
                    .append(" WHERE LOWER(").append(ConfigConst.FULL_NAME).append(") = LOWER('")
                    .append(attributeName.trim()).append("')");
            str = builder.toString();
        }
        return str;
    }

    /**
     * This method registers a given attribute into the hdb database It inserts
     * a record in the "Attribute Definition Table" This methos does not take
     * care of id parameter of the given attribute as this parameter is managed
     * in the database side (autoincrement).
     *
     * @param attributeHeavy
     *            the attribute to register
     * @throws ArchivingException
     */

    @Override
    public void registerAttribute(final AttributeHeavy attributeHeavy) throws ArchivingException {
        if (connector != null) {
            Connection conn = null;
            CallableStatement cstmtRegisterHdb = null;
            final String myStatement = new StringBuilder("{call ").append(connector.getSchema())
                    .append(".crtab (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}")
                    .toString();
            Object[] listValues = null;
            StringBuilder valueConcat = new StringBuilder();

            try {
                conn = connector.getConnection();
                if (conn != null) {
                    cstmtRegisterHdb = conn.prepareCall(myStatement);

                    listValues = new Object[] { attributeHeavy.getRegistration_time(),
                            attributeHeavy.getAttributeCompleteName(), attributeHeavy.getAttribute_device_name(),
                            attributeHeavy.getDomain(), attributeHeavy.getFamily(), attributeHeavy.getMember(),
                            attributeHeavy.getAttribute_name(), attributeHeavy.getDataType(),
                            attributeHeavy.getDataFormat(), attributeHeavy.getWritable(), attributeHeavy.getMax_dim_x(),
                            attributeHeavy.getMax_dim_y(), attributeHeavy.getLevel(), attributeHeavy.getCtrl_sys(),
                            attributeHeavy.getArchivable(), attributeHeavy.getSubstitute(),
                            attributeHeavy.getDescription(), attributeHeavy.getLabel(), attributeHeavy.getUnit(),
                            attributeHeavy.getStandard_unit(), attributeHeavy.getDisplay_unit(),
                            attributeHeavy.getFormat(), attributeHeavy.getMin_value(), attributeHeavy.getMax_value(),
                            attributeHeavy.getMin_alarm(), attributeHeavy.getMax_alarm(), "" };

                    valueConcat.append("(");
                    for (int i = 1; i < listValues.length + 1; ++i) {
                        final Object value = listValues[i - 1];
                        if (value instanceof Timestamp) {
                            cstmtRegisterHdb.setTimestamp(i, (Timestamp) value);
                            valueConcat.append(value);
                        } else if (value instanceof Integer) {
                            final int intValue = ((Integer) value).intValue();
                            cstmtRegisterHdb.setInt(i, intValue);
                            valueConcat.append(intValue);
                        } else if (value instanceof String) {
                            cstmtRegisterHdb.setString(i, (String) value);
                            valueConcat.append((String) value);
                        }
                        valueConcat.append(",");
                    }
                    valueConcat.append(")");

                    cstmtRegisterHdb.executeUpdate();
                    listValues = null;
                    valueConcat = null;
                }
            } catch (final SQLException e) {
                // ConnectionCommands.rollback(conn, DbUtilsFactory.getInstance(connector));
                String message = "";
                if (e.getMessage().equalsIgnoreCase(GlobalConst.COMM_FAILURE_ORACLE)
                        || e.getMessage().indexOf(GlobalConst.COMM_FAILURE_MYSQL) != -1) {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.ADB_CONNECTION_FAILURE;
                } else {
                    message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.STATEMENT_FAILURE;
                }

                final String reason = GlobalConst.QUERY_FAILURE;
                final String desc = "Failed while executing OracleAdtAptAttributes.registerAttribute() method...";
                final String query = myStatement + " = " + valueConcat.toString();
                throw new ArchivingException(message, reason, query, ErrSeverity.WARN, desc, this.getClass().getName(),
                        e);

            } finally {
                ConnectionCommands.close(cstmtRegisterHdb);
                connector.closeConnection(conn);
            }
        }
    }

    @Override
    public void createAttributeTableIfNotExist(final String attributeName, final AttributeHeavy attr)
            throws ArchivingException {
        // TODO Auto-generated method stub
    }

}
